package impl;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import prob.IJinder;

import prob.Persona;

public class Jinder implements IJinder<Persona> {

	public static Random rd = new Random();
	
	List<Persona> gente;
	Set<Persona> baneados;
	HashMap<Persona,List<Persona>>loves;
	HashMap<Persona,List<Persona>>rejects;
	
	public Jinder() {
		this.gente = new ArrayList<Persona>();
		this.baneados = new HashSet<Persona>();
		this.loves= new HashMap<Persona,List<Persona>>();
		this.rejects = new HashMap<Persona,List<Persona>>();
	}
	
	public Jinder(List<Persona> list) {
		this.gente = list;
		this.baneados = new HashSet<Persona>();
		this.loves= new HashMap<Persona,List<Persona>>();
		this.rejects = new HashMap<Persona,List<Persona>>();
	}
	//1
	@Override
	public boolean afegir(Persona p) {
		for(Persona a : this.gente) {
			if(a.equals(p))
				return false;
		}
		this.gente.add(p);
		return true;
	}

	@Override
	public void ordenar() {
		this.gente.sort(null);
	}

	@Override
	public Persona treure() {
		if(this.gente.isEmpty())
			return null;
		else {
			Persona a = this.gente.get(0);
			this.gente.remove(0);
			return a;
		}
	}

	@Override
	public void reset() {
		this.gente.clear();
		this.gente.clear();
	}

	@Override
	public int size() {
		return this.gente.size();
	}

	@Override
	public List<Persona> toList() {
		return this.gente;
	}
	//2
	@Override
	public boolean banejar(Persona p) {
		for(Persona a : this.gente) {
			if(p.equals(a)) {
				this.gente.remove(p);
				this.baneados.add(p);
				return true;
			}
		}
		return false;
	}

	@Override
	public List<Persona> banejatsToList() {
		List<Persona> a = new ArrayList<>();
		a.addAll(baneados);
		return a;
	}

	@Override
	public List<Persona> candidats(Persona p) {
		List<Persona> a = new ArrayList<>();
		for(Persona b : this.gente) {
			if(p.getSexBusca()==b.getSexPropi() && p.getSexPropi()==b.getSexBusca()) {
				a.add(b);
			}
		}
		return a;
	}
	public Set<Persona> getBaneados() {
		return baneados;
	}
	public void setBaneados(Set<Persona> baneados) {
		this.baneados = baneados;
	}
	
	//3
	@Override
	public Persona personaRandom() {
		return this.gente.get(rd.nextInt(this.gente.size()));
	}
	
	@Override
	public Persona Peek() {
		return this.gente.get(0);
	}
	
	@Override
	public boolean love(Persona jo, Persona amorcito) {
		if(jo.getSexBusca()!=amorcito.getSexPropi() && jo.getSexPropi()!=amorcito.getSexBusca()) 
			return false;
		for(Persona a : baneados) {
			if(a.equals(amorcito) || a.equals(jo)) 
				return false;
		}
		if(jo==amorcito)
			return false;
		for(Persona a : getLoves(jo)) {
			if(a.equals(amorcito)) 
				return false;
		}
		for(Persona a : getRejects(jo)) {
			if(a.equals(amorcito)) 
				return false;
		}
		getLoves(jo).add(amorcito);
		return true;
	}

	@Override
	public boolean reject(Persona jo, Persona arggg) {
		if(jo.getSexBusca()!=arggg.getSexPropi() && jo.getSexPropi()!=arggg.getSexBusca()) 
			return false;
		for(Persona a : baneados) {
			if(a.equals(jo) || a.equals(arggg)) 
				return false;
		}
		if(jo==arggg)
			return false;
		for(Persona a : getLoves(jo)) {
			if(a.equals(arggg)) 
				return false;
		}
		for(Persona a : getRejects(jo)) {
			if(a.equals(arggg)) 
				return false;
		}
		getRejects(jo).add(arggg);
		return true;
	}

	@Override
	public int jwipear(Persona jo) {
		this.loves.put(jo, new ArrayList<Persona>());
		this.rejects.put(jo, new ArrayList<Persona>());
		List<Persona> b = candidats(jo);
		for(Persona a : b) {
			if(!baneados.contains(jo)){
				if(!getRejects(jo).contains(a)) {
					if(a.getEdat()<jo.getEdat()) {
						love(jo, a);
					}
					else {
						if(jo.compareTo(a)>=1) {
							love(jo, a);
						}
						else {
							reject(jo,a);
						}
					}
				}
			}
		}
		return getLoves(jo).size();
	}
	
	@Override
	public List<Persona> getLoves(Persona p) {
		return loves.get(p);
	}

	@Override
	public List<Persona> getRejects(Persona p) {
		return rejects.get(p);
	}
}

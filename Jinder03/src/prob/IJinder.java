package prob;

import java.util.List;

public interface IJinder<Persona> {

	// Jinder 01
	/**
	 * 
	 * @param p Persona
	 * @return boolean
	 * 
	 *         Si la persona ja existeix (hi ha una altra iguual), no l'afegeix i
	 *         retorna fals Si la persona no existeix, però està banejada, no
	 *         l'afegeix i retorna fals Si la persona no existeix, ni està banejada,
	 *         afegeix la persona al sistema, com a última i retorna true
	 */
	public boolean afegir(Persona p);

	/**
	 * ordena les persones del nostre sistema
	 */
	public void ordenar();

	/**
	 * @return Persona
	 * 
	 *         Si el sistema conté persones, retorna la primera i l'elimina del
	 *         nostre sistema Si el sistema no te persones, retorna null
	 */
	public Persona treure();

	/**
	 * Elimina totes les persones del nostre sistema i ho deixa tot buit
	 */
	public void reset();

	/**
	 * Retorna el número de persones del nostre sistema
	 * 
	 * @return
	 */
	int size();

	/**
	 * Retorna una llista amb totes les persones del nostre sistema
	 */
	List<Persona> toList();

	// Jinder 02

	/**
	 * Busca la Persona
	 * <p>
	 * a la estructura jinder, si la troba la treu i la fica fica com a banejada,
	 * retornant true si no la troba o ja està banejada, no fa res i retorna false
	 * 
	 * @return
	 */
	public boolean banejar(Persona p);

	/**
	 * retorna una llista amb els banejats
	 * 
	 * @return
	 */
	public List<Persona> banejatsToList();

	// versió 03 Inici

	/**
	 * 
	 * @param jo
	 * @return Genera els candidats automàticament per la persona <jo>. Els
	 *         candidats seran aquells que siguin del sexe que busca <jo> i ha de
	 *         passar també que el sexe que busqui el candidat sigui el de <jo> És a
	 *         dir, si jo te sexe 'F' i buscar 'M' ... solament afegirà com a
	 *         candidats les persones que siguien 'M' i busquin 'F'
	 */         
	 public List<Persona> candidats(Persona jo);

	/**
	 * Retorna una Persona aleatòria de entre les que no estan banejades.
	 */
	public Persona personaRandom();

	/**
	 * Retorna la primera Persona del sistema però no la elimina.
	 */
	public Persona Peek();

 	/**
	 * 
	 * @param jo
	 * @param amorcito
	 * @return
	 * 
	 *         La Persona <jo> troba interesant a la Persona <amorcito>, i queda
	 *         enregistrat com a tal, retornant <true> Si no és possible enregistrar
	 *         aquest fet, retornarà <fals>. Motius: - <jo> i <amorcito> no
	 *         comparteixen preferències sexuals - <jo> està banejat - <amorcito>
	 *         està banejat - <jo> i <amorcito> som la mateixa persona - <jo> haviat
	 *         rebutjat o fet love amb <amorcito> prèviament
	 */
	public boolean love(Persona jo, Persona amorcito);

	/**
	 * 
	 * @param jo
	 * @param arggg
	 * @return
	 * 
	 *         La Persona <jo> no vol explorar res amb la Persona <arggg>i queda
	 *         enregistrat com a tal, retornant <true> Si no és possible enregistrar
	 *         aquest fet, retornarà <fals>. Motius: - <jo> està banejat - <arggg>
	 *         està banejat - <jo> i <arggg> som la mateixa persona - <jo> haviat
	 *         rebutjat o fet love amb <arggg> prèviament
	 */
	public boolean reject(Persona jo, Persona arggg);

	/**
	 * genera automàticament els loves i rejects de la Persona <jo>
	 * 
	 * @param jo
	 * @return
	 * 
	 *         Obtindrà els candidats de <jo> Per cada candidat <c> mirarà si la
	 *         edat de <c> és inferior. Aleshores li fa un <love> automàtic
	 *         (l'afegeix com a love) Cas contrari, compararà <jo> amb <c> amb el
	 *         comparador de persones. Si <jo> és més gran, l'afegeix a <loves>, cas
	 *         contrari, l'afegeig a <reject> Retorna el número de <loves>
	 */
	public int jwipear(Persona jo);

	public List<Persona> getLoves(Persona p);

	public List<Persona> getRejects(Persona p);
}
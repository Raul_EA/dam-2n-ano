package prob;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Persona implements Comparable<Persona> {
	private String nom;
	private int edat;
	private SEXE sexPropi;
	private SEXE sexBusca;


	public Persona(String nom, int edat, SEXE sexPropi, SEXE sexBusca) {
		this.nom = nom;
		this.edat = edat;
		this.sexPropi = sexPropi;
		this.sexBusca = sexBusca;
	}
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public SEXE getSexPropi() {
		return sexPropi;
	}

	public void setSexPropi(SEXE sexPropi) {
		this.sexPropi = sexPropi;
	}

	public SEXE getSexBusca() {
		return sexBusca;
	}

	public void setSexBusca(SEXE sexBusca) {
		this.sexBusca = sexBusca;
	}

	@Override
	/**
	 * 
	 * @param that
	 * @return compara les persones pel nom i la edat. Les condiera iguals si tenen
	 *         el mateix nom i edat
	 */
	public int compareTo(Persona that) {
		if(that == null) {
			return 1;
		}else if(this.nom.compareTo(that.getNom())!=0) {
			return this.nom.compareTo(that.getNom());
		}else {
			if(this.edat!=that.getEdat()) {
				if(this.edat>that.getEdat())
					return 1;
				else if(this.edat<that.getEdat())
					return -1;
			}
		}
		return 0;
	}

	/**
	 * Redefinició de equals
	 */
	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;
		if (!(aThat instanceof Persona))
			return false;
		Persona that = (Persona) aThat;
		for (int i = 0; i < this.getSigFields().length; ++i) {
			if (!Objects.equals(this.getSigFields()[i], that.getSigFields()[i])) {
				return false;
			}
		}
		return true;
	}

	/**
	 * A class that overrides equals must also override hashCode.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(getSigFields());
	}

	private Object[] getSigFields() {
		Object[] result = { nom, edat };
		return result;
	}
	@Override
	public String toString() {
		return "Persona [nom=" + nom + ", edat=" + edat + ", sexPropi=" + sexPropi + ", sexBusca=" + sexBusca + "]";
	}

	
}

package run;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import impl.Jinder;
import prob.Persona;
import prob.SEXE;

public class Main {

	public static void main(String[] args) {

		Jinder j = new Jinder();
		Persona p1 = new Persona("Daniel", 20, SEXE.M, SEXE.F);
		j.afegir(p1);		
		Persona p2 = new Persona ("Ana", 23, SEXE.F, SEXE.M);
		j.afegir(p2);		
		Persona p3 = new Persona ("Berta", 34, SEXE.F, SEXE.M);
		j.afegir(p3);		
		Persona p4 = new Persona ("Carles", 44, SEXE.M, SEXE.F);
		j.afegir(p4);		
		Persona p5 = new Persona ("Fran", 37, SEXE.M, SEXE.M);
		j.afegir(p5);		
		Persona p6 = new Persona ("Gabriel", 47, SEXE.M, SEXE.M);
		j.afegir(p6);		
		Persona p7 = new Persona ("Hipolit", 65, SEXE.M, SEXE.F);
		j.afegir(p7);		
		Persona p8 = new Persona ("Ines", 40, SEXE.F, SEXE.M);
		j.afegir(p8);		

		List<Persona> c = new ArrayList<Persona>();
		Persona p = j.Peek();
		System.out.println("Soc: " + p);
		j.jwipear(p);
		c = j.getLoves(p);
		System.out.println("Loves:");
		System.out.println(c);
		c = j.getRejects(p);
		System.out.println("Rejects:");
		System.out.println(c);
	}
}	



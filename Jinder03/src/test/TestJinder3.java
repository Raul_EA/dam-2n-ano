package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import prob.Persona;
import prob.SEXE;
import impl.Jinder;

class TestJinder3 {

	@Test
	void testJinderCandidats() {

		Jinder j = new Jinder();
		Persona p1 = new Persona("Daniel", 20, SEXE.M, SEXE.F);
		j.afegir(p1);
		Persona p2 = new Persona("Ana", 23, SEXE.F, SEXE.M);
		j.afegir(p2);
		Persona p3 = new Persona("Berta", 34, SEXE.F, SEXE.M);
		j.afegir(p3);
		Persona p4 = new Persona("Carles", 44, SEXE.M, SEXE.F);
		j.afegir(p4);
		Persona p5 = new Persona("Fran", 37, SEXE.M, SEXE.M);
		j.afegir(p5);
		Persona p6 = new Persona("Gabriel", 47, SEXE.M, SEXE.M);
		j.afegir(p6);
		Persona p7 = new Persona("Hipolit", 65, SEXE.M, SEXE.F);
		j.afegir(p7);
		Persona p8 = new Persona("Ines", 40, SEXE.F, SEXE.M);
		j.afegir(p8);
		assertNotNull(j);
		List<Persona> c = j.candidats(p8);
		Persona p = j.Peek();
		assertEquals(true, p.equals(p1));
		j.jwipear(p);
		c = j.getLoves(p);
		assertEquals(true, c.contains(p2));
		assertEquals(true, c.contains(p3));
		assertEquals(false, c.contains(p8));
		assertEquals(false, c.contains(p1));

		c = j.getRejects(p);
		assertEquals(false, c.contains(p2));
		assertEquals(false, c.contains(p1));
		assertEquals(true, c.contains(p8));
	}
}

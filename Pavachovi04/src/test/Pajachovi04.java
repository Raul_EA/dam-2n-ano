package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import impl.Botiga;
import impl.Comanda;
import prob.Albara;
import prob.AlbaraLinia;
import prob.Producte;
import prob.ProducteLot;
import prob.ProducteNormal;

class Pajachovi04 {

	@Test
	void pajachovi03Comanda() {
		try {
			Botiga pajachovi = new Botiga("Pajachovi");
			LocalDate f = LocalDate.parse("2023-11-09");
			
			ProducteLot p1 = new ProducteLot("Pan", 50, 70);
			p1.afegirLot(f.plusWeeks(1), 20);
			p1.afegirLot(f.plusWeeks(1), 10);
			p1.afegirLot(f.plusWeeks(2), 10);
			p1.afegirLot(f.plusWeeks(-1), 10);

			ProducteLot p2 = new ProducteLot("Jamon", 20, 40);
			p2.afegirLot(f.plusWeeks(1), 2);
			p2.afegirLot(f.plusWeeks(1), 3);

			ProducteLot p3 = new ProducteLot("Chorizo", 60, 80);
			p3.afegirLot(f.plusWeeks(1), 12);
			p3.afegirLot(f.plusWeeks(1), 5);
			p3.afegirLot(f.plusWeeks(-10), 4);

			ProducteNormal p4 = new ProducteNormal("Vino", 40, 90, 20, 50);

			pajachovi.afegirProducte(p1);
			pajachovi.afegirProducte(p2);
			pajachovi.afegirProducte(p3);
			pajachovi.afegirProducte(p4);
			
			//generar comanda automàtica
			Comanda comanda = pajachovi.comandaAutomatica();
			comanda.setData(LocalDate.parse("2023-12-31"));;
			String cadena = "Comanda: 1\nData: 2023-12-31\nEstat: NOVA\nJamon: 40\nChorizo: 80\nVino: 90\n";
			assertEquals(cadena, comanda.toString());

			pajachovi.afegirComanda(comanda);
			comanda.enviarComanda();
			
			Albara<AlbaraLinia> a = pajachovi.albaraAutomatic(comanda);
			cadena = "Albara de Comanda: 1\nEstat: PENDENT\nproducte = Jamon, caducitat=2029-12-31, quantitat=40\nproducte = Chorizo, caducitat=2029-12-31, quantitat=80\nproducte = Vino, caducitat=null, quantitat=90\n";
			assertEquals(cadena, a.toString());
			
			
			pajachovi.rebreComanda(a);

			cadena = "Botiga Pajachovi\nCataleg:\n[1]: Pan(min: 50, rep: 70){2023-11-02=10, 2023-11-16=30, 2023-11-23=10}\n[2]: Jamon(min: 20, rep: 40){2023-11-16=5, 2029-12-31=40}\n[3]: Chorizo(min: 60, rep: 80){2023-08-31=4, 2023-11-16=17, 2029-12-31=80}\n[4]: Vino(min: 40, rep: 90)( Stock: 110 Màx: 50)]";

			System.out.println(pajachovi);
			System.out.println(cadena);
			
			assertEquals(cadena, pajachovi.toString());


		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			System.err.println(e);
		}
	}

}
package impl;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import prob.ComandaEstat;
import prob.Producte;
import prob.ProgramException;
import prob.iComanda;

public class Comanda<P> implements iComanda{

	static int comptador = 0;
	
	int numComanda;
	LocalDate data;
	Map<P,Integer> linies;
	ComandaEstat estat;
	
	public Comanda() {
		numComanda = ++comptador;
		data = LocalDate.now();
		estat = ComandaEstat.NOVA;
		linies = new TreeMap<P,Integer>();
	}
	
	@Override
	public void afegirLinia(Object p, int q) throws ProgramException {
		if (p==null) throw new IllegalArgumentException("no s'admeten nulls");
		if (q<=0) throw new IllegalArgumentException("la quantitat ha de ser positiva");
		if (estat!=ComandaEstat.NOVA) throw new ProgramException ("La comanda ja està enviada");
		if (linies.containsKey(p))
			throw new ProgramException("No es pot afegir un producte ja demanat");
		linies.put((P)p, q);
	}

	@Override
	public ComandaEstat getEstat() {
		return estat;
	}

	@Override
	public LocalDate getData() {
		return data;
	}

	@Override
	public int getNumComanda() {
		return numComanda;
	}

	@Override
	public void setEstat(ComandaEstat nouEstat) throws ProgramException {
		if (estat == ComandaEstat.NOVA && nouEstat == ComandaEstat.ENVIADA) 
				estat = nouEstat;
		else if (estat == ComandaEstat.ENVIADA && nouEstat == ComandaEstat.REBUDA)
			estat = nouEstat;
		else 
			throw new ProgramException ("Canvi d'estat incorrecte");
	}

	@Override
	public Map getLinies() {
		return linies;
	}

	@Override
	public void trereLinia(Object p) throws ProgramException {
		if (p==null) throw new IllegalArgumentException("no s'admeten nulls");
		if (estat!=ComandaEstat.NOVA) throw new ProgramException ("La comanda ja està enviada");
		if (!linies.containsKey(p))
			throw new ProgramException("No es pot treure un producte no demanat");
		linies.remove(p);
	}

	@Override
	public String toString()  {
		String cadena = "Comanda: " + this.getNumComanda() + "\n";
		cadena += "Data: " + this.getData() + "\n";
		cadena += "Estat: " + this.getEstat() + "\n";

		Iterator entries = this.getLinies().entrySet().iterator();
		while (entries.hasNext()) {
		    Map.Entry entry = (Map.Entry) entries.next();
		    Producte key = (Producte)entry.getKey();
		    Integer value = (Integer)entry.getValue();
		    cadena +=  ((Producte)key).getNom() + ": " + value + "\n";
		}
		return cadena;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	@Override
	public void enviarComanda() throws ProgramException{
		if(this.estat==ComandaEstat.NOVA)
			this.estat=ComandaEstat.ENVIADA;
		else
			throw new ProgramException(null);
	}
}

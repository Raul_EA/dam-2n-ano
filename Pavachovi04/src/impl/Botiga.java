package impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import prob.Albara;
import prob.AlbaraEstat;
import prob.AlbaraLinia;
import prob.ComandaEstat;
import prob.Producte;
import prob.ProducteLot;
import prob.ProducteNormal;
import prob.ProgramException;
import prob.iBotiga;
import prob.iComanda;

import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Botiga<P extends Producte> implements iBotiga<P>, Iterable<P> {

	private String nomBotiga;
	private List<P> cataleg;
	private List<Comanda> comandes;

	public Botiga() {
		cataleg = new ArrayList();
		comandes = new ArrayList();
	}

	public Botiga(String string) {
		this();
		setNomBotiga(string);
	}

	@Override
	public void afegirProducte(P producte) throws ProgramException {
		if (cataleg.contains(producte))
			throw new ProgramException("Producte ja existeix");
		cataleg.add(producte);
	}

	@Override
	public String toString() {
		String cadena = "Botiga " + nomBotiga + "\nCataleg:";
		for (Producte p : cataleg)
			cadena += "\n" + p;
		cadena += "]";
		return cadena;
	}

	public List<Producte> llista() {
		return List.copyOf(cataleg);
	}

	@Override
	public void setNomBotiga(String nomBotiga) {
		if (nomBotiga == null)
			throw new IllegalArgumentException("Nom Botiga no pot ser null");
		this.nomBotiga = nomBotiga;
	}

	@Override
	public int getStock(Producte producte) throws ProgramException {
		if (cataleg.contains(producte))
			return producte.stock();
		else
			throw new ProgramException("Producte no existeix");
	}

	@Override
	public Map<P, Integer> getInventari() {
		Map<P, Integer> inv = new TreeMap<P, Integer>();
		for (P p : cataleg)
			inv.put(p, p.stock());

		return inv;
	}

	@Override
	public String veureInventari() {
		String cadena = "Inventari";
		Map<P, Integer> inv = getInventari();
		for (P p : inv.keySet())
			cadena = cadena + "\n" + p.getIdProducte() + "\t" + p.getNom() + ": " + inv.get(p);
		return cadena;
	}

	@Override
	public Map<String, Integer> getCaducats() {
		Map<String, Integer> inv = new TreeMap<String, Integer>();
		for (Producte p : cataleg)
			if (p instanceof ProducteLot) {
				int caducats = 0;
				for (LocalDate d : ((ProducteLot) p).getLots().keySet())
					if (d.isBefore(LocalDate.now()))
						caducats += ((ProducteLot) p).getLots().get(d);
				if (caducats > 0)
					inv.put(p.getNom(), caducats);
			}

		return inv;
	}

	@Override
	public void eliminarCaducats() {
		int n = 0;
		for (Producte p : cataleg)
			if (p instanceof ProducteLot) {
				((ProducteLot) p).getLots().entrySet().removeIf(entry -> (entry.getKey().isBefore(LocalDate.now())));
			}
	}

	@Override
	// quan iterem la botiga serà pel catàleg dels seus productes
	public Iterator<P> iterator() {
		return cataleg.iterator();
	}

	@Override
	public Comanda<P> comandaAutomatica() {
		Comanda<P> coma=new Comanda<P>();
		for(Producte a : this.cataleg) {
			if(a instanceof ProducteNormal) {
				ProducteNormal b = (ProducteNormal) a;
				if(a.getMinim()>b.stock()) {
					try {
						coma.afegirLinia(b, b.getReposicio());
					} catch (ProgramException e) {
						e.printStackTrace();
					}
				}
			}else if(a instanceof ProducteLot) {
				ProducteLot b = (ProducteLot)a;
				if(a.getMinim()>b.stock()) {
					try {
						coma.afegirLinia(b, b.getReposicio());
					} catch (ProgramException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return coma;
	}

	@Override
	public void afegirComanda(Comanda<P> c) throws ProgramException {
		if(c==null)
			throw new IllegalArgumentException();
		if(c.getEstat()!=ComandaEstat.NOVA || this.comandes.contains(c)) 
			throw new ProgramException("PETOOO");
		this.comandes.add(c);
	}

	@Override
	public void rebreComanda(Albara<AlbaraLinia<P>> a) throws ProgramException {
		if(a==null)
			throw new IllegalArgumentException();
		boolean non=false;
		for(Comanda b:this.comandes) {
			if(b.numComanda==a.getNumComanda()) {
				non=true;
			}
		}
		if(!non)
			throw new ProgramException("Comanda no asociada");
		for(AlbaraLinia<P> al:a.getLiniesAlbara()) {
			non=true;
			for(Producte p:this.cataleg) {
				if(p==al.getProducte())
					non=false;
			}
			if(non==true)
				throw new ProgramException("Producto no ta");
		}
		
		if(a.getEstat()==AlbaraEstat.PENDENT)
			throw new ProgramException("No esta en pendiente");
		
		for(AlbaraLinia<P> al:a.getLiniesAlbara()) {
			if(this.cataleg.get(this.cataleg.indexOf(al.getProducte())) instanceof ProducteNormal) {
				ProducteNormal p = (ProducteNormal)this.cataleg.get(this.cataleg.indexOf(al.getProducte()));
				p.afegirQuantitat(al.getQuantitat());
			}else if(this.cataleg.get(this.cataleg.indexOf(al.getProducte())) instanceof ProducteLot) {
				ProducteLot p = (ProducteLot)this.cataleg.get(this.cataleg.indexOf(al.getProducte()));
				p.afegirLot(LocalDate.of(2029, 12, 31), al.getQuantitat());;
			}		
		}
		for(Comanda b:this.comandes) {
			if(b.numComanda==a.getNumComanda()) {
				b.estat=ComandaEstat.REBUDA;
			}
		}
		a.setEstat(AlbaraEstat.REBUT);
	}

	@Override
	public Albara<AlbaraLinia<P>> albaraAutomatic(Comanda<P> c) throws ProgramException {
		Albara<AlbaraLinia<P>> a = new Albara<AlbaraLinia<P>>();
		a.setEstat(AlbaraEstat.PENDENT);
		a.setNumComanda(c.numComanda);
		List<AlbaraLinia<P>> l = new ArrayList<AlbaraLinia<P>>();
		c.linies.forEach((k,q)->{
			AlbaraLinia<P> al;
			if(k instanceof ProducteNormal) {
				al = new AlbaraLinia<P>(k, null, q);
			}else {
				al = new AlbaraLinia<P>(k, LocalDate.of(2029, 12, 31), q);
			}
			l.add(al);
		});
		a.setLiniesAlbara(l);
		return a;
	}
}
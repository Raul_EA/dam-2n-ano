package run;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import impl.Botiga;
import impl.Comanda;
import prob.Albara;
import prob.AlbaraLinia;
import prob.Producte;
import prob.ProducteLot;
import prob.ProducteNormal;
import prob.ProgramException;

public class MainGenerator {

	public static void main(String[] args) {

		Botiga pajachovi = new Botiga("Pajachovi");

			List<Producte> lp = misLotes();
			for (Producte p : lp)
				try {
					pajachovi.afegirProducte(p);
				} catch (ProgramException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			lp = misNormales();
			for (Producte p : lp)
				try {
					pajachovi.afegirProducte(p);
				} catch (ProgramException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			System.out.println(pajachovi);	
			
			System.out.println(pajachovi.getCaducats());
			pajachovi.eliminarCaducats();
			System.out.println(pajachovi.veureInventari());
			
			Comanda<Producte> c = pajachovi.comandaAutomatica();
			System.out.println(c);
			
			try {
				pajachovi.afegirComanda(c);
				c.enviarComanda();
				Albara<AlbaraLinia> a = pajachovi.albaraAutomatic(c);
				System.out.println(a);
				pajachovi.rebreComanda(a);
				System.out.println(pajachovi.veureInventari());				
			} catch (ProgramException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	}

	static private List<Producte> misLotes() {
		Random rd = new Random();
		List<Producte> lProductes = new ArrayList();
		ProducteLot p;

		List<String> pLotes = Arrays.asList("Te Dharamsala", "Cerveza tibetana Barley", "Sirope de regaliz",
				"Especias Cajun del chef Anton", "Mezcla Gumbo del chef Anton", "Mermelada de grosellas de la abuela",
				"Peras secas orgánicas del tio Bob", "Salsa de arandanos Northwoods", "Buey Mishi Kobe", "Pez espada",
				"Queso Cabrales", "Queso Manchego La Pastora", "Algas Konbu", "Cuajada de judias",
				"Salsa de soja baja en sodio", "Postre de merengue Pavlova", "Cordero Alice Springs",
				"Langostinos tigre Carnarvon", "Pastas de te de chocolate", "Mermelada de Sir Rodneys",
				"Bollos de Sir Rodneys", "Pan de centeno crujiente estilo Gustafs", "Pan fino",
				"Refresco Guarana Fantastica", "Crema de chocolate y nueces NuNuCa", "Ositos de goma Gumbar",
				"Chocolate Schoggi", "Col fermentada Rossle", "Salchicha Thuringer", "Arenque blanco del noroeste",
				"Queso gorgonzola Telino", "Queso Mascarpone Fabioli", "Queso de cabra", "Cerveza Sasquatch",
				"Cerveza negra Steeleye", "Escabeche de arenque", "Salmon ahumado Gravad", "Vino Cote de Blaye",
				"Licor verde Chartreuse", "Carne de cangrejo de Boston", "Crema de almejas estilo Nueva Inglaterra",
				"Tallarines de Singapur", "Cafe de Malasia", "Azucar negra Malacca", "Arenque ahumado",
				"Arenque salado", "Galletas Zaanse", "Chocolate holandes", "Regaliz", "Chocolate blanco",
				"Manzanas secas Manjimup", "Cereales para Filo", "Empanada de carne", "Empanada de cerdo", "Pate chino",
				"Gnocchi de la abuela Alicia", "Raviolis Angelo", "Caracoles de Borgonya",
				"Raclet de queso Courdavault", "Camembert Pierrot", "Sirope de arce", "Tarta de azucar",
				"Sandwich de vegetales", "Bollos de pan de Wimmer", "Salsa de pimiento picante de Luisiana",
				"Especias picantes de Luisiana", "Cerveza Laughing Lumberjack", "Barras de pan de Escocia",
				"Queso Gudbrandsdals", "Cerveza Outback", "Crema de queso Flotemys", "Queso Mozzarella Giovanni",
				"Caviar rojo", "Queso de soja Longlife", "Cerveza Klosterbier Rhonbrau", "Licor Cloudberry",
				"Salsa verde original Frankfurter");

		for (String s : pLotes) {
			p = new ProducteLot(s, rd.nextInt(0,10), rd.nextInt(1,20));
			int rLotes = rd.nextInt(1, 11);
			for (int i = 0; i < rLotes; i++) 
				try {
					p.afegirLot(LocalDate.now().plusWeeks(rd.nextInt(-2,4)), rd.nextInt(100));
				} catch (ProgramException e) {
					e.printStackTrace();
				}
			lProductes.add(p);
		}
		return lProductes;
	}

	static private List<Producte> misNormales() {
		Random rd = new Random();
		List<Producte> lProductes = new ArrayList();
		ProducteNormal p;

		List<String> pNormal = Arrays.asList("Cullera", "Ganivet", "Cullareta","Got","Plat", "Tovallo", "Taula",
										    "Paella", "Escombra", "Recollidor", "Pot Gran", "Pot Petit", "Caixa Gran",
										    "Caixa Petita", "Armari", "Gerra", "Guants", "Frigorific", "Freidora",
										    "Encenedor", "Tapa Gran", "Tapa Petita", "Cadira");

		for (String s : pNormal) {
			p = new ProducteNormal(s, rd.nextInt(0,10), rd.nextInt(1,50), rd.nextInt(0,5), rd.nextInt(10,50));
			lProductes.add(p);
		}
		return lProductes;
	}

}

package prob;

import java.util.Map;

import impl.Comanda;

public interface iBotiga<P> {
	
	/**
	 * Li dona un nom a la botiga
	 * @param nomBotiga   .. és el nom de la botiga
	 * @throws IllegalArgumentException si el nomBotiga és null.
	 */
	
	void setNomBotiga(String nomBotiga);
	
	/**
	 * Afegeix un producte a la botiga.
	 * @param producte .. el producte que afegeix
	 * @throws IllegalArgumentException si producte és null.
	 * @throws ProgramException si el producte ja existeix
	 */
	void afegirProducte(P p) throws ProgramException;
	
	/**
	 * Gets the stock for a product.
	 * @throws IllegalArgumentException if product is null.
	 * @throws ProgramException si el producte no existeix
	 * @return
	 */
	public int getStock(P p) throws ProgramException;

	/**
	 * Gets the inventari .
	 * @return Map <Producte, Integer>
	 */
	public Map<P, Integer> getInventari();

	public Map<String, Integer> getCaducats();
	
 	public String veureInventari();
 	
 	public void eliminarCaducats();

 	//v4 
 	
 	/**
 	 * Genera una comanda automàtica amb els productes que estan per sota del seu stoc mínim
 	 * La comanda tindrà com a data la data actual
 	 * La comanda tindrà l'estat  <NOVA> 
 	 * @return la Comanda generada
 	 */
 	public Comanda<P> comandaAutomatica();

 	/**
 	 * Afegeix una comanda a la llista de comandes de la botiga
 	 * 
 	 * @param c		Comanda a afegir a la botiga
 	 * @throws IllegalArgumentException	-->  Si la comanda és null
 	 * @throws ProgramException         -->  Si la comanda no està en estat NOVA
 	 * 							        -->  Si la comanda ja estava a la llista de comandes
 	 */
 	public void afegirComanda(Comanda<P> c) throws ProgramException;
 	
 	/**
 	 * Genera un albarà automàtic a partir de la comanda passada com a paràmetre
 	 * L'albarà tindrà associat la comanda passada com a paràmetre i quedarà en estat PENDENT
 	 * Per simplificar, suposarem que rep totes les quantitats demanades i que la data de caducitat és 2029-12-31
 	 * @param c (Comanda)
  	 * @throws IllegalArgumentException	-->  Si la comanda és nul·la
  	 * @throws ProgramException         -->  Si la comanda no està en estat ENVIADA
  	 * 									-->  Si la comanda no està afegida a la llista de comandes de la botiga
 	 */

 	public Albara<AlbaraLinia<P>> albaraAutomatic(Comanda<P> p) throws ProgramException;
 	/**
 	 *  Procesa l'albarà <a> 
 	 *  			- actualitzant l'stock corresponent i afegeix lots si cal 
 	 *  			- Actualitza l'estat de la comanda associada a REBUDA
 	 *  			- Actualitza l'estat de l'albarà a REBUT
 	 * @param a  (Albarà)
  	 * @throws IllegalArgumentException	-->  Si l'albarà és null
 	 * @throws ProgramException         -->  Si la comanda asociada no existeix
 	 * 									-->  Si algun producte no existeix
 	 * 									-->  Si l'albarà no estava en estat PENDENT
 	 */
 	public void rebreComanda(Albara<AlbaraLinia<P>> a) throws ProgramException;
}

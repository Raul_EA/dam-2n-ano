package prob;

import java.util.Objects;

public abstract class  Producte implements Comparable<Producte>{
	
	private static int comptador = 0;
	protected int minim;
	protected String nom;
	protected int reposicio;
	protected int idProducte;
	
	public Producte(String nom) {
	    if (nom == null) {
	        throw new IllegalArgumentException("nom de producte no pot ser null");
	    }
		this.nom = nom;
		this.reposicio = 0;
		this.minim = 0;
		this.idProducte =  ++comptador;
	}
	
	public Producte(String nom, int minim, int reposicio) {
		this(nom);
	    if (reposicio < 0) {
            throw new IllegalArgumentException("reposició no pot ser negativa");
        }
	    if (minim < 0) {
            throw new IllegalArgumentException("minim no pot ser negatiu");
        }
		this.reposicio = reposicio;
		this.minim = minim;
	}


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        //Producte<?> other = (Producte<?>) obj;
        return nom.equals(((Producte)(obj)).nom);
    }

    public int getMinim() {
		return minim;
	}
    
    public String getNom() {
		return nom;
	}

	public int getReposicio() {
		return reposicio;
	}


	@Override
    public int hashCode() {
        return Objects.hash(nom);
    }


	public void setMinim(int minim) {
		this.minim = minim;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public void setReposicio(int reposicio) {
		this.reposicio = reposicio;
	}

	public abstract int stock ();
	
	@Override
	public String toString() {
		return ("[" + idProducte + "]: "+ nom + "(min: " + minim + ", rep: " + reposicio + ")");
	}

	public int getIdProducte() {
		return idProducte;
	}
	
}

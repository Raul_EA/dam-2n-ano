package prob;

import java.time.LocalDate;
import java.util.Map;

public interface iComanda <P>{

	/**
	 * Afegeix una línia a la comanda
	 * @param p .. afegeix a la quantitat indicada
	 * @throws IllegalArgumentException si p és null.
	 * @throws IllegalArgumentException si q és zero o negatiu
	 * @throws ProgramException si p ja existeix a la comanda. No queda afegida la nova línia
	 * @throws ProgramException si la comanda no està en estat NOVA. No queda afegida la nova línia	 * 
	 */
	void afegirLinia(P p, int q) throws ProgramException;

	/**
	 * Treure una línia a la comanda
	 * @param  p ... element que s'ha de treure de la comanda
	 * @throws IllegalArgumentException si p és null.
	 * @throws ProgramException si p no existeix a la comanda. 
	 * @throws ProgramException si la comanda no està en estat NOVA. No queda afegida la nova línia	 * 
	 */
	void trereLinia(P p) throws ProgramException;

	public ComandaEstat getEstat();
	public LocalDate getData ();
	public int getNumComanda ();
	
	/**
	 * Gets the stock for a product.
	 * @throws ProgramException si el canvi no és possible.
	 * Els canvis possibles són: de NOVA a ENVIADA
	 *                           d' ENVIADA a REBUDA
	 *                           de REBUDA a PROCESSADA
	 * @return
	 */
	public void setEstat (ComandaEstat nouEstat) throws ProgramException;

	/**
	 * Retorna un diccionari amb els elements P demanats i la seva quantitat
	 * @return
	 */
	public Map<P, Integer> getLinies();
	
	/**
	 * envia la comanda a proveïdor,
	 * Canvia l'estat de NOVA a ENVIADA
	 * @throws ProgramException si la comanda no està en estat NOVA. No queda modificat l'estat	 * 
	 */
	public void enviarComanda() throws ProgramException;
	
	public String toString();
}

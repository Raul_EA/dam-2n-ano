package prob;

import java.time.LocalDate;

public class AlbaraLinia<P> {
	private P producte;
	private LocalDate caducitat;
	private int quantitat;

	public AlbaraLinia(P p, LocalDate ld, int q) {
		producte = p;
		caducitat = ld;
		quantitat = q;
	}
	
	
	public P getProducte() {
		return producte;
	}
	public void setProducte(P p) {
		this.producte = p;
	}
	public LocalDate getCaducitat() {
		return caducitat;
	}
	public void setCaducitat(LocalDate caducitat) {
		this.caducitat = caducitat;
	}
	public int getQuantitat() {
		return quantitat;
	}
	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}
	@Override
	public String toString() {
		return "producte = " + ((Producte) producte).getNom() + ", caducitat=" + caducitat + ", quantitat=" + quantitat;
	}

	
	
}

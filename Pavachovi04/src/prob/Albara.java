package prob;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Albara<AlbaraLinia> {
	
	private List<AlbaraLinia> liniesAlbara;
	private AlbaraEstat estat;
	private int numComanda;
	
	public int getNumComanda() {
		return numComanda;
	}

	public void setNumComanda(int numComanda) {
		this.numComanda = numComanda;
	}

	public Albara() {
		estat = AlbaraEstat.PENDENT;
		liniesAlbara = new ArrayList<>();
	}
	
	public void afegirLinia(AlbaraLinia la) {
		liniesAlbara.add(la);
	}

	public List<AlbaraLinia> getLiniesAlbara() {
		return liniesAlbara;
	}

	public void setLiniesAlbara(List<AlbaraLinia> liniesAlbara) {
		this.liniesAlbara = liniesAlbara;
	}

	public AlbaraEstat getEstat() {
		return estat;
	}

	public void setEstat(AlbaraEstat estat) {
		this.estat = estat;
	}

	@Override
	public String toString() {
		String cadena = "Albara de Comanda: " + this.getNumComanda() + "\n";
		cadena += "Estat: " + this.getEstat() + "\n";
		for (AlbaraLinia la: getLiniesAlbara()) {
			cadena +=  la.toString() + "\n";
		}
		return cadena;
	}	
}

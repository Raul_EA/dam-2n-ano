package Serializacion;

import java.io.Serializable;

public class Tripulantes implements Serializable{
	private static final long serialVersionUID = 1L;
	String nombre;
	int edad;
	boolean felicidad;
	
	public Tripulantes(String a, int b, boolean c){
		nombre = a;
		edad = b;
		felicidad = c;
	}
	
	@Override
	public String toString() {
		return "Tripulante [nombre=" + nombre + ", edad=" + edad + ", felicidad=" + felicidad + "]";
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public boolean isFelicidad() {
		return felicidad;
	}
	public void setFelicidad(boolean felicidad) {
		this.felicidad = felicidad;
	}

	
}

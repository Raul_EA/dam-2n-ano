package Serializacion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Random;

public class Funciones {
	static Random rd = new Random();

	public static void Generar() {

		File f = new File("tripulantes.dat");

		Tripulantes a = new Tripulantes("Jose", rd.nextInt(1, 99), rd.nextBoolean());
		Tripulantes b = new Tripulantes("Miguel", rd.nextInt(1, 99), rd.nextBoolean());
		Tripulantes c = new Tripulantes("Juan", rd.nextInt(1, 99), rd.nextBoolean());
		Tripulantes d = new Tripulantes("Guillermo", rd.nextInt(1, 99), rd.nextBoolean());
		Tripulantes g = new Tripulantes("Diego", rd.nextInt(1, 99), rd.nextBoolean());

		ObjectInputStream ois = null;

		try {
			FileOutputStream fos = new FileOutputStream(f, true);
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			oos.writeObject(a);
			oos.writeObject(b);
			oos.writeObject(c);
			oos.writeObject(d);
			oos.writeObject(g);

			oos.close();
			fos.close();

			ArrayList<Tripulantes> arr = new ArrayList<Tripulantes>();
			arr.add(a);
			Salas z = new Salas("Sala A", arr);

			arr = new ArrayList<Tripulantes>();
			arr.add(b);
			Salas y = new Salas("Sala B", arr);

			arr = new ArrayList<Tripulantes>();
			arr.add(c);
			Salas x = new Salas("Sala C", arr);

			arr = new ArrayList<Tripulantes>();
			arr.add(d);
			Salas w = new Salas("Sala D", arr);

			arr = new ArrayList<Tripulantes>();
			arr.add(g);
			Salas v = new Salas("Sala E", arr);

			f = new File("salas.dat");

			fos = new FileOutputStream(f);
			oos = new ObjectOutputStream(fos);

			oos.writeObject(z);
			oos.writeObject(y);
			oos.writeObject(x);
			oos.writeObject(w);
			oos.writeObject(v);

			oos.close();
			fos.close();

			f = new File("tripulantes.dat");

			FileInputStream fis = new FileInputStream(f);
			ois = new ObjectInputStream(fis);

			while (true) {
				Object obj = ois.readObject();
				if (obj instanceof Tripulantes)
					System.out.println((Tripulantes) obj);
			}

		} catch (IOException e) {
		} catch (ClassNotFoundException e) {
		}
	}

	public static void Mover(String a, String b) {
		File f = new File("tripulantes.dat");
		ArrayList<Salas> s = new ArrayList<Salas>();
		ObjectInputStream ois = null;
		FileInputStream fis = null;
		;
		ObjectOutputStream oos = null;
		FileOutputStream fos = null;
		;
		Boolean flag = false;
		Boolean flag2 = false;
		String sala = null;
		Object c = null;
		Tripulantes tri = null;
		try {
			fis = new FileInputStream(f);
			ois = new ObjectInputStream(fis);
			Tripulantes d = null;
			while (true) {
				c = ois.readObject();

				if (c instanceof Tripulantes) {
					d = ((Tripulantes) c);
					if (d.getNombre().equals(a)) {
						flag = true;
						tri = d;
					}
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			if (flag) {
				f = new File("salas.dat");
				try {
					fis = new FileInputStream(f);
					ois = new ObjectInputStream(fis);
					Salas d = null;

					while (true) {
						c = ois.readObject();

						if (c instanceof Salas) {
							d = ((Salas) c);

							for (int i = 0; i < d.lista.size(); i++) {
								if (d.lista.get(i).nombre.equals(a)) {
									flag2 = true;
									sala = d.nombre;
								}
							}
						}
					}
				} catch (IOException e1) {
					
					f = new File("salas.dat");

					Salas d = null;
					try {
						ois.close();
						fis.close();
						fis = new FileInputStream(f);
						ois = new ObjectInputStream(fis);

						if (flag2) {
							while (true) {
								c = ois.readObject();

								if (c instanceof Salas) {
									d = ((Salas) c);

									if (d.nombre.equals(sala)) {
										for(int i=0;i<d.lista.size();i++) {
											if(d.lista.get(i).nombre.equals(tri.nombre))
												d.lista.remove(i);
										}
										s.add(d);
									} else if (d.nombre.equals(b)) {
										d.lista.add(tri);
										s.add(d);
									} else {
										s.add(d);
									}
								}
							}
						} else {
							while (true) {
								c = ois.readObject();

								if (c instanceof Salas) {
									d = ((Salas) c);

									if (d.nombre.equals(b)) {
										d.lista.add(tri);
										s.add(d);
									} else {
										s.add(d);
									}
								}
							}
						}
					} catch (FileNotFoundException e2) {
						e2.printStackTrace();
					} catch (IOException e2) {
						f = new File("salas.dat");
						File f2 = new File("copia.dat");

						try {
							ois.close();
							fis.close();
							fos = new FileOutputStream(f2);
							oos = new ObjectOutputStream(fos);

							for (int i = 0; i < s.size(); i++) {
								oos.writeObject(s.get(i));
							}

							oos.close();
							fos.close();
							ois.close();
							fis.close();
						} catch (IOException e3) {
							e3.printStackTrace();
						}

					} catch (ClassNotFoundException e2) {
						e2.printStackTrace();
					}
				} catch (ClassNotFoundException e2) {

				}
			} else {
				System.out.println("No existe dicho tripulante");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static void EliCop() {
		File f = new File("salas.dat");
		File copy = new File("copia.dat");

		f.delete();
		copy.renameTo(f);
	}

	public static void Leer(String a) {
		File f = new File("copia.dat");

		try {
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);
			while(true) {
				Object b = ois.readObject();
				Salas c = (Salas)b;
				
				if (a.equals("TOTES")) {
					System.out.println(c);
				}else if(c.nombre.equals(a)){
					System.out.println(c);
				}
			}
		} catch (IOException e) {
		} catch (ClassNotFoundException e) {
		}
	}
}

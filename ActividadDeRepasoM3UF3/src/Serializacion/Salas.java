package Serializacion;

import java.io.Serializable;
import java.util.ArrayList;

public class Salas implements Serializable{
	private static final long serialVersionUID = 1L;
	String nombre;
	ArrayList<Tripulantes> lista;
	
	public Salas(String a, ArrayList<Tripulantes> b) {
		nombre=a;
		lista=b;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Tripulantes> getLista() {
		return lista;
	}

	public void setLista(ArrayList<Tripulantes> lista) {
		this.lista = lista;
	}

	@Override
	public String toString() {
		return "Sales [nombre=" + nombre + ", lista=" + lista + "]";
	}
	
}

package GestionDeFicheros;

import java.io.File;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		boolean a = true;
		
		File f = null;
		
		while (a == true) {
			switch (Menu.MenuGF()) {
			case 1:
				f=new File(Funciones.Selecion());
				break;
			case 2:
				if(f==null)
					System.out.println("Primero escoja un fichero en la primera opcion");
				else {
					System.out.println("Introduzca el nombre del capitan:");
					String b = sc.nextLine();
					System.out.println("Introduzca el nombre del planeta:");
					String c = sc.nextLine();
					System.out.println("Introduzca el numero de creditos:");
					int d = sc.nextInt();
					sc.nextLine();
					Funciones.MeterMision(b,c,d,f);
				}
				break;
			case 3:
				if(f==null)
					System.out.println("Primero escoja un fichero en la primera opcion");
				else {
					System.out.println("Introduzca el nombre del capitan:");
					String b = sc.nextLine();
					System.out.println("Introduzca el nombre del planeta:");
					String c = sc.nextLine();
					Funciones.MeterDineros(b,c,f);
				}
				break;
			case 4:
				if(f==null)
					System.out.println("Primero escoja un fichero en la primera opcion");
				else {
					System.out.println("Introduzca el nombre del capitan:");
					String b = sc.nextLine();
					Funciones.Exportar(b,f);
				}
				break;
			case 5:
				a = false;
				break;
			default:
				System.out.println("VALOR NO VALIDO");
			}
		}
		sc.close();
	}

}

package GestionDeFicheros;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class Funciones {

	static Scanner sc = new Scanner(System.in);

	public static String Selecion() {
		System.out.println("Porfavor introduzca el nombre del archivo:");
		File a = new File(sc.nextLine() + ".csv");

		if (!a.exists()) {
			try {
				FileOutputStream b = new FileOutputStream(a);
				OutputStreamWriter c = new OutputStreamWriter(b);
				c.write("");
				c.close();
			} catch (IOException e) {
				System.err.println(e);
			}
		}
		System.out.println(a.getName());
		return a.getName();
	}

	public static void MeterMision(String b, String c, int d, File f) {
		boolean flag = true;

		try {
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);

			while (br.ready()) {
				String z = br.readLine();
				String y[] = z.split(";");
				if (y[0].equals(b) && y[1].equals(c)) {
					flag = false;
				}
			}

			br.close();

			if (flag) {
				FileWriter fw = new FileWriter(f, true);
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(b + ";" + c + ";" + d + "\n");
				bw.close();
			} else {
				System.out.println("REPETIDO");
			}
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	public static void MeterDineros(String b, String c, File f) {	
		File copy = new File(f.getName()+"_copy.csv");
		
		try {
			FileWriter fw = new FileWriter(copy, true);
			BufferedWriter bw = new BufferedWriter(fw);
			
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);

			while (br.ready()) {
				String z = br.readLine();
				String y[] = z.split(";");
				if (y[0].equals(b) && y[1].equals(c)) {
					int i = Integer.parseInt(y[2]);
					bw.write(b + ";" + c + ";" + (i+10000) + "\n");
					System.out.println(b + " " + c + " " + (i+10000) + "\n");
				}else {
					bw.write(y[0] + ";" + y[1] + ";" + y[2]+ "\n");
				}
			}

			br.close();
			bw.close();	
			
			f.delete();
			copy.renameTo(f);
			
		} catch (IOException e) {
			System.err.println(e);
		}
	}
	
	public static void Exportar(String b,File f) {
		try {
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			
			if(b.equals("TOTS")) {
				
				while(br.ready()) {
					String z = br.readLine();
					String y[] = z.split(";");
					System.out.println(y[0] + " " + y[1] + " " + y[2]);
				}
			}else {
				while(br.ready()) {
					String z = br.readLine();
					String y[] = z.split(";");
					if(y[0].equals(b)) {
						System.out.println(y[0] + " " + y[1] + " " + y[2]);
					}
				}
			}
			br.close();
		}catch(IOException e){
			
		}
	}
}

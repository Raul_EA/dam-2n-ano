package application;
	
import java.util.Stack;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			primaryStage.setTitle("JavaFx");
			
			Button boton = new Button("JavaFX");
			boton.setDefaultButton(true);
			boton.setPrefSize(100, 50);
			boton.setLayoutX(105);
			boton.setLayoutY(110);
			
			Label l = new Label("AAAAAAAAAAAAAAAAA");
			l.setLayoutX(105);
			l.setLayoutY(300);
            l.setVisible(false);
        
            StackPane root = new StackPane();
            Scene scene = new Scene( root, 500, 500 );
			primaryStage.setScene( scene );
			
            root.getChildren().add(l);
        
			boton.setOnAction(new EventHandler<ActionEvent>() {
	            @Override
	            public void handle(ActionEvent event) {
	                if(l.isVisible())
	                	l.setVisible(false);
	                else
	                	l.setVisible(true);
	    		}

	        });
			
			root.getChildren().add(boton);
			
			primaryStage.show();	
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}

package Model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="Tamagotchi")
public class Tamagotchi {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	@Column(name="nom", length=50,nullable = false)
	private String nom;
	@Column(name="descripcion", length=50,nullable = false)
	private String descripcion;
	@Column(name="gana",precision=6,scale=2)
	private double gana=50.00;
	@Column(name="viu")
	private Boolean viu;
	@Column(name="felicitat")
	private int felicitat=50;
	@Column(name="etapa",nullable = false)
	private Etapa etapa;
	@Column(name="dataNaixement")
	private LocalDateTime dataNaixement = LocalDateTime.now();
	
	public Tamagotchi() {
		super();
	}

	public Tamagotchi(int id, String nom, String descripcion, double gana, Boolean viu, int felicitat, Etapa etapa,
			LocalDateTime dataNaixement) {
		super();
		this.id = id;
		this.nom = nom;
		this.descripcion = descripcion;
		this.gana = gana;
		this.viu = viu;
		this.felicitat = felicitat;
		this.etapa = etapa;
		this.dataNaixement = dataNaixement;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getGana() {
		return gana;
	}

	public void setGana(double gana) {
		this.gana = gana;
	}

	public Boolean getViu() {
		return viu;
	}

	public void setViu(Boolean viu) {
		this.viu = viu;
	}

	public int getFelicitat() {
		return felicitat;
	}

	public void setFelicitat(int felicitat) {
		this.felicitat = felicitat;
	}

	public Etapa getEtapa() {
		return etapa;
	}

	public void setEtapa(Etapa etapa) {
		this.etapa = etapa;
	}

	public LocalDateTime getDataNaixement() {
		return dataNaixement;
	}

	public void setDataNaixement(LocalDateTime dataNaixement) {
		this.dataNaixement = dataNaixement;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "Tamagotchi [id=" + id + ", nom=" + nom + ", descripcion=" + descripcion + ", gana=" + gana + ", viu="
				+ viu + ", felicitat=" + felicitat + ", etapa=" + etapa + ", dataNaixement=" + dataNaixement + "]";
	}

	
	
}

package Main;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import Model.Etapa;
import Model.Tamagotchi;

public class Main 
{
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static synchronized SessionFactory getSessionFactory() 
	{
		if (sessionFactory == null) 
		{
			// exception handling omitted for brevityaa
			serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
			sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
		}
		return sessionFactory;
	}
	
	
	public static void main(String[] args) 
	{
		try 
		{
			//Instanciem el nostre objecte session
			session = getSessionFactory().openSession();
			//Abans de començar qualsevol consulta o operacio a la BBDD hem d'obrir una nova transaction
			session.beginTransaction();
			Tamagotchi a = new Tamagotchi();
			a.setNom("Jose");
			a.setDescripcion("cosa random 1");
			a.setEtapa(Etapa.Baby);
			session.save(a);
			Tamagotchi b = new Tamagotchi();
			b.setNom("Miguel");
			b.setDescripcion("cosa random 2");
			b.setEtapa(Etapa.Baby);
			session.save(b);
			
			//Quan volguem guardar les dades o els canvis a la BBDD hem de fer un commit.
			session.getTransaction().commit();
			session.getTransaction().begin();
			
			Tamagotchi c = session.find(Tamagotchi.class, 1);
			System.out.println(c);
			c.setNom("Updategotchi");
			
			session.merge(c);
			
			session.getTransaction().commit();
			session.getTransaction().begin();
			
			List registres = session.createQuery("select a from Tamagotchi a").getResultList();
			System.out.println(registres);
			session.remove(registres.get(1));
			session.getTransaction().commit();
			session.getTransaction().begin();
			
			registres = session.createQuery("select a from Tamagotchi a").getResultList();
			System.out.println(registres);
			
			
		}
		catch (Exception sqlException) 
		{
			sqlException.printStackTrace();
			if (null != session.getTransaction()) 
			{
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			sqlException.printStackTrace();
		} 
		finally 
		{
			if (session != null) 
			{
				session.close();
			}
		}

	}
		
}


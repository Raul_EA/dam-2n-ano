package Main;

import java.time.LocalDateTime;

import DAO.DAOAlimentos;
import DAO.DAOJoguina;
import DAO.DAOTamagotchi;
import Model.Aliment;
import Model.Etapa;
import Model.Joguina;
import Model.Tamagotchi;

public class Main {
	
	
	public static void main(String[] args) {
	
		DAOAlimentos DAOAliment = new DAOAlimentos();
		DAOJoguina DAOJoguina = new DAOJoguina();
		DAOTamagotchi DAOTamagotchi = new DAOTamagotchi();
		
		DAOAliment.generarAliment("Patata", "Es una patata, yo k se", 300);
		DAOJoguina.generarJoguina("Pelota", "Es una pelota", 100);
		DAOTamagotchi.generarTamagotchi("Manolo", "El del bombo", 50, true, 2, Etapa.Egg, LocalDateTime.now());
		DAOTamagotchi.generarTamagotchi("Paco", "Paquito el hombre rana", 100, false, 20, Etapa.Adult, LocalDateTime.now());
		
		System.err.println(DAOTamagotchi.getByID(3));
		
		DAOTamagotchi.assignar(DAOTamagotchi.getByID(3), DAOJoguina.getByID(2));
		DAOTamagotchi.assignar(DAOTamagotchi.getByID(3), DAOAliment.getByID(1));
		
		DAOTamagotchi.hacerAmigo(DAOTamagotchi.getByID(3), DAOTamagotchi.getByID(4));
		
		System.err.println(DAOTamagotchi.getByID(3));
		
		DAOAliment.modValorNut(DAOAliment.getByID(1), 5000);
		DAOJoguina.modDiversion(2, 200);
		
		DAOTamagotchi.usarAliment(DAOTamagotchi.getByID(3));
		DAOTamagotchi.usarJoguina(DAOTamagotchi.getByID(3));
		
		DAOTamagotchi.estaVivo(DAOTamagotchi.getByID(3));
		
		System.err.println(DAOTamagotchi.getByID(3).toString());
	}

}

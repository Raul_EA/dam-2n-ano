package Model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="aliments")
public class Aliment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	@Column(name="nom",length=50,nullable=false)
	private String nom;
	@Column(name="descripcion",length=50,nullable=false)
	private String descripcion;
	@Column(name="valorNutricional",precision=6,scale=2)
	private double valorNutricional = 10.00;
	@OneToMany(mappedBy = "aliment", cascade = CascadeType.ALL)
    private List<Tamagotchi> tamagotchis = new ArrayList<Tamagotchi>();
	
	public Aliment() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getValorNutricional() {
		return valorNutricional;
	}

	public void setValorNutricional(double valorNutricional) {
		this.valorNutricional = valorNutricional;
	}

	public List<Tamagotchi> getTamagotchis() {
		return tamagotchis;
	}

	public void setTamagotchis(List<Tamagotchi> tamagotchis) {
		this.tamagotchis = tamagotchis;
	}

	@Override
	public String toString() {
		return "Aliment [id=" + id + ", nom=" + nom + ", descripcion=" + descripcion + ", valorNutricional="
				+ valorNutricional ;
	}

}

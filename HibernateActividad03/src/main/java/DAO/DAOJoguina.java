package DAO;

import Model.Joguina;

public class DAOJoguina extends GenericDAO<Joguina, Integer> implements IJoguinaDAO<Joguina, Integer>{

	public DAOJoguina() {
		super(Joguina.class);
	}
	
	public void generarJoguina(String nom, String descripcion, int nivellDiversio) {
		Joguina x = new Joguina();
		x.setNom(nom);
		x.setDescripcion(descripcion);
		x.setNivellDiversio(nivellDiversio);
		super.save(x);
	}
	
	@Override
	public void modDiversion(Integer id, int nivell) {
		Joguina a = super.getByID(id);
		a.setNivellDiversio(nivell);
		super.update(a);
	}

}

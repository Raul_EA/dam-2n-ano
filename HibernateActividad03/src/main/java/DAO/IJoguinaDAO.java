package DAO;

import java.io.Serializable;

public interface IJoguinaDAO<T, ID extends Serializable> extends IGenericDAO<T, ID> 
{
	void modDiversion(ID id, int nivell);
}

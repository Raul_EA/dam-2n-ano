package DAO;

import java.io.Serializable;
import java.util.List;

import Model.Aliment;
import Model.Joguina;

public interface ITamagotchiDAO<T, ID extends Serializable> extends IGenericDAO<T, ID> 
{
	void ponerJugueteID(Joguina joguina, ID id);
	void ponerAlimentoID(Aliment aliment, ID id);
	boolean estaVivo(T entity);
	void assignar(T entity,Object obj);
	void usarJoguina(T entity);
	void usarAliment(T entity);
	void hacerAmigo(T entity,T amigo);
	List<T> amigos(T entity);
	void hambre(T entity);
}

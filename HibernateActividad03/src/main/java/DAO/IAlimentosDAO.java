package DAO;

import java.io.Serializable;
import java.util.List;

import Model.Tamagotchi;

public interface IAlimentosDAO<T, ID extends Serializable> extends IGenericDAO<T, ID>  {
	List<Tamagotchi> listaDeTipo(T entity);
	void modValorNut(T entity, int valor);
}

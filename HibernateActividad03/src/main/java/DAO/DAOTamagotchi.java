package DAO;

import java.time.LocalDateTime;
import java.util.List;

import Model.Aliment;
import Model.Etapa;
import Model.Joguina;
import Model.Tamagotchi;

public class DAOTamagotchi extends GenericDAO<Tamagotchi, Integer> implements ITamagotchiDAO<Tamagotchi, Integer> {

	public DAOTamagotchi() {
		super(Tamagotchi.class);
	}

	public void generarTamagotchi(String nom, String descripcion, double gana, Boolean viu, int felicitat, Etapa etapa,
			LocalDateTime dataNaixement) {
		Tamagotchi x = new Tamagotchi();
		x.setNom(nom);
		x.setDescripcion(descripcion);
		x.setGana(gana);
		x.setViu(viu);
		x.setFelicitat(felicitat);
		x.setEtapa(etapa);
		x.setDataNaixement(dataNaixement);
		super.save(x);
	}
	
	@Override
	public void ponerJugueteID(Joguina joguina, Integer id) {
		Tamagotchi a =super.getByID(id);
		a.setJoguina(joguina);
		super.update(a);
	}

	@Override
	public void ponerAlimentoID(Aliment aliment, Integer id) {
		Tamagotchi a =super.getByID(id);
		a.setAliment(aliment);
		super.update(a);
	}

	@Override
	public boolean estaVivo(Tamagotchi entity) {
		return entity.getViu();
	}

	@Override
	public void assignar(Tamagotchi entity,Object obj) {
		if(obj instanceof Aliment) {
			Aliment a = (Aliment) obj;
			entity.setAliment(a);
			super.update(entity);
		}else if(obj instanceof Joguina){
			Joguina a = (Joguina) obj;
			entity.setJoguina(a);
			super.update(entity);
		}
	}

	@Override
	public void usarJoguina(Tamagotchi entity) {
		entity.setFelicitat(entity.getFelicitat()+entity.getJoguina().getNivellDiversio());
		entity.setJoguina(null);
		super.update(entity);
	}

	@Override
	public void usarAliment(Tamagotchi entity) {
		entity.setGana(entity.getGana()-entity.getAliment().getValorNutricional());
		entity.setAliment(null);
		if(entity.getGana()<0)
			entity.setGana(0);
		super.update(entity);
	}

	@Override
	public void hacerAmigo(Tamagotchi entity, Tamagotchi amigo) {
		entity.getAmigos().add(amigo);
		amigo.getAmigos().add(entity);
		super.update(entity); 
		super.update(amigo);
	}

	@Override
	public List<Tamagotchi> amigos(Tamagotchi entity) {
		return entity.getAmigos();
	}

	@Override
	public void hambre(Tamagotchi entity) {
		if(entity.getGana()>=200) {
			if(entity.getAliment()!=null)
				usarAliment(entity);
			else
				entity.setViu(false);
			super.update(entity);
		}
	}

}

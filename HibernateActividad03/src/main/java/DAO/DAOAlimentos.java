package DAO;

import java.util.List;

import Model.Aliment;
import Model.Tamagotchi;

public class DAOAlimentos extends GenericDAO<Aliment, Integer> implements IAlimentosDAO<Aliment, Integer> {

	public DAOAlimentos() {
		super(Aliment.class);
	}

	public void generarAliment(String nom, String descripcion, double valorNutricional) {
		Aliment x = new Aliment();
		x.setNom(nom);
		x.setDescripcion(descripcion);
		x.setValorNutricional(valorNutricional);
		super.save(x);
	}
	
	@Override
	public List<Tamagotchi> listaDeTipo(Aliment entity) {
		return entity.getTamagotchis();
	}

	@Override
	public void modValorNut(Aliment entity, int valor) {
		entity.setValorNutricional(valor);
		super.update(entity);
	}
}

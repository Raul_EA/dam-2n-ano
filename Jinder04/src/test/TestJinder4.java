package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import prob.Persona;
import prob.SEXE;
import impl.Jinder;

class TestJinder4 {

	@Test
	void testJinderMatches() {
		Jinder j = new Jinder();
		Persona p1 = new Persona("Daniel", 20, SEXE.M, SEXE.F);
		j.afegir(p1);		
		Persona p2 = new Persona ("Ana", 23, SEXE.F, SEXE.M);
		j.afegir(p2);		
		Persona p3 = new Persona ("Berta", 34, SEXE.F, SEXE.M);
		j.afegir(p3);		
		Persona p4 = new Persona ("Carles", 44, SEXE.M, SEXE.F);
		j.afegir(p4);		
		Persona p5 = new Persona ("Fran", 37, SEXE.M, SEXE.M);
		j.afegir(p5);		
		Persona p6 = new Persona ("Gabriel", 47, SEXE.M, SEXE.M);
		j.afegir(p6);		
		Persona p7 = new Persona ("Hipolit", 65, SEXE.M, SEXE.F);
		j.afegir(p7);		
		Persona p8 = new Persona ("Ines", 40, SEXE.F, SEXE.M);
		j.afegir(p8);		

		for(Persona p:j.toList()) 
			j.jwipear(p);
		
		assertEquals(true, j.getLoves(p1).contains(p2));
		assertEquals(false, j.getLoves(p1).contains(p7));
		assertEquals(true, j.getLoves(p1).contains(p3));
		assertEquals(true, j.getMatches(p1).contains(p2));
		assertEquals(false, j.getMatches(p1).contains(p7));
		assertEquals(true, j.getMatches(p1).contains(p3));		
		assertEquals(false, j.getRejects(p1).contains(p2));
		assertEquals(false, j.getRejects(p1).contains(p7));
		assertEquals(true, j.getRejects(p1).contains(p8));		
		assertEquals(true, j.getMatches(p2).contains(p1));
		assertEquals(false, j.getMatches(p7).contains(p1));
		assertEquals(true, j.getMatches(p3).contains(p1));		
		assertEquals(8,j.chatGlobal().size());
	}
}

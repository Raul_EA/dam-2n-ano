package prob;

import java.time.LocalDateTime;

public class Missatge {
	static int nMissatge = 0;
	private int id;
	private LocalDateTime moment;
	private Persona PersonaEnvia;
	private Persona PersonaRep;
	private String text;
	
	public Missatge(Persona personaEnvia, Persona personaRep, String text) {
		super();
		this.id = ++ nMissatge;
		PersonaEnvia = personaEnvia;
		PersonaRep = personaRep;
		this.moment = LocalDateTime.now();
		this.text = text;
	}
	public LocalDateTime getMoment() {
		return moment;
	}
	public void setMoment(LocalDateTime moment) {
		this.moment = moment;
	}
	public Persona getPersonaEnvia() {
		return PersonaEnvia;
	}
	public void setPersonaEnvia(Persona personaEnvia) {
		PersonaEnvia = personaEnvia;
	}
	public Persona getPersonaRep() {
		return PersonaRep;
	}
	public void setPersonaRep(Persona personaRep) {
		PersonaRep = personaRep;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	public String toString() {
		return (id + " - " + moment + " : " + PersonaEnvia + " --> " + PersonaRep + " : " + text);
	}
}

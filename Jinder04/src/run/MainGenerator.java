package run;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import impl.Jinder;
import prob.Missatge;
import prob.Persona;
import prob.SEXE;

public class MainGenerator {

	public static void main(String[] args) {
		
		Jinder j = new Jinder(Generador.generador(43));
		
		Persona p = j.personaRandom();
		
		System.out.println("Persona Random:" + p);
		j.jwipear(p);

		System.out.println("Loves: ");
		List l = j.getLoves(p);
		System.out.println(l);
		System.out.println("Rejects: ");
		l = j.getRejects(p);
		System.out.println(l);

		for(Persona v :j.getLoves(p))
			j.jwipear(v);
				
		System.out.println("Matches de " + p);
		System.out.println(j.getMatches(p));

		System.out.println("======= MISSATGES ========");
		for(Missatge m: j.chatGlobal())
			System.out.println(m);
	}
		
}	



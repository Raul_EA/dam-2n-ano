package run;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import impl.Jinder;
import prob.Missatge;
import prob.Persona;
import prob.SEXE;

public class Main {

	public static void main(String[] args) {

		Jinder j = new Jinder();
		Persona p1 = new Persona("Daniel", 20, SEXE.M, SEXE.F);
		j.afegir(p1);		
		Persona p2 = new Persona ("Ana", 23, SEXE.F, SEXE.M);
		j.afegir(p2);		
		Persona p3 = new Persona ("Berta", 34, SEXE.F, SEXE.M);
		j.afegir(p3);		
		Persona p4 = new Persona ("Carles", 44, SEXE.M, SEXE.F);
		j.afegir(p4);		
		Persona p5 = new Persona ("Fran", 37, SEXE.M, SEXE.M);
		j.afegir(p5);		
		Persona p6 = new Persona ("Gabriel", 47, SEXE.M, SEXE.M);
		j.afegir(p6);		
		Persona p7 = new Persona ("Hipolit", 65, SEXE.M, SEXE.F);
		j.afegir(p7);		
		Persona p8 = new Persona ("Ines", 40, SEXE.F, SEXE.M);
		j.afegir(p8);		

		for(Persona p:j.toList()) 
			j.jwipear(p);
		
		for (Persona p: j.toList()) {
			System.out.println("*** Sóc ***" + p);
			System.out.println("*** Loves:   " + j.getLoves(p));
			System.out.println("*** Rejects: " + j.getRejects(p));
			System.out.println("*** Matches: " + j.getMatches(p));			
		}
		System.out.println("missatges: " + j.chatGlobal().size());
	}
}	



package run;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import impl.Jinder;
import prob.Missatge;
import prob.Persona;
import prob.SEXE;

public class MainGeneratorTotal {

	public static void main(String[] args) {
		Jinder j = new Jinder(Generador.generador(5));

		long ti = System.currentTimeMillis();
		for (Persona p : j.toList())
			j.jwipear(p);
		long tf = System.currentTimeMillis();
		System.out.println("Persones: " + j.size() + "  Temps: " + (tf - ti) + " ms");

		System.out.println("missatges: " + j.chatGlobal().size());

		Persona p = j.personaRandom();
		System.out.println("Random: " + p);
		System.out.println("Loves: ");
		System.out.println(j.getLoves(p));
		System.out.println("Rejects: ");
		System.out.println(j.getRejects(p));
		System.out.println("Matches: ");
		System.out.println(j.getMatches(p));

	}
}

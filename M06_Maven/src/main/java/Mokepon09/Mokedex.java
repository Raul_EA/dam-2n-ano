package Mokepon09;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="mokedex")
@XmlType(propOrder = { "nom_entrenador", "entrades" })
public class Mokedex {
	String nom_entrenador;
	List<Entrada> entrades = new ArrayList<Entrada>();
	
	@XmlElementWrapper(name="entradas")
	@XmlElement(name="entrada")
	public List<Entrada> getEntrades() {
		return entrades;
	}
	
	public void setEntrades(List<Entrada> entrades) {
		this.entrades = entrades;
	}
	
	@XmlElement
	public String getNom_entrenador() {
		return nom_entrenador;
	}
	
	public void setNom_entrenador(String nom_entrenador) {
		this.nom_entrenador = nom_entrenador;
	}

	@Override
	public String toString() {
		return "Mokedex [nom_entrenador=" + nom_entrenador + ", entrades=" + entrades + "]";
	}

}

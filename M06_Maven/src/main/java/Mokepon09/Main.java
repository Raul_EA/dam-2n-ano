package Mokepon09;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class Main {
	
	public static void main(String[] args) {
		String f = "exemple.xml";

		Entrada a = new Entrada();
		a.id=48;
		a.nom="Daniel";
		a.tipus="Agua";
		a.peso=67;
		a.localizaciones.add("Parque");
		a.localizaciones.add("Mercadona");
		a.evoluciones.pre_evolucion="Danonino";
		a.evoluciones.post_evolucion="Danieloncho";
		
		afegirMokedex(f,a);
		
		modificarPes(f,48,88.9);
		
		afegirLocalitzacio(f,48,"Gym");
		
		postEvolucio(f,48);
	}

	static void afegirMokedex(String nomFitxer, Entrada novaEntrada) {
		File f = new File(nomFitxer);

		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Mokedex.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Mokedex p = (Mokedex) jaxbUnmarshaller.unmarshal(f);
			
			p.entrades.add(novaEntrada);
			
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(p, f);

		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}
	
	static void modificarPes (String nomFitxer, int idEntrada, double d){
		File f = new File(nomFitxer);
		boolean flag=false;
		
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Mokedex.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Mokedex p = (Mokedex) jaxbUnmarshaller.unmarshal(f);
			
			for(Entrada a : p.entrades) {
				if(a.id==idEntrada) {
					a.peso+=d;
					flag=true;
				}
			}
			
			if(!flag) {
				System.err.println("Id no existente");
			}
				
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(p, f);

		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}
	
	static void afegirLocalitzacio(String nomFitxer, int idEntrada, String novaLoc) {
		File f = new File(nomFitxer);
		boolean flag=false;
		
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Mokedex.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Mokedex p = (Mokedex) jaxbUnmarshaller.unmarshal(f);
			
			for(Entrada a : p.entrades) {
				if(a.id==idEntrada) {
					a.localizaciones.add(novaLoc);
					flag=true;
				}
			}
			
			if(!flag) {
				System.err.println("Id no existente");
			}
			
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(p, f);

		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}
	
	static void postEvolucio(String nomFitxer, int idEntrada) {
		File f = new File(nomFitxer);
		boolean flag=false;
		
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Mokedex.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Mokedex p = (Mokedex) jaxbUnmarshaller.unmarshal(f);
			
			for(Entrada a : p.entrades) {
				if(a.id==idEntrada) {
					if(a.evoluciones.post_evolucion.equals("-")) {
						System.out.println("No tiene post evolucion");
						flag=true;
					}else{
						System.out.println(a.evoluciones.post_evolucion);
						flag=true;
					}
				}
			}
			
			if(!flag) {
				System.err.println("Id no existente");
			}
				
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(p, f);

		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}
}

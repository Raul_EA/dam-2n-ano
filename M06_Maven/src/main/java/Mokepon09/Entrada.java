package Mokepon09;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

// l'ordre en que apareixeran els seus elements interns en el XML
@XmlType(propOrder = { "nom", "tipus","peso","evoluciones","localizaciones" })
public class Entrada {

	int id;
	String nom, tipus;
	double peso;
	Evoluciones evoluciones = new Evoluciones();
	List<String> localizaciones= new ArrayList<String>();
	
	@XmlElement
	public Evoluciones getEvoluciones() {
		return evoluciones;
	}

	public void setEvoluciones(Evoluciones evoluciones) {
		this.evoluciones = evoluciones;
	}

	@XmlElement
	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}
	@XmlElementWrapper
	@XmlElement(name="localizacion")
	public List<String> getLocalizaciones() {
		return localizaciones;
	}

	public void setLocalizaciones(List<String> localizaciones) {
		this.localizaciones = localizaciones;
	}

	// es un atribut. els atributs son els que es posen a la propia etiqueta arrel
	@XmlAttribute
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	// es un element. Com que no especifiquem name, s'assumeix que es diu nom
	@XmlElement
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	// aquests tags es posen sempre abans del getter
	@XmlElement
	public String getTipus() {
		return tipus;
	}

	public void setTipus(String t) {
		this.tipus = t;
	}
	@Override
	public String toString() {
		return "Entrada [id=" + id + ", nom=" + nom + ", tipus=" + tipus + "]";
	}
}

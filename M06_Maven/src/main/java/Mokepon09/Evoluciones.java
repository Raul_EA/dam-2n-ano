package Mokepon09;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "pre_evolucion", "post_evolucion" })
public class Evoluciones {
	String pre_evolucion;
	String post_evolucion;
	
	@XmlElement(name="pre_evolucion")
	public String getPre_evolucion() {
		return pre_evolucion;
	}
	public void setPre_evolucion(String pre_evolucion) {
		this.pre_evolucion = pre_evolucion;
	}
	@XmlElement(name="post_evolucion")
	public String getPost_evolucion() {
		return post_evolucion;
	}
	public void setPost_evolucion(String post_evolucion) {
		this.post_evolucion = post_evolucion;
	}
	@Override
	public String toString() {
		return "Evoluciones [pre_evolucion=" + pre_evolucion + ", post_evolucion=" + post_evolucion + "]";
	}
}

package PruebaExamen;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class ThingDef2 {
	
	private String ParentName;
	private String defName;
	private String description;
	private int stackLimit;
	private StatBases2 statBases;
	private boolean healthAffectsPrice;
	private List<String> thingCategories = new ArrayList<>();
	private boolean intricate;
	private String label;
	
	public String getParentName() {
		return ParentName;
	}
	
	public void setParentName(String parentName) {
		this.ParentName = parentName;
	}
	public String getDefName() {
		return defName;
	}
	public void setDefName(String defName) {
		this.defName = defName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getStackLimit() {
		return stackLimit;
	}
	public void setStackLimit(int stackLimit) {
		this.stackLimit = stackLimit;
	}
	public StatBases2 getStatbases() {
		return statBases;
	}
	public void setStatbases(StatBases2 statbases) {
		this.statBases = statbases;
	}
	public List<String> getThingCategories() {
		return thingCategories;
	}
	public void setThingCategories(List<String> thingCategories) {
		this.thingCategories = thingCategories;
	}
	public boolean isIntrincate() {
		return intricate;
	}
	
	public void setIntrincate(boolean intrincate) {
		this.intricate = intrincate;
	}
	

	public boolean isHealthAffectsPrice() {
		return healthAffectsPrice;
	}

	public void setHealthAffectsPrice(boolean healthAffectsPrice) {
		this.healthAffectsPrice = healthAffectsPrice;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "ThingDef [parentName=" + ParentName + ", defName=" + defName + ", description=" + description
				+ ", stackLimit=" + stackLimit + ", statbases=" + statBases + ", healthAffectsPrice="
				+ healthAffectsPrice + ", thingCategories=" + thingCategories + ", intrincate=" + intricate
				+ ", label=" + label + "]";
	}
}

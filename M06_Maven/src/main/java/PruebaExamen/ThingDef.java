package PruebaExamen;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class ThingDef {
	
	private String parentName;
	private String defName;
	private String description;
	private int stackLimit;
	private StatBases statbases;
	private String healthAffectsPrice;
	private List<String> thingCategories = new ArrayList<>();
	private boolean intrincate;
	private String label;
	
	@XmlAttribute(name="ParentName")
	public String getParentName() {
		return parentName;
	}
	
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	@XmlElement
	public String getDefName() {
		return defName;
	}
	public void setDefName(String defName) {
		this.defName = defName;
	}
	@XmlElement
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@XmlElement
	public int getStackLimit() {
		return stackLimit;
	}
	public void setStackLimit(int stackLimit) {
		this.stackLimit = stackLimit;
	}
	@XmlElement(name="statBases")
	public StatBases getStatbases() {
		return statbases;
	}
	public void setStatbases(StatBases statbases) {
		this.statbases = statbases;
	}
	@XmlElementWrapper(name="thingCategories")
	@XmlElement(name="li")
	public List<String> getThingCategories() {
		return thingCategories;
	}
	public void setThingCategories(List<String> thingCategories) {
		this.thingCategories = thingCategories;
	}
	@XmlElement(name="intricate")
	public boolean isIntrincate() {
		return intrincate;
	}
	
	public void setIntrincate(boolean intrincate) {
		this.intrincate = intrincate;
	}
	@XmlElement(name="healthAffectsPrice")
	public String getHealthAffectsPrice() {
		return healthAffectsPrice;
	}

	public void setHealthAffectsPrice(String healthAffectsPrice) {
		this.healthAffectsPrice = healthAffectsPrice;
	}

	@XmlElement(name="label")
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "ThingDef [parentName=" + parentName + ", defName=" + defName + ", description=" + description
				+ ", stackLimit=" + stackLimit + ", statbases=" + statbases + ", healthAffectsPrice="
				+ healthAffectsPrice + ", thingCategories=" + thingCategories + ", intrincate=" + intrincate
				+ ", label=" + label + "]";
	}
}

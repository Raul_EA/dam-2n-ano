package PruebaExamen;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class Main {

	public static void main(String[] args) {
		
		/*
		File f = new File("items.xml");
		File f2 = new File("items_copy.xml");
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Item.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Item p = (Item) jaxbUnmarshaller.unmarshal(f);
			
			System.out.println(p);
			
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(p, f2);

		} catch (JAXBException je) {
			je.printStackTrace();
		}
		*/
		//addCategory("AAAAAA","Neutroamine");
		System.out.println(getIntricateValue(0));
	}
	public static void addCategory(String categoryName, String defName) {
		File f = new File("items.xml");
		File f2 = new File("items_added.xml");
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Item.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Item p = (Item) jaxbUnmarshaller.unmarshal(f);
			
			for(ThingDef a : p.getDefs()) {
				if(	a.getDefName().equals(defName)) {
					a.getThingCategories().add(categoryName);
				}
			}
			
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(p, f2);

		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}
	public static ArrayList<String> getIntricateValue(int minValue){
		File f = new File("items.xml");
		ArrayList<String> b = new ArrayList<>();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Item.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Item p = (Item) jaxbUnmarshaller.unmarshal(f);
			
			for(ThingDef a : p.getDefs()) {
				if(a.isIntrincate() && a.getStatbases().getMarketValue()>=minValue) {
					b.add(a.getDefName());
				}
			}
		} catch (JAXBException je) {
			je.printStackTrace();
		}
		return b;
	}
}

package PruebaExamen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class FuncionesJSON {

	public static File f = new File("items.json");

	public static void main(String[] args) {
		// setGravity(9.1);
		// System.out.println(countByCategory("Manufactured"));
		Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
		try {
			Item2 m = gson.fromJson(new FileReader(f), Item2.class);
			System.out.println(m);
			FileWriter fw = new FileWriter("items_copy.json");
			fw.write(gson.toJson(m));
			fw.close();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void setGravity(double value) {
		try {
			JsonElement arrel = JsonParser.parseReader(new FileReader(f));
			JsonObject jsonObject = arrel.getAsJsonObject();
			JsonArray a = (JsonArray) jsonObject.get("Defs");

			for (int i = 0; i < a.size(); i++) {
				JsonObject b = (JsonObject) a.get(i);
				JsonObject c = b.getAsJsonObject("statBases");
				c.addProperty("Mass", (c.get("Mass").getAsDouble() * value));
			}
			Gson escribir = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
			FileWriter fw;

			try {
				fw = new FileWriter("itemsGravity.json");
				fw.append(escribir.toJson(arrel));
				fw.flush();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedOperationException e) {
			System.err.println("Es nulo");
		}
	}

	public static int countByCategory(String categoryName) {
		int z = 0;
		try {
			JsonElement arrel = JsonParser.parseReader(new FileReader(f));
			JsonObject jsonObject = arrel.getAsJsonObject();
			JsonArray a = (JsonArray) jsonObject.get("Defs");

			for (int i = 0; i < a.size(); i++) {
				JsonObject b = (JsonObject) a.get(i);
				JsonArray c = b.getAsJsonArray("thingCategories");
				for (JsonElement d : c) {
					if (d.getAsString().equals(categoryName))
						z++;
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedOperationException e) {
			System.err.println("Es nulo");
		}
		return z;
	}
}

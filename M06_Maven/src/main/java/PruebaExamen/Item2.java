package PruebaExamen;

import java.util.ArrayList;
import java.util.List;

public class Item2 {
	
	private String ModName;
	private List<ThingDef2> Defs = new ArrayList<>();
	
	public String getModName() {
		return ModName;
	}
	
	public void setModName(String modName) {
		this.ModName = modName;
	}
	
	public List<ThingDef2> getDefs() {
		return Defs;
	}
	public void setDefs(List<ThingDef2> defs) {
		Defs = defs;
	}
	
	@Override
	public String toString() {
		return "Item [ModName=" + ModName + ", Defs=" + Defs + "]";
	}
	
	
}

package PruebaExamen;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="Mod")
@XmlType(propOrder = { "modName","defs"})

public class Item {
	
	private String modName;
	private List<ThingDef> Defs = new ArrayList<>();
	
	@XmlElement(name="ModName")
	public String getModName() {
		return modName;
	}
	
	public void setModName(String modName) {
		this.modName = modName;
	}
	
	@XmlElementWrapper(name="Defs")
	@XmlElement(name="ThingDef")
	public List<ThingDef> getDefs() {
		return Defs;
	}
	public void setDefs(List<ThingDef> defs) {
		Defs = defs;
	}
	
	@Override
	public String toString() {
		return "Item [ModName=" + modName + ", Defs=" + Defs + "]";
	}
	
	
}

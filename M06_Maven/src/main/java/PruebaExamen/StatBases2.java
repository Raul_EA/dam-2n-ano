package PruebaExamen;

public class StatBases2 {
	
	private int MaxHitPoints;
	private double MarketValue;
	private double Mass;
	private double Flammability;
	private double DeteriorationRate;
	
	public int getMaxHitPoints() {
		return MaxHitPoints;
	}
	public void setMaxHitPoints(int maxHitPoints) {
		this.MaxHitPoints = maxHitPoints;
	}
	public double getMarketValue() {
		return MarketValue;
	}
	public void setMarketValue(int marketValue) {
		this.MarketValue = marketValue;
	}
	public double getMass() {
		return Mass;
	}
	public void setMass(double mass) {
		this.Mass = mass;
	}
	public double getFlammability() {
		return this.Flammability;
	}
	public void setFlammability(double flammability) {
		this.Flammability = flammability;
	}
	public double getDeteriorationRate() {
		return DeteriorationRate;
	}
	public void setDeteriorationRate(double deteriorationRate) {
		this.DeteriorationRate = deteriorationRate;
	}
	@Override
	public String toString() {
		return "StatBases [maxHitPoints=" + MaxHitPoints + ", marketValue=" + MarketValue + ", mass=" + Mass
				+ ", flammability=" + Flammability + ", deteriorationRate=" + DeteriorationRate + "]";
	}
}

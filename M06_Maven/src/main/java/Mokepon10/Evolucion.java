package Mokepon10;

public class Evolucion {
	String pre_evolucion;
	String post_evolucion;
	
	public String getPre_evolucion() {
		return pre_evolucion;
	}
	public void setPre_evolucion(String pre_evolucion) {
		this.pre_evolucion = pre_evolucion;
	}
	public String getPost_evolucion() {
		return post_evolucion;
	}
	public void setPost_evolucion(String post_evolucion) {
		this.post_evolucion = post_evolucion;
	}
	@Override
	public String toString() {
		return "Evolucion [pre_evolucion=" + pre_evolucion + ", post_evolucion=" + post_evolucion + "]";
	}
	
}

package Mokepon10;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class SinObjJSON {

	public static void main(String[] args) {
		//SinObjetosJSON();
		/*
		JsonObject a = new JsonObject();
		a.addProperty("a", "b");
		a.addProperty("c","d");
		afegirMokedex("mokedex.json",a);
		
		modificarPes("mokedex.json",1,55);
		
		afegirLocalizacion("mokedex.json",1,"Sarda");
		
		postEvolucio("mokedex.json",3);
		*/
		
	}
	static public void postEvolucio(String nomFitxer, int idEntrada) {
		try {
			JsonElement arrel = JsonParser.parseReader(new FileReader(nomFitxer));
			JsonObject jsonObject = arrel.getAsJsonObject();
			JsonArray list = (JsonArray) jsonObject.get("entradas");
			
			JsonObject b = null;
			JsonObject c = null;
			
			for(int i=0;i<list.size();i++) {
				b = (JsonObject)list.get(i);
				if(b.get("id").getAsInt()==idEntrada) {
					c = b.get("evoluciones").getAsJsonObject();
					System.out.println(c.get("post-evolucion").getAsString());
				}else {
					System.err.println("No existe este id");
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch(UnsupportedOperationException e) {
			System.err.println("Es nulo");
		}
	}
	
	
	static public void afegirLocalizacion(String nomFitxer,int idEntrada,String novaLoc) {
		try {
			JsonElement arrel = JsonParser.parseReader(new FileReader(nomFitxer));
			JsonObject jsonObject = arrel.getAsJsonObject();
			JsonArray list = (JsonArray) jsonObject.get("entradas");
			
			JsonObject b = null;
			JsonArray c = null;
			
			for(int i=0;i<list.size();i++) {
				b = (JsonObject)list.get(i);
				if(b.get("id").getAsInt()==idEntrada) {
					c = b.get("localizaciones").getAsJsonArray();
					c.add(novaLoc);
				}
			}
			
			Gson escribir = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
			FileWriter fw;
			
			try {
				fw = new FileWriter(nomFitxer);
				fw.append(escribir.toJson(arrel));
				fw.flush();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	static public void modificarPes(String nomFitxer,int idEntrada,double pesExtra) {
		try {
			JsonElement arrel = JsonParser.parseReader(new FileReader(nomFitxer));
			JsonObject jsonObject = arrel.getAsJsonObject();
			JsonArray list = (JsonArray) jsonObject.get("entradas");
			
			JsonObject b = null;
			
			for(int i=0;i<list.size();i++) {
				b = (JsonObject)list.get(i);
				if(b.get("id").getAsInt()==idEntrada) {
					b.addProperty("pes",(b.get("pes").getAsDouble()+pesExtra));
				}
			}
			Gson escribir = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
			FileWriter fw;
			
			try {
				fw = new FileWriter(nomFitxer);
				fw.append(escribir.toJson(arrel));
				fw.flush();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	static public void afegirMokedex(String nomFitxer, JsonObject novaEntrada) {
		try {
			JsonElement arrel = JsonParser.parseReader(new FileReader(nomFitxer));
			JsonObject jsonObject = arrel.getAsJsonObject();
			JsonArray list = (JsonArray) jsonObject.get("entradas");
			list.add(novaEntrada);
			Gson escribir = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
			FileWriter fw;
			
			try {
				fw = new FileWriter(nomFitxer);
				fw.append(escribir.toJson(arrel));
				fw.flush();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	static public void SinObjetosJSON() {
		JsonObject obj = new JsonObject();

		obj.addProperty("nomEntrenador", "Red");

		JsonArray list = new JsonArray();
		JsonObject obj2 = new JsonObject();
		obj2.addProperty("id", 1);
		obj2.addProperty("nom", "Mulmasaur");
		obj2.addProperty("pes", 15.2);

		JsonObject obj3 = new JsonObject();
		
		obj3.add("pre-evolucion", JsonNull.INSTANCE);
		obj3.addProperty("post-evolucion", "Menusaur");

		obj2.add("evoluciones", obj3);

		JsonArray list2 = new JsonArray();
		list2.add("Ruta 1");
		list2.add("Ruta 6");
		list2.add("Ruta 12");
		list2.add("Bosque 33");

		obj2.add("localizaciones", list2);

		list.add(obj2);
		obj.add("entradas", list);

		Gson escribir = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		System.out.println(escribir.toJson(obj));

		FileWriter fw;
		try {
			fw = new FileWriter("mokedex.json");
			fw.append(escribir.toJson(obj));
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			JsonElement arrel = JsonParser.parseReader(new FileReader("mokedex.json"));
			JsonObject jsonObject = arrel.getAsJsonObject();
			System.out.println(jsonObject);
			System.out.println(jsonObject.keySet());

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
}

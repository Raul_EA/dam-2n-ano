package Mokepon10;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		/*
		Entrada a = new Entrada();
		a.id = 3;
		a.nom = "Jose";
		a.pes = 9.9;
		a.localizaciones.add("Mi casa");
		a.evoluciones.post_evolucion = "Alzehimer";
		a.evoluciones.pre_evolucion = "Carlos";
		afegirMokedex("mokedex.json", a);
		
		GigantamaxPostEvolució("mokedex.json",1);
		*/
		esborrarLocalitzacio("mokedex.json",1,"Ruta 1");
	}

	static public void afegirMokedex(String nomFitxer, Entrada novaEntrada) {
		Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
		try {
			Mokedex m = gson.fromJson(new FileReader(nomFitxer), Mokedex.class);
			m.entradas.add(novaEntrada);
			FileWriter fw = new FileWriter(nomFitxer);
			fw.append(gson.toJson(m));
			fw.close();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static public void GigantamaxPostEvolució(String nomFitxer, int idEntrada) {
		Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
		try {
			Mokedex m = gson.fromJson(new FileReader(nomFitxer), Mokedex.class);
			boolean flag1=false,flag2=false;
			for (Entrada a : m.entradas) {
				if (a.id == idEntrada) {
					flag1=true;
					if (a.evoluciones.post_evolucion != null && !a.evoluciones.post_evolucion.equals("")) {
						flag2=true;
						for (Entrada b : m.entradas) {
							if (b.nom.equals(a.evoluciones.post_evolucion)) {
								b.pes = b.pes * 10;
							} 
						}
					}
				} 
			}

			FileWriter fw = new FileWriter(nomFitxer);
			fw.append(gson.toJson(m));
			fw.close();
			
			if(!flag1)
				throw new Exception("No existe id");
			if(!flag2)
				throw new Exception("No existe evolucion");
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static public void esborrarLocalitzacio(String nomFitxer, int idEntrada, String Loc) {
		Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
		try {
			boolean flag1=false,flag2=false;
			Mokedex m = gson.fromJson(new FileReader(nomFitxer), Mokedex.class);
			for(Entrada a : m.entradas) {
				if(a.id==idEntrada) {
					flag1=true;
					for(String b : a.localizaciones) {
						if(b.equals(Loc)) {
							flag2=true;
						}
					}
				}
				if(flag2) {
					a.localizaciones.remove(Loc);
				}
			}
			FileWriter fw = new FileWriter(nomFitxer);
			fw.append(gson.toJson(m));
			fw.close();
			if(!flag1)
				throw new Exception("No existe id");
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}

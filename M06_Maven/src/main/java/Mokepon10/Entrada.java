package Mokepon10;

import java.util.ArrayList;
import java.util.List;

public class Entrada {
	int id;
	String nom;
	double pes;
	Evolucion evoluciones = new Evolucion();
	List<String> localizaciones = new ArrayList<>();
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public double getPes() {
		return pes;
	}
	public void setPes(double pes) {
		this.pes = pes;
	}
	public Evolucion getEvoluciones() {
		return evoluciones;
	}
	public void setEvoluciones(Evolucion evoluciones) {
		this.evoluciones = evoluciones;
	}
	public List<String> getLocalizaciones() {
		return localizaciones;
	}
	public void setLocalizaciones(List<String> localizaciones) {
		this.localizaciones = localizaciones;
	}
	@Override
	public String toString() {
		return "Entrada [id=" + id + ", nom=" + nom + ", pes=" + pes + ", evoluciones=" + evoluciones
				+ ", localizaciones=" + localizaciones + "]";
	}
	
}

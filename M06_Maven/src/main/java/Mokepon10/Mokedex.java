package Mokepon10;

import java.util.List;

public class Mokedex {
	String nomEntrenador;
	List<Entrada> entradas;
	public String getNomEntrenador() {
		return nomEntrenador;
	}
	public void setNomEntrenador(String nomEntrenador) {
		this.nomEntrenador = nomEntrenador;
	}
	public List<Entrada> getEntradas() {
		return entradas;
	}
	public void setEntradas(List<Entrada> entradas) {
		this.entradas = entradas;
	}
	@Override
	public String toString() {
		return "Mokedex [nomEntrenador=" + nomEntrenador + ", entradas=" + entradas + "]";
	}
	
}

package kk;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class Main extends JFrame {

	private static final long serialVersionUID = -4718224085890371497L;

	static Random rd = new Random();

	static ArrayList<Integer> numeros = new ArrayList<Integer>(
			Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 25, 50, 75, 100));

	static int o = rd.nextInt(100, 999);

	static String op;

	static int estado = 1;

	static JButton init = null;

	public static void main(String[] args) {

		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setSize(500, 750);
		frame.setTitle("Cifras");
		frame.setResizable(false);

		Dimension buttonSize = new Dimension(100, 40);

		JPanel panelPrincipal = new JPanel(new BorderLayout());
		frame.add(panelPrincipal);

		JPanel panelArribaMas = new JPanel(new BorderLayout());
		panelPrincipal.add(panelArribaMas, BorderLayout.NORTH);

		JLabel lbl = new JLabel("OBJETIVO: " + o, SwingConstants.CENTER);

		panelArribaMas.add(lbl);

		JPanel panelArriba = new JPanel(new FlowLayout());
		JButton nuevo = new JButton("Nou enigme");

		JButton limpia = new JButton("Neteja operacions");

		panelArriba.add(nuevo);
		panelArriba.add(limpia);

		panelArribaMas.add(panelArriba, BorderLayout.NORTH);

		JPanel panelDerecha = new JPanel(new GridLayout(6, 1));
		JButton d1 = new JButton("?");
		JButton d2 = new JButton("?");
		JButton d3 = new JButton("?");
		JButton d4 = new JButton("?");
		JButton d5 = new JButton("?");

		d1.setPreferredSize(buttonSize);
		d2.setPreferredSize(buttonSize);
		d3.setPreferredSize(buttonSize);
		d4.setPreferredSize(buttonSize);
		d5.setPreferredSize(buttonSize);

		panelDerecha.add(d1);
		panelDerecha.add(d2);
		panelDerecha.add(d3);
		panelDerecha.add(d4);
		panelDerecha.add(d5);

		d1.setEnabled(false);
		d2.setEnabled(false);
		d3.setEnabled(false);
		d4.setEnabled(false);
		d5.setEnabled(false);

		panelPrincipal.add(panelDerecha, BorderLayout.EAST);

		JPanel panelIzquierda = new JPanel(new GridLayout(6, 1));

		ArrayList<Integer> a = new ArrayList<Integer>();
		for (int i = 0; i < 6; i++) {
			a.add(numeros.get(rd.nextInt(numeros.size() - 1)));
		}

		Collections.sort(a, Collections.reverseOrder());

		JButton i1 = new JButton(a.get(0).toString());
		JButton i2 = new JButton(a.get(1).toString());
		JButton i3 = new JButton(a.get(2).toString());
		JButton i4 = new JButton(a.get(3).toString());
		JButton i5 = new JButton(a.get(4).toString());
		JButton i6 = new JButton(a.get(5).toString());

		i1.setPreferredSize(buttonSize);
		i2.setPreferredSize(buttonSize);
		i3.setPreferredSize(buttonSize);
		i4.setPreferredSize(buttonSize);
		i5.setPreferredSize(buttonSize);
		i6.setPreferredSize(buttonSize);

		panelIzquierda.add(i1);
		panelIzquierda.add(i2);
		panelIzquierda.add(i3);
		panelIzquierda.add(i4);
		panelIzquierda.add(i5);
		panelIzquierda.add(i6);

		panelPrincipal.add(panelIzquierda, BorderLayout.WEST);

		JPanel panelMedio = new JPanel(new GridLayout(5, 1));

		JLabel lbl1 = new JLabel("-", SwingConstants.CENTER);
		JLabel lbl2 = new JLabel("-", SwingConstants.CENTER);
		JLabel lbl3 = new JLabel("-", SwingConstants.CENTER);
		JLabel lbl4 = new JLabel("-", SwingConstants.CENTER);
		JLabel lbl5 = new JLabel("-", SwingConstants.CENTER);

		panelMedio.add(lbl1);
		panelMedio.add(lbl2);
		panelMedio.add(lbl3);
		panelMedio.add(lbl4);
		panelMedio.add(lbl5);

		panelPrincipal.add(panelMedio, BorderLayout.CENTER);

		JPanel panelAbajo = new JPanel(new FlowLayout());

		JButton abajo1 = new JButton("+");
		JButton abajo2 = new JButton("-");
		JButton abajo3 = new JButton("*");
		JButton abajo4 = new JButton("/");

		abajo1.setPreferredSize(buttonSize);
		abajo2.setPreferredSize(buttonSize);
		abajo3.setPreferredSize(buttonSize);
		abajo4.setPreferredSize(buttonSize);

		nuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				i1.setEnabled(true);
				i2.setEnabled(true);
				i3.setEnabled(true);
				i4.setEnabled(true);
				i5.setEnabled(true);
				i6.setEnabled(true);

				d1.setText("?");
				d2.setText("?");
				d3.setText("?");
				d4.setText("?");
				d5.setText("?");

				d1.setEnabled(false);
				d2.setEnabled(false);
				d3.setEnabled(false);
				d4.setEnabled(false);
				d5.setEnabled(false);

				lbl1.setText("-");
				lbl2.setText("-");
				lbl3.setText("-");
				lbl4.setText("-");
				lbl5.setText("-");

				lbl.setText("OBJETIVO: " + o);

				estado = 1;
				op = null;

				o = rd.nextInt(100, 999);
				lbl.setText("OBJETIVO: " + o);

				ArrayList<Integer> a = new ArrayList<Integer>();
				for (int i = 0; i < 6; i++) {
					a.add(numeros.get(rd.nextInt(numeros.size() - 1)));
				}

				Collections.sort(a, Collections.reverseOrder());

				i1.setText(a.get(0).toString());
				i2.setText(a.get(1).toString());
				i3.setText(a.get(2).toString());
				i4.setText(a.get(3).toString());
				i5.setText(a.get(4).toString());
				i6.setText(a.get(5).toString());

				abajo1.setEnabled(true);
				abajo2.setEnabled(true);
				abajo3.setEnabled(true);
				abajo4.setEnabled(true);
			}
		});

		abajo1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (op == null) {
					op = "+";
					JLabel lll = null;
					switch (estado) {
					case 1:
						lll = lbl1;
						break;
					case 2:
						lll = lbl2;
						break;
					case 3:
						lll = lbl3;
						break;
					case 4:
						lll = lbl4;
						break;
					case 5:
						lll = lbl5;
						break;
					}
					lll.setText(lll.getText() + "+");
				}
			}
		});
		abajo2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (op == null) {
					op = "-";
					JLabel lll = null;
					switch (estado) {
					case 1:
						lll = lbl1;
						break;
					case 2:
						lll = lbl2;
						break;
					case 3:
						lll = lbl3;
						break;
					case 4:
						lll = lbl4;
						break;
					case 5:
						lll = lbl5;
						break;
					}
					lll.setText(lll.getText() + "-");
				}
			}
		});
		abajo3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (op == null) {
					op = "*";
					JLabel lll = null;
					switch (estado) {
					case 1:
						lll = lbl1;
						break;
					case 2:
						lll = lbl2;
						break;
					case 3:
						lll = lbl3;
						break;
					case 4:
						lll = lbl4;
						break;
					case 5:
						lll = lbl5;
						break;
					}
					lll.setText(lll.getText() + "*");
				}
			}
		});
		abajo4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (op == null) {
					op = "/";
					JLabel lll = null;
					switch (estado) {
					case 1:
						lll = lbl1;
						break;
					case 2:
						lll = lbl2;
						break;
					case 3:
						lll = lbl3;
						break;
					case 4:
						lll = lbl4;
						break;
					case 5:
						lll = lbl5;
						break;
					}
					lll.setText(lll.getText() + "/");
				}
			}
		});

		panelAbajo.add(abajo1);
		panelAbajo.add(abajo2);
		panelAbajo.add(abajo3);
		panelAbajo.add(abajo4);

		panelPrincipal.add(panelAbajo, BorderLayout.SOUTH);

		i1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JLabel lll = null;
				switch (estado) {
				case 1:
					lll = lbl1;
					break;
				case 2:
					lll = lbl2;
					break;
				case 3:
					lll = lbl3;
					break;
				case 4:
					lll = lbl4;
					break;
				case 5:
					lll = lbl5;
					break;
				}

				if (lll.getText().equals("-") || lll.getText().equals("OPERACION NO VALIDA")) {
					lll.setText(i1.getText());
					i1.setEnabled(false);
					init = i1;
					op=null;
				} else if (!lll.getText().contains("=") && op != null) {
					i1.setEnabled(false);
					int a = Integer.parseInt(lll.getText().substring(0, lll.getText().length() - 1));
					int b = Integer.parseInt(i1.getText());
					int res = 0;
					boolean flag = false;
					switch (op) {
					case "+":
						res = a + b;
						break;
					case "-":
						res = a - b;
						break;
					case "*":
						res = a * b;
						break;
					case "/":
						res = a / b;
						if (a % b != 0)
							flag = true;
						break;
					}

					lll.setText(lll.getText() + i1.getText() + "=" + res);

					if (res <= 0 || flag) {
						lll.setText("OPERACION NO VALIDA");
						init.setEnabled(true);
						i1.setEnabled(true);
					} else {
						if (res == o) {
							lbl.setText("HAS LLEGADO AL OBJETIVO");
							i1.setEnabled(false);
							i2.setEnabled(false);
							i3.setEnabled(false);
							i4.setEnabled(false);
							i5.setEnabled(false);
							i6.setEnabled(false);

							d1.setEnabled(false);
							d2.setEnabled(false);
							d3.setEnabled(false);
							d4.setEnabled(false);
							d5.setEnabled(false);

							abajo1.setEnabled(false);
							abajo2.setEnabled(false);
							abajo3.setEnabled(false);
							abajo4.setEnabled(false);
						} else {
							JButton bf = null;
							switch (estado) {
							case 1:
								bf = d1;
								d1.setEnabled(true);
								break;
							case 2:
								bf = d2;
								d2.setEnabled(true);
								break;
							case 3:
								bf = d3;
								d3.setEnabled(true);
								break;
							case 4:
								bf = d4;
								d4.setEnabled(true);
								break;
							case 5:
								bf = d5;
								d5.setEnabled(false);
								lbl.setText("HAS PERDIDO");
								break;
							}
							bf.setText(Integer.toString(res));
							estado++;
							op = null;
						}
					}
				}
			}
		});
		i2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JLabel lll = null;
				switch (estado) {
				case 1:
					lll = lbl1;
					break;
				case 2:
					lll = lbl2;
					break;
				case 3:
					lll = lbl3;
					break;
				case 4:
					lll = lbl4;
					break;
				case 5:
					lll = lbl5;
					break;
				}

				if (lll.getText().equals("-") || lll.getText().equals("OPERACION NO VALIDA")) {
					lll.setText(i2.getText());
					i2.setEnabled(false);
					init = i2;
					op=null;
				} else if (!lll.getText().contains("=") && op != null) {
					i2.setEnabled(false);
					int a = Integer.parseInt(lll.getText().substring(0, lll.getText().length() - 1));
					int b = Integer.parseInt(i2.getText());
					int res = 0;
					boolean flag = false;
					switch (op) {
					case "+":
						res = a + b;
						break;
					case "-":
						res = a - b;
						break;
					case "*":
						res = a * b;
						break;
					case "/":
						res = a / b;
						if (a % b != 0)
							flag = true;
						break;
					}

					lll.setText(lll.getText() + i2.getText() + "=" + res);
					if (res <= 0 || flag) {
						lll.setText("OPERACION NO VALIDA");
						init.setEnabled(true);
						i2.setEnabled(true);
					} else {
						if (res == o) {
							lbl.setText("HAS LLEGADO AL OBJETIVO");
							i1.setEnabled(false);
							i2.setEnabled(false);
							i3.setEnabled(false);
							i4.setEnabled(false);
							i5.setEnabled(false);
							i6.setEnabled(false);

							d1.setEnabled(false);
							d2.setEnabled(false);
							d3.setEnabled(false);
							d4.setEnabled(false);
							d5.setEnabled(false);

							abajo1.setEnabled(false);
							abajo2.setEnabled(false);
							abajo3.setEnabled(false);
							abajo4.setEnabled(false);
						} else {
							JButton bf = null;
							switch (estado) {
							case 1:
								bf = d1;
								d1.setEnabled(true);
								break;
							case 2:
								bf = d2;
								d2.setEnabled(true);
								break;
							case 3:
								bf = d3;
								d3.setEnabled(true);
								break;
							case 4:
								bf = d4;
								d4.setEnabled(true);
								break;
							case 5:
								bf = d5;
								d5.setEnabled(false);
								lbl.setText("HAS PERDIDO");
								break;
							}
							bf.setText(Integer.toString(res));
							estado++;
							op = null;
						}
					}
				}
			}
		});
		i3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JLabel lll = null;
				switch (estado) {
				case 1:
					lll = lbl1;
					break;
				case 2:
					lll = lbl2;
					break;
				case 3:
					lll = lbl3;
					break;
				case 4:
					lll = lbl4;
					break;
				case 5:
					lll = lbl5;
					break;
				}

				if (lll.getText().equals("-") || lll.getText().equals("OPERACION NO VALIDA")) {
					lll.setText(i3.getText());
					i3.setEnabled(false);
					init = i3;
					op=null;
				} else if (!lll.getText().contains("=") && op != null) {
					i3.setEnabled(false);
					int a = Integer.parseInt(lll.getText().substring(0, lll.getText().length() - 1));
					int b = Integer.parseInt(i3.getText());
					int res = 0;
					boolean flag = false;
					switch (op) {
					case "+":
						res = a + b;
						break;
					case "-":
						res = a - b;
						break;
					case "*":
						res = a * b;
						break;
					case "/":
						res = a / b;
						if (a % b != 0)
							flag = true;
						break;
					}

					lll.setText(lll.getText() + i3.getText() + "=" + res);
					
					if (res <= 0 || flag) {
						lll.setText("OPERACION NO VALIDA");
						init.setEnabled(true);
						i3.setEnabled(true);
					} else {
						if (res == o) {
							lbl.setText("HAS LLEGADO AL OBJETIVO");
							i1.setEnabled(false);
							i2.setEnabled(false);
							i3.setEnabled(false);
							i4.setEnabled(false);
							i5.setEnabled(false);
							i6.setEnabled(false);

							d1.setEnabled(false);
							d2.setEnabled(false);
							d3.setEnabled(false);
							d4.setEnabled(false);
							d5.setEnabled(false);

							abajo1.setEnabled(false);
							abajo2.setEnabled(false);
							abajo3.setEnabled(false);
							abajo4.setEnabled(false);
						} else {
							JButton bf = null;
							switch (estado) {
							case 1:
								bf = d1;
								d1.setEnabled(true);
								break;
							case 2:
								bf = d2;
								d2.setEnabled(true);
								break;
							case 3:
								bf = d3;
								d3.setEnabled(true);
								break;
							case 4:
								bf = d4;
								d4.setEnabled(true);
								break;
							case 5:
								bf = d5;
								d5.setEnabled(false);
								lbl.setText("HAS PERDIDO");
								break;
							}
							bf.setText(Integer.toString(res));
							estado++;
							op = null;
						}
					}
				}
			}
		});
		i4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JLabel lll = null;
				switch (estado) {
				case 1:
					lll = lbl1;
					break;
				case 2:
					lll = lbl2;
					break;
				case 3:
					lll = lbl3;
					break;
				case 4:
					lll = lbl4;
					break;
				case 5:
					lll = lbl5;
					break;
				}

				if (lll.getText().equals("-") || lll.getText().equals("OPERACION NO VALIDA")) {
					lll.setText(i4.getText());
					i4.setEnabled(false);
					init = i4;
					op=null;
				} else if (!lll.getText().contains("=") && op != null) {
					i4.setEnabled(false);
					int a = Integer.parseInt(lll.getText().substring(0, lll.getText().length() - 1));
					int b = Integer.parseInt(i4.getText());
					int res = 0;
					boolean flag = false;
					switch (op) {
					case "+":
						res = a + b;
						break;
					case "-":
						res = a - b;
						break;
					case "*":
						res = a * b;
						break;
					case "/":
						res = a / b;
						if (a % b != 0)
							flag = true;
						break;
					}
					lll.setText(lll.getText() + i4.getText() + "=" + res);
					
					if (res <= 0 || flag) {
						lll.setText("OPERACION NO VALIDA");
						init.setEnabled(true);
						i4.setEnabled(true);
					} else {
						if (res == o) {
							lbl.setText("HAS LLEGADO AL OBJETIVO");
							i1.setEnabled(false);
							i2.setEnabled(false);
							i3.setEnabled(false);
							i4.setEnabled(false);
							i5.setEnabled(false);
							i6.setEnabled(false);

							d1.setEnabled(false);
							d2.setEnabled(false);
							d3.setEnabled(false);
							d4.setEnabled(false);
							d5.setEnabled(false);

							abajo1.setEnabled(false);
							abajo2.setEnabled(false);
							abajo3.setEnabled(false);
							abajo4.setEnabled(false);
						} else {
							JButton bf = null;
							switch (estado) {
							case 1:
								bf = d1;
								d1.setEnabled(true);
								break;
							case 2:
								bf = d2;
								d2.setEnabled(true);
								break;
							case 3:
								bf = d3;
								d3.setEnabled(true);
								break;
							case 4:
								bf = d4;
								d4.setEnabled(true);
								break;
							case 5:
								bf = d5;
								d5.setEnabled(false);
								lbl.setText("HAS PERDIDO");
								break;
							}
							bf.setText(Integer.toString(res));
							estado++;
							op = null;
						}
					}
				}
			}
		});

		i5.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JLabel lll = null;
				switch (estado) {
				case 1:
					lll = lbl1;
					break;
				case 2:
					lll = lbl2;
					break;
				case 3:
					lll = lbl3;
					break;
				case 4:
					lll = lbl4;
					break;
				case 5:
					lll = lbl5;
					break;
				}

				if (lll.getText().equals("-") || lll.getText().equals("OPERACION NO VALIDA")) {
					lll.setText(i5.getText());
					i5.setEnabled(false);
					init = i5;
					op=null;
				} else if (!lll.getText().contains("=") && op != null) {
					i5.setEnabled(false);
					int a = Integer.parseInt(lll.getText().substring(0, lll.getText().length() - 1));
					int b = Integer.parseInt(i5.getText());
					int res = 0;
					boolean flag = false;
					switch (op) {
					case "+":
						res = a + b;
						break;
					case "-":
						res = a - b;
						break;
					case "*":
						res = a * b;
						break;
					case "/":
						res = a / b;
						if (a % b != 0)
							flag = true;
						break;
					}

					lll.setText(lll.getText() + i5.getText() + "=" + res);

					if (res <= 0 || flag) {
						lll.setText("OPERACION NO VALIDA");
						init.setEnabled(true);
						i5.setEnabled(true);
					} else {
						if (res == o) {
							lbl.setText("HAS LLEGADO AL OBJETIVO");

							i1.setEnabled(false);
							i2.setEnabled(false);
							i3.setEnabled(false);
							i4.setEnabled(false);
							i5.setEnabled(false);
							i6.setEnabled(false);

							d1.setEnabled(false);
							d2.setEnabled(false);
							d3.setEnabled(false);
							d4.setEnabled(false);
							d5.setEnabled(false);

							abajo1.setEnabled(false);
							abajo2.setEnabled(false);
							abajo3.setEnabled(false);
							abajo4.setEnabled(false);
						} else {
							JButton bf = null;
							switch (estado) {
							case 1:
								bf = d1;
								d1.setEnabled(true);
								break;
							case 2:
								bf = d2;
								d2.setEnabled(true);
								break;
							case 3:
								bf = d3;
								d3.setEnabled(true);
								break;
							case 4:
								bf = d4;
								d4.setEnabled(true);
								break;
							case 5:
								bf = d5;
								d5.setEnabled(false);
								lbl.setText("HAS PERDIDO");
								break;
							}
							bf.setText(Integer.toString(res));
							estado++;
							op = null;
						}
					}
				}
			}
		});
		i6.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JLabel lll = null;
				switch (estado) {
				case 1:
					lll = lbl1;
					break;
				case 2:
					lll = lbl2;
					break;
				case 3:
					lll = lbl3;
					break;
				case 4:
					lll = lbl4;
					break;
				case 5:
					lll = lbl5;
					break;
				}

				if (lll.getText().equals("-") || lll.getText().equals("OPERACION NO VALIDA")) {
					lll.setText(i6.getText());
					i6.setEnabled(false);
					init = i6;
					op=null;
				} else if (!lll.getText().contains("=") && op != null) {
					i6.setEnabled(false);
					int a = Integer.parseInt(lll.getText().substring(0, lll.getText().length() - 1));
					int b = Integer.parseInt(i6.getText());
					int res = 0;
					boolean flag = false;
					switch (op) {
					case "+":
						res = a + b;
						break;
					case "-":
						res = a - b;
						break;
					case "*":
						res = a * b;
						break;
					case "/":
						res = a / b;
						if (a % b != 0)
							flag = true;
						break;
					}

					lll.setText(lll.getText() + i6.getText() + "=" + res);

					if (res <= 0 || flag) {
						lll.setText("OPERACION NO VALIDA");
						init.setEnabled(true);
						i6.setEnabled(true);
					} else {
						if (res == o) {
							lbl.setText("HAS LLEGADO AL OBJETIVO");

							i1.setEnabled(false);
							i2.setEnabled(false);
							i3.setEnabled(false);
							i4.setEnabled(false);
							i5.setEnabled(false);
							i6.setEnabled(false);

							d1.setEnabled(false);
							d2.setEnabled(false);
							d3.setEnabled(false);
							d4.setEnabled(false);
							d5.setEnabled(false);

							abajo1.setEnabled(false);
							abajo2.setEnabled(false);
							abajo3.setEnabled(false);
							abajo4.setEnabled(false);
						} else {
							JButton bf = null;
							switch (estado) {
							case 1:
								bf = d1;
								d1.setEnabled(true);
								break;
							case 2:
								bf = d2;
								d2.setEnabled(true);
								break;
							case 3:
								bf = d3;
								d3.setEnabled(true);
								break;
							case 4:
								bf = d4;
								d4.setEnabled(true);
								break;
							case 5:
								bf = d5;
								d5.setEnabled(false);
								lbl.setText("HAS PERDIDO");
								break;
							}
							bf.setText(Integer.toString(res));
							estado++;
							op = null;
						}
					}
				}
			}
		});
		limpia.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				i1.setEnabled(true);
				i2.setEnabled(true);
				i3.setEnabled(true);
				i4.setEnabled(true);
				i5.setEnabled(true);
				i6.setEnabled(true);

				d1.setText("?");
				d2.setText("?");
				d3.setText("?");
				d4.setText("?");
				d5.setText("?");

				d1.setEnabled(false);
				d2.setEnabled(false);
				d3.setEnabled(false);
				d4.setEnabled(false);
				d5.setEnabled(false);

				lbl1.setText("-");
				lbl2.setText("-");
				lbl3.setText("-");
				lbl4.setText("-");
				lbl5.setText("-");

				lbl.setText("OBJETIVO: " + o);

				estado = 1;
				op = null;
			}
		});

		d1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JLabel lll = null;
				switch (estado) {
				case 1:
					lll = lbl1;
					break;
				case 2:
					lll = lbl2;
					break;
				case 3:
					lll = lbl3;
					break;
				case 4:
					lll = lbl4;
					break;
				case 5:
					lll = lbl5;
					break;
				}

				if (lll.getText().equals("-") || lll.getText().equals("OPERACION NO VALIDA")) {
					lll.setText(d1.getText());
					d1.setEnabled(false);
					init = d1;
					op=null;
				} else if (!lll.getText().contains("=") && op != null) {
					d1.setEnabled(false);
					int a = Integer.parseInt(lll.getText().substring(0, lll.getText().length() - 1));
					int b = Integer.parseInt(d1.getText());
					int res = 0;
					boolean flag = false;
					switch (op) {
					case "+":
						res = a + b;
						break;
					case "-":
						res = a - b;
						break;
					case "*":
						res = a * b;
						break;
					case "/":
						res = a / b;
						if (a % b != 0)
							flag = true;
						break;
					}

					lll.setText(lll.getText() + d1.getText() + "=" + res);
					if (res <= 0 || flag) {
						lll.setText("OPERACION NO VALIDA");
						init.setEnabled(true);
						d1.setEnabled(true);
					} else {
						if (res == o) {
							lbl.setText("HAS LLEGADO AL OBJETIVO");
							i1.setEnabled(false);
							i2.setEnabled(false);
							i3.setEnabled(false);
							i4.setEnabled(false);
							i5.setEnabled(false);
							i6.setEnabled(false);

							d1.setEnabled(false);
							d2.setEnabled(false);
							d3.setEnabled(false);
							d4.setEnabled(false);
							d5.setEnabled(false);

							abajo1.setEnabled(false);
							abajo2.setEnabled(false);
							abajo3.setEnabled(false);
							abajo4.setEnabled(false);

						} else {
							JButton bf = null;
							switch (estado) {
							case 1:
								bf = d1;
								d1.setEnabled(true);
								break;
							case 2:
								bf = d2;
								d2.setEnabled(true);
								break;
							case 3:
								bf = d3;
								d3.setEnabled(true);
								break;
							case 4:
								bf = d4;
								d4.setEnabled(true);
								break;
							case 5:
								bf = d5;
								d5.setEnabled(false);
								lbl.setText("HAS PERDIDO");
								break;
							}
							bf.setText(Integer.toString(res));
							estado++;
							op = null;
						}
					}
				}
			}
		});
		d2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JLabel lll = null;
				switch (estado) {
				case 1:
					lll = lbl1;
					break;
				case 2:
					lll = lbl2;
					break;
				case 3:
					lll = lbl3;
					break;
				case 4:
					lll = lbl4;
					break;
				case 5:
					lll = lbl5;
					break;
				}

				if (lll.getText().equals("-") || lll.getText().equals("OPERACION NO VALIDA")) {
					lll.setText(d2.getText());
					d2.setEnabled(false);
					init = d2;
					op=null;
				} else if (!lll.getText().contains("=") && op != null) {
					d2.setEnabled(false);
					int a = Integer.parseInt(lll.getText().substring(0, lll.getText().length() - 1));
					int b = Integer.parseInt(d2.getText());
					int res = 0;
					boolean flag = false;
					switch (op) {
					case "+":
						res = a + b;
						break;
					case "-":
						res = a - b;
						break;
					case "*":
						res = a * b;
						break;
					case "/":
						res = a / b;
						if (a % b != 0)
							flag = true;
						break;
					}
					
					lll.setText(lll.getText() + d2.getText() + "=" + res);

					if (res <= 0 || flag) {
						lll.setText("OPERACION NO VALIDA");
						init.setEnabled(true);
						d2.setEnabled(true);
					} else {

						if (res == o) {
							lbl.setText("HAS LLEGADO AL OBJETIVO");
							i1.setEnabled(false);
							i2.setEnabled(false);
							i3.setEnabled(false);
							i4.setEnabled(false);
							i5.setEnabled(false);
							i6.setEnabled(false);

							d1.setEnabled(false);
							d2.setEnabled(false);
							d3.setEnabled(false);
							d4.setEnabled(false);
							d5.setEnabled(false);

							abajo1.setEnabled(false);
							abajo2.setEnabled(false);
							abajo3.setEnabled(false);
							abajo4.setEnabled(false);

						} else {
							JButton bf = null;
							switch (estado) {
							case 1:
								bf = d1;
								d1.setEnabled(true);
								break;
							case 2:
								bf = d2;
								d2.setEnabled(true);
								break;
							case 3:
								bf = d3;
								d3.setEnabled(true);
								break;
							case 4:
								bf = d4;
								d4.setEnabled(true);
								break;
							case 5:
								bf = d5;
								d5.setEnabled(false);
								lbl.setText("HAS PERDIDO");
								break;
							}
							estado++;
							op = null;
							bf.setText(Integer.toString(res));
						}
					}
				}
			}
		});
		d3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JLabel lll = null;
				switch (estado) {
				case 1:
					lll = lbl1;
					break;
				case 2:
					lll = lbl2;
					break;
				case 3:
					lll = lbl3;
					break;
				case 4:
					lll = lbl4;
					break;
				case 5:
					lll = lbl5;
					break;
				}

				if (lll.getText().equals("-") || lll.getText().equals("OPERACION NO VALIDA")) {
					lll.setText(d3.getText());
					d3.setEnabled(false);
					init = d3;
					op=null;
				} else if (!lll.getText().contains("=") && op != null) {
					d3.setEnabled(false);
					int a = Integer.parseInt(lll.getText().substring(0, lll.getText().length() - 1));
					int b = Integer.parseInt(d3.getText());
					int res = 0;
					boolean flag = false;
					switch (op) {
					case "+":
						res = a + b;
						break;
					case "-":
						res = a - b;
						break;
					case "*":
						res = a * b;
						break;
					case "/":
						res = a / b;
						if (a % b != 0)
							flag = true;
						break;
					}

					lll.setText(lll.getText() + d3.getText() + "=" + res);
					
					if (res <= 0 || flag) {
						lll.setText("OPERACION NO VALIDA");
						init.setEnabled(true);
						d3.setEnabled(true);
					} else {

						if (res == o) {
							lbl.setText("HAS LLEGADO AL OBJETIVO");
							i1.setEnabled(false);
							i2.setEnabled(false);
							i3.setEnabled(false);
							i4.setEnabled(false);
							i5.setEnabled(false);
							i6.setEnabled(false);

							d1.setEnabled(false);
							d2.setEnabled(false);
							d3.setEnabled(false);
							d4.setEnabled(false);
							d5.setEnabled(false);

							abajo1.setEnabled(false);
							abajo2.setEnabled(false);
							abajo3.setEnabled(false);
							abajo4.setEnabled(false);

						} else {
							JButton bf = null;
							switch (estado) {
							case 1:
								bf = d1;
								d1.setEnabled(true);
								break;
							case 2:
								bf = d2;
								d2.setEnabled(true);
								break;
							case 3:
								bf = d3;
								d3.setEnabled(true);
								break;
							case 4:
								bf = d4;
								d4.setEnabled(true);
								break;
							case 5:
								bf = d5;
								d5.setEnabled(false);
								lbl.setText("HAS PERDIDO");
								break;
							}
							op = null;
							estado++;
							bf.setText(Integer.toString(res));
						}
					}
				}
			}
		});
		d4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JLabel lll = null;
				switch (estado) {
				case 1:
					lll = lbl1;
					break;
				case 2:
					lll = lbl2;
					break;
				case 3:
					lll = lbl3;
					break;
				case 4:
					lll = lbl4;
					break;
				case 5:
					lll = lbl5;
					break;
				}

				if (lll.getText().equals("-") || lll.getText().equals("OPERACION NO VALIDA")) {
					lll.setText(d4.getText());
					d4.setEnabled(false);
					init = d4;
					op=null;
				} else if (!lll.getText().contains("=") && op != null) {
					d4.setEnabled(false);
					int a = Integer.parseInt(lll.getText().substring(0, lll.getText().length() - 1));
					int b = Integer.parseInt(d4.getText());
					int res = 0;
					boolean flag = false;
					switch (op) {
					case "+":
						res = a + b;
						break;
					case "-":
						res = a - b;
						break;
					case "*":
						res = a * b;
						break;
					case "/":
						res = a / b;
						if (a % b != 0)
							flag = true;
						break;
					}

					lll.setText(lll.getText() + d4.getText() + "=" + res);
					op = null;
					if (res <= 0 || flag) {
						lll.setText("OPERACION NO VALIDA");
						init.setEnabled(true);
						d4.setEnabled(true);
					} else {

						if (res == o) {
							lbl.setText("HAS LLEGADO AL OBJETIVO");
							i1.setEnabled(false);
							i2.setEnabled(false);
							i3.setEnabled(false);
							i4.setEnabled(false);
							i5.setEnabled(false);
							i6.setEnabled(false);

							d1.setEnabled(false);
							d2.setEnabled(false);
							d3.setEnabled(false);
							d4.setEnabled(false);
							d5.setEnabled(false);

							abajo1.setEnabled(false);
							abajo2.setEnabled(false);
							abajo3.setEnabled(false);
							abajo4.setEnabled(false);
						} else {
							JButton bf = null;
							switch (estado) {
							case 1:
								bf = d1;
								d1.setEnabled(true);
								break;
							case 2:
								bf = d2;
								d2.setEnabled(true);
								break;
							case 3:
								bf = d3;
								d3.setEnabled(true);
								break;
							case 4:
								bf = d4;
								d4.setEnabled(true);
								break;
							case 5:
								bf = d5;
								d5.setEnabled(false);
								lbl.setText("HAS PERDIDO");
								break;
							}
							op = null;
							estado++;
							bf.setText(Integer.toString(res));
						}

					}
				}
			}
		});
		frame.setVisible(true);
	}
}

package impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import prob.Producte;
import prob.ProducteLot;
import prob.ProgramException;
import prob.iBotiga;

import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Botiga implements iBotiga<Producte>{

	private String nomBotiga;
	private Set<Producte> productos = new HashSet<Producte>();
	
	public Botiga (){
		super();
	}
	
	public Botiga(String string) {
		nomBotiga=string;
	}

	@Override
	public void afegirProducte(Producte producte) throws ProgramException {
		if(producte==null)
			throw new IllegalArgumentException();
		if(productos.contains(producte))
			throw new ProgramException("YA ESTA EN LA LISTA COJONE");
	}

	public List<Producte> llista(){
		List<Producte> a = new ArrayList<Producte>();
		a.addAll(this.productos);
		return a;
	}

	@Override
	public void setNomBotiga(String nomBotiga) {
		this.nomBotiga=nomBotiga;
	}

	@Override
	public int getStock(Producte producte) throws ProgramException {
		if(producte==null)
			throw new ProgramException("NULO COÑOOOOOOOOOOOOOOOO");
		return producte.stock();
	}
}
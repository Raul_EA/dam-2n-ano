package prob;

public abstract class  Producte implements Comparable<Producte>{
	
	private static int comptador = 0;
	protected int minim;
	protected String nom;
	protected int reposicio;
	protected int idProducte;
	
	public Producte(String nom){
		comptador++;
		idProducte=comptador;
		if(nom==null) {
			throw new IllegalArgumentException();
		}else {
			this.nom=nom;
		}
	}
	
	public Producte(String nom, int minim, int reposicio) {
		comptador++;
		idProducte=comptador;
		if(nom==null || reposicio <0 || minim<0) {
			throw new IllegalArgumentException();
		}else {
			this.nom=nom;
			this.minim=minim;
			this.reposicio=reposicio;
		}
	}


    @Override
    public boolean equals(Object obj) {
    	if(!(obj instanceof Producte))
    		return false;
    	else {
    		Producte a = (Producte) obj;
        	if(this.nom.equals(a.nom))
        		return true;
    	}
    	return false;
    }

    public int getMinim() {
		return minim;
	}
    
    public String getNom() {
		return nom;
	}

	public int getReposicio() {
		return reposicio;
	}


	@Override
    public int hashCode() {
        return this.nom.hashCode();
    }


	public void setMinim(int minim) {
		this.minim = minim;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public void setReposicio(int reposicio) {
		this.reposicio = reposicio;
	}

	public abstract int stock ();
	
	@Override
	public String toString() {
		return ("[" + idProducte + "]: "+ nom + "(min: " + minim + ", rep: " + reposicio + ")");
	}

	public int getIdProducte() {
		return idProducte;
	}
	
}

package prob;

import java.util.Map;

public interface iBotiga<P> {
	
	/**
	 * Li dona un nom a la botiga
	 * @param nomBotiga   .. és el nom de la botiga
	 * @throws IllegalArgumentException si el nomBotiga és null.
	 */
	
	void setNomBotiga(String nomBotiga);
	
	/**
	 * Afegeix un producte a la botiga.
	 * @param producte .. el producte que afegeix
	 * @throws IllegalArgumentException si producte és null.
	 * @throws ProgramException si el producte ja existeix
	 */
	void afegirProducte(P producte) throws ProgramException;
	
	public int getStock(P producte) throws ProgramException;

}

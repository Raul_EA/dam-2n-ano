package prob;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public class ProducteLot extends Producte {

	private Map<LocalDate, Integer> lots = new HashMap<LocalDate, Integer>();

	public void setLots(Map<LocalDate, Integer> lots) {
		this.lots = lots;
	}

	public Map<LocalDate, Integer> getLots() {
		return lots;
	}

	public ProducteLot(String nom) {
		super(nom);

	}

	public ProducteLot(String nom, int min, int repo) {
		super(nom, min, repo);
	}

	public void afegirLot(LocalDate date, int quantitat) throws ProgramException {
		if (date == null)
			throw new ProgramException("NO SE PUEDEN NULOS PUTOOOO");
		if (quantitat < 0)
			throw new IllegalArgumentException();
		if (!lots.containsKey(date))
			this.lots.put(date, quantitat);
		else {
			this.lots.put(date, this.lots.get(date) + quantitat);
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return (super.equals(obj));
	}

	@Override
	public int compareTo(Producte that) {
		if (this.idProducte > that.idProducte)
			return 1;
		else
			return -1;
	}

	@Override
	public int stock() {
		int b = 0;
		for (int e : lots.values()) {
			b += e;
		}
		return b;
	}

	@Override
	public String toString() {
		return "ProducteLot [lots=" + lots + "]";
	}

}

package testA;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import impl.Subhasta;
import prob.Client;
import prob.Oferta;

public class SubhastaTestExtra extends SubhastaTest02 {
    
    @Test
    void testOfertaNoAcceptadaBannejat() {
        Subhasta a = new Subhasta(10);
        Client b1 = new Client("Anacleto");
        a.bannejar(b1);
        try {
            a.oferir(new Oferta(b1, 5));
            fail("Client Bannejat no pot fer ofertes");
        } catch (IllegalStateException e) {}
    }
    
    @Test
    void testLlegirSoloAcceptades() {
        Subhasta a = new Subhasta(10);
        Client b1 = new Client("Anacleto");
        Client b2 = new Client("Basilio");
        assertTrue(a.isAcceptable(new Oferta(b1, 10)));
        assertNull(a.getMillorOferta(b1));
        a.oferir(new Oferta(b1, 15));
        assertTrue(a.isAcceptable(new Oferta(b2, 20)));
        assertNull(a.getMillorOferta(b2));
    }
    
    @Test
    void testAcceptadesDesdeBannejat() {
        Subhasta a = new Subhasta(10);
        Client b1 = new Client("Anacleto");
        a.bannejar(b1);

        assertTrue(a.isAcceptable(new Oferta(b1, 10)));
        assertTrue(a.isAcceptable(new Oferta(b1, 15)));
        assertFalse(a.isAcceptable(new Oferta(b1, 5)));
    }    
}

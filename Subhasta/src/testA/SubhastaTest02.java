package testA;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import impl.Subhasta;
import prob.Client;
import prob.Oferta;

public class SubhastaTest02 {

    
    @BeforeAll
    static void beforeAll() {
    }
    
    // testing creació d'objectes
    
    @Test
    void testEmpty() {
        Subhasta a = new Subhasta(10);        
        assertNull(a.getMillorOferta());
        assertEquals(10, a.getPreuInicial());
    }
        
    @Test
    void testNewOferta() {        
        Client b1 = new Client("Anacleto");
        assertEquals("Anacleto", b1.getNom());
        Oferta of = new Oferta(b1, 15);
        assertEquals(b1, of.getClient());
        assertEquals(15, of.getOferta());
    }

    @Test
    void testNoOf() {
        Subhasta a = new Subhasta(10);        
        assertNull(a.getMillorOferta());
        Client b1 = new Client("Anacleto");
        assertNull(a.getMillorOferta(b1));
    }

    // testing ofertes
    
    @Test
    void testAcceptable() {
        Subhasta a = new Subhasta(10);
        Client b = new Client("Anacleto");        
        assertFalse(a.isAcceptable(new Oferta(b, 9)));
        assertTrue(a.isAcceptable(new Oferta(b, 10)));
        assertTrue(a.isAcceptable(new Oferta(b, 11)));
        a.oferir(new Oferta(b, 15));
        assertFalse(a.isAcceptable(new Oferta(b, 14)));
        assertFalse(a.isAcceptable(new Oferta(b, 15)));
        assertTrue(a.isAcceptable(new Oferta(b, 16)));
    }

    @Test
    void test1Oferta() {
        Subhasta a = new Subhasta(10);
        Client b1 = new Client("Anacleto");
        assertNotNull(b1);
        Oferta of = new Oferta(b1, 15);
        assertTrue(a.oferir(of));
        assertEquals(a.getMillorOferta(), of);
        assertEquals(a.getMillorOferta(b1), of);
    }
    
    @Test
    void testOfertaInsuficient() {
        Subhasta a = new Subhasta(10);
        Client b1 = new Client("Anacleto");
        assertNotNull(b1);
        assertFalse(a.oferir(new Oferta(b1, 9)));
        assertNull(a.getMillorOferta());
    }
    
    @Test
    void testOfertaIgual() {
        Subhasta a = new Subhasta(10);
        Client b1 = new Client("Anacleto");
        assertNotNull(b1);
        assertTrue(a.oferir(new Oferta(b1, 10)));
        assertNotNull(a.getMillorOferta());
    }

    @Test
    void testOfertaMillorada() {
        Subhasta a = new Subhasta(10);
        Client b1 = new Client("Anacleto");
        Client b2 = new Client("Basilio");
        Oferta bid1 = new Oferta(b1, 15);
        Oferta bid2 = new Oferta(b2, 25);
        assertTrue(a.oferir(bid1));
        assertTrue(a.oferir(bid2));
        assertEquals(a.getMillorOferta(), bid2);
        assertEquals(a.getMillorOferta(b1), bid1);
        assertEquals(a.getMillorOferta(b2), bid2);
    }
    
    @Test
    void testPitjorOferta() {   	
        Subhasta a = new Subhasta(10);
        Client b1 = new Client("Anacleto");
        Client b2 = new Client("Basilio");
        Oferta of1 = new Oferta(b1, 25);
        Oferta of2 = new Oferta(b2, 15);
        assertTrue(a.oferir(of1));
        assertFalse(a.oferir(of2));
        assertEquals(a.getMillorOferta(), of1);
        assertEquals(a.getMillorOferta(b1), of1);
        assertNull(a.getMillorOferta(b2));
    }
    
    @Test
    void testMateixaOferta() {
        Subhasta a = new Subhasta(10);
        Client b1 = new Client("Anacleto");
        Client b2 = new Client("Basilio");
        Oferta of1 = new Oferta(b1, 25);
        Oferta of2 = new Oferta(b2, 25);
        assertTrue(a.oferir(of1));
        assertFalse(a.oferir(of2));
        assertEquals(a.getMillorOferta(), of1);
        assertEquals(a.getMillorOferta(b1), of1);
        assertNull(a.getMillorOferta(b2));
    }
    
    @Test
    void testMultiplesOfertes() {
        Subhasta a = new Subhasta(10);
        Client b1 = new Client("Anacleto");
        Client b2 = new Client("Basilio");
        Client b3 = new Client("Claudio");
        Oferta of1 = new Oferta(b1, 10);
        assertTrue(a.oferir(of1));
        assertEquals(of1, a.getMillorOferta());
        assertEquals(of1, a.getMillorOferta(b1));
        Oferta of2 = new Oferta(b2, 20);
        assertTrue(a.oferir(of2));
        assertEquals(of2, a.getMillorOferta());
        assertEquals(of2, a.getMillorOferta(b2));
        Oferta of3 = new Oferta(b3, 30);
        assertTrue(a.oferir(of3));
        assertEquals(of3, a.getMillorOferta());
        assertEquals(of3, a.getMillorOferta(b3));
        Oferta bid4 = new Oferta(b1, 40);
        assertTrue(a.oferir(bid4));
        assertEquals(bid4, a.getMillorOferta());
        assertEquals(bid4, a.getMillorOferta(b1));
        Oferta bid5 = new Oferta(b2, 50);
        assertTrue(a.oferir(bid5));
        assertEquals(bid5, a.getMillorOferta());
        assertEquals(bid5, a.getMillorOferta(b2));
    }
    
    // testing bannejats
    
    @Test
    void testIsBannejat() {
        Subhasta a = new Subhasta(10);
        Client b = new Client("Anacleto");
        assertFalse(a.isBannejat(b));
        assertTrue(a.bannejar(b));
        assertTrue(a.isBannejat(b));
        assertFalse(a.bannejar(b));
        assertTrue(a.isBannejat(b));
    }
    
    @Test
    void testClientBannejat() {
        Subhasta a = new Subhasta(10);
        Client b1 = new Client("Anacleto");
        Client b2 = new Client("Basilio");
        Oferta of1 = new Oferta(b1, 15);
        Oferta of2 = new Oferta(b2, 25);
        assertTrue(a.oferir(of1));
        assertTrue(a.oferir(of2));
        assertTrue(a.bannejar(b2));
        assertEquals(a.getMillorOferta(), of1);
        assertEquals(a.getMillorOferta(b1), of1);
        try {
            a.getMillorOferta(b2);
            fail("Client bannejat should throw IllegalStateException");
        } catch (IllegalStateException e) {} 
    }
    
    @Test
    void testClientBannejat2() {
        Subhasta a = new Subhasta(10);
        Client b1 = new Client("Anacleto");
        Client b2 = new Client("Basilio");
        Oferta of1 = new Oferta(b1, 15);
        Oferta of2 = new Oferta(b2, 25);
        assertTrue(a.oferir(of1));
        assertTrue(a.oferir(of2));
        assertTrue(a.bannejar(b1));
        assertEquals(a.getMillorOferta(), of2);
        try {
            a.getMillorOferta(b1);
            fail("Client bennejat should throw IllegalStateException");
        } catch (IllegalStateException e) {}
        assertEquals(a.getMillorOferta(b2), of2);
    }
    
    @Test
    void testBannejatTotal() {
        Subhasta a = new Subhasta(10);
        Client b1 = new Client("Anacleto");
        Client b2 = new Client("Basilio");
        Oferta of1 = new Oferta(b1, 15);
        Oferta of2 = new Oferta(b1, 25);
        assertTrue(a.oferir(of1));
        assertTrue(a.oferir(of2));
        assertTrue(a.bannejar(b1));
        assertTrue(a.bannejar(b2));
        assertNull(a.getMillorOferta());
        try {
            a.getMillorOferta(b1);
            fail("Client bannejat should throw IllegalStateException");
        } catch (IllegalStateException e) {}
        try {
            a.getMillorOferta(b2);
            fail("Client Bannejat should throw IllegalStateException");
        } catch (IllegalStateException e) {}
    }
    
    @Test
    void testClientBannejat3() {
        Subhasta a = new Subhasta(10);
        Client b1 = new Client("Anacleto");
        Oferta of1 = new Oferta(b1, 15);
        assertTrue(a.oferir(of1));
        assertTrue(a.bannejar(b1));
        try {
            a.oferir(of1);
            fail("Client Banneat should throw IllegalStateException");
        } catch (IllegalStateException e) {}   
        assertFalse(a.bannejar(b1));
    }
}

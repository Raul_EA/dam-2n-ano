package testA;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import impl.Subhasta;
import prob.Client;
import prob.Oferta;

public class SubhastaTest01 {

    
    @BeforeAll
    static void beforeAll() {
    }
    
    // testing creació d'objectes
    
    @Test
    void testEmpty() {
        Subhasta a = new Subhasta(10);        
        assertNull(a.getMillorOferta());
        assertEquals(10, a.getPreuInicial());
    }
        
    @Test
    void testCompareOferta() {        
        Client b1 = new Client("Anacleto");
        Oferta of = new Oferta(b1, 15);
        Oferta of2 = new Oferta(b1, 20);
        assertEquals(0, of.compareTo(of));
        assertEquals(true, of.compareTo(of2)<0);
        assertEquals(false, of2.compareTo(of)<0);
        
    }

    @Test
    void testCompareSubhasta01() {        
        Client b1 = new Client("Anacleto");
        Subhasta s1 = new Subhasta(10);
        Subhasta s2 = new Subhasta(10);
        assertEquals(0, s1.compareTo(s2));
    }
    @Test        
    void testCompareSubhasta02() {        
    	System.out.println("KKKKKKKKKKKKKKK");
    	Client b1 = new Client("Anacleto");
        Subhasta s1 = new Subhasta(10);
        Subhasta s2 = new Subhasta(10);    
        Oferta of1 = new Oferta(b1, 15);
        Oferta of2 = new Oferta(b1, 20);
        s1.oferir(of1);
        assertEquals(true, s1.compareTo(s2)>0);
        s2.oferir(of1);
        
//        System.out.println("KKK" + s1.compareTo(s2));
        assertEquals(0, s1.compareTo(s2));        
    }
    
    @Test
    void testCompareSubhasta03() {        
        Client b1 = new Client("Anacleto");
        Subhasta s1 = new Subhasta(10);
        Subhasta s2 = new Subhasta(10);    
        Oferta of1 = new Oferta(b1, 15);
        Oferta of2 = new Oferta(b1, 20);
        s1.oferir(of1);
        assertEquals(true, s1.compareTo(s2)>0);
        s2.oferir(of2);
        assertEquals(true, s1.compareTo(s2)<0);        
    }
}

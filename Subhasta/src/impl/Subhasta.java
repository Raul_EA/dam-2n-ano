package impl;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import prob.iSubhasta;
import prob.Oferta;
import prob.Client;

public class Subhasta implements iSubhasta, Comparable {
	
	private Set<Client> baneados = new HashSet<Client>();
	private Set<Oferta> ofertas = new HashSet<Oferta>();
	private double precioInicial;
	
    public Subhasta(double preuInicial) {
    	this.precioInicial=preuInicial;
    }
    
    @Override
    public double getPreuInicial() {
    	return this.precioInicial;
    }

    @Override
    public boolean oferir(Oferta of) {
    	if(this.baneados.contains(of.getClient()))
    		throw new IllegalStateException();
    	if(this.isAcceptable(of)) {
    		this.ofertas.add(of);
    		return true;
    	}
    	return false;
    }
    
    @Override
    public boolean isAcceptable(Oferta of) {
    	if( of.getOferta()<this.getPreuInicial() && this.ofertas.size()==0)
    		return false;
    	if(this.ofertas.size()==0 ) {
    		return true;
    	}else {
    		if(this.getMillorOferta().getOferta()<of.getOferta())
    			return true;
    	}
    	return false;
    }
    
    @Override
    public Oferta getMillorOferta() {
    	Oferta ofertaMillor=null;
    	for(Oferta o : this.ofertas) {
    		if(ofertaMillor==null) {
    			ofertaMillor=o;
    		}else {
    			if(o.getOferta()>ofertaMillor.getOferta() )
        			ofertaMillor=o;
    		}
    	}
    	return ofertaMillor;
    }
    
    @Override
    public Oferta getMillorOferta(Client c) {
    	Oferta mejor=null;
    	if(c==null || this.baneados.contains(c))
    		throw new IllegalStateException();
    	
    	for(Oferta o : this.ofertas) {
    		if(o.getClient()==c) {
    			if(mejor==null) {
    				mejor=o;
    			}else {
    				if(o.getOferta()>mejor.getOferta())
    					mejor=o;
    			}
    		}
    			
    	}
    	return mejor;
    }

    @Override
    public boolean bannejar(Client clientBannejat) {
    	if(this.isBannejat(clientBannejat) || clientBannejat==null)
    		return false;
    	
    	Set<Oferta> nuevasOfertas = new HashSet<Oferta>();
    	
    	for(Oferta o : this.ofertas) {
    		if(o.getClient()!=clientBannejat)
    			nuevasOfertas.add(o);
    	}
    	this.ofertas=nuevasOfertas;
    	this.baneados.add(clientBannejat);
    	
    	return true;
    }
    
    @Override
    public boolean isBannejat(Client c) {
        if(this.baneados.contains(c))
        	return true;
        return false;
    }

    

	@Override
	public String toString() {
		return "Subhasta [baneados=" + baneados + ", ofertas=" + ofertas + ", precioInicial=" + precioInicial + "]";
	}

	@Override
	public int compareTo(Object o) {
		if(o==null)
			return 1;
		
		if(o instanceof Subhasta) {
			Subhasta sub = (Subhasta) o;

			if(this.getMillorOferta()==null && sub.getMillorOferta()==null)
				return 0;
			return this.getMillorOferta().compareTo(sub.getMillorOferta())	;		
		}
		return 0;
	}
}

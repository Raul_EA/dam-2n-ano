package prob;

/**
 * an auction.
 *
 */
public interface iSubhasta {

    /**
     * Preu inicial
     * @return
     */
    double getPreuInicial();

    /**
     * Millor oferta de la subhasta. Null si no és vàlida
     * @return
     */
    Oferta getMillorOferta();
    
    /**
     * Millor oferta d'un client. Null si no te ofertes acceptades
     * Throws IllegalStateException si el client està bannejat
     * @return
     */
    Oferta getMillorOferta(Client b);

    /**
     * Retorna true si la oferta és acceptable:
     * 1) és la primera i és igual o superior al preu de partida
     * 2) és superior a l'actual millor
     * @param b
     * @return
     */
    boolean isAcceptable(Oferta b);

    /**
     * Añade una oferta a la subhasta si l'orferta és acceptable
	 * Ofertes clients bannejats no són acceptades i genrea throw IllegalStateException.
     * Retorna true si l'oferta és acceptada
     * @param b
     * @return
     */
    boolean oferir(Oferta b);
        
    /**
     * Banneja un client, inclús si no ha fet cap oferta.
     * Caldrà recalcular la millor oferta.
	 * Retorna true si el client és bannejat, false si ja està bannejat 
     * @param b
     * @return
     */
    boolean bannejar(Client b);

    /**
     * Returns true si el client està bannejat
     * @param b
     * @return
     */
    boolean isBannejat(Client b);
}

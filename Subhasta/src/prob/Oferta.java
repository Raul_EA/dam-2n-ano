package prob;

public class Oferta implements Comparable{

    private Client client;
    private double oferta;
    
    public Oferta(Client client, double oferta) {
        this.client = client;
        this.oferta = oferta;
    }
    
    public Client getClient() {
        return client;
    }

    public double getOferta() {
        return oferta;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(oferta);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((client == null) ? 0 : client.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Oferta other = (Oferta) obj;
        if (Double.doubleToLongBits(oferta) != Double.doubleToLongBits(other.oferta))
            return false;
        if (client == null) {
            if (other.client != null)
                return false;
        } else if (!client.equals(other.client))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Oferta [client=" + client + ", oferta=" + oferta + "]";
    }

	@Override
	public int compareTo(Object o) {
		if(o==null)
			return 1;
		if(o instanceof Oferta) {
			Oferta of = (Oferta) o;
			if(this.oferta>of.getOferta())
				return 1;
			else if (this.oferta<of.getOferta())
				return -1;
			else
				return 0;
		}
		return 0;
	}
}

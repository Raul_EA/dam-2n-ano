package run;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import impl.Subhasta;
import prob.Client;
import prob.Oferta;

public class Main {
    
 
    public static void main(String[] args) {      
    	
        Subhasta a = new Subhasta(10);
        Client b1 = new Client("Anacleto");
        a.bannejar(b1);
        Oferta of1 = new Oferta(b1, 15);
        assertTrue(a.oferir(of1));
        try {
            a.oferir(new Oferta(b1, 5));
            fail("Client Bannejat no pot fer ofertes");
        } catch (IllegalStateException e) {}

/*    	
    	
    	Subhasta a = new Subhasta(0);
    	Client b1 = new Client ("Anna");
    	Client b2 = new Client("Berta");
    	Client b3 = new Client("Carla");
    	
    	Oferta of1 = new Oferta(b1,10);
    	a.oferir(of1);
    	Oferta of2 = new Oferta(b2,15);
    	a.oferir(of2);
    	Oferta of3 = new Oferta(b1, 12);
    	a.oferir(of3);
    	a.oferir(of2);
    	a.bannejar(b3);
    	a.bannejar(b2);
    	a.bannejar(b2);
    	a.bannejar(b1);
    	
    	
  */  	
    	System.out.println(a);
    }
    

    
}

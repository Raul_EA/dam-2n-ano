package run;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import impl.Botiga;
import impl.Comanda;
import prob.Producte;
import prob.ProducteLot;
import prob.ProducteNormal;
import prob.ProgramException;

public class Main {

	public static void main(String[] args) {

		try {
			Botiga pajachovi = new Botiga("Pajachovi");

			ProducteLot p1 = new ProducteLot("Pan",50,70);
			p1.afegirLot(LocalDate.now().plusWeeks(1), 20);
			p1.afegirLot(LocalDate.now().plusWeeks(1), 10);
			p1.afegirLot(LocalDate.now().plusWeeks(2), 10);
			p1.afegirLot(LocalDate.now().plusWeeks(-1), 10);

			ProducteLot p2 = new ProducteLot("Jamon",20,40);
			p2.afegirLot(LocalDate.now().plusWeeks(1), 2);
			p2.afegirLot(LocalDate.now().plusWeeks(1), 3);
			
			ProducteLot p3 = new ProducteLot("Chorizo",60,80);
			p3.afegirLot(LocalDate.now().plusWeeks(1), 12);
			p3.afegirLot(LocalDate.now().plusWeeks(1), 5);
			p3.afegirLot(LocalDate.now().plusWeeks(-10), 4);

			ProducteNormal p4 = new ProducteNormal("Vino",40,90,20,50);
			
			pajachovi.afegirProducte(p1);
			pajachovi.afegirProducte(p2);
			pajachovi.afegirProducte(p3);
			pajachovi.afegirProducte(p4);
			System.out.println(pajachovi);

			//generar comanda
			Comanda<Producte> laMevaComanda = new Comanda<Producte>();
			laMevaComanda.afegirLinia(p1, 10);
//			laMevaComanda.afegirLinia(p1, 5);		//provocarà excepció
//			laMevaComanda.trereLinia(p4);			//provocarà excepció
			laMevaComanda.afegirLinia(p2, 20);
			laMevaComanda.afegirLinia(p3, 30);
			laMevaComanda.afegirLinia(p4, 40);
			System.out.println(laMevaComanda);
			laMevaComanda.trereLinia(p1);
			System.out.println(laMevaComanda);
			laMevaComanda.afegirLinia(p1, 6);
			System.out.println(laMevaComanda);

		}
		catch (Exception e) {
			System.out.println(e.getStackTrace());
			System.err.println(e);
		}
	}	
}

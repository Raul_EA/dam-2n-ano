package impl;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import prob.ComandaEstat;
import prob.Producte;
import prob.ProgramException;
import prob.iComanda;

public class Comanda<P> implements iComanda<P>{

	static int comptador = 0;
	
	int numComanda;
	LocalDate data;
	Map<P,Integer> linies;
	ComandaEstat estat;
	
	public Comanda() {
		numComanda = ++comptador;
		data = LocalDate.now();
		estat = ComandaEstat.NOVA;
		linies = new TreeMap<P,Integer>();
	}
	
	@Override
	public void afegirLinia(Object p, int q) throws ProgramException {
		if(p==null || q<=0 ) {
			throw new IllegalArgumentException();
		}else if(estat != ComandaEstat.NOVA) {
			throw new ProgramException("No esta en comando nuevo");
		}else if (linies.containsKey(p)){
			throw new ProgramException("Ya esta contenido");
		}
		linies.put((P) p, q);
	}

	@Override
	public ComandaEstat getEstat() {
		return estat;
	}

	@Override
	public LocalDate getData() {
		return data;
	}

	@Override
	public int getNumComanda() {
		return numComanda;
	}

	@Override
	public void setEstat(ComandaEstat nouEstat) throws ProgramException {
		if(this.estat==ComandaEstat.NOVA && nouEstat==ComandaEstat.ENVIADA){
			this.estat=nouEstat;
		}else if(this.estat==ComandaEstat.ENVIADA && nouEstat==ComandaEstat.REBUDA) {
			this.estat=nouEstat;
		}else if(this.estat==ComandaEstat.REBUDA && nouEstat==ComandaEstat.PROCESSADA) {
			this.estat=nouEstat;
		}else {
			throw new ProgramException("Cambio no valido, puto");
		}
	}

	@Override
	public Map getLinies() {
		return linies;
	}

	@Override
	public void trereLinia(Object p) throws ProgramException {
		if(p==null ) {
			throw new IllegalArgumentException();
		}else if (!linies.containsKey(p)){
			throw new ProgramException("Ya esta contenido");
		}else if(estat != ComandaEstat.NOVA) {
			throw new ProgramException("No esta en comando nuevo");
		}
		linies.remove(p);
	}

	@Override
	public String toString()  {
		String cad="Comanda: "+this.numComanda+"\nData: "+this.data+"\nEstat: "+this.estat;
		for(var i : this.linies.entrySet()) {
			Producte a = (Producte) i.getKey();
			cad+="\n"+a.getNom()+": "+i.getValue();
		}
		return cad;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}
}

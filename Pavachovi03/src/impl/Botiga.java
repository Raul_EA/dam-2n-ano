package impl;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import prob.Producte;
import prob.ProducteLot;
import prob.ProducteNormal;
import prob.ProgramException;
import prob.iBotiga;
import prob.iComanda;

import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Botiga<P extends Producte> implements iBotiga<P>, Iterable<P>{

	private String nomBotiga;
	private Set<P> cataleg;
	
	public Botiga (){
		cataleg = new TreeSet();
	}
	
	public Botiga(String string) {
		this();
		setNomBotiga(string);
	}

	@Override
	public void afegirProducte(P producte) throws ProgramException {
		if (cataleg.contains(producte))
			throw new ProgramException ("Producte ja existeix");
		cataleg.add(producte);
	}

	@Override
	public String toString() {
		String cadena = "Botiga " + nomBotiga + "\nCataleg:";
		for (Producte p:cataleg) 
			cadena+="\n" + p;
		cadena+= "]";
		return cadena;
	}

	public List<Producte> llista(){
		return List.copyOf(cataleg);
	}

	@Override
	public void setNomBotiga(String nomBotiga) {
		if (nomBotiga == null)
			throw new IllegalArgumentException("Nom Botiga no pot ser null");
		this.nomBotiga = nomBotiga;
	}

	@Override
	public int getStock(Producte producte) throws ProgramException {
		if (cataleg.contains(producte))
			return producte.stock();
		else
			throw new ProgramException ("Producte no existeix");
	}

	@Override
	public Map<P, Integer> getInventari() {
		Map<P, Integer>  inv = new TreeMap<P, Integer>();
		for (P p: cataleg) 
			inv.put(p, p.stock());
	
		return inv;
	}

	@Override
	public String veureInventari() {
		String cadena = "Inventari";
		Map<P, Integer> inv = getInventari();
		for (P p : inv.keySet()) 
			cadena = cadena + "\n" + p.getIdProducte() + "\t" + p.getNom() + ": " + inv.get(p);
		return cadena;
	}

	@Override
	public Map<String, Integer> getCaducats() {
		Map<String, Integer>  inv = new TreeMap<String, Integer>();
		for (Producte p: cataleg) 
			if (p instanceof ProducteLot) {
				int caducats = 0;
				for (LocalDate d : ((ProducteLot) p).getLots().keySet())
					if (d.isBefore(LocalDate.now()))
						caducats += ((ProducteLot) p).getLots().get(d);
				if (caducats >0)
					inv.put(p.getNom(), caducats);
			}
		
		return inv;
	}

	@Override
	public void eliminarCaducats() {
		int n=0;
		for (Producte p: cataleg) 
			if (p instanceof ProducteLot) {
				((ProducteLot) p).getLots().entrySet().removeIf(
						entry -> (entry.getKey().isBefore(LocalDate.now())));
			}	
	}

	@Override
	//quan iterem la botiga serà pel catàleg dels seus productes
	public Iterator<P> iterator() {
		return cataleg.iterator();
	}	
}
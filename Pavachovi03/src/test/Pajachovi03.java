package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import impl.Botiga;
import impl.Comanda;
import prob.Producte;
import prob.ProducteLot;
import prob.ProducteNormal;

class Pajachovi03 {

	@Test
	void pajachovi03Comanda() {
		try {
			Botiga pajachovi = new Botiga("Pajachovi");
			LocalDate f = LocalDate.parse("2023-11-09");
			
			ProducteLot p1 = new ProducteLot("Pan", 0, 10);
			p1.afegirLot(f.plusWeeks(1), 20);
			p1.afegirLot(f.plusWeeks(1), 10);
			p1.afegirLot(f.plusWeeks(2), 10);
			p1.afegirLot(f.plusWeeks(-1), 10);

			ProducteLot p2 = new ProducteLot("Jamon", 2, 4);
			p2.afegirLot(f.plusWeeks(1), 2);
			p2.afegirLot(f.plusWeeks(1), 3);

			ProducteLot p3 = new ProducteLot("Chorizo", 2, 2);
			p3.afegirLot(f.plusWeeks(1), 12);
			p3.afegirLot(f.plusWeeks(1), 5);
			p3.afegirLot(f.plusWeeks(-10), 4);

			ProducteNormal p4 = new ProducteNormal("Vino", 0, 10, 20, 50);

			pajachovi.afegirProducte(p1);
			pajachovi.afegirProducte(p2);
			pajachovi.afegirProducte(p3);
			pajachovi.afegirProducte(p4);
			
			//generar comanda
			Comanda<Producte> laMevaComanda = new Comanda<Producte>();
			laMevaComanda.afegirLinia(p1, 10);
		//	laMevaComanda.afegirLinia(p1, 5);		//provocarà excepció
		//	laMevaComanda.trereLinia(p4);			//provocarà excepció
			laMevaComanda.afegirLinia(p2, 20);
			laMevaComanda.afegirLinia(p3, 30);
			laMevaComanda.afegirLinia(p4, 40);
			laMevaComanda.setData(LocalDate.parse("2023-11-08"));
			
			String cadena = "Comanda: 1\nData: 2023-11-08\nEstat: NOVA\nPan: 10\nJamon: 20\nChorizo: 30\nVino: 40";
			assertEquals(cadena, laMevaComanda.toString());
			laMevaComanda.trereLinia(p1);
			cadena = "Comanda: 1\nData: 2023-11-08\nEstat: NOVA\nJamon: 20\nChorizo: 30\nVino: 40";
			assertEquals(cadena, laMevaComanda.toString());
			laMevaComanda.afegirLinia(p1, 6);
			cadena = "Comanda: 1\nData: 2023-11-08\nEstat: NOVA\nPan: 6\nJamon: 20\nChorizo: 30\nVino: 40";
			assertEquals(cadena, laMevaComanda.toString());			

		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			System.err.println(e);
		}
	}

}
package main;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Main {
	
	public static Scanner sc = new Scanner(System.in);
	
	public static Connection con = null;
	
	public static DatabaseMetaData dbmd = null;
	
	public static java.sql.Statement st = null; 
	
	public static void Menu() {
		System.out.println("MENU:\n0.-Salir\n1.-Ver tablas y columnas\n2.-Ver datos\n3.-Inscriure Alumne a Grup\n4.-Veure GrupMoodle\n5.-Veure Alumne\n6.-Inscripció Automàtica\n7.-Inscripció Automàtica Function (opcional)\n"
				+ "8.-Resum Grups");
	}
	
	public static void main(String[] args) {

		int input = 1;
		conectar();
		while(input!=0) {
			Menu();
			input = sc.nextInt();
			sc.nextLine();
			switch (input) {
            case 1:
            	VerTablas();
                break;
            case 2:
            	System.out.println("Introduce la tabla:");
            	String tb = sc.nextLine();
            	VerDatos(tb);
                break;
            case 3:
            	AlumnoEnGrupo();
                break;
            case 4:
            	VerGrupoMoodle();
                break;
            case 5:
            	VerAlumne();
                break;
            case 6:
            	InscripcioAuto();
                break;
            case 8:
                verificarYEliminarTablaResumGrup();
                crearTablaResumGrup();
                insertarDatosEnResumGrup();
            	break;
            case 0:
                System.out.println("Saliendo...");
    			if (con != null)
    				try {
    					
    					con.close();

    				} catch (SQLException e) {
    					System.err.println("Error de tancament de connexio: " + e.getMessage());
    				}
                break;
            default:
                System.out.println("Valor introducido incorrecto");
                break;
			}
		}
	}

public static void verificarYEliminarTablaResumGrup() {
    try {
        st = con.createStatement();
        DatabaseMetaData metaData = con.getMetaData();
        ResultSet rs = metaData.getTables(null, null, "ResumGrup", null);

        if (rs.next()) {
            String dropQuery = "DROP TABLE ResumGrup";
            st.executeUpdate(dropQuery);
            System.out.println("La tabla ResumGrup ha sido eliminada.");
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }
}

public static void crearTablaResumGrup() {
    try {
        st = con.createStatement();
        String createTableQuery = "CREATE TABLE ResumGrup (" +
                "nomGrupMoodle VARCHAR(100) PRIMARY KEY," +
                "nomProfessor VARCHAR(100)," +
                "cognom1Professor VARCHAR(100)," +
                "cognom2Professor VARCHAR(100)," +
                "totalInscrits INT" +
                ")";
        st.executeUpdate(createTableQuery.toString());
        System.out.println("La tabla ResumGrup ha sido creada con éxito.");
    } catch (SQLException e) {
        e.printStackTrace();
    }
}

public static void insertarDatosEnResumGrup() {
    try {
        st = con.createStatement();
        String query = "SELECT g.nomGrupMoodle, g.ProfessorReponsable, a.totalAlumnos " +
                "FROM grupmoodle g " +
                "LEFT JOIN (SELECT idGrup, COUNT(*) AS totalAlumnos FROM inscripcions GROUP BY idGrup) a " +
                "ON g.idGrupMoodle = a.idGrup";
        
        ResultSet rs = st.executeQuery(query);

        while (rs.next()) {
            String nomGrupMoodle = rs.getString("nomGrupMoodle");
            String profesorResponsable = rs.getString("ProfessorReponsable");
            int totalAlumnos = rs.getInt("totalAlumnos");

            String[] partesNombreProfesor = profesorResponsable.split(" ");
            String nomProfessor = partesNombreProfesor[0];
            String cognom1Professor = partesNombreProfesor.length > 1 ? partesNombreProfesor[1] : "";
            String cognom2Professor = partesNombreProfesor.length > 2 ? partesNombreProfesor[2] : "";

            String insertQuery = "INSERT INTO ResumGrup (nomGrupMoodle, nomProfessor, cognom1Professor, cognom2Professor, totalInscrits) " +
                    "VALUES (?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = con.prepareStatement(insertQuery);
            preparedStatement.setString(1, nomGrupMoodle);
            preparedStatement.setString(2, nomProfessor);
            preparedStatement.setString(3, cognom1Professor);
            preparedStatement.setString(4, cognom2Professor);
            preparedStatement.setInt(5, totalAlumnos);
            preparedStatement.executeUpdate();
        }

        System.out.println("Los datos han sido insertados en la tabla ResumGrup.");
    } catch (SQLException e) {
        e.printStackTrace();
    }
}
	public static void InscripcioAuto() {
	    System.out.println("Introduce el grupo para la inscripcion:");
	    String ca = sc.nextLine();

	    ResultSet resultado = null;

	    try {
	        st = con.createStatement();
	        String c = "SELECT * FROM grupmoodle WHERE nomGrupMoodle = '" + ca + "'";
	        resultado = st.executeQuery(c);

	        if (!resultado.next()) {
	            System.out.println("No existe grupo.");
	            return;
	        }

	        String idc = resultado.getString(1);
	        String p = resultado.getString(5);

	        c = "SELECT * FROM cursos WHERE tutor = " + p;
	        resultado = st.executeQuery(c);
	        String cu = null;
	        if (resultado.next())
	            cu = resultado.getString(1);

	        c = "SELECT * FROM alumne WHERE curs = " + cu;
	        resultado = st.executeQuery(c);

	        ArrayList<String> sel = new ArrayList<String>();

	        while (resultado.next()) {
	            sel.add(resultado.getString(1));
	        }

	        Iterator<String> iterator = sel.iterator();
	        while (iterator.hasNext()) {
	            String idAlumno = iterator.next();
	            String query = "SELECT * FROM inscripcions WHERE idGrup=" + idc + " AND idAlumne = " + idAlumno;
	            ResultSet result = st.executeQuery(query);
	            if (result.next()) {
	                iterator.remove(); 
	            }
	        }

	        for (String idAlumno : sel) {
	            String insertQuery = "INSERT INTO inscripcions(idAlumne,idGrup) VALUES(" + idAlumno + "," + idc + ")";
	            st.executeUpdate(insertQuery);
	        }

	    } catch (SQLException e) {
	        e.printStackTrace();
	    } finally {
	        if (resultado != null) {
	            try {
	                resultado.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}

	public static void VerAlumne() {
		System.out.println("Introduce usuario alumne:");
		String ca = sc.nextLine();
		
		try {
			st = con.createStatement();
			
			String c = "SELECT * FROM alumne WHERE usuariAlumne = '" + ca + "'" ;
			
			ResultSet resultado = st.executeQuery(c.toString());
			
			if(!resultado.next()) {
				System.out.println("No existe alumno");
				return;
			}
			
			String ida = resultado.getString(1);
			
			System.out.println("idAlumne: "+resultado.getString(1)+"\nnomAlumne: "+resultado.getString(2)+"\ncognom1Alumne: "+resultado.getString(3)+"\ncognom2Alumne: "+resultado.getString(4)+"\nDNI: "+resultado.getString(5)+"\nusuariAlumne: "+resultado.getString(6)+"\ncurs: "+resultado.getString(7)+"\nbaixa:"+resultado.getString(8));
			
			
			c = "SELECT * FROM inscripcions WHERE idAlumne = " + ida + "" ;
			
			resultado = st.executeQuery(c.toString());
			ResultSet resultado2=null;

			ArrayList<String> sel = new ArrayList<String>();
			
			while(resultado.next()) {
				sel.add("SELECT * FROM grupmoodle WHERE idGrupMoodle = " + resultado.getString(3));
			}	
			
			System.out.println();
			
			for (String select:sel){
				resultado2 = st.executeQuery(select);
				if(resultado2.next())
					System.out.println(resultado2.getString(1)+" "+resultado2.getString(2)+" "+resultado2.getString(3)+" "+resultado2.getString(4)+" "+resultado2.getString(5));
			}
		}catch (SQLException e) {
		        e.printStackTrace();
		}

	}
	public static void VerGrupoMoodle() {
		System.out.println("Introduce nombre Grupo Moodle:");
		String ca = sc.nextLine();
		
		try {
			st = con.createStatement();
			
			String c = "SELECT * FROM grupmoodle WHERE nomGrupMoodle = '" + ca + "'" ;
			
			ResultSet resultado = st.executeQuery(c.toString());
			
			if(!resultado.next()) {
				System.out.println("No existe grupo");
				return;
			}
			
			String idg = resultado.getString(1);
			
			System.out.println("idGrupMoodle: "+resultado.getString(1)+"\nnomGrupMoodle: "+resultado.getString(2)+"\ndescripcionGrupMoodle: "+resultado.getString(3)+"\nactiu: "+resultado.getString(4)+"\nProfesorResponsable: "+resultado.getString(5));
			
			
			c = "SELECT * FROM inscripcions WHERE idGrup = " + idg + "" ;
			
			resultado = st.executeQuery(c.toString());
			ResultSet resultado2=null;

			ArrayList<String> sel = new ArrayList<String>();
			
			while(resultado.next()) {
				sel.add("SELECT * FROM alumne WHERE idAlumne = " + resultado.getString(2));
			}	
			
			for (String select:sel){
				resultado2 = st.executeQuery(select);
				if(resultado2.next())
					System.out.println(resultado2.getString(1)+" "+resultado2.getString(2)+" "+resultado2.getString(3)+" "+resultado2.getString(4)+" "+resultado2.getString(5)+" "+resultado2.getString(6)+" "+resultado2.getString(7)+" "+resultado2.getString(8));
			}
		}catch (SQLException e) {
		        e.printStackTrace();
		}
	}
	
	public static void AlumnoEnGrupo() {
		System.out.println("Introduce idalumn:");
		String ca = sc.nextLine();
		
		String c = "SELECT * FROM alumne WHERE idalumne = " + ca + "" ;
		
		try {
		st = con.createStatement();
		
		ResultSet resultado = st.executeQuery(c.toString());
		
		if(!resultado.next()) {
			System.out.println("Codigo no encontrado");
			return;
		}
        if(resultado.getString(8).equals("1")) {
        	System.out.println("Alumno dado de baja");
    		return;
        }
        
        System.out.println("Introduce el grupo de Moodle:");
		String gm = sc.nextLine();
		
		c = "SELECT * FROM grupmoodle WHERE UPPER(nomGrupMoodle) = '" + gm.toUpperCase()+"'" ;
		
		ResultSet resultado2 = st.executeQuery(c.toString());
		
		if(!resultado2.next()) {
			System.out.println("Grupo no encontrado");
			return;
		}
        if(resultado2.getString(4).equals("0")) {
        	System.out.println("Grupo no activo");
    		return;
        }
        
        String idg = resultado2.getString(1);
        
        c = "SELECT * FROM inscripcions WHERE idalumne = " + ca + " AND idGrup = " + idg ;
        
        resultado2 = st.executeQuery(c.toString());
        
        if(resultado2.next()) {
        	System.out.println("Ya esta inscrito en el grupo de moodle");
        	return;
        }

        c = "INSERT INTO inscripcions(idalumne,idGrup) VALUES ("+ca+","+idg+")"; //10500 BigData
        
        st.executeUpdate(c.toString());
        
        System.out.println("Alumno inscrito correctamente");
		
		}catch (SQLException e) {
	        e.printStackTrace();
	    }
	}
	public static void VerDatos(String taula) {
	    ResultSet columnes = null;
	    ResultSet rs = null;
	    try {
	        DatabaseMetaData metaData = con.getMetaData();
	        ResultSet rsColumns = metaData.getColumns(null, null, taula, null);
	        
	        StringBuilder consultaSQL = new StringBuilder("SELECT ");
	        boolean firstColumn = true;
	        while (rsColumns.next()) {
	            String nombreColumna = rsColumns.getString("COLUMN_NAME");
	            if (!firstColumn) {
	                consultaSQL.append(", ");
	            }
	            consultaSQL.append(nombreColumna);
	            firstColumn = false;
	        }
	        consultaSQL.append(" FROM "+taula);
	        
	        System.out.println(consultaSQL);
	        
	        java.sql.Statement statement = con.createStatement();
	        ResultSet resultado = statement.executeQuery(consultaSQL.toString());
	        
	        java.sql.ResultSetMetaData metaDataResultado = resultado.getMetaData();
	        
	        while (resultado.next()) {
	            for (int i = 1; i <= metaDataResultado.getColumnCount(); i++) { 
	                System.out.print(metaDataResultado.getColumnName(i) + ": " + resultado.getString(i) + ", ");
	            }
	            System.out.println();
	        }
	        
	        rsColumns.close();
	        resultado.close();
	        statement.close();
	        	
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	}

	public static void VerColumnas(String taula) {
		ResultSet columnes = null;
		
		try {
			
			columnes = dbmd.getColumns("moodle", null, taula, null);

			while (columnes.next()) {
				String nom = columnes.getString("COLUMN_NAME"); 
				String tipus = columnes.getString("TYPE_NAME"); 
				String mida = columnes.getString("COLUMN_SIZE"); 
				String nula = columnes.getString("IS_NULLABLE"); 
				
				System.out.println("Columna: "+nom+", Tipus: "+tipus+
						", Mida: "+mida+", Pot ser nul·la? "+nula);
				
			}
		}catch(SQLException e) {
            e.printStackTrace();
		} 
		finally {
			if (columnes != null) {
				try {
					columnes.close();
				} catch (SQLException e) {
					;
				}
			}
		}
	}
	
	public static void VerTablas() {
		ResultSet rs = null;
		ResultSet rs2 = null;
		try {
			
			 String[] types=null;
	
			 rs = dbmd.getTables("moodle", null, null, types);
			 
			 while (rs.next()) {
				
				String esquema = rs.getString(2);
				String taula = rs.getString(3);
				String tipus = rs.getString(4);
				 
				ResultSet resultSet = con.createStatement().executeQuery("SELECT COUNT(*) AS num_registros FROM " + taula);
				
				int numRegistros = 0;
				
				if (resultSet.next()) {
                	numRegistros = resultSet.getInt("num_registros");
                }
				 
				System.out.println(tipus + " - Cataleg: " + rs.getString(1) +
						", Esquema: "+esquema+", Nom: "+taula+", Numero de registros: "+
						numRegistros + "\nColumnas: ");
				VerColumnas(taula);
				
			 }
			 
			 rs.close();
		}catch(SQLException e) {
            e.printStackTrace();
		}finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					;
				}
			}
		}
	}
	
	public static void conectar() {
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/moodle?user=root&password=1212");
			dbmd = con.getMetaData();
			System.out.println("Base de dades connectada!");

		} catch (SQLException e) {
			System.err.println("Error d'apertura de connexio: " + e.getMessage());

		} 
	}
}

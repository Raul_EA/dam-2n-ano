package main;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class Examen {
	public static Scanner sc = new Scanner(System.in);
	
	public static Connection con = null;
	
	public static DatabaseMetaData dbmd = null;
	
	public static java.sql.Statement st = null; 
	
	public static void Menu() {
		System.out.println("MENU:\n0.-Salir\n1.-Autonomies\n2.-Resum Autonomies\n3.-Resultats Club - Exercici\n4.-Exercicis");
	}
	
	public static void main(String[] args) {

		int input = 1;
		conectar();
		while(input!=0) {
			Menu();
			input = sc.nextInt();
			sc.nextLine();
			switch (input) {
            case 1:
            	Autonomies();
                break;
            case 2:
            	ResumAutonomies();
                break;
            case 3:
            	ResultatsClub();
                break;
            case 4:
            	Exercicis();
                break;
            case 0:
                System.out.println("Saliendo...");
    			if (con != null)
    				try {
    					
    					con.close();

    				} catch (SQLException e) {
    					System.err.println("Error de tancament de connexio: " + e.getMessage());
    				}
                break;
            default:
                System.out.println("Valor introducido incorrecto");
                break;
			}
		}
	}
	public static void Autonomies() {
		
		try {
			st = con.createStatement();
		    String c = "SELECT DISTINCT Comunitat FROM clubs";
		    ResultSet resultado = st.executeQuery(c);
		    
		    while(resultado.next()) {
		    	System.out.println(resultado.getString(1));
		    }
		    
		    System.out.println("Escoje una comunidad autonoma:");
		    String a = sc.nextLine();
		    
			st = con.createStatement();
		    c = "SELECT club FROM clubs WHERE Comunitat = '" + a.toUpperCase() + "'";
		    resultado = st.executeQuery(c);
		    
		    ArrayList<String> a2 = new ArrayList<String>();
		    
		    while(resultado.next()) {
		    	a2.add(resultado.getString(1));
		    }
		    ArrayList<String> a3 = new ArrayList<String>();
		    for(String b : a2) {
		    	st = con.createStatement();
			    c = "SELECT EsportistaNom FROM esportistes WHERE EsportistaClub = '" + b + "'";
			    resultado = st.executeQuery(c);
			    while(resultado.next()) {
			    		a3.add(resultado.getString(1));
			    }
		    }
		    
		    for(String d : a3) {
		    	System.out.println(d);
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public static void ResumAutonomies() {
		try {
			st = con.createStatement();
		    String c = "SELECT Comunitat\n"
		    		+ "FROM clubs c\n"
		    		+ "GROUP BY Comunitat\n"
		    		+ "ORDER BY Comunitat ";
		    ResultSet resultado = st.executeQuery(c);
		    
		    ArrayList<String> a = new ArrayList<String>();
		    
		    while(resultado.next()) {
		    	a.add(resultado.getString(1));
		    }
		    
			st = con.createStatement();
		    c = "SELECT COUNT(EsportistaNom)\n"
		    		+ "FROM clubs c\n"
		    		+ "LEFT JOIN esportistes e \n"
		    		+ "ON c.club = e.EsportistaClub \n"
		    		+ "GROUP BY Comunitat ORDER BY Comunitat\n";
		    resultado = st.executeQuery(c);
		    
		    ArrayList<String> a2 = new ArrayList<String>();
		    
		    while(resultado.next()) {
		    	a2.add(resultado.getString(1));
		    }
		    
			st = con.createStatement();
		    c = "SELECT COUNT(club)\n"
		    		+ "FROM clubs c\n"
		    		+ "GROUP BY Comunitat ORDER BY Comunitat\n";
		    resultado = st.executeQuery(c);
		    
		    ArrayList<String> a3 = new ArrayList<String>();
		    
		    while(resultado.next()) {
		    	a3.add(resultado.getString(1));
		    }
		    
		    for(int i=0;i<a.size();i++) {
			    System.out.println("Comunidad: "+a.get(i)+" Numero clubes: "+a3.get(i)+" Numero esportistas: "+a2.get(i));
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public static void ResultatsClub() {
		try {
			System.out.println("Introduce nombre club:");
			String a = sc.nextLine();
			
			st = con.createStatement();
		    String c = "SELECT EsportistaNom FROM esportistes e WHERE EsportistaClub = '"+a+"'";
		    ResultSet resultado = st.executeQuery(c);
		    
		    ArrayList<String> a2 = new ArrayList<String>();
		    
		    while(resultado.next()) {
		    	a2.add(resultado.getString(1));
		    }
			
			System.out.println("Introduce un ejercicio:");
			String b = sc.nextLine();	
			
			ArrayList<String> a3 = new ArrayList<String>();
			
			for(String h : a2) {
				st = con.createStatement();
			    c = " getPosicion('"+h+"',"+b+"),";
			    a3.add(c);
			}
			String j = "SELECT";
			for(String h:a3) {
				j += h; 
			}

			j=j.substring(0, (j.length()-1));
			resultado = st.executeQuery(j);
			
			for(int i=1;i<=a2.size();i++) {
				System.out.println(resultado.getString(i));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public static void Exercicis() {
		 try {
			 	st = con.createStatement();
			 	String u = "DROP TABLE IF EXISTS Exercicis";
			 	st.executeUpdate(u);
			 			
		       	st = con.createStatement();
		        String createTableQuery = "CREATE TABLE Exercicis (" +
		                "ExerciciID VARCHAR(5) PRIMARY KEY," +
		                "Barem float," +
		                "ExerciciDescripcio VARCHAR(35))";
		        st.executeUpdate(createTableQuery.toString());
		        System.out.println("La tabla ResumGrup ha sido creada con éxito.");
		        
				st = con.createStatement();
			    String c = "SELECT DISTINCT Exercici FROM resultats e";
			    ResultSet resultado = st.executeQuery(c);
			    
			    ArrayList<String> a = new ArrayList<String>();
			    
			    while(resultado.next()) {
			    	a.add(resultado.getString(1));
			    }
			    
			    for(int i=0;i<a.size();i++) {
					st = con.createStatement();
					c = "INSERT INTO Exercicis VALUES ('"+a.get(i)+"','"+2.2+"','"+a.get(i)+"')";
				    System.out.println(c);
				    st.executeUpdate(c);
			    }
			    
		    } catch (SQLException e) {
		    	e.printStackTrace();
		    }
	}
	public static void conectar() {
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sincronitzada?user=root&password=1212");
			dbmd = con.getMetaData();
			System.out.println("Base de dades connectada!");

		} catch (SQLException e) {
			System.err.println("Error d'apertura de connexio: " + e.getMessage());

		} 
	}
}

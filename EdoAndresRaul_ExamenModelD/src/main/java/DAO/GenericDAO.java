package DAO;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;

public abstract class GenericDAO<T, ID extends Serializable> implements IGenericDAO<T,ID>{

	Class<T> entityClass;
	
	public GenericDAO(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	@Override
	public void save(T entity) 
	{
		Session session = SessionHandler.getSessionFactory().getCurrentSession(); 
		session.getTransaction().begin();
		session.persist(entity);		
		session.getTransaction().commit();
	}

	@Override
	public T getByID(ID id) 
	{
		Session session = SessionHandler.getSessionFactory().getCurrentSession(); 
		session.getTransaction().begin();
		T entity = session.find(entityClass, id);
		session.getTransaction().commit();
		return entity;
	}

	@Override
	public void update(T entity) 
	{
		Session session = SessionHandler.getSessionFactory().getCurrentSession(); 
		session.getTransaction().begin();
		session.merge(entity);
		session.getTransaction().commit();
		
	}

	@Override
	public void delete(T entity) 
	{
		Session session = SessionHandler.getSessionFactory().getCurrentSession(); 
		session.getTransaction().begin();
		session.remove(entity);
		session.getTransaction().commit();
		
	}

	@Override
	public void deleteByID(ID id) 
	{
		Session session = SessionHandler.getSessionFactory().getCurrentSession(); 
		session.getTransaction().begin();
		T entity = session.find(entityClass, id);
		session.remove(entity);
		session.getTransaction().commit();
		
	}

	@Override
	public List<T> findAll() {
		Session session = SessionHandler.getSessionFactory().getCurrentSession(); 
		session.getTransaction().begin();
		List<T> registres = session.createQuery("from "+entityClass.getName()).getResultList();
		session.getTransaction().commit();
		return registres;
	}

}

package DAO;

import java.util.List;

import Model.Mena;
import Model.TipoMena;

public class MenaDAO extends GenericDAO<Mena, Integer>{
	public MenaDAO() {
		super(Mena.class);
	}
	
	public void setInitialMinerals() {
		List<Mena> m = super.findAll();
		for(Mena mm : m) {
			if(mm.getTipo()==TipoMena.coure)
				mm.setExtraible(mm.getExtraible()-5);
			if(mm.getTipo()==TipoMena.ferro)
				mm.setExtraible(mm.getExtraible()-10);
			
			if(mm.getExtraible()<0)
				mm.setExtraible(0);
			super.update(mm);
		}
	}
}

package DAO;

import java.io.Serializable;
import java.util.List;

public interface IGenericDAO<T, ID extends Serializable> {
	void save(T entity);
	T getByID(ID id);
	void update(T entity);
	void delete(T entity);
	void deleteByID(ID id);
	List<T> findAll();
}

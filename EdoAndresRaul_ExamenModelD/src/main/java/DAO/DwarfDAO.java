package DAO;

import Model.Dwarf;

public class DwarfDAO extends GenericDAO<Dwarf, Integer>{

	public DwarfDAO() 
	{
		super(Dwarf.class);
	}

}

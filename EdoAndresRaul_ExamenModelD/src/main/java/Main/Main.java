package Main;

import java.util.List;

import DAO.DwarfDAO;
import DAO.MenaDAO;
import Model.Dwarf;
import Model.Mena;
import Model.TipoMena;
import Model.Eina;

public class Main {
	
	
	public static void main(String[] args) {
	
		MenaDAO m = new MenaDAO();
		DwarfDAO d = new DwarfDAO();
		
		Dwarf d1 = new Dwarf();
		Dwarf d2 = new Dwarf();
		Dwarf d3 = new Dwarf();
		Dwarf d4 = new Dwarf();
		Dwarf d5 = new Dwarf();
		
		d1.setNom("Jose");
		d2.setNom("Miguel");
		d3.setNom("David");
		d4.setNom("Bartolo");
		d5.setNom("Ivan");
		
		d1.setEinaEq(Eina.pic);
		d2.setEinaEq(Eina.pala);
		d3.setEinaEq(Eina.pic);
		d4.setEinaEq(Eina.martell);
		d5.setEinaEq(Eina.pic);
		
		d.save(d1);
		d.save(d2);
		d.save(d3);
		d.save(d4);
		d.save(d5);
		
		Mena m1 = new Mena();
		Mena m2 = new Mena();
		
		m1.setTipo(TipoMena.coure);
		m2.setTipo(TipoMena.ferro);
		
		m.save(m1);
		m.save(m2);
		
		d1.setMena(m1);
		d2.setMena(m1);
		d3.setMena(m2);
		d4.setMena(m2);
		d5.setMena(m2);
		
		d.update(d1);
		d.update(d2);
		d.update(d3);
		d.update(d4);
		d.update(d5);

		System.err.println(d1);
		System.err.println(d2);
		System.err.println(d3);
		System.err.println(d4);
		System.err.println(d5);
		
		System.err.println(m1);
		System.err.println(m2);
		
		m.setInitialMinerals();
		m.setInitialMinerals();
		m.setInitialMinerals();

		List<Mena> mm = m.findAll();
		
		System.err.println(mm);
		
	}

}

package Model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="dwarf")
public class Dwarf {
	
	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="iddwarf")
	private int iddwarf;
	@Column(name="nom")
	private String nom;
	@Column(name="obs")
	private int objs=1;
	@Column(name="objsMax")
	private int objsMax=10;
	@Column(name="einaEq")
	private Eina einaEq;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="mena")
	private Mena mena;
	
	public Dwarf() {
		super();
	}

	public int getIddwarf() {
		return iddwarf;
	}

	public void setIddwarf(int iddwarf) {
		this.iddwarf = iddwarf;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getObjs() {
		return objs;
	}

	public void setObjs(int objs) {
		this.objs = objs;
	}

	public int getObjsMax() {
		return objsMax;
	}

	public void setObjsMax(int objsMax) {
		this.objsMax = objsMax;
	}

	public Eina getEinaEq() {
		return einaEq;
	}

	public void setEinaEq(Eina einaEq) {
		this.einaEq = einaEq;
	}

	public Mena getMena() {
		return mena;
	}

	public void setMena(Mena mena) {
		this.mena = mena;
	}

	@Override
	public String toString() {
		return "Dwarf [iddwarf=" + iddwarf + ", nom=" + nom + ", objs=" + objs + ", objsMax=" + objsMax + ", einaEq="
				+ einaEq + ", mena=" + mena + "]";
	}
}

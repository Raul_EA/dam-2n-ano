package Model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="mena")
public class Mena {
	
	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idmena")
	private int idmena;
	@Column(name="tipo")
	private TipoMena tipo;
	@Column(name="extraible")
	private int extraible=25;
	@OneToMany(mappedBy = "mena")
	private Set<Dwarf> enanos = new HashSet<Dwarf>();
	
	public Mena() {
		super();
	}

	public int getIdmena() {
		return idmena;
	}

	public void setIdmena(int idmena) {
		this.idmena = idmena;
	}

	public TipoMena getTipo() {
		return tipo;
	}

	public void setTipo(TipoMena tipo) {
		this.tipo = tipo;
	}

	public int getExtraible() {
		return extraible;
	}

	public void setExtraible(int extraible) {
		this.extraible = extraible;
	}

	public Set<Dwarf> getEnanos() {
		return enanos;
	}

	public void setEnanos(Set<Dwarf> enanos) {
		this.enanos = enanos;
	}

	@Override
	public String toString() {
		return "Mena [idmena=" + idmena + ", tipo=" + tipo + ", extraible=" + extraible + "]";
	}
	
}

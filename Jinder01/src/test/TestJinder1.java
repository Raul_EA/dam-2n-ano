package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.jupiter.api.Test;
import prob.Persona;
import prob.SEXE;
import impl.Jinder;

class TestJinder1 {

	@Test
	void testJinder() {
		
		Jinder j = new Jinder();
		Persona p1 = new Persona("Daniel", 20, SEXE.M, SEXE.F);
		Persona p2 = new Persona ("Ana", 23, SEXE.F, SEXE.M);
		j.afegir(p1);		
		assertNotNull(j);
		j.afegir(p1);		
		j.afegir(p2);		
		j.afegir(p2);		
		assertEquals(2,j.size());
		j.ordenar();
		Persona p3 = j.treure();
		assertEquals("Ana",p3.getNom());
		p3 = j.treure();
		assertEquals("Daniel",p3.getNom());
		assertEquals(0,j.size());		
	}
}

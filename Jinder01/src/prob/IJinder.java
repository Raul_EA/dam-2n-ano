package prob;

import java.util.List;

public interface IJinder<Persona> {
	/**
	 * 
	 * @param p Persona
	 * @return boolean
	 * 
	 * Si la persona ja existeix (hi ha una altra iguual), no l'afegeix i retorna fals
	 * Si la persona mno existeix, afegeix la persona al sistema, com a última i retorna true
	 */
	boolean afegir (Persona p); 
	
	/**
	 * ordena les persones del nostre sistema
	 */
	void ordenar();
	/**
	 * @return Persona
	 * 
	 * Si el sistema conté persones, retorna la primera i l'elimina del nostre sistema
	 * Si el sistema no te persones, retorna null
	 */
	Persona treure ();
	
	/**
	 * Elimina totes les persones del nostre sistema i ho deixa tot buit
	 */
	void reset();
	/**
	 * Retorna el número de persones del nostre sistema
	 * @return
	 */
	int size();
	/**
	 * Retorna una llista amb totes les persones del nostre sistema
	 */
	List<Persona> toList();
}

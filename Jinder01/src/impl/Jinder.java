package impl;

import java.util.ArrayList;
import java.util.List;

import prob.IJinder;
import prob.Persona;

public class Jinder implements IJinder<Persona>{
	
	List<Persona> gente;
	
	public Jinder() {
		this.gente = new ArrayList<Persona>();
	}
	
	@Override
	public boolean afegir(Persona p) {
		for(Persona a : this.gente) {
			if(a.equals(p))
				return false;
		}
		this.gente.add(p);
		return true;
	}

	@Override
	public void ordenar() {
		this.gente.sort(null);
	}

	@Override
	public Persona treure() {
		if(this.gente.isEmpty())
			return null;
		else {
			Persona a = this.gente.get(0);
			this.gente.remove(0);
			return a;
		}
	}

	@Override
	public void reset() {
		this.gente = new ArrayList<Persona>();
	}

	@Override
	public int size() {
		return this.gente.size();
	}

	@Override
	public List<Persona> toList() {
		return this.gente;
	}
}

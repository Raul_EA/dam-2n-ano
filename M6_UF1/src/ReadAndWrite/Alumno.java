package ReadAndWrite;

import java.io.Serializable;

public class Alumno implements Serializable{

	@Override
	public String toString() {
		return "Alumno [nombre=" + nombre + ", edad=" + edad + ", depresion=" + depresion + "]";
	}

	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private int edad;
	private boolean depresion;
	
	public Alumno(String nombre, int edad, boolean depresion) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.depresion = depresion;
	}
	public Alumno() {
		this.nombre = "";
		this.edad = 0;
		this.depresion = false;
	}
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public boolean isDepresion() {
		return depresion;
	}

	public void setDepresion(boolean depresion) {
		this.depresion = depresion;
	}
	
	
}

package ReadAndWrite;

import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.File;

public class ReadFile {

	public static void main(String[] args) {
		File lista = new File("src/ReadAndWrite/listaAlumnos.txt");
		try {
			FileReader fr = new FileReader(lista);
			BufferedReader br = new BufferedReader(fr);
			while(br.ready()) {
				System.out.println(br.readLine());
			}
			br.close();
		}catch (IOException e) {
		}
		
	}

}

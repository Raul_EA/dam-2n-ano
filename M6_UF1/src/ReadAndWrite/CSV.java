package ReadAndWrite;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class CSV {

	public static void main(String[] args) {
		File csv = new File("src/ReadAndWrite/bdAlumnos.csv");
		
		Alumno a = new Alumno("Ismael",18,true);
		Alumno b = new Alumno("Abel",19,false);
		Alumno c = new Alumno("Jose",22,true);
		Alumno d = new Alumno("Miguel",27,true);
		
		ArrayList<Alumno> lista= new ArrayList<>(Arrays.asList(a,b,c,d));		
		
		try {
			FileWriter fw = new FileWriter(csv);
			BufferedWriter bw = new BufferedWriter(fw);
			for(int i=0;i<lista.size();i++) {
				String lineaCSV = lista.get(i).getNombre()+";"+lista.get(i).getEdad()+";"+lista.get(i).isDepresion();
				bw.write(lineaCSV);
				bw.newLine();
				System.out.println(lineaCSV);
			}
			
			bw.close();
			
			File copy = new File("src/ReadAndWrite/bdAlumnos2.csv");
			FileReader fr = new FileReader(csv);
			BufferedReader br = new BufferedReader(fr);
			FileWriter fw2 = new  FileWriter(copy);
			
			Alumno z = new Alumno();
			
			bw = new BufferedWriter(fw2);
			
			while(br.ready()) {
				String linea = br.readLine();
				String datos[] = linea.split(";");
				if(datos[0].equals("Abel")) {
					z.setNombre(datos[0]);
					z.setEdad(Integer.parseInt(datos[1]));
					z.setDepresion(datos[2].equals("true"));
					bw.write(datos[0]+";"+datos[1]+";"+datos[2]);
					bw.newLine();
				}else {
					bw.write(linea);
					bw.newLine();
				}
			}
			bw.close();
			br.close();
			
			csv.delete();
			copy.renameTo(csv);
		}catch (IOException e) {
			
		}
	}

}

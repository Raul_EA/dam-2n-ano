package ReadAndWrite;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class WriteReadObjects {
	public static void main(String[] args) {
		File f = new File("src/ReadAndWrite/bdAlumnos.dat");
		
		Alumno a = new Alumno("Paco",33,true);
		Alumno b = new Alumno("Manolo",24,false);
		
		try {
			FileOutputStream fos = new FileOutputStream(f);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(a);
			oos.writeObject(b);
			
			oos.close();
			
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);		
			try {
				while(true) {
					System.out.println((Alumno) ois.readObject());
				}
			}catch (IOException | ClassNotFoundException e) {
				ois.close();
			}	
			
			File copy = new File("src/ReadAndWrite/copia.dat");
			fos = new FileOutputStream(copy);
			oos = new ObjectOutputStream(fos);
			
			fis = new FileInputStream(copy);
			ois = new ObjectInputStream(fis);
			
			try {
				while(true) {
					Object o = ois.readObject();
					if (o instanceof Alumno) {
						if(((Alumno)o).isDepresion()){
							((Alumno)o).setDepresion(false);
							oos.writeObject(o);
						}					
					}
				}
			}catch (IOException | ClassNotFoundException e) {
				ois.close();
				oos.close();
				
				f.delete();
				copy.renameTo(f);
				
				fis = new FileInputStream(f);
				ois = new ObjectInputStream(fis);	
				
				try {
					while(true) {
						System.out.println((Alumno) ois.readObject());
					}
				}catch (IOException | ClassNotFoundException i) {
					ois.close();
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}

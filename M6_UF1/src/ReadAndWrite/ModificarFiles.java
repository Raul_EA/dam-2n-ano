package ReadAndWrite;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ModificarFiles {

	public static void main(String[] args) {
		File lista = new File("src/ReadAndWrite/listaAlumnos.txt");
		File copy = new File("src/ReadAndWrite/listaAlumnos2.txt");
		
		FileWriter fw = null;
		try {
			FileReader fr = new FileReader(lista);
			BufferedReader br = new BufferedReader(fr);
			
			fw = new FileWriter(copy);
			BufferedWriter bw = new BufferedWriter(fw);
			while(br.ready()) {
				String line = br.readLine();
				if(line.contains("Pablo")) {
					line = line.replace("Pablo", "Ramon");
				}
				System.out.println(line);
				bw.write(line);
				if(br.ready())
					bw.newLine();
			}
			bw.close();
			br.close();
			
			lista.delete();
			copy.renameTo(lista);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

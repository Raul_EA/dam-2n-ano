package ReadAndWrite;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class WriteBinarios {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		File f = new File("src/ReadAndWrite/dades.bin");
		FileOutputStream fos = null;
		FileInputStream fis = null;
		try {
			fos = new FileOutputStream(f);
			DataOutputStream dos = new DataOutputStream(fos);
			
			int a = 88;
			String b = "Cancer";
			boolean c = true;

			dos.writeInt(a);
			dos.writeUTF(b);
			dos.writeBoolean(c);
			dos.close();
			
			fis = new FileInputStream(f);
			DataInputStream dis = new DataInputStream(fis);
			
			while(dis.available()>0) {
				System.out.println(dis.readInt());
				System.out.println(dis.readUTF());
				System.out.println(dis.readBoolean());
			}
			
			dis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

package ReadAndWrite;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;


public class WriteFIles {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		File lista = new File("src/ReadAndWrite/listaAlumnos.txt");
		FileWriter fw = null;
		try {
			fw = new FileWriter(lista, true);
			BufferedWriter bw = new BufferedWriter(fw);
			String imputs = sc.nextLine();
			while (!imputs.equals("fin")) {
				bw.write(imputs);
				bw.newLine();
				imputs = sc.nextLine();
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		sc.close();
		
		sc.close();
	}

}

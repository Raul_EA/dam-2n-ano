package prob;

import java.time.LocalDateTime;
import java.util.Objects;

public class Lot implements Comparable{
	private LocalDateTime caducitat;
	private int quantitat;
	
	public LocalDateTime getCaducitat() {
		return caducitat;
	}
	public void setCaducitat(LocalDateTime caducitat) {
		this.caducitat = caducitat;
	}
	public int getQuantitat() {
		return quantitat;
	}
	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lot other = (Lot) obj;
		return Objects.equals(caducitat, other.caducitat);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(caducitat);
	}
	
	@Override
	public int compareTo(Object that) {
		// TODO Auto-generated method stub
		if (this == that)
			return 0;
		int res = this.caducitat.compareTo(((Lot) that).getCaducitat());
		if (res == 0)
			return (this.quantitat - ((Lot) that).getQuantitat());
		else
			return res;
	}

	@Override
	public String toString() {
		return ("Lot: " + caducitat + " Quantitat: " + quantitat);
	}
}

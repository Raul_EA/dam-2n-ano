package prob;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public class ProducteNormal extends Producte{

	private int quantitat;
	private int quantitatMaxima;

	public ProducteNormal(String nom){
		super(nom);
		quantitat = 0;
		quantitatMaxima = 0;
	}
	
	public ProducteNormal(String nom, int min, int repo){
		super(nom, min, repo);
		quantitat = 0;
		quantitatMaxima = 0;
	}

	public ProducteNormal(String nom, int min, int repo, int q, int qM){
		super(nom, min, repo);
		quantitat = q;
		quantitatMaxima = qM;
	}

	public void afegirQuantitat(int quantitat) throws ProgramException {
		if(quantitat<0) throw new IllegalArgumentException("La quantitat no pot ser negativa");
		if (this.quantitat + quantitat >this.quantitatMaxima) throw new ProgramException ("Sobrepasses quantitat màxima");
		this.quantitat += quantitat;
		return;
	}
	
	@Override
    public int hashCode() {
        return Objects.hash(super.hashCode());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        return (super.equals(obj));
    }

    @Override
	public String toString() {
		return (super.toString() + "( Stock: " + quantitat + " Màx: " + quantitatMaxima + ")");
	}

	@Override
	public int compareTo(Producte that) {
		if (this == that)
			return 0;
		return(this.idProducte - that.getIdProducte());
	}

	@Override
	public int stock() {
		return quantitat;
	}
    
}

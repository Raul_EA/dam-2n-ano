package prob;

import java.util.Map;

public interface iBotiga<P> {
	
	/**
	 * Li dona un nom a la botiga
	 * @param nomBotiga   .. és el nom de la botiga
	 * @throws IllegalArgumentException si el nomBotiga és null.
	 */
	
	void setNomBotiga(String nomBotiga);
	
	/**
	 * Afegeix un producte a la botiga.
	 * @param producte .. el producte que afegeix
	 * @throws IllegalArgumentException si producte és null.
	 * @throws ProgramException si el producte ja existeix
	 */
	void afegirProducte(P p) throws ProgramException;
	
	/**
	 * Gets the stock for a product.
	 * @throws IllegalArgumentException if product is null.
	 * @throws ProgramException si el producte no existeix
	 * @return
	 */
	public int getStock(P p) throws ProgramException;

	/**
	 * Gets the inventari .
	 * @return Map <Producte, Integer>
	 */
	public Map<P, Integer> getInventari();

	public Map<String, Integer> getCaducats();
	
 	public String veureInventari();
 	
 	public void eliminarCaducats();
	
}

package prob;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public class ProducteLot extends Producte {

	private Map<LocalDate, Integer> lots;

	public void setLots(Map<LocalDate, Integer> lots) {
		this.lots = lots;
	}

	public Map<LocalDate, Integer> getLots() {
		return lots;
	}

	public ProducteLot(String nom) {
		super(nom);
		lots = new TreeMap<>();
	}

	public ProducteLot(String nom, int min, int repo) {
		super(nom, min, repo);
		lots = new TreeMap<>();
	}

	public void afegirLot(LocalDate date, int quantitat) throws ProgramException {
		if (quantitat < 0)
			throw new IllegalArgumentException("La quantitat no pot ser negativa");
		if (date == null)
			throw new ProgramException("No hi ha data");
		int q = 0;
		if (lots.containsKey(date))
			q = lots.get(date);
		q = q + quantitat;
		lots.put(date, q);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return (super.equals(obj));
	}

	@Override
	public String toString() {
		return (super.toString() + lots);
	}

	@Override
	public int compareTo(Producte that) {
		if (this == that)
			return 0;
		return (this.idProducte  - that.getIdProducte());
	}

	@Override
	public int stock() {
		int total = 0;
		for (Integer i: lots.values())
			total += i;
		return total;
	}

}

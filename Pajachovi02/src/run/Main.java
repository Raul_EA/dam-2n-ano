package run;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import impl.Botiga;
import prob.Producte;
import prob.ProducteLot;
import prob.ProducteNormal;
import prob.ProgramException;

public class Main {

	public static void main(String[] args) {

		try {
			Botiga pajachovi = new Botiga("Pajachovi");

			ProducteLot p1 = new ProducteLot("Pan",0,10);
			p1.afegirLot(LocalDate.now().plusWeeks(1), 20);
			p1.afegirLot(LocalDate.now().plusWeeks(1), 10);
			p1.afegirLot(LocalDate.now().plusWeeks(2), 10);
			p1.afegirLot(LocalDate.now().plusWeeks(-1), 10);

			ProducteLot p2 = new ProducteLot("Jamon",2,4);
			p2.afegirLot(LocalDate.now().plusWeeks(1), 2);
			p2.afegirLot(LocalDate.now().plusWeeks(1), 3);
			
			ProducteLot p3 = new ProducteLot("Chorizo",2,2);
			p3.afegirLot(LocalDate.now().plusWeeks(1), 12);
			p3.afegirLot(LocalDate.now().plusWeeks(1), 5);
			p3.afegirLot(LocalDate.now().plusWeeks(-10), 4);

			ProducteNormal p4 = new ProducteNormal("Vino",0,10,20,50);
			
			pajachovi.afegirProducte(p1);
			pajachovi.afegirProducte(p2);
			pajachovi.afegirProducte(p3);
			pajachovi.afegirProducte(p4);
			System.out.println(pajachovi);
			
			System.out.println("\nInventari Inicial:\n");
			System.out.println(pajachovi.getInventari());
			System.out.println("\nInventari Inicial Resumit:\n");
			System.out.println(pajachovi.veureInventari());
			System.out.println("\nCaducats:\n");
			System.out.println(pajachovi.getCaducats());
			System.out.println("\n...Eliminació caducats ...");
			pajachovi.eliminarCaducats();
			System.out.println("\nInventari final:\n");
			System.out.println(pajachovi.getInventari());
			System.out.println("\nInventari final Resumit:\n");
			System.out.println(pajachovi.veureInventari());			
			System.out.println("\n\nCaducats:\n");
			System.out.println(pajachovi.getCaducats());
		}
		catch (Exception e) {
			System.out.println(e.getStackTrace());
			System.err.println(e);
		}
	}	
}

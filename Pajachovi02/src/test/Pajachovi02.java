package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import impl.Botiga;
import prob.ProducteLot;
import prob.ProducteNormal;

class Pajachovi02 {

	@Test
	void pajachovi02Inventari() {
		try {
			Botiga pajachovi = new Botiga("Pajachovi");
			LocalDate f = LocalDate.parse("2023-11-09");
			
			ProducteLot p1 = new ProducteLot("Pan", 0, 10);
			p1.afegirLot(f.plusWeeks(1), 20);
			p1.afegirLot(f.plusWeeks(1), 10);
			p1.afegirLot(f.plusWeeks(2), 10);
			p1.afegirLot(f.plusWeeks(-1), 10);

			ProducteLot p2 = new ProducteLot("Jamon", 2, 4);
			p2.afegirLot(f.plusWeeks(1), 2);
			p2.afegirLot(f.plusWeeks(1), 3);

			ProducteLot p3 = new ProducteLot("Chorizo", 2, 2);
			p3.afegirLot(f.plusWeeks(1), 12);
			p3.afegirLot(f.plusWeeks(1), 5);
			p3.afegirLot(f.plusWeeks(-10), 4);

			ProducteNormal p4 = new ProducteNormal("Vino", 0, 10, 20, 50);

			pajachovi.afegirProducte(p1);
			pajachovi.afegirProducte(p2);
			pajachovi.afegirProducte(p3);
			pajachovi.afegirProducte(p4);
			
			String cadena = "Botiga Pajachovi\nCataleg:\n[1]: Pan(min: 0, rep: 10){2023-11-02=10, 2023-11-16=30, 2023-11-23=10}\n[2]: Jamon(min: 2, rep: 4){2023-11-16=5}\n[3]: Chorizo(min: 2, rep: 2){2023-08-31=4, 2023-11-16=17}\n[4]: Vino(min: 0, rep: 10)( Stock: 20 Màx: 50)]";
			assertEquals(cadena, pajachovi.toString());

			cadena = "{[1]: Pan(min: 0, rep: 10){2023-11-02=10, 2023-11-16=30, 2023-11-23=10}=50, [2]: Jamon(min: 2, rep: 4){2023-11-16=5}=5, [3]: Chorizo(min: 2, rep: 2){2023-08-31=4, 2023-11-16=17}=21, [4]: Vino(min: 0, rep: 10)( Stock: 20 Màx: 50)=20}";		
			assertEquals(cadena, pajachovi.getInventari().toString());
			
			cadena = "Pan: 50\n2	Jamon: 5\n3	Chorizo: 21\n4	Vino: 20";
			assertEquals(cadena, pajachovi.veureInventari());

		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			System.err.println(e);
		}
	}

}
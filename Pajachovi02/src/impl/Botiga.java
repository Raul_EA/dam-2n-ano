package impl;

import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import prob.Producte;
import prob.ProducteLot;
import prob.ProgramException;
import prob.iBotiga;

import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Botiga implements iBotiga<Producte> {

	private String nomBotiga;
	private Set<Producte> cataleg;

	public Botiga() {
		cataleg = new TreeSet<Producte>();
	}

	public Botiga(String string) {
		this();
		setNomBotiga(string);
	}

	@Override
	public void afegirProducte(Producte producte) throws ProgramException {
		if (cataleg.contains(producte))
			throw new ProgramException("Producte ja existeix");
		cataleg.add(producte);
	}

	@Override
	public String toString() {
		String cadena = "Botiga " + nomBotiga + "\nCataleg:";
		for (Producte p : cataleg)
			cadena += "\n" + p;
		cadena += "]";
		return cadena;
	}

	@Override
	public void setNomBotiga(String nomBotiga) {
		if (nomBotiga == null)
			throw new IllegalArgumentException("Nom Botiga no pot ser null");
		this.nomBotiga = nomBotiga;
	}

	@Override
	public int getStock(Producte producte) throws ProgramException {
		if (producte == null)
			throw new IllegalArgumentException();
		else if (cataleg.contains(producte))
			return producte.stock();
		else
			throw new ProgramException("Producte no existeix");
	}

	@Override
	public Map<Producte, Integer> getInventari() {
		Map<Producte, Integer> b = new TreeMap<Producte, Integer>();
		for (Producte a : this.cataleg) {
			b.put(a, a.stock());
		}
		return b;
	}

	@Override
	public String veureInventari() {
		String c = "";
		for (Producte a : this.cataleg) {
			c += a.getNom() + ": " + a.stock() + "\n";
		}
		return c;
	}

	@Override
	public Map<String, Integer> getCaducats() {
		Map<String, Integer> c = new TreeMap<String, Integer>();
		Map<Producte, Integer> b = new TreeMap<Producte, Integer>();
		LocalDate today = LocalDate.now();
		for (Producte a : this.cataleg) {
			if (a instanceof ProducteLot) {
				ProducteLot d = (ProducteLot) a;
				for (LocalDate e : d.getLots().keySet()) {
					if (e.isBefore(today)) {
						c.put(a.getNom(), d.getLots().get(e));
					}
				}
			}
		}
		return c;
	}

	@Override
	public void eliminarCaducats() {
		List<LocalDate> z = new ArrayList<LocalDate>();
		LocalDate today = LocalDate.now();
		for (Producte a : this.cataleg) {
			if (a instanceof ProducteLot) {
				ProducteLot d = (ProducteLot) a;
				for (LocalDate e : d.getLots().keySet()) {
					if (e.isBefore(today)) {
						z.add(e);
					}
				}
				for(LocalDate e : z) {
					d.getLots().remove(e);
				}
				z.clear();
			}
		}
	}
}
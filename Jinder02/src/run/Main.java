package run;

import impl.Jinder;
import prob.Persona;
import prob.SEXE;

public class Main {

	public static void main(String[] args) {
		Jinder j = new Jinder();
		Persona p1 = new Persona("Daniel", 20, SEXE.M, SEXE.F);
		j.afegir(p1);		
		p1 = new Persona ("Ana", 23, SEXE.F, SEXE.M);
		j.afegir(p1);		
		p1 = new Persona ("Berta", 34, SEXE.F, SEXE.M);
		j.afegir(p1);		
		p1 = new Persona ("Carles", 44, SEXE.M, SEXE.F);
		j.afegir(p1);		
		p1 = new Persona ("Fran", 37, SEXE.M, SEXE.M);
		j.afegir(p1);		
		p1 = new Persona ("Gabriel", 47, SEXE.M, SEXE.M);
		j.afegir(p1);		
		p1 = new Persona ("Hipolit", 65, SEXE.M, SEXE.F);
		j.afegir(p1);		
		p1 = new Persona ("Ines", 40, SEXE.F, SEXE.M);
		j.afegir(p1);		
		
		System.out.println("Sistema:");
		System.out.println(j.toList());
		
		System.out.println("Candidats per a " + p1);
		System.out.println(j.candidats(p1));
		
		if (j.banejar(p1))
			System.out.println("He banejat " + p1.getNom() + " a la primera");
		else
			System.out.println("No he pogut banejar " + p1.getNom() + " a la primera");

		if (j.banejar(p1))
			System.out.println("He banejat " + p1.getNom() + " a la segona");
		else
			System.out.println("No he pogut banejar " + p1.getNom() + " a la segona");
		
		System.out.println("Banejats: " + j.banejatsToList());
		System.out.println("Sistema: " + j.toList());
}

}

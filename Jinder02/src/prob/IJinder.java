package prob;

import java.util.List;

public interface IJinder<Persona> {

	//Jinder 01
	/**
	 * 
	 * @param p Persona
	 * @return boolean
	 * 
	 * Si la persona ja existeix (hi ha una altra iguual), no l'afegeix i retorna fals
	 * Si la persona no existeix, però està banejada, no l'afegeix i retorna fals
	 * Si la persona no existeix, ni està banejada, afegeix la persona al sistema, com a última i retorna true
	 */
	boolean afegir (Persona p); 
	
	/**
	 * ordena les persones del nostre sistema
	 */
	void ordenar();
	/**
	 * @return Persona
	 * 
	 * Si el sistema conté persones, retorna la primera i l'elimina del nostre sistema
	 * Si el sistema no te persones, retorna null
	 */
	Persona treure ();
	
	/**
	 * Elimina totes les persones del nostre sistema i ho deixa tot buit
	 */
	void reset();
	/**
	 * Retorna el número de persones del nostre sistema
	 * @return
	 */
	int size();
	/**
	 * Retorna una llista amb totes les persones del nostre sistema
	 */
	List<Persona> toList();
	
	// Jinder 02
	
	/**
	 * Busca la Persona <p> a la estructura de candidats, 
	 * si la troba la treu i la fica fica com a banejada, retornant true
	 * si no la troba o ja està banejada, no fa res i retorna false 
	 * @return
	 */
	boolean banejar (Persona p);
	
	/**
	 * retorna una llista amb els banejats
	 * @return
	 */
	List<Persona> banejatsToList();
	
	/**	
	 * retorna amb la llista de persones candidades segons les preferències de la persona <p>
	 * seran aquelles del sexe que busca <p> i cal tenir també present el sexe que busca la persona candidata
	 * */
	List<Persona> candidats(Persona p);
}
package prob;


public class Persona implements Comparable<Persona> {
	private String nom;
	private int edat;
	private SEXE sexPropi;
	private SEXE sexBusca;

	public Persona(String nom, int edat, SEXE sexPropi, SEXE sexBusca) {
		this.nom = nom;
		this.edat = edat;
		this.sexPropi = sexPropi;
		this.sexBusca = sexBusca;
	}

	@Override
	public String toString() {
		return (nom + " " + edat + " sóc: " + this.sexPropi + " busco: " + this.sexBusca);
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public SEXE getSexPropi() {
		return sexPropi;
	}

	public void setSexPropi(SEXE sexPropi) {
		this.sexPropi = sexPropi;
	}

	public SEXE getSexBusca() {
		return sexBusca;
	}

	public void setSexBusca(SEXE sexBusca) {
		this.sexBusca = sexBusca;
	}

	@Override
	/**
	 * 
	 * @param that
	 * @return compara les persones pel nom i la edat. Les condiera iguals si tenen
	 *         el mateix nom i edat
	 */
	public int compareTo(Persona that) {
		if(that == null) {
			return 1;
		}else if(this.nom.compareTo(that.getNom())!=0) {
			return this.nom.compareTo(that.getNom());
		}else {
			if(this.edat!=that.getEdat()) {
				if(this.edat>that.getEdat())
					return 1;
				else if(this.edat<that.getEdat())
					return -1;
			}
		}
		return 0;
	}
	
	   /**
	   * Redefinició de equals. Considera dues persones iguals si tenen el mateix nom i edat
	   */
	   @Override public boolean equals(Object aThat) {
		   if(aThat == null) {
			   return false;
		   }else if(!(aThat instanceof Persona)){
			   return false;
		   }else {
			   Persona that = (Persona)aThat;
			   if(!(this.nom.equals(that.getNom()))) {
				   return false;
			   }else if(!(this.edat == that.getEdat())){
				   return false;
			   }else {
				   return true;
			   }
		   } 
	   }

	   /**
	   * A class that overrides equals must also override hashCode.
	   * Fa el hashCode coherent amb equals  (dos objectes iguals haurien de retornar el mateix hashCode
	   */
	   @Override public int hashCode() {
		   return (int) this.edat * this.nom.hashCode();
	   }
	   
}

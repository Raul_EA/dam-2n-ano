package impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import prob.IJinder;
import prob.Persona;

public class Jinder implements IJinder<Persona>{

	List<Persona> gente;
	Set<Persona> baneados;
	
	public Jinder() {
		this.gente = new ArrayList<Persona>();
		this.baneados = new HashSet<Persona>();
	}
	
	@Override
	public boolean afegir(Persona p) {
		for(Persona a : this.gente) {
			if(a.equals(p))
				return false;
		}
		this.gente.add(p);
		return true;
	}

	@Override
	public void ordenar() {
		this.gente.sort(null);
	}

	@Override
	public Persona treure() {
		if(this.gente.isEmpty())
			return null;
		else {
			Persona a = this.gente.get(0);
			this.gente.remove(0);
			return a;
		}
	}

	@Override
	public void reset() {
		this.gente.clear();
		this.gente.clear();
	}

	@Override
	public int size() {
		return this.gente.size();
	}

	@Override
	public List<Persona> toList() {
		return this.gente;
	}

	@Override
	public boolean banejar(Persona p) {
		for(Persona a : this.gente) {
			if(p.equals(a)) {
				this.gente.remove(p);
				this.baneados.add(p);
				return true;
			}
		}
		return false;
	}

	@Override
	public List<Persona> banejatsToList() {
		List<Persona> a = new ArrayList<>();
		a.addAll(baneados);
		return a;
	}

	@Override
	public List<Persona> candidats(Persona p) {
		List<Persona> a = new ArrayList<>();
		for(Persona b : this.gente) {
			if(p.getSexBusca()==b.getSexPropi() && p.getSexPropi()==b.getSexBusca()) {
				a.add(b);
			}
		}
		return a;
	}
}

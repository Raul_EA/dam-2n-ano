package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import prob.Persona;
import prob.SEXE;
import impl.Jinder;

class TestJinder2 {

	@Test
	void testJinderCandidats() {
		
		Jinder j = new Jinder();
		Persona p1 = new Persona("Daniel", 20, SEXE.M, SEXE.F);
		j.afegir(p1);		
		Persona p2 = new Persona ("Ana", 23, SEXE.F, SEXE.M);
		j.afegir(p2);		
		Persona p3 = new Persona ("Berta", 34, SEXE.F, SEXE.M);
		j.afegir(p3);		
		Persona p4 = new Persona ("Carles", 44, SEXE.M, SEXE.F);
		j.afegir(p4);		
		Persona p5 = new Persona ("Fran", 37, SEXE.M, SEXE.M);
		j.afegir(p5);		
		Persona p6 = new Persona ("Gabriel", 47, SEXE.M, SEXE.M);
		j.afegir(p6);		
		Persona p7 = new Persona ("Hipolit", 65, SEXE.M, SEXE.F);
		j.afegir(p7);		
		Persona p8 = new Persona ("Ines", 40, SEXE.F, SEXE.M);
		j.afegir(p8);		
		assertNotNull(j);
		List <Persona> c = j.candidats(p8);
		assertEquals(true, c.contains(p1));
		assertEquals(true, c.contains(p4));
		assertEquals(true, c.contains(p7));
		assertEquals(false, c.contains(p2));
		assertEquals(false, c.contains(p3));
		assertEquals(false, c.contains(p5));
		assertEquals(false, c.contains(p8));
	}
	
	
	@Test
	void testJinderBanejar() {
		Jinder j = new Jinder();
		Persona p1 = new Persona("Daniel", 20, SEXE.M, SEXE.F);
		j.afegir(p1);		
		Persona p2 = new Persona ("Ana", 23, SEXE.F, SEXE.M);
		j.afegir(p2);		
		Persona p3 = new Persona ("Berta", 34, SEXE.F, SEXE.M);
		j.afegir(p3);		
		Persona p4 = new Persona ("Carles", 44, SEXE.M, SEXE.F);
		j.afegir(p4);		
		Persona p5 = new Persona ("Fran", 37, SEXE.M, SEXE.M);
		j.afegir(p5);		
		Persona p6 = new Persona ("Gabriel", 47, SEXE.M, SEXE.M);
		j.afegir(p6);		
		Persona p7 = new Persona ("Hipolit", 65, SEXE.M, SEXE.F);
		j.afegir(p7);		
		Persona p8 = new Persona ("Ines", 40, SEXE.F, SEXE.M);
		j.afegir(p8);		

		j.banejar(p1);
		j.banejar(p1);
		j.banejar(p8);
		List <Persona> b = j.banejatsToList();
		assertEquals(true, b.contains(p1));
		assertEquals(false, b.contains(p4));
		assertEquals(false, b.contains(p7));
		assertEquals(false,b.contains(p2));
		assertEquals(false, b.contains(p3));
		assertEquals(false, b.contains(p5));
		assertEquals(true, b.contains(p8));
	}
}

package JSON;

public class Magic {
	private String name;
	private int mana;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getMana() {
		return mana;
	}
	public void setMana(int mana) {
		this.mana = mana;
	}
	
}

package JSON;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Character {
	private int lvl;
	private String name;
	@SerializedName(value="class")
	private String clase;
	private boolean isMain;
	private int HP;
	private int mana;
	private List<Magic> magics;

	public int getLvl() {
		return lvl;
	}
	public void setLvl(int lvl) {
		this.lvl = lvl;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getClase() {
		return clase;
	}
	public void setClase(String clase) {
		this.clase = clase;
	}

	public boolean isMain() {
		return isMain;
	}
	public void setMain(boolean isMain) {
		this.isMain = isMain;
	}

	public int getHp() {
		return HP;
	}
	public void setHp(int hp) {
		this.HP = hp;
	}

	public int getMana() {
		return mana;
	}
	public void setMana(int mana) {
		this.mana = mana;
	}

	public List<Magic> getMagics() {
		return magics;
	}
	public void setMagics(List<Magic> magics) {
		this.magics = magics;
	}

	
}

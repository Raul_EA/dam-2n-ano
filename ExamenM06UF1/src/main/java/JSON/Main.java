package JSON;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import Mokepon10.Entrada;
import Mokepon10.Mokedex;

public class Main {
	static File f = new File("party.json");
	
	public static void main(String[] args) {
		activarMain("ShadowHeart");
		restaurarMana("Alumne de DAM",100);
		Magic a = new Magic();
		a.setMana(300);
		a.setName("BOMBA  NUCLEAR");
		pujarNivell("Gale", a);
	}
	public static void activarMain(String nomChar) {
		try {
			JsonElement arrel = JsonParser.parseReader(new FileReader(f));
			JsonObject jsonObject = arrel.getAsJsonObject();
			JsonArray a = (JsonArray) jsonObject.get("characters");
			for(int i=0;i<a.size();i++) {
				JsonObject b = (JsonObject)a.get(i);
				b.addProperty("isMain", false);
			}
			for(int i=0;i<a.size();i++) {
				JsonObject b = (JsonObject)a.get(i);
				if(b.get("name").getAsString().equals(nomChar)) {
					if(b.get("isMain").getAsBoolean()==false) {
						b.addProperty("isMain", true);
					}
				}
			}
			
			Gson escribir = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
			FileWriter fw;
			
			try {
				fw = new FileWriter("activeMain.json");
				fw.append(escribir.toJson(arrel));
				fw.flush();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch(UnsupportedOperationException e) {
			System.err.println("Es nulo");
		}
	}
	public static void restaurarMana(String nomChar, int manaAfegit) {
		Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
		try {
			Party m = gson.fromJson(new FileReader(f), Party.class);
			
			for(Character a : m.getCharacters()) {
				if(a.getName().equals(nomChar)) {
					a.setMana(a.getMana()+manaAfegit);
				}
			}

			FileWriter fw = new FileWriter("restoredMana.json");
			fw.append(gson.toJson(m));
			fw.close();
			
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void pujarNivell(String nomChar, Magic magic) {
		Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
		try {
			Party m = gson.fromJson(new FileReader(f), Party.class);
			
			for(Character a : m.getCharacters()) {
				if(a.getName().equals(nomChar)) {
					a.setLvl(a.getLvl()+1);
					a.setHp(a.getHp()+40);
					a.setMana(a.getMana()+15);
					a.getMagics().add(magic);
				}
			}

			FileWriter fw = new FileWriter("nextLevel.json");
			fw.append(gson.toJson(m));
			fw.close();
			
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

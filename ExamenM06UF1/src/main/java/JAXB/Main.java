package JAXB;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


public class Main {
	public static File f = new File("party.xml");

	public static void main(String[] args) {
		Item i=new Item();
		i.setId(4);
		i.setName("Palo");
		i.setDescription("Un palo grande");
		i.setQuantity(88);
		i.setType("Weapon");
		afegirItem(i);
		
		utilitzarItem(1);
		utilitzarMagia("Alumne de DAM","Remove curse" );
	}
	public static void afegirItem(Item i) {
		File f2 = new File("addItem.xml");
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Party.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Party p = (Party) jaxbUnmarshaller.unmarshal(f);
			
			p.getInventory().add(i);
			
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(p, f2);

		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}
	public static void utilitzarItem(int itemId) {
		File f2 = new File("useItem.xml");
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Party.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Party p = (Party) jaxbUnmarshaller.unmarshal(f);
			
			for(Item a : p.getInventory()) {
				if(a.getId()==itemId && a.getQuantity()>0) {
					a.setQuantity(a.getQuantity()-1);
					System.out.println(a.getName());
				}
			}
			
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(p, f2);

		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}
	public static void utilitzarMagia(String nomCharacter, String nomMagia) {
		File f2 = new File("useMagic.xml");
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Party.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Party p = (Party) jaxbUnmarshaller.unmarshal(f);
			
			for(Character a : p.getCharacters()) {
				if(a.getName().equals(nomCharacter)) {
					for(Magic b : a.getMagics()) {
						if(b.getName().equals(nomMagia)) {
							if(a.getMana()>=b.getMana()) {
								System.out.println(b.getName());
								a.setMana(a.getMana()-b.getMana());
							}
						}
					}
				}
			}
			
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(p, f2);

		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}
}
/*
File f2 = new File("addItem.xml");
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Party.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Party p = (Party) jaxbUnmarshaller.unmarshal(f);
			
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(p, f2);

		} catch (JAXBException je) {
			je.printStackTrace();
		}
*/
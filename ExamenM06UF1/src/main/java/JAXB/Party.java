package JAXB;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="party")
@XmlType(propOrder = {"characters","inventory"})
public class Party {
	List<Character> characters;
	List<Item> inventory;
	
	@XmlElementWrapper(name="characters")
	@XmlElement(name="character")
	public List<Character> getCharacters() {
		return characters;
	}
	public void setCharacters(List<Character> characters) {
		this.characters = characters;
	}@XmlElementWrapper(name="inventory")
	@XmlElement(name="item")
	public List<Item> getInventory() {
		return inventory;
	}
	public void setInventory(List<Item> inventory) {
		this.inventory = inventory;
	}
	@Override
	public String toString() {
		return "Party [characters=" + characters + ", inventory=" + inventory + "]";
	}
	
}

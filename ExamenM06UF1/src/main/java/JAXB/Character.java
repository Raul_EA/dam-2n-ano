package JAXB;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"name","clase","main","hp","mana","magics"})
public class Character {
	private int lvl;
	private String name;
	private String clase;
	private boolean main;
	private int hp;
	private int mana;
	private List<Magic> magics;
	@XmlAttribute()
	public int getLvl() {
		return lvl;
	}
	public void setLvl(int lvl) {
		this.lvl = lvl;
	}
	@XmlElement(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name="class")
	public String getClase() {
		return clase;
	}
	public void setClase(String clase) {
		this.clase = clase;
	}
	@XmlElement(name="isMain")
	public boolean isMain() {
		return main;
	}
	public void setMain(boolean isMain) {
		this.main = isMain;
	}
	@XmlElement(name="HP")
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	@XmlElement(name="mana")
	public int getMana() {
		return mana;
	}
	public void setMana(int mana) {
		this.mana = mana;
	}
	@XmlElementWrapper(name="magics")
	@XmlElement(name="magic")
	public List<Magic> getMagics() {
		return magics;
	}
	public void setMagics(List<Magic> magics) {
		this.magics = magics;
	}
	@Override
	public String toString() {
		return "Character [lvl=" + lvl + ", name=" + name + ", clase=" + clase + ", isMain=" + main + ", hp=" + hp
				+ ", mana=" + mana + ", magics=" + magics + "]";
	}
	
}

package factura_bis;

public class Programa {

	public static void main(String[] args) {

		Factura f = new Factura(1, "ordenador", 500.0);
		LineaFactura linea1 = new LineaFactura(1, 200.0, "cpu");
		LineaFactura linea2 = new LineaFactura(2, 300.0, "disc");
		f.addLinea(linea1);
		f.addLinea(linea2);

		Factura copia = null;
		try {
			copia = (Factura) f.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		copia.setNumero(2);

		System.out.println("abans d'afegir una nova línia ...:");
		System.out.println(f);
		System.out.println();
		System.out.println(copia);

		f.setImporte(f.getImporte() * 2);
		f.addLinea(new LineaFactura(3, 20.0, "ventilador"));
		f.getLineas().get(0).setImporte(999);
		System.out.println("\n\ndesprés d'afegir una nova línia i actualitzar una de les línies...:");
		System.out.println(f);
		System.out.println();
		System.out.println(copia);

		System.out.println("Ta arreglao");
	}
}
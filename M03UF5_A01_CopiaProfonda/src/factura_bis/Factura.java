package factura_bis;

import java.util.ArrayList;
import java.util.List;

public class Factura implements Cloneable {

	private int numero;
	private String concepto;
	private double importe;
	private List<LineaFactura> lineas = new ArrayList<LineaFactura>();

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}

	public List<LineaFactura> getLineas() {
		return lineas;
	}

	public void setLineas(List<LineaFactura> lineas) {
		this.lineas = lineas;
	}

	public Factura(int numero, String concepto, double importe) {
		super();
		this.numero = numero;
		this.concepto = concepto;
		this.importe = importe;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		Factura nueva = new Factura(this.numero, this.concepto, this.importe);
		
		nueva.lineas = new ArrayList<LineaFactura>(); 
		for(int i=0;i<this.getLineas().size();i++) {
			nueva.lineas.add(new LineaFactura(this.lineas.get(i).getNumero(),this.lineas.get(i).getImporte(),this.lineas.get(i).getConcepto()));
		}
		
		return nueva;
	}

	public void addLinea(LineaFactura linea) {

		this.lineas.add(linea);
	}

	public String toString() {
		String cadena = "";
		cadena = " factura nº: " + this.getNumero();
		cadena = cadena + " concepto: " + this.getConcepto();
		cadena = cadena + " importe: " + this.getImporte();

		for (LineaFactura lf : this.getLineas()) {

			cadena = cadena + "\n línea: " + lf.getNumero();
			cadena = cadena + " - concepto - " + lf.getConcepto();
			cadena = cadena + " - importe: " + lf.getImporte();
		}
		return cadena;
	}
}
/*
 * Còpia Profonda ... cal que omplis els mètodes que estan a TODO per tal que funcioni correctament
 */

package poligons;

public class Main {

	public static void main(String[] args) {

		Poligon p = new Poligon(4);

		p.addSegment(1, 0, 3, 0);
		p.addSegment(3, 0, 4, 2);
		p.addSegment(4, 2, 3, 5);
		p.addSegment(3, 5, 1, 0);

		Poligon p2 = new Poligon(p);

		System.out.println("*** Inici *** p ***: \n" + p);
		System.out.println("*** Inici *** p2 ***: \n" + p2);

		//p.segments[0].p0.setX(99);
		//p.segments[0].p0.setY(99);

		p.girarPoligon();
		System.out.println("*** Girar *** p ***: \n" + p);
		System.out.println("*** Inici *** p2 ***: \n" + p2);
	}
}

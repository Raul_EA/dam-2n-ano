package poligons;

public class Poligon {
    public Segment [] segments;
    public int num_segments;
    public int max_segments;

    public Poligon(int num_segments) {
        max_segments = num_segments;
        segments = new Segment [max_segments];
    }

    public void addSegment(Segment s) {
    	if(this.num_segments<this.max_segments) {
    		this.num_segments++;
    		Segment seg = new Segment(s);
    		this.segments[this.num_segments]=seg;
    	}else {
    		System.out.println("No se puede añadir mas segmentos");
    	}
    }

    public void addSegment(int x0, int y0, int x1, int y1) {
    	if(this.num_segments<this.max_segments) {
    		this.num_segments++;
    		Segment seg = new Segment(x0,y0,x1,y1);
    		this.segments[this.num_segments-1]=seg;
    	}else {
    		System.out.println("No se puede añadir mas segmentos");
    	}
    }

    public String toString() {
        String str = new String("");
        for (int i=0; i<num_segments; i++) {
            str += segments[i].toString() + "\n";
        }
        return str;
    }
    
    public void girarPoligon() {
    	for(int i=0; i<num_segments;i++) {
    		segments[i].getPunt0().girar();
    		segments[i].getPunt1().girar();
    	}
    }
    
  
    public Poligon(Poligon p) {
    	this.max_segments=p.max_segments;
    	this.num_segments=p.num_segments;
    	segments = new Segment [max_segments];
    	for(int i=0;i<max_segments;i++) {
    		this.segments[i]=new Segment(p.segments[i]);
    	}
    }
    
}

package factura;

public class Factura implements Cloneable {

  private int numero;
  private String concepto;
  private double importe;
  
  public int getNumero() {
    return numero;
  }
  public void setNumero(int numero) {
    this.numero = numero;
  }
  public String getConcepto() {
    return concepto;
  }
  public void setConcepto(String concepto) {
    this.concepto = concepto;
  }
  public double getImporte() {
    return importe;
  }
  public void setImporte(double importe) {
    this.importe = importe;
  }
  public Factura(int numero, String concepto, double importe) {
    super();
    this.numero = numero;
    this.concepto = concepto;
    this.importe = importe;
  }
  
  @Override
  protected Object clone() throws CloneNotSupportedException {
  
    Factura nueva= new Factura (this.numero,this.concepto,this.importe);
    return nueva;
  }
  
  
}
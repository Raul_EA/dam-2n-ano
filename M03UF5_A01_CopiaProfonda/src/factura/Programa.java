package factura;

public class Programa{

  public static void main(String[] args) {

    Factura f= new Factura(1,"ordenador",500);
    try {
      Factura nueva=(Factura)f.clone();
      
      nueva.setNumero(2);
      
      System.out.println(f.getNumero());
      System.out.println(f.getConcepto());
      System.out.println(f.getImporte());
      
      System.out.println("****************");
      
      System.out.println(nueva.getNumero());
      System.out.println(nueva.getConcepto());
      System.out.println(nueva.getImporte());
    } catch (CloneNotSupportedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
  }
}
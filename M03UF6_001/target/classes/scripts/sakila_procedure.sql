CREATE DEFINER=`root`@`localhost` PROCEDURE `sakila`.`film_in_stock`(IN p_film_id INT, IN p_store_id INT, OUT p_film_count INT)
    READS SQL DATA
begin
     SELECT inventory_id
     FROM inventory
     WHERE film_id = p_film_id
     AND store_id = p_store_id;

     SELECT FOUND_ROWS() INTO p_film_count;
END
--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1
-- Dumped by pg_dump version 15.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: ies23; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE ies23 WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'Catalan_Spain.1252';


ALTER DATABASE ies23 OWNER TO postgres;

\connect ies23

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: editar_qualificacions(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.editar_qualificacions() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare 
  -- qAlum usuaris;
     qAlum record;
     qActiu int;
begin
	if (old is null) then
		raise notice 'inserint!!!';
	else
		raise notice 'modificant!!!';
	end if;

	select u.actiu into qActiu from usuaris u where idusuari = new.idalumne;
    if qActiu = 0  then 
       	raise notice 'alumne % no actiu', new.idalumne;
       	return old;	-- rebutja canvi
     else 
     	raise notice 'alumne % actiu', new.idalumne;
        return new;   -- accepta canvi
    end if;
end;
$$;


ALTER FUNCTION public.editar_qualificacions() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: alumnes_moduls; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.alumnes_moduls (
    id bigint NOT NULL,
    idalumne integer,
    idmodul integer,
    nota integer,
    data date
);


ALTER TABLE public.alumnes_moduls OWNER TO postgres;

--
-- Name: alumnes_moduls_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.alumnes_moduls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alumnes_moduls_id_seq OWNER TO postgres;

--
-- Name: alumnes_moduls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.alumnes_moduls_id_seq OWNED BY public.alumnes_moduls.id;


--
-- Name: aules; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aules (
    id integer NOT NULL,
    nomaula character varying(10) DEFAULT NULL::character varying,
    capacitat integer
);


ALTER TABLE public.aules OWNER TO postgres;

--
-- Name: cicles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cicles (
    id integer NOT NULL,
    nom character varying(10) NOT NULL,
    descripcio character varying(50) DEFAULT NULL::character varying,
    nivell character varying(1) DEFAULT 'S'::character varying
);


ALTER TABLE public.cicles OWNER TO postgres;

--
-- Name: grups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grups (
    id integer NOT NULL,
    nom character varying(10) NOT NULL,
    tutor integer,
    cicle integer,
    curs integer
);


ALTER TABLE public.grups OWNER TO postgres;

--
-- Name: grupsunitatsformatives; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grupsunitatsformatives (
    id bigint NOT NULL,
    idgrup integer,
    idunitatformativa integer,
    idprofessor integer,
    idaula integer
);


ALTER TABLE public.grupsunitatsformatives OWNER TO postgres;

--
-- Name: grupsunitatsformatives_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.grupsunitatsformatives_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grupsunitatsformatives_id_seq OWNER TO postgres;

--
-- Name: grupsunitatsformatives_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.grupsunitatsformatives_id_seq OWNED BY public.grupsunitatsformatives.id;


--
-- Name: log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.log (
    idlog integer NOT NULL,
    data date DEFAULT now(),
    hora time without time zone DEFAULT now(),
    idusuari integer,
    missatge character varying(100),
    error boolean DEFAULT false
);


ALTER TABLE public.log OWNER TO postgres;

--
-- Name: log_idlog_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.log ALTER COLUMN idlog ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.log_idlog_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: moduls; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.moduls (
    modul integer NOT NULL,
    hores integer NOT NULL,
    nom character varying(40) NOT NULL,
    cicle integer NOT NULL,
    id integer NOT NULL
);


ALTER TABLE public.moduls OWNER TO postgres;

--
-- Name: qualificacions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qualificacions (
    id bigint NOT NULL,
    idalumne integer,
    idunitatformativa integer,
    idprofessor integer,
    nota1c integer,
    nota2c integer
);


ALTER TABLE public.qualificacions OWNER TO postgres;

--
-- Name: qualificacions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.qualificacions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.qualificacions_id_seq OWNER TO postgres;

--
-- Name: qualificacions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.qualificacions_id_seq OWNED BY public.qualificacions.id;


--
-- Name: tasques; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tasques (
    id integer NOT NULL,
    idunitatformativa integer NOT NULL,
    nomtasca character varying(20) DEFAULT NULL::character varying,
    descripcio character varying(100) DEFAULT NULL::character varying,
    hores integer,
    obligatoria smallint
);


ALTER TABLE public.tasques OWNER TO postgres;

--
-- Name: tasques_alumnes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tasques_alumnes (
    id bigint NOT NULL,
    idtasca integer NOT NULL,
    idalumne integer NOT NULL,
    nota integer,
    observacions character varying(100) DEFAULT NULL::character varying
);


ALTER TABLE public.tasques_alumnes OWNER TO postgres;

--
-- Name: tasques_alumnes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tasques_alumnes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasques_alumnes_id_seq OWNER TO postgres;

--
-- Name: tasques_alumnes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tasques_alumnes_id_seq OWNED BY public.tasques_alumnes.id;


--
-- Name: unitatsformatives; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unitatsformatives (
    id integer NOT NULL,
    idmodul integer,
    unitat integer,
    hores integer
);


ALTER TABLE public.unitatsformatives OWNER TO postgres;

--
-- Name: usuari_cicle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuari_cicle (
    idusuari smallint NOT NULL,
    cicle integer NOT NULL,
    notafinal real,
    data date
);


ALTER TABLE public.usuari_cicle OWNER TO postgres;

--
-- Name: usuaris; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuaris (
    idusuari bigint NOT NULL,
    nomusuari character varying(20) DEFAULT NULL::character varying,
    cognom1usuari character varying(20) DEFAULT NULL::character varying,
    emailusuari character varying(30) DEFAULT NULL::character varying,
    password character varying(20) DEFAULT NULL::character varying,
    idgrup integer,
    rol character(1) DEFAULT 'A'::bpchar,
    datanaixement date,
    actiu smallint DEFAULT 1,
    repetidor smallint DEFAULT 0,
    cognom2usuari character varying(20) DEFAULT NULL::character varying,
    bloquejat smallint DEFAULT 0,
    intents integer DEFAULT 0 NOT NULL,
    sexe character(1),
    ultimacces date
);


ALTER TABLE public.usuaris OWNER TO postgres;

--
-- Name: usuaris_idusuari_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuaris_idusuari_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuaris_idusuari_seq OWNER TO postgres;

--
-- Name: usuaris_idusuari_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuaris_idusuari_seq OWNED BY public.usuaris.idusuari;


--
-- Name: alumnes_moduls id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.alumnes_moduls ALTER COLUMN id SET DEFAULT nextval('public.alumnes_moduls_id_seq'::regclass);


--
-- Name: grupsunitatsformatives id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grupsunitatsformatives ALTER COLUMN id SET DEFAULT nextval('public.grupsunitatsformatives_id_seq'::regclass);


--
-- Name: qualificacions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qualificacions ALTER COLUMN id SET DEFAULT nextval('public.qualificacions_id_seq'::regclass);


--
-- Name: tasques_alumnes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasques_alumnes ALTER COLUMN id SET DEFAULT nextval('public.tasques_alumnes_id_seq'::regclass);


--
-- Name: usuaris idusuari; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuaris ALTER COLUMN idusuari SET DEFAULT nextval('public.usuaris_idusuari_seq'::regclass);


--
-- Data for Name: alumnes_moduls; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.alumnes_moduls (id, idalumne, idmodul, nota, data) FROM stdin;
514	173	1	5	2022-03-04
515	174	1	6	2022-03-04
516	175	1	7	2022-03-04
517	176	1	7	2022-03-04
519	178	1	6	2022-03-04
522	181	1	9	2022-03-04
523	182	1	10	2022-03-04
524	183	1	9	2022-03-04
525	184	1	5	2022-03-04
526	185	1	9	2022-03-04
527	186	1	9	2022-03-04
528	187	1	8	2022-03-04
531	267	1	5	2022-03-04
534	172	2	5	2022-03-04
535	173	2	10	2022-03-04
536	174	2	5	2022-03-04
537	175	2	6	2022-03-04
538	176	2	5	2022-03-04
539	177	2	5	2022-03-04
540	178	2	10	2022-03-04
543	181	2	9	2022-03-04
545	183	2	9	2022-03-04
546	184	2	6	2022-03-04
548	186	2	9	2022-03-04
550	265	2	8	2022-03-04
551	266	2	5	2022-03-04
555	172	3	6	2022-03-04
556	173	3	6	2022-03-04
557	174	3	10	2022-03-04
561	178	3	8	2022-03-04
562	179	3	7	2022-03-04
564	181	3	10	2022-03-04
566	183	3	6	2022-03-04
567	184	3	9	2022-03-04
568	185	3	7	2022-03-04
569	186	3	6	2022-03-04
570	187	3	8	2022-03-04
573	267	3	9	2022-03-04
574	268	3	9	2022-03-04
579	175	4	7	2022-03-04
583	179	4	7	2022-03-04
584	180	4	7	2022-03-04
586	182	4	9	2022-03-04
588	184	4	9	2022-03-04
592	265	4	9	2022-03-04
593	266	4	8	2022-03-04
599	174	5	8	2022-03-04
600	175	5	8	2022-03-04
601	176	5	7	2022-03-04
602	177	5	10	2022-03-04
603	178	5	8	2022-03-04
604	179	5	8	2022-03-04
605	180	5	5	2022-03-04
606	181	5	10	2022-03-04
608	183	5	10	2022-03-04
609	184	5	7	2022-03-04
611	186	5	8	2022-03-04
612	187	5	8	2022-03-04
613	265	5	5	2022-03-04
614	266	5	10	2022-03-04
615	267	5	6	2022-03-04
616	268	5	10	2022-03-04
618	172	6	6	2022-03-04
619	173	6	7	2022-03-04
621	175	6	6	2022-03-04
623	177	6	9	2022-03-04
624	178	6	9	2022-03-04
625	179	6	10	2022-03-04
626	180	6	9	2022-03-04
627	181	6	6	2022-03-04
633	187	6	8	2022-03-04
634	265	6	8	2022-03-04
635	266	6	7	2022-03-04
636	267	6	10	2022-03-04
637	268	6	8	2022-03-04
638	171	7	9	2022-03-04
639	172	7	9	2022-03-04
640	173	7	10	2022-03-04
642	175	7	5	2022-03-04
643	176	7	10	2022-03-04
644	177	7	7	2022-03-04
646	179	7	7	2022-03-04
647	180	7	10	2022-03-04
648	181	7	7	2022-03-04
649	182	7	6	2022-03-04
650	183	7	8	2022-03-04
653	186	7	6	2022-03-04
654	187	7	8	2022-03-04
656	266	7	5	2022-03-04
657	267	7	9	2022-03-04
658	268	7	7	2022-03-04
659	171	8	10	2022-03-04
660	172	8	10	2022-03-04
661	173	8	7	2022-03-04
663	175	8	8	2022-03-04
664	176	8	10	2022-03-04
667	179	8	10	2022-03-04
668	180	8	10	2022-03-04
669	181	8	9	2022-03-04
670	182	8	6	2022-03-04
671	183	8	9	2022-03-04
672	184	8	9	2022-03-04
673	185	8	7	2022-03-04
674	186	8	7	2022-03-04
676	265	8	5	2022-03-04
677	266	8	6	2022-03-04
680	171	9	8	2022-03-04
681	172	9	10	2022-03-04
682	173	9	5	2022-03-04
683	174	9	5	2022-03-04
684	175	9	8	2022-03-04
685	176	9	7	2022-03-04
686	177	9	10	2022-03-04
687	178	9	9	2022-03-04
688	179	9	6	2022-03-04
690	181	9	6	2022-03-04
691	182	9	9	2022-03-04
696	187	9	7	2022-03-04
698	266	9	8	2022-03-04
699	267	9	9	2022-03-04
701	171	10	6	2022-03-04
702	172	10	7	2022-03-04
703	173	10	8	2022-03-04
704	174	10	7	2022-03-04
706	176	10	7	2022-03-04
707	177	10	9	2022-03-04
708	178	10	6	2022-03-04
709	179	10	9	2022-03-04
710	180	10	8	2022-03-04
713	183	10	6	2022-03-04
714	184	10	6	2022-03-04
716	186	10	8	2022-03-04
718	265	10	8	2022-03-04
719	266	10	8	2022-03-04
720	267	10	5	2022-03-04
721	268	10	10	2022-03-04
723	172	12	7	2022-03-04
725	174	12	9	2022-03-04
727	176	12	8	2022-03-04
729	178	12	8	2022-03-04
730	179	12	9	2022-03-04
732	181	12	7	2022-03-04
733	182	12	9	2022-03-04
734	183	12	6	2022-03-04
736	185	12	7	2022-03-04
737	186	12	10	2022-03-04
738	187	12	9	2022-03-04
739	265	12	7	2022-03-04
742	268	12	10	2022-03-04
743	171	13	9	2022-03-04
744	172	13	5	2022-03-04
745	173	13	6	2022-03-04
746	174	13	6	2022-03-04
748	176	13	5	2022-03-04
750	178	13	9	2022-03-04
753	181	13	7	2022-03-04
754	182	13	9	2022-03-04
756	184	13	6	2022-03-04
758	186	13	6	2022-03-04
759	187	13	8	2022-03-04
761	266	13	10	2022-03-04
762	267	13	8	2022-03-04
763	268	13	10	2022-03-04
764	171	14	5	2022-03-04
765	172	14	6	2022-03-04
769	176	14	10	2022-03-04
771	178	14	9	2022-03-04
772	179	14	10	2022-03-04
774	181	14	8	2022-03-04
775	182	14	9	2022-03-04
776	183	14	10	2022-03-04
779	186	14	5	2022-03-04
780	187	14	5	2022-03-04
784	268	14	8	2022-03-04
785	171	15	6	2022-03-04
786	172	15	5	2022-03-04
787	173	15	9	2022-03-04
788	174	15	7	2022-03-04
789	175	15	9	2022-03-04
791	177	15	9	2022-03-04
793	179	15	5	2022-03-04
794	180	15	10	2022-03-04
795	181	15	7	2022-03-04
797	183	15	9	2022-03-04
799	185	15	8	2022-03-04
801	187	15	10	2022-03-04
802	265	15	6	2022-03-04
803	266	15	9	2022-03-04
804	267	15	5	2022-03-04
805	268	15	8	2022-03-04
806	171	16	7	2022-03-04
807	172	16	9	2022-03-04
810	175	16	9	2022-03-04
811	176	16	5	2022-03-04
812	177	16	6	2022-03-04
813	178	16	6	2022-03-04
815	180	16	5	2022-03-04
816	181	16	6	2022-03-04
818	183	16	6	2022-03-04
820	185	16	7	2022-03-04
822	187	16	8	2022-03-04
823	265	16	8	2022-03-04
824	266	16	6	2022-03-04
825	267	16	6	2022-03-04
826	268	16	9	2022-03-04
827	244	1	7	2022-03-04
828	273	1	10	2022-03-04
829	274	1	7	2022-03-04
830	275	1	5	2022-03-04
833	278	1	6	2022-03-04
834	279	1	7	2022-03-04
835	280	1	7	2022-03-04
837	282	1	6	2022-03-04
838	283	1	5	2022-03-04
839	284	1	6	2022-03-04
840	285	1	6	2022-03-04
842	287	1	6	2022-03-04
843	288	1	5	2022-03-04
844	289	1	8	2022-03-04
845	290	1	6	2022-03-04
847	292	1	7	2022-03-04
852	275	2	8	2022-03-04
853	276	2	9	2022-03-04
854	277	2	9	2022-03-04
855	278	2	7	2022-03-04
856	279	2	8	2022-03-04
857	280	2	9	2022-03-04
858	281	2	10	2022-03-04
860	283	2	6	2022-03-04
862	285	2	5	2022-03-04
863	286	2	10	2022-03-04
864	287	2	7	2022-03-04
868	291	2	7	2022-03-04
869	292	2	9	2022-03-04
872	273	3	9	2022-03-04
873	274	3	9	2022-03-04
874	275	3	7	2022-03-04
875	276	3	8	2022-03-04
876	277	3	8	2022-03-04
877	278	3	5	2022-03-04
879	280	3	9	2022-03-04
881	282	3	10	2022-03-04
882	283	3	6	2022-03-04
883	284	3	7	2022-03-04
884	285	3	7	2022-03-04
885	286	3	5	2022-03-04
889	290	3	8	2022-03-04
890	291	3	7	2022-03-04
891	292	3	9	2022-03-04
894	273	4	10	2022-03-04
895	274	4	10	2022-03-04
897	276	4	10	2022-03-04
899	278	4	5	2022-03-04
901	280	4	5	2022-03-04
902	281	4	6	2022-03-04
903	282	4	8	2022-03-04
904	283	4	8	2022-03-04
905	284	4	8	2022-03-04
906	285	4	6	2022-03-04
907	286	4	5	2022-03-04
908	287	4	8	2022-03-04
911	290	4	8	2022-03-04
912	291	4	9	2022-03-04
913	292	4	8	2022-03-04
915	244	5	10	2022-03-04
918	275	5	9	2022-03-04
919	276	5	5	2022-03-04
920	277	5	5	2022-03-04
921	278	5	10	2022-03-04
923	280	5	5	2022-03-04
924	281	5	6	2022-03-04
926	283	5	8	2022-03-04
927	284	5	8	2022-03-04
928	285	5	8	2022-03-04
929	286	5	6	2022-03-04
930	287	5	6	2022-03-04
931	288	5	9	2022-03-04
932	289	5	10	2022-03-04
933	290	5	9	2022-03-04
934	291	5	7	2022-03-04
935	292	5	5	2022-03-04
936	330	5	6	2022-03-04
939	274	6	5	2022-03-04
940	275	6	7	2022-03-04
941	276	6	8	2022-03-04
945	280	6	6	2022-03-04
946	281	6	7	2022-03-04
947	282	6	6	2022-03-04
948	283	6	9	2022-03-04
950	285	6	5	2022-03-04
953	288	6	8	2022-03-04
956	291	6	7	2022-03-04
959	244	7	10	2022-03-04
961	274	7	9	2022-03-04
962	275	7	6	2022-03-04
963	276	7	6	2022-03-04
964	277	7	10	2022-03-04
966	279	7	6	2022-03-04
967	280	7	8	2022-03-04
971	284	7	8	2022-03-04
973	286	7	10	2022-03-04
974	287	7	10	2022-03-04
975	288	7	9	2022-03-04
978	291	7	10	2022-03-04
979	292	7	5	2022-03-04
983	274	8	8	2022-03-04
985	276	8	10	2022-03-04
986	277	8	9	2022-03-04
987	278	8	5	2022-03-04
988	279	8	9	2022-03-04
989	280	8	8	2022-03-04
990	281	8	5	2022-03-04
991	282	8	7	2022-03-04
993	284	8	6	2022-03-04
995	286	8	8	2022-03-04
996	287	8	10	2022-03-04
997	288	8	5	2022-03-04
998	289	8	6	2022-03-04
999	290	8	5	2022-03-04
1000	291	8	7	2022-03-04
1001	292	8	9	2022-03-04
1003	244	9	10	2022-03-04
1004	273	9	6	2022-03-04
1007	276	9	6	2022-03-04
1008	277	9	7	2022-03-04
1009	278	9	8	2022-03-04
1010	279	9	8	2022-03-04
1011	280	9	6	2022-03-04
1012	281	9	6	2022-03-04
1013	282	9	9	2022-03-04
1014	283	9	7	2022-03-04
1015	284	9	10	2022-03-04
1016	285	9	7	2022-03-04
1018	287	9	10	2022-03-04
1019	288	9	8	2022-03-04
1020	289	9	7	2022-03-04
1024	330	9	9	2022-03-04
1026	273	10	8	2022-03-04
1027	274	10	7	2022-03-04
1028	275	10	9	2022-03-04
1032	279	10	10	2022-03-04
1033	280	10	6	2022-03-04
1034	281	10	9	2022-03-04
1035	282	10	9	2022-03-04
1036	283	10	6	2022-03-04
1037	284	10	5	2022-03-04
1040	287	10	5	2022-03-04
1041	288	10	5	2022-03-04
1043	290	10	7	2022-03-04
1046	330	10	5	2022-03-04
1048	273	12	9	2022-03-04
1049	274	12	7	2022-03-04
1050	275	12	6	2022-03-04
1051	276	12	9	2022-03-04
1052	277	12	9	2022-03-04
1053	278	12	5	2022-03-04
1054	279	12	6	2022-03-04
1055	280	12	6	2022-03-04
1056	281	12	10	2022-03-04
1059	284	12	9	2022-03-04
1061	286	12	8	2022-03-04
1064	289	12	6	2022-03-04
1069	244	13	8	2022-03-04
1070	273	13	9	2022-03-04
1071	274	13	8	2022-03-04
1074	277	13	6	2022-03-04
1075	278	13	9	2022-03-04
1076	279	13	6	2022-03-04
1078	281	13	10	2022-03-04
1079	282	13	6	2022-03-04
1080	283	13	7	2022-03-04
1081	284	13	8	2022-03-04
1082	285	13	7	2022-03-04
1085	288	13	8	2022-03-04
1086	289	13	9	2022-03-04
1087	290	13	10	2022-03-04
1089	292	13	7	2022-03-04
1091	244	14	7	2022-03-04
1093	274	14	7	2022-03-04
1095	276	14	5	2022-03-04
1096	277	14	6	2022-03-04
1098	279	14	8	2022-03-04
1099	280	14	9	2022-03-04
1100	281	14	9	2022-03-04
1101	282	14	9	2022-03-04
1102	283	14	6	2022-03-04
1107	288	14	5	2022-03-04
1108	289	14	6	2022-03-04
1109	290	14	6	2022-03-04
1110	291	14	8	2022-03-04
1111	292	14	5	2022-03-04
1112	330	14	10	2022-03-04
1118	277	15	10	2022-03-04
1119	278	15	5	2022-03-04
1120	279	15	8	2022-03-04
1122	281	15	10	2022-03-04
1123	282	15	9	2022-03-04
1124	283	15	6	2022-03-04
1127	286	15	9	2022-03-04
1128	287	15	6	2022-03-04
1131	290	15	6	2022-03-04
1132	291	15	9	2022-03-04
1133	292	15	9	2022-03-04
1134	330	15	8	2022-03-04
1138	275	16	7	2022-03-04
1139	276	16	10	2022-03-04
1140	277	16	8	2022-03-04
1141	278	16	10	2022-03-04
1142	279	16	6	2022-03-04
1143	280	16	8	2022-03-04
1144	281	16	10	2022-03-04
1145	282	16	9	2022-03-04
1147	284	16	10	2022-03-04
1148	285	16	8	2022-03-04
1149	286	16	9	2022-03-04
1150	287	16	10	2022-03-04
1152	289	16	8	2022-03-04
1153	290	16	5	2022-03-04
1154	291	16	10	2022-03-04
1155	292	16	5	2022-03-04
1156	330	16	5	2022-03-04
512	171	1	\N	2022-03-04
513	172	1	\N	2022-03-04
518	177	1	\N	2022-03-04
520	179	1	\N	2022-03-04
521	180	1	\N	2022-03-04
529	265	1	\N	2022-03-04
530	266	1	\N	2022-03-04
532	268	1	\N	2022-03-04
533	171	2	\N	2022-03-04
541	179	2	\N	2022-03-04
542	180	2	\N	2022-03-04
544	182	2	\N	2022-03-04
547	185	2	\N	2022-03-04
549	187	2	\N	2022-03-04
552	267	2	\N	2022-03-04
553	268	2	\N	2022-03-04
554	171	3	\N	2022-03-04
558	175	3	\N	2022-03-04
559	176	3	\N	2022-03-04
560	177	3	\N	2022-03-04
563	180	3	\N	2022-03-04
565	182	3	\N	2022-03-04
571	265	3	\N	2022-03-04
572	266	3	\N	2022-03-04
575	171	4	\N	2022-03-04
576	172	4	\N	2022-03-04
577	173	4	\N	2022-03-04
578	174	4	\N	2022-03-04
580	176	4	\N	2022-03-04
581	177	4	\N	2022-03-04
582	178	4	\N	2022-03-04
585	181	4	\N	2022-03-04
587	183	4	\N	2022-03-04
589	185	4	\N	2022-03-04
590	186	4	\N	2022-03-04
591	187	4	\N	2022-03-04
594	267	4	\N	2022-03-04
595	268	4	\N	2022-03-04
596	171	5	\N	2022-03-04
597	172	5	\N	2022-03-04
598	173	5	\N	2022-03-04
607	182	5	\N	2022-03-04
610	185	5	\N	2022-03-04
617	171	6	\N	2022-03-04
620	174	6	\N	2022-03-04
622	176	6	\N	2022-03-04
628	182	6	\N	2022-03-04
629	183	6	\N	2022-03-04
630	184	6	\N	2022-03-04
631	185	6	\N	2022-03-04
632	186	6	\N	2022-03-04
641	174	7	\N	2022-03-04
645	178	7	\N	2022-03-04
651	184	7	\N	2022-03-04
652	185	7	\N	2022-03-04
655	265	7	\N	2022-03-04
662	174	8	\N	2022-03-04
665	177	8	\N	2022-03-04
666	178	8	\N	2022-03-04
675	187	8	\N	2022-03-04
678	267	8	\N	2022-03-04
679	268	8	\N	2022-03-04
689	180	9	\N	2022-03-04
692	183	9	\N	2022-03-04
693	184	9	\N	2022-03-04
694	185	9	\N	2022-03-04
695	186	9	\N	2022-03-04
697	265	9	\N	2022-03-04
700	268	9	\N	2022-03-04
705	175	10	\N	2022-03-04
711	181	10	\N	2022-03-04
712	182	10	\N	2022-03-04
715	185	10	\N	2022-03-04
717	187	10	\N	2022-03-04
722	171	12	\N	2022-03-04
724	173	12	\N	2022-03-04
726	175	12	\N	2022-03-04
728	177	12	\N	2022-03-04
731	180	12	\N	2022-03-04
735	184	12	\N	2022-03-04
740	266	12	\N	2022-03-04
741	267	12	\N	2022-03-04
747	175	13	\N	2022-03-04
749	177	13	\N	2022-03-04
751	179	13	\N	2022-03-04
752	180	13	\N	2022-03-04
755	183	13	\N	2022-03-04
757	185	13	\N	2022-03-04
760	265	13	\N	2022-03-04
766	173	14	\N	2022-03-04
767	174	14	\N	2022-03-04
768	175	14	\N	2022-03-04
770	177	14	\N	2022-03-04
773	180	14	\N	2022-03-04
777	184	14	\N	2022-03-04
778	185	14	\N	2022-03-04
781	265	14	\N	2022-03-04
782	266	14	\N	2022-03-04
783	267	14	\N	2022-03-04
790	176	15	\N	2022-03-04
792	178	15	\N	2022-03-04
796	182	15	\N	2022-03-04
798	184	15	\N	2022-03-04
800	186	15	\N	2022-03-04
808	173	16	\N	2022-03-04
809	174	16	\N	2022-03-04
814	179	16	\N	2022-03-04
817	182	16	\N	2022-03-04
819	184	16	\N	2022-03-04
821	186	16	\N	2022-03-04
831	276	1	\N	2022-03-04
832	277	1	\N	2022-03-04
836	281	1	\N	2022-03-04
841	286	1	\N	2022-03-04
846	291	1	\N	2022-03-04
848	330	1	\N	2022-03-04
849	244	2	\N	2022-03-04
850	273	2	\N	2022-03-04
851	274	2	\N	2022-03-04
859	282	2	\N	2022-03-04
861	284	2	\N	2022-03-04
865	288	2	\N	2022-03-04
866	289	2	\N	2022-03-04
867	290	2	\N	2022-03-04
870	330	2	\N	2022-03-04
871	244	3	\N	2022-03-04
878	279	3	\N	2022-03-04
880	281	3	\N	2022-03-04
886	287	3	\N	2022-03-04
887	288	3	\N	2022-03-04
888	289	3	\N	2022-03-04
892	330	3	\N	2022-03-04
893	244	4	\N	2022-03-04
896	275	4	\N	2022-03-04
898	277	4	\N	2022-03-04
900	279	4	\N	2022-03-04
909	288	4	\N	2022-03-04
910	289	4	\N	2022-03-04
914	330	4	\N	2022-03-04
916	273	5	\N	2022-03-04
917	274	5	\N	2022-03-04
922	279	5	\N	2022-03-04
925	282	5	\N	2022-03-04
937	244	6	\N	2022-03-04
938	273	6	\N	2022-03-04
942	277	6	\N	2022-03-04
943	278	6	\N	2022-03-04
944	279	6	\N	2022-03-04
949	284	6	\N	2022-03-04
951	286	6	\N	2022-03-04
952	287	6	\N	2022-03-04
954	289	6	\N	2022-03-04
955	290	6	\N	2022-03-04
957	292	6	\N	2022-03-04
958	330	6	\N	2022-03-04
960	273	7	\N	2022-03-04
965	278	7	\N	2022-03-04
968	281	7	\N	2022-03-04
969	282	7	\N	2022-03-04
970	283	7	\N	2022-03-04
972	285	7	\N	2022-03-04
976	289	7	\N	2022-03-04
977	290	7	\N	2022-03-04
980	330	7	\N	2022-03-04
981	244	8	\N	2022-03-04
982	273	8	\N	2022-03-04
984	275	8	\N	2022-03-04
992	283	8	\N	2022-03-04
994	285	8	\N	2022-03-04
1002	330	8	\N	2022-03-04
1005	274	9	\N	2022-03-04
1006	275	9	\N	2022-03-04
1017	286	9	\N	2022-03-04
1021	290	9	\N	2022-03-04
1022	291	9	\N	2022-03-04
1023	292	9	\N	2022-03-04
1025	244	10	\N	2022-03-04
1029	276	10	\N	2022-03-04
1030	277	10	\N	2022-03-04
1031	278	10	\N	2022-03-04
1038	285	10	\N	2022-03-04
1039	286	10	\N	2022-03-04
1042	289	10	\N	2022-03-04
1044	291	10	\N	2022-03-04
1045	292	10	\N	2022-03-04
1047	244	12	\N	2022-03-04
1057	282	12	\N	2022-03-04
1058	283	12	\N	2022-03-04
1060	285	12	\N	2022-03-04
1062	287	12	\N	2022-03-04
1063	288	12	\N	2022-03-04
1065	290	12	\N	2022-03-04
1066	291	12	\N	2022-03-04
1067	292	12	\N	2022-03-04
1068	330	12	\N	2022-03-04
1072	275	13	\N	2022-03-04
1073	276	13	\N	2022-03-04
1077	280	13	\N	2022-03-04
1083	286	13	\N	2022-03-04
1084	287	13	\N	2022-03-04
1088	291	13	\N	2022-03-04
1090	330	13	\N	2022-03-04
1092	273	14	\N	2022-03-04
1094	275	14	\N	2022-03-04
1097	278	14	\N	2022-03-04
1103	284	14	\N	2022-03-04
1104	285	14	\N	2022-03-04
1105	286	14	\N	2022-03-04
1106	287	14	\N	2022-03-04
1113	244	15	\N	2022-03-04
1114	273	15	\N	2022-03-04
1115	274	15	\N	2022-03-04
1116	275	15	\N	2022-03-04
1117	276	15	\N	2022-03-04
1121	280	15	\N	2022-03-04
1125	284	15	\N	2022-03-04
1126	285	15	\N	2022-03-04
1129	288	15	\N	2022-03-04
1130	289	15	\N	2022-03-04
1135	244	16	\N	2022-03-04
1136	273	16	\N	2022-03-04
1137	274	16	\N	2022-03-04
1146	283	16	\N	2022-03-04
1151	288	16	\N	2022-03-04
\.


--
-- Data for Name: aules; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aules (id, nomaula, capacitat) FROM stdin;
1	C2	30
2	C3	30
3	C4	30
4	F3	22
6	A1	20
7	1.7	20
8	1.8	16
5	1.6	20
10	D3	40
11	1.1	24
12	1.2	24
13	1.3	24
14	2.3	24
9	1.5	30
\.


--
-- Data for Name: cicles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cicles (id, nom, descripcio, nivell) FROM stdin;
2	DAM 	Desenvolupament d'Aplicacions	S
3	DAMVI	Desenvolupament d'Aplicacions VideoJocs	S
1	ASIX	Administracio de Sistemes Informatics i Xarxes	S
4	SMX	Sistemes MicroInformatics i Xarxes	M
5	SOC	Servei Ocupacio Catalunya	S
\.


--
-- Data for Name: grups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.grups (id, nom, tutor, cicle, curs) FROM stdin;
1	ASIXA1	\N	1	1
2	DAM1B	\N	2	1
3	DAM1C	\N	2	1
4	DAMVI1D	\N	3	1
5	DAMVI1E	\N	3	1
6	ASIX2	\N	1	2
8	DAM2	\N	2	2
9	DAMVI2B	\N	3	2
7	DAMVI2A	\N	3	2
\.


--
-- Data for Name: grupsunitatsformatives; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.grupsunitatsformatives (id, idgrup, idunitatformativa, idprofessor, idaula) FROM stdin;
1	1	1	325	3
2	1	2	271	4
3	1	3	326	3
4	1	18	271	4
5	6	1	269	5
6	6	2	325	3
7	6	3	329	5
8	6	18	328	1
9	1	4	270	1
10	1	5	329	5
11	1	34	272	3
12	6	4	328	6
13	6	5	272	1
14	6	34	272	3
15	1	7	269	6
16	1	8	271	6
17	1	9	328	2
18	6	7	328	6
19	6	8	328	4
20	6	9	272	4
21	1	10	329	4
22	1	11	329	3
23	1	12	325	2
24	6	10	328	4
25	6	11	328	1
26	6	12	328	3
27	1	13	270	5
28	1	19	329	3
29	1	26	327	5
30	6	13	329	5
31	6	19	271	5
32	6	26	328	6
33	1	20	269	4
34	1	21	269	5
35	6	20	269	5
36	6	21	272	3
37	1	14	270	1
38	1	15	328	4
39	6	14	272	2
40	6	15	327	6
41	1	16	270	1
42	1	17	269	6
43	1	27	326	3
44	6	16	328	5
45	6	17	272	6
46	6	27	270	6
47	1	6	325	1
48	1	35	269	3
49	6	6	270	6
50	6	35	325	1
51	1	22	271	3
52	1	23	270	2
53	1	24	270	6
54	1	25	269	1
55	6	22	327	2
56	6	23	329	5
57	6	24	269	3
58	6	25	270	5
59	1	32	272	4
60	1	33	325	6
61	6	32	272	1
62	6	33	270	5
63	1	28	327	3
64	1	29	271	6
65	1	30	325	2
66	1	31	327	3
67	6	28	328	2
68	6	29	269	1
69	6	30	271	1
70	6	31	270	1
71	1	36	327	3
72	6	36	325	3
73	1	37	269	6
74	6	37	270	1
\.


--
-- Data for Name: log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.log (idlog, data, hora, idusuari, missatge, error) FROM stdin;
\.


--
-- Data for Name: moduls; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.moduls (modul, hores, nom, cicle, id) FROM stdin;
6	132	Admin de Sistemes Operatius	1	6
9	66	Implantacio d'Aplicacions WEB	1	8
8	99	Serveis de Xarxa i Internet	1	12
13	66	Empresa i Iniciativa Empresarial	1	14
1	165	Implantacio Sistemes Operatius	1	1
2	132	Gestio de base de dades	1	2
3	165	Programacio basica	1	3
4	99	Llenguatge de Marques	1	4
5	66	Fonaments de Maquinari	1	5
7	132	Planificacio i Admin Xarxes	1	9
10	66	Administracio de SGBDs	1	7
11	66	Seguretat i Alta Disponibilitat	1	13
12	66	Formacio i Orientacio Laboral	1	10
14	297	Projecte	1	15
15	383	Formacio en centres de treball	1	16
1	132	Sistemes Informacis	2	17
2	165	Bases de Dades	2	18
3	231	Programacio	2	19
4	99	Llenguatge de Marques	2	20
5	66	Entorns de Desenvolupament	2	21
6	99	Acces a Dades	2	22
7	99	Desenvolupament Interficies	2	23
8	132	Programacio Multimedia i Mobils	2	24
9	66	Programacio de Serveis i Processos	2	25
10	99	Sistemes de Gestio Empresarial	2	26
11	66	Formacio i Orientacio Laboral	2	27
13	297	Projecte	2	29
12	66	Empresa i Iniciativa Emprenedora	2	28
14	383	FCT	2	30
13	297	Projecte	3	59
14	383	FCT	3	60
1	121	Sistemes Informacis	3	47
2	110	Bases de Dades	3	48
3	187	Programacio	3	49
4	77	Llenguatge de Marques	3	50
5	55	Entorns de Desenvolupament	3	51
6	88	Acces a Dades	3	52
7	88	Desenvolupament Interficies	3	53
8	77	Programacio Multimedia i Mobils	3	54
9	55	Programacio de Serveis i Processos	3	55
10	55	Sistemes de Gestio Empresarial	3	56
11	66	Formacio i Orientacio Laboral	3	57
12	66	Empresa i Iniciativa Emprenedora	3	58
15	33	Game Design	3	61
16	88	Disseny 2Di 3D	3	62
17	154	Programacio de Videojocs 2D i 3D	3	63
\.


--
-- Data for Name: qualificacions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.qualificacions (id, idalumne, idunitatformativa, idprofessor, nota1c, nota2c) FROM stdin;
1	171	1	1	3	\N
2	172	1	1	5	\N
3	173	1	1	7	\N
4	174	1	1	1	\N
5	175	1	1	10	\N
6	176	1	1	7	\N
7	177	1	1	5	\N
9	179	1	1	9	\N
10	180	1	1	4	\N
11	181	1	1	5	\N
12	182	1	1	1	\N
13	183	1	1	1	\N
14	184	1	1	10	\N
15	185	1	1	5	\N
16	186	1	1	8	\N
17	187	1	1	2	\N
18	265	1	1	8	\N
19	266	1	1	2	\N
20	267	1	1	7	\N
21	268	1	1	8	\N
22	171	2	1	7	\N
23	172	2	1	2	\N
24	173	2	1	9	\N
25	174	2	1	6	\N
26	175	2	1	4	\N
27	176	2	1	10	\N
28	177	2	1	1	\N
29	178	2	1	1	\N
30	179	2	1	2	\N
31	180	2	1	7	\N
32	181	2	1	9	\N
33	182	2	1	5	\N
34	183	2	1	4	\N
35	184	2	1	7	\N
36	185	2	1	1	\N
37	186	2	1	4	\N
38	187	2	1	5	\N
39	265	2	1	5	\N
40	266	2	1	7	\N
41	267	2	1	10	\N
42	268	2	1	7	\N
43	171	3	1	7	\N
44	172	3	1	1	\N
45	173	3	1	6	\N
46	174	3	1	7	\N
47	175	3	1	6	\N
48	176	3	1	10	\N
49	177	3	1	10	\N
50	178	3	1	1	\N
51	179	3	1	2	\N
52	180	3	1	8	\N
53	181	3	1	4	\N
54	182	3	1	6	\N
55	183	3	1	8	\N
56	184	3	1	1	\N
57	185	3	1	9	\N
58	186	3	1	2	\N
59	187	3	1	3	\N
60	265	3	1	7	\N
61	266	3	1	6	\N
62	267	3	1	10	\N
63	268	3	1	10	\N
64	171	18	1	2	\N
65	172	18	1	7	\N
66	173	18	1	1	\N
67	174	18	1	10	\N
68	175	18	1	10	\N
69	176	18	1	8	\N
70	177	18	1	8	\N
71	178	18	1	5	\N
72	179	18	1	2	\N
73	180	18	1	4	\N
74	181	18	1	5	\N
75	182	18	1	2	\N
76	183	18	1	5	\N
77	184	18	1	10	\N
78	185	18	1	3	\N
79	186	18	1	3	\N
80	187	18	1	8	\N
81	265	18	1	1	\N
82	266	18	1	8	\N
83	267	18	1	6	\N
84	268	18	1	7	\N
85	171	4	1	6	\N
86	172	4	1	10	\N
87	173	4	1	1	\N
88	174	4	1	3	\N
89	175	4	1	1	\N
90	176	4	1	7	\N
91	177	4	1	2	\N
92	178	4	1	7	\N
93	179	4	1	9	\N
94	180	4	1	3	\N
95	181	4	1	6	\N
96	182	4	1	2	\N
97	183	4	1	3	\N
98	184	4	1	7	\N
99	185	4	1	6	\N
100	186	4	1	9	\N
101	187	4	1	7	\N
102	265	4	1	6	\N
103	266	4	1	9	\N
104	267	4	1	8	\N
105	268	4	1	3	\N
106	171	5	1	9	\N
107	172	5	1	8	\N
108	173	5	1	1	\N
109	174	5	1	9	\N
110	175	5	1	1	\N
111	176	5	1	8	\N
112	177	5	1	5	\N
113	178	5	1	2	\N
114	179	5	1	6	\N
115	180	5	1	4	\N
116	181	5	1	8	\N
117	182	5	1	1	\N
118	183	5	1	10	\N
119	184	5	1	6	\N
120	185	5	1	9	\N
121	186	5	1	7	\N
122	187	5	1	9	\N
123	265	5	1	1	\N
124	266	5	1	9	\N
125	267	5	1	10	\N
126	268	5	1	3	\N
127	171	34	1	3	\N
128	172	34	1	5	\N
129	173	34	1	8	\N
130	174	34	1	5	\N
131	175	34	1	1	\N
132	176	34	1	10	\N
133	177	34	1	7	\N
134	178	34	1	4	\N
135	179	34	1	7	\N
136	180	34	1	2	\N
137	181	34	1	1	\N
138	182	34	1	9	\N
139	183	34	1	10	\N
140	184	34	1	5	\N
141	185	34	1	2	\N
142	186	34	1	6	\N
143	187	34	1	2	\N
144	265	34	1	1	\N
145	266	34	1	1	\N
146	267	34	1	10	\N
147	268	34	1	5	\N
148	171	7	1	4	\N
149	172	7	1	6	\N
150	173	7	1	7	\N
151	174	7	1	8	\N
152	175	7	1	9	\N
153	176	7	1	9	\N
154	177	7	1	7	\N
155	178	7	1	8	\N
156	179	7	1	9	\N
157	180	7	1	10	\N
158	181	7	1	3	\N
159	182	7	1	3	\N
160	183	7	1	5	\N
161	184	7	1	5	\N
162	185	7	1	2	\N
163	186	7	1	3	\N
164	187	7	1	9	\N
165	265	7	1	6	\N
166	266	7	1	3	\N
167	267	7	1	5	\N
168	268	7	1	6	\N
169	171	8	1	3	\N
170	172	8	1	9	\N
171	173	8	1	5	\N
172	174	8	1	8	\N
173	175	8	1	2	\N
174	176	8	1	6	\N
175	177	8	1	5	\N
176	178	8	1	5	\N
177	179	8	1	2	\N
178	180	8	1	2	\N
179	181	8	1	6	\N
180	182	8	1	3	\N
181	183	8	1	6	\N
182	184	8	1	9	\N
183	185	8	1	8	\N
184	186	8	1	2	\N
185	187	8	1	5	\N
186	265	8	1	10	\N
187	266	8	1	6	\N
188	267	8	1	6	\N
189	268	8	1	2	\N
190	171	9	1	2	\N
191	172	9	1	3	\N
192	173	9	1	9	\N
193	174	9	1	6	\N
194	175	9	1	3	\N
195	176	9	1	4	\N
196	177	9	1	3	\N
197	178	9	1	2	\N
198	179	9	1	9	\N
199	180	9	1	8	\N
200	181	9	1	4	\N
201	182	9	1	7	\N
202	183	9	1	1	\N
203	184	9	1	3	\N
204	185	9	1	2	\N
205	186	9	1	10	\N
206	187	9	1	5	\N
207	265	9	1	2	\N
208	266	9	1	5	\N
209	267	9	1	9	\N
210	268	9	1	8	\N
211	171	10	1	2	\N
212	172	10	1	6	\N
213	173	10	1	3	\N
214	174	10	1	7	\N
215	175	10	1	6	\N
216	176	10	1	8	\N
217	177	10	1	10	\N
218	178	10	1	8	\N
219	179	10	1	1	\N
220	180	10	1	7	\N
221	181	10	1	4	\N
222	182	10	1	6	\N
223	183	10	1	10	\N
224	184	10	1	1	\N
225	185	10	1	2	\N
226	186	10	1	9	\N
227	187	10	1	9	\N
228	265	10	1	8	\N
229	266	10	1	3	\N
230	267	10	1	8	\N
231	268	10	1	2	\N
232	171	11	1	5	\N
233	172	11	1	8	\N
234	173	11	1	3	\N
235	174	11	1	2	\N
236	175	11	1	9	\N
237	176	11	1	10	\N
238	177	11	1	3	\N
239	178	11	1	5	\N
240	179	11	1	7	\N
241	180	11	1	6	\N
242	181	11	1	1	\N
243	182	11	1	4	\N
244	183	11	1	8	\N
245	184	11	1	5	\N
246	185	11	1	4	\N
247	186	11	1	2	\N
248	187	11	1	8	\N
249	265	11	1	6	\N
250	266	11	1	2	\N
251	267	11	1	4	\N
252	268	11	1	4	\N
253	171	12	1	8	\N
254	172	12	1	5	\N
255	173	12	1	1	\N
256	174	12	1	8	\N
257	175	12	1	8	\N
258	176	12	1	7	\N
259	177	12	1	9	\N
260	178	12	1	4	\N
261	179	12	1	3	\N
262	180	12	1	10	\N
263	181	12	1	4	\N
264	182	12	1	7	\N
265	183	12	1	2	\N
266	184	12	1	10	\N
267	185	12	1	2	\N
268	186	12	1	2	\N
269	187	12	1	2	\N
270	265	12	1	2	\N
271	266	12	1	6	\N
272	267	12	1	2	\N
273	268	12	1	3	\N
274	171	13	1	7	\N
275	172	13	1	7	\N
276	173	13	1	3	\N
277	174	13	1	2	\N
278	175	13	1	10	\N
279	176	13	1	3	\N
280	177	13	1	5	\N
281	178	13	1	6	\N
282	179	13	1	2	\N
283	180	13	1	5	\N
284	181	13	1	8	\N
285	182	13	1	3	\N
286	183	13	1	3	\N
287	184	13	1	4	\N
288	185	13	1	9	\N
289	186	13	1	4	\N
290	187	13	1	1	\N
291	265	13	1	3	\N
292	266	13	1	2	\N
293	267	13	1	8	\N
294	268	13	1	7	\N
295	171	19	1	9	\N
296	172	19	1	2	\N
297	173	19	1	3	\N
298	174	19	1	10	\N
299	175	19	1	9	\N
300	176	19	1	5	\N
301	177	19	1	6	\N
302	178	19	1	6	\N
303	179	19	1	2	\N
304	180	19	1	10	\N
305	181	19	1	3	\N
306	182	19	1	7	\N
307	183	19	1	6	\N
308	184	19	1	8	\N
309	185	19	1	1	\N
310	186	19	1	2	\N
311	187	19	1	7	\N
312	265	19	1	7	\N
313	266	19	1	3	\N
314	267	19	1	4	\N
315	268	19	1	10	\N
316	171	26	1	9	\N
317	172	26	1	2	\N
318	173	26	1	4	\N
319	174	26	1	2	\N
320	175	26	1	7	\N
321	176	26	1	8	\N
322	177	26	1	1	\N
323	178	26	1	10	\N
324	179	26	1	6	\N
325	180	26	1	10	\N
326	181	26	1	10	\N
327	182	26	1	1	\N
328	183	26	1	4	\N
329	184	26	1	4	\N
330	185	26	1	1	\N
331	186	26	1	2	\N
332	187	26	1	7	\N
333	265	26	1	9	\N
334	266	26	1	1	\N
335	267	26	1	7	\N
336	268	26	1	4	\N
337	171	20	1	9	\N
338	172	20	1	2	\N
339	173	20	1	3	\N
340	174	20	1	10	\N
341	175	20	1	9	\N
342	176	20	1	5	\N
343	177	20	1	8	\N
344	178	20	1	6	\N
345	179	20	1	4	\N
346	180	20	1	10	\N
347	181	20	1	10	\N
348	182	20	1	10	\N
349	183	20	1	8	\N
350	184	20	1	2	\N
351	185	20	1	2	\N
352	186	20	1	5	\N
353	187	20	1	6	\N
354	265	20	1	8	\N
355	266	20	1	1	\N
356	267	20	1	8	\N
357	268	20	1	10	\N
358	171	21	1	2	\N
359	172	21	1	1	\N
360	173	21	1	8	\N
361	174	21	1	8	\N
362	175	21	1	5	\N
363	176	21	1	10	\N
364	177	21	1	5	\N
365	178	21	1	6	\N
366	179	21	1	5	\N
367	180	21	1	5	\N
368	181	21	1	1	\N
369	182	21	1	7	\N
370	183	21	1	3	\N
371	184	21	1	1	\N
372	185	21	1	8	\N
373	186	21	1	5	\N
374	187	21	1	10	\N
375	265	21	1	4	\N
376	266	21	1	2	\N
377	267	21	1	5	\N
378	268	21	1	9	\N
379	171	6	1	10	\N
380	172	6	1	10	\N
381	173	6	1	2	\N
382	174	6	1	10	\N
383	175	6	1	2	\N
384	176	6	1	9	\N
385	177	6	1	10	\N
386	178	6	1	4	\N
387	179	6	1	7	\N
388	180	6	1	3	\N
389	181	6	1	4	\N
390	182	6	1	2	\N
391	183	6	1	5	\N
392	184	6	1	1	\N
393	185	6	1	7	\N
394	186	6	1	1	\N
395	187	6	1	4	\N
396	265	6	1	8	\N
397	266	6	1	6	\N
398	267	6	1	5	\N
399	268	6	1	9	\N
400	171	35	1	6	\N
401	172	35	1	6	\N
402	173	35	1	9	\N
403	174	35	1	7	\N
404	175	35	1	9	\N
405	176	35	1	2	\N
406	177	35	1	5	\N
407	178	35	1	7	\N
408	179	35	1	8	\N
409	180	35	1	7	\N
410	181	35	1	4	\N
411	182	35	1	8	\N
412	183	35	1	9	\N
413	184	35	1	8	\N
414	185	35	1	1	\N
415	186	35	1	3	\N
416	187	35	1	1	\N
417	265	35	1	6	\N
418	266	35	1	5	\N
419	267	35	1	8	\N
420	268	35	1	5	\N
421	171	32	1	10	\N
422	172	32	1	6	\N
423	173	32	1	9	\N
424	174	32	1	5	\N
425	175	32	1	6	\N
426	176	32	1	7	\N
427	177	32	1	5	\N
428	178	32	1	3	\N
429	179	32	1	9	\N
430	180	32	1	5	\N
431	181	32	1	10	\N
432	182	32	1	2	\N
433	183	32	1	10	\N
434	184	32	1	4	\N
435	185	32	1	9	\N
436	186	32	1	4	\N
437	187	32	1	1	\N
438	265	32	1	4	\N
439	266	32	1	5	\N
440	267	32	1	4	\N
441	268	32	1	5	\N
442	171	33	1	3	\N
443	172	33	1	8	\N
444	173	33	1	3	\N
445	174	33	1	8	\N
446	175	33	1	1	\N
447	176	33	1	2	\N
448	177	33	1	5	\N
449	178	33	1	10	\N
450	179	33	1	4	\N
451	180	33	1	1	\N
452	181	33	1	1	\N
453	182	33	1	2	\N
454	183	33	1	6	\N
455	184	33	1	4	\N
456	185	33	1	2	\N
457	186	33	1	6	\N
458	187	33	1	2	\N
459	265	33	1	4	\N
460	266	33	1	1	\N
461	267	33	1	5	\N
462	268	33	1	2	\N
463	171	16	1	3	\N
464	172	16	1	9	\N
465	173	16	1	3	\N
466	174	16	1	1	\N
467	175	16	1	5	\N
468	176	16	1	1	\N
469	177	16	1	9	\N
470	178	16	1	2	\N
471	179	16	1	3	\N
472	180	16	1	9	\N
473	181	16	1	5	\N
474	182	16	1	7	\N
475	183	16	1	8	\N
476	184	16	1	2	\N
477	185	16	1	3	\N
478	186	16	1	10	\N
479	187	16	1	8	\N
480	265	16	1	3	\N
481	266	16	1	7	\N
482	267	16	1	8	\N
483	268	16	1	7	\N
484	171	17	1	10	\N
485	172	17	1	8	\N
486	173	17	1	1	\N
487	174	17	1	2	\N
488	175	17	1	4	\N
489	176	17	1	3	\N
490	177	17	1	3	\N
491	178	17	1	8	\N
492	179	17	1	7	\N
493	180	17	1	2	\N
494	181	17	1	10	\N
495	182	17	1	3	\N
496	183	17	1	6	\N
497	184	17	1	10	\N
498	185	17	1	10	\N
499	186	17	1	9	\N
500	187	17	1	6	\N
501	265	17	1	1	\N
502	266	17	1	8	\N
503	267	17	1	5	\N
504	268	17	1	1	\N
505	171	27	1	10	\N
506	172	27	1	5	\N
507	173	27	1	4	\N
508	174	27	1	5	\N
509	175	27	1	1	\N
510	176	27	1	10	\N
511	177	27	1	7	\N
512	178	27	1	3	\N
513	179	27	1	7	\N
514	180	27	1	2	\N
515	181	27	1	1	\N
516	182	27	1	7	\N
517	183	27	1	1	\N
518	184	27	1	4	\N
519	185	27	1	5	\N
520	186	27	1	3	\N
521	187	27	1	1	\N
522	265	27	1	7	\N
523	266	27	1	8	\N
524	267	27	1	2	\N
525	268	27	1	4	\N
526	171	14	1	2	\N
527	172	14	1	8	\N
528	173	14	1	5	\N
529	174	14	1	1	\N
530	175	14	1	7	\N
531	176	14	1	1	\N
532	177	14	1	6	\N
533	178	14	1	4	\N
534	179	14	1	3	\N
535	180	14	1	4	\N
536	181	14	1	9	\N
537	182	14	1	1	\N
538	183	14	1	10	\N
539	184	14	1	4	\N
540	185	14	1	1	\N
541	186	14	1	3	\N
542	187	14	1	9	\N
543	265	14	1	9	\N
544	266	14	1	4	\N
545	267	14	1	5	\N
546	268	14	1	3	\N
547	171	15	1	8	\N
548	172	15	1	1	\N
549	173	15	1	10	\N
550	174	15	1	9	\N
551	175	15	1	3	\N
552	176	15	1	7	\N
553	177	15	1	4	\N
554	178	15	1	10	\N
555	179	15	1	8	\N
556	180	15	1	7	\N
557	181	15	1	4	\N
558	182	15	1	5	\N
559	183	15	1	5	\N
560	184	15	1	8	\N
561	185	15	1	5	\N
562	186	15	1	2	\N
563	187	15	1	4	\N
564	265	15	1	5	\N
565	266	15	1	1	\N
566	267	15	1	1	\N
567	268	15	1	10	\N
568	171	22	1	8	\N
569	172	22	1	7	\N
570	173	22	1	2	\N
571	174	22	1	6	\N
572	175	22	1	7	\N
573	176	22	1	3	\N
574	177	22	1	6	\N
575	178	22	1	10	\N
576	179	22	1	10	\N
577	180	22	1	2	\N
578	181	22	1	9	\N
579	182	22	1	8	\N
580	183	22	1	2	\N
581	184	22	1	6	\N
582	185	22	1	4	\N
583	186	22	1	3	\N
584	187	22	1	10	\N
585	265	22	1	2	\N
586	266	22	1	10	\N
587	267	22	1	5	\N
588	268	22	1	2	\N
589	171	23	1	6	\N
590	172	23	1	3	\N
591	173	23	1	4	\N
592	174	23	1	4	\N
593	175	23	1	6	\N
594	176	23	1	7	\N
595	177	23	1	5	\N
596	178	23	1	4	\N
597	179	23	1	7	\N
598	180	23	1	1	\N
599	181	23	1	4	\N
600	182	23	1	7	\N
601	183	23	1	3	\N
602	184	23	1	5	\N
603	185	23	1	3	\N
604	186	23	1	9	\N
605	187	23	1	4	\N
606	265	23	1	6	\N
607	266	23	1	5	\N
608	267	23	1	9	\N
609	268	23	1	10	\N
610	171	24	1	2	\N
611	172	24	1	1	\N
612	173	24	1	8	\N
613	174	24	1	5	\N
614	175	24	1	1	\N
615	176	24	1	1	\N
616	177	24	1	10	\N
617	178	24	1	5	\N
618	179	24	1	6	\N
619	180	24	1	4	\N
620	181	24	1	10	\N
621	182	24	1	8	\N
622	183	24	1	8	\N
623	184	24	1	8	\N
624	185	24	1	3	\N
625	186	24	1	3	\N
626	187	24	1	4	\N
627	265	24	1	10	\N
628	266	24	1	8	\N
629	267	24	1	8	\N
630	268	24	1	7	\N
631	171	25	1	2	\N
632	172	25	1	8	\N
633	173	25	1	3	\N
634	174	25	1	2	\N
635	175	25	1	10	\N
636	176	25	1	5	\N
637	177	25	1	1	\N
638	178	25	1	2	\N
639	179	25	1	5	\N
640	180	25	1	7	\N
641	181	25	1	3	\N
642	182	25	1	1	\N
643	183	25	1	8	\N
644	184	25	1	3	\N
645	185	25	1	4	\N
646	186	25	1	8	\N
647	187	25	1	7	\N
648	265	25	1	3	\N
649	266	25	1	3	\N
650	267	25	1	5	\N
651	268	25	1	4	\N
652	171	28	1	7	\N
653	172	28	1	10	\N
654	173	28	1	9	\N
655	174	28	1	4	\N
656	175	28	1	3	\N
657	176	28	1	3	\N
658	177	28	1	5	\N
659	178	28	1	6	\N
660	179	28	1	4	\N
661	180	28	1	1	\N
662	181	28	1	2	\N
663	182	28	1	9	\N
664	183	28	1	8	\N
665	184	28	1	1	\N
666	185	28	1	1	\N
667	186	28	1	10	\N
668	187	28	1	7	\N
669	265	28	1	6	\N
670	266	28	1	6	\N
671	267	28	1	5	\N
672	268	28	1	3	\N
673	171	29	1	3	\N
674	172	29	1	3	\N
675	173	29	1	7	\N
676	174	29	1	4	\N
677	175	29	1	9	\N
678	176	29	1	1	\N
679	177	29	1	9	\N
680	178	29	1	2	\N
681	179	29	1	1	\N
682	180	29	1	9	\N
683	181	29	1	1	\N
684	182	29	1	6	\N
685	183	29	1	9	\N
686	184	29	1	4	\N
687	185	29	1	6	\N
688	186	29	1	5	\N
689	187	29	1	6	\N
690	265	29	1	5	\N
691	266	29	1	8	\N
692	267	29	1	5	\N
693	268	29	1	1	\N
694	171	30	1	10	\N
695	172	30	1	5	\N
696	173	30	1	6	\N
697	174	30	1	5	\N
698	175	30	1	6	\N
699	176	30	1	5	\N
700	177	30	1	6	\N
701	178	30	1	6	\N
702	179	30	1	3	\N
703	180	30	1	3	\N
704	181	30	1	5	\N
705	182	30	1	8	\N
706	183	30	1	5	\N
707	184	30	1	2	\N
708	185	30	1	1	\N
709	186	30	1	8	\N
710	187	30	1	10	\N
711	265	30	1	3	\N
712	266	30	1	4	\N
713	267	30	1	2	\N
714	268	30	1	5	\N
715	171	31	1	1	\N
716	172	31	1	1	\N
717	173	31	1	9	\N
718	174	31	1	2	\N
719	175	31	1	4	\N
720	176	31	1	5	\N
721	177	31	1	9	\N
722	178	31	1	1	\N
723	179	31	1	6	\N
724	180	31	1	9	\N
725	181	31	1	6	\N
726	182	31	1	4	\N
727	183	31	1	10	\N
728	184	31	1	9	\N
729	185	31	1	5	\N
730	186	31	1	6	\N
731	187	31	1	4	\N
732	265	31	1	10	\N
733	266	31	1	9	\N
734	267	31	1	3	\N
735	268	31	1	1	\N
736	171	36	1	2	\N
737	172	36	1	8	\N
738	173	36	1	2	\N
739	174	36	1	5	\N
740	175	36	1	10	\N
741	176	36	1	6	\N
742	177	36	1	6	\N
743	178	36	1	5	\N
744	179	36	1	5	\N
745	180	36	1	9	\N
746	181	36	1	10	\N
747	182	36	1	4	\N
748	183	36	1	10	\N
749	184	36	1	6	\N
750	185	36	1	8	\N
751	186	36	1	3	\N
752	187	36	1	2	\N
753	265	36	1	1	\N
754	266	36	1	8	\N
755	267	36	1	6	\N
756	268	36	1	4	\N
757	171	37	1	1	\N
758	172	37	1	5	\N
759	173	37	1	9	\N
760	174	37	1	9	\N
761	175	37	1	8	\N
762	176	37	1	4	\N
763	177	37	1	4	\N
764	178	37	1	8	\N
765	179	37	1	6	\N
766	180	37	1	7	\N
767	181	37	1	8	\N
768	182	37	1	7	\N
769	183	37	1	2	\N
770	184	37	1	8	\N
771	185	37	1	2	\N
772	186	37	1	8	\N
773	187	37	1	4	\N
774	265	37	1	5	\N
775	266	37	1	1	\N
776	267	37	1	1	\N
777	268	37	1	10	\N
778	171	38	1	8	\N
779	172	38	1	10	\N
780	173	38	1	6	\N
781	174	38	1	7	\N
782	175	38	1	7	\N
783	176	38	1	3	\N
784	177	38	1	4	\N
785	178	38	1	8	\N
786	179	38	1	1	\N
787	180	38	1	1	\N
788	181	38	1	9	\N
789	182	38	1	3	\N
790	183	38	1	8	\N
791	184	38	1	8	\N
792	185	38	1	9	\N
793	186	38	1	7	\N
794	187	38	1	1	\N
795	265	38	1	1	\N
796	266	38	1	3	\N
797	267	38	1	3	\N
798	268	38	1	3	\N
799	244	1	1	7	\N
800	273	1	1	6	\N
801	274	1	1	9	\N
802	275	1	1	5	\N
803	276	1	1	10	\N
804	277	1	1	2	\N
805	278	1	1	9	\N
806	279	1	1	10	\N
807	280	1	1	10	\N
808	281	1	1	3	\N
809	282	1	1	2	\N
810	283	1	1	10	\N
811	284	1	1	3	\N
812	285	1	1	5	\N
813	286	1	1	8	\N
814	287	1	1	1	\N
815	288	1	1	2	\N
816	289	1	1	5	\N
817	290	1	1	1	\N
818	291	1	1	10	\N
819	292	1	1	6	\N
820	330	1	1	8	\N
821	244	2	1	4	\N
822	273	2	1	5	\N
823	274	2	1	3	\N
824	275	2	1	8	\N
825	276	2	1	1	\N
826	277	2	1	9	\N
827	278	2	1	5	\N
828	279	2	1	4	\N
829	280	2	1	5	\N
830	281	2	1	2	\N
831	282	2	1	6	\N
832	283	2	1	1	\N
833	284	2	1	9	\N
834	285	2	1	2	\N
835	286	2	1	9	\N
836	287	2	1	2	\N
837	288	2	1	3	\N
838	289	2	1	6	\N
839	290	2	1	2	\N
840	291	2	1	1	\N
841	292	2	1	10	\N
842	330	2	1	4	\N
843	244	3	1	2	\N
844	273	3	1	9	\N
845	274	3	1	5	\N
846	275	3	1	9	\N
847	276	3	1	9	\N
848	277	3	1	8	\N
849	278	3	1	2	\N
850	279	3	1	4	\N
851	280	3	1	4	\N
852	281	3	1	7	\N
853	282	3	1	3	\N
854	283	3	1	5	\N
855	284	3	1	6	\N
856	285	3	1	2	\N
857	286	3	1	2	\N
858	287	3	1	6	\N
859	288	3	1	10	\N
860	289	3	1	2	\N
861	290	3	1	2	\N
862	291	3	1	2	\N
863	292	3	1	5	\N
864	330	3	1	7	\N
865	244	18	1	8	\N
866	273	18	1	10	\N
867	274	18	1	4	\N
868	275	18	1	2	\N
869	276	18	1	5	\N
870	277	18	1	10	\N
871	278	18	1	2	\N
872	279	18	1	2	\N
873	280	18	1	3	\N
874	281	18	1	7	\N
875	282	18	1	8	\N
876	283	18	1	8	\N
877	284	18	1	5	\N
878	285	18	1	9	\N
879	286	18	1	2	\N
880	287	18	1	2	\N
881	288	18	1	4	\N
882	289	18	1	1	\N
883	290	18	1	3	\N
884	291	18	1	3	\N
885	292	18	1	4	\N
886	330	18	1	1	\N
887	244	4	1	2	\N
888	273	4	1	6	\N
889	274	4	1	6	\N
890	275	4	1	9	\N
891	276	4	1	8	\N
892	277	4	1	10	\N
893	278	4	1	9	\N
894	279	4	1	2	\N
895	280	4	1	2	\N
896	281	4	1	5	\N
897	282	4	1	9	\N
898	283	4	1	10	\N
899	284	4	1	1	\N
900	285	4	1	5	\N
901	286	4	1	1	\N
902	287	4	1	10	\N
903	288	4	1	5	\N
904	289	4	1	4	\N
905	290	4	1	4	\N
906	291	4	1	8	\N
907	292	4	1	8	\N
908	330	4	1	5	\N
909	244	5	1	1	\N
910	273	5	1	8	\N
911	274	5	1	9	\N
912	275	5	1	1	\N
913	276	5	1	5	\N
914	277	5	1	3	\N
915	278	5	1	7	\N
916	279	5	1	9	\N
917	280	5	1	2	\N
918	281	5	1	2	\N
919	282	5	1	3	\N
920	283	5	1	9	\N
921	284	5	1	5	\N
922	285	5	1	6	\N
923	286	5	1	8	\N
924	287	5	1	10	\N
925	288	5	1	5	\N
926	289	5	1	4	\N
927	290	5	1	5	\N
928	291	5	1	3	\N
929	292	5	1	7	\N
930	330	5	1	9	\N
931	244	34	1	3	\N
932	273	34	1	5	\N
933	274	34	1	7	\N
934	275	34	1	10	\N
935	276	34	1	10	\N
936	277	34	1	9	\N
937	278	34	1	2	\N
938	279	34	1	5	\N
939	280	34	1	9	\N
940	281	34	1	9	\N
941	282	34	1	6	\N
942	283	34	1	4	\N
943	284	34	1	1	\N
944	285	34	1	3	\N
945	286	34	1	1	\N
946	287	34	1	6	\N
947	288	34	1	8	\N
948	289	34	1	9	\N
949	290	34	1	1	\N
950	291	34	1	7	\N
951	292	34	1	10	\N
952	330	34	1	10	\N
953	244	7	1	9	\N
954	273	7	1	6	\N
955	274	7	1	3	\N
956	275	7	1	5	\N
957	276	7	1	5	\N
958	277	7	1	2	\N
959	278	7	1	4	\N
960	279	7	1	2	\N
961	280	7	1	7	\N
962	281	7	1	9	\N
963	282	7	1	2	\N
964	283	7	1	2	\N
965	284	7	1	5	\N
966	285	7	1	9	\N
967	286	7	1	10	\N
968	287	7	1	3	\N
969	288	7	1	5	\N
970	289	7	1	6	\N
971	290	7	1	5	\N
972	291	7	1	5	\N
973	292	7	1	1	\N
974	330	7	1	8	\N
975	244	8	1	9	\N
976	273	8	1	7	\N
977	274	8	1	10	\N
978	275	8	1	10	\N
979	276	8	1	7	\N
980	277	8	1	4	\N
981	278	8	1	10	\N
982	279	8	1	6	\N
983	280	8	1	1	\N
984	281	8	1	6	\N
985	282	8	1	6	\N
986	283	8	1	2	\N
987	284	8	1	9	\N
988	285	8	1	2	\N
989	286	8	1	1	\N
990	287	8	1	1	\N
991	288	8	1	9	\N
992	289	8	1	2	\N
993	290	8	1	3	\N
994	291	8	1	6	\N
995	292	8	1	3	\N
996	330	8	1	5	\N
997	244	9	1	8	\N
998	273	9	1	3	\N
999	274	9	1	1	\N
1000	275	9	1	6	\N
1001	276	9	1	7	\N
1002	277	9	1	6	\N
1003	278	9	1	6	\N
1004	279	9	1	5	\N
1005	280	9	1	6	\N
1006	281	9	1	4	\N
1007	282	9	1	3	\N
1008	283	9	1	3	\N
1009	284	9	1	7	\N
1010	285	9	1	2	\N
1011	286	9	1	8	\N
1012	287	9	1	6	\N
1013	288	9	1	3	\N
1014	289	9	1	8	\N
1015	290	9	1	10	\N
1016	291	9	1	5	\N
1017	292	9	1	4	\N
1018	330	9	1	6	\N
1019	244	10	1	7	\N
1020	273	10	1	8	\N
1021	274	10	1	7	\N
1022	275	10	1	1	\N
1023	276	10	1	4	\N
1024	277	10	1	8	\N
1025	278	10	1	6	\N
1026	279	10	1	5	\N
1027	280	10	1	8	\N
1028	281	10	1	3	\N
1029	282	10	1	10	\N
1030	283	10	1	3	\N
1031	284	10	1	2	\N
1032	285	10	1	10	\N
1033	286	10	1	5	\N
1034	287	10	1	3	\N
1035	288	10	1	9	\N
1036	289	10	1	7	\N
1037	290	10	1	8	\N
1038	291	10	1	6	\N
1039	292	10	1	8	\N
1040	330	10	1	3	\N
1041	244	11	1	8	\N
1042	273	11	1	1	\N
1043	274	11	1	2	\N
1044	275	11	1	4	\N
1045	276	11	1	5	\N
1046	277	11	1	1	\N
1047	278	11	1	10	\N
1048	279	11	1	8	\N
1049	280	11	1	9	\N
1050	281	11	1	2	\N
1051	282	11	1	3	\N
1052	283	11	1	7	\N
1053	284	11	1	4	\N
1054	285	11	1	10	\N
1055	286	11	1	8	\N
1056	287	11	1	9	\N
1057	288	11	1	2	\N
1058	289	11	1	1	\N
1059	290	11	1	10	\N
1060	291	11	1	4	\N
1061	292	11	1	9	\N
1062	330	11	1	4	\N
1063	244	12	1	4	\N
1064	273	12	1	5	\N
1065	274	12	1	3	\N
1066	275	12	1	9	\N
1067	276	12	1	5	\N
1068	277	12	1	6	\N
1069	278	12	1	8	\N
1070	279	12	1	9	\N
1071	280	12	1	1	\N
1072	281	12	1	6	\N
1073	282	12	1	7	\N
1074	283	12	1	5	\N
1075	284	12	1	6	\N
1076	285	12	1	4	\N
1077	286	12	1	2	\N
1078	287	12	1	6	\N
1079	288	12	1	2	\N
1080	289	12	1	5	\N
1081	290	12	1	7	\N
1082	291	12	1	9	\N
1083	292	12	1	6	\N
1084	330	12	1	9	\N
1085	244	13	1	9	\N
1086	273	13	1	5	\N
1087	274	13	1	8	\N
1088	275	13	1	5	\N
1089	276	13	1	9	\N
1090	277	13	1	1	\N
1091	278	13	1	9	\N
1092	279	13	1	8	\N
1093	280	13	1	5	\N
1094	281	13	1	10	\N
1095	282	13	1	3	\N
1096	283	13	1	7	\N
1097	284	13	1	4	\N
1098	285	13	1	10	\N
1099	286	13	1	6	\N
1100	287	13	1	8	\N
1101	288	13	1	3	\N
1102	289	13	1	2	\N
1103	290	13	1	10	\N
1104	291	13	1	5	\N
1105	292	13	1	4	\N
1106	330	13	1	2	\N
1107	244	19	1	8	\N
1108	273	19	1	4	\N
1109	274	19	1	7	\N
1110	275	19	1	2	\N
1111	276	19	1	10	\N
1112	277	19	1	2	\N
1113	278	19	1	10	\N
1114	279	19	1	5	\N
1115	280	19	1	4	\N
1116	281	19	1	5	\N
1117	282	19	1	2	\N
1118	283	19	1	4	\N
1119	284	19	1	2	\N
1120	285	19	1	9	\N
1121	286	19	1	7	\N
1122	287	19	1	7	\N
1123	288	19	1	4	\N
1124	289	19	1	7	\N
1125	290	19	1	3	\N
1126	291	19	1	2	\N
1127	292	19	1	3	\N
1128	330	19	1	7	\N
1129	244	26	1	8	\N
1130	273	26	1	7	\N
1131	274	26	1	1	\N
1132	275	26	1	4	\N
1133	276	26	1	7	\N
1134	277	26	1	1	\N
1135	278	26	1	3	\N
1136	279	26	1	1	\N
1137	280	26	1	5	\N
1138	281	26	1	3	\N
1139	282	26	1	10	\N
1140	283	26	1	8	\N
1141	284	26	1	10	\N
1142	285	26	1	8	\N
1143	286	26	1	7	\N
1144	287	26	1	9	\N
1145	288	26	1	6	\N
1146	289	26	1	2	\N
1147	290	26	1	2	\N
1148	291	26	1	2	\N
1149	292	26	1	2	\N
1150	330	26	1	6	\N
1151	244	20	1	4	\N
1152	273	20	1	2	\N
1153	274	20	1	7	\N
1154	275	20	1	6	\N
1155	276	20	1	1	\N
1156	277	20	1	4	\N
1157	278	20	1	9	\N
1158	279	20	1	3	\N
1159	280	20	1	6	\N
1160	281	20	1	2	\N
1161	282	20	1	9	\N
1162	283	20	1	10	\N
1163	284	20	1	3	\N
1164	285	20	1	2	\N
1165	286	20	1	3	\N
1166	287	20	1	9	\N
1167	288	20	1	3	\N
1168	289	20	1	10	\N
1169	290	20	1	9	\N
1170	291	20	1	5	\N
1171	292	20	1	6	\N
1172	330	20	1	6	\N
1173	244	21	1	1	\N
1174	273	21	1	7	\N
1175	274	21	1	10	\N
1176	275	21	1	8	\N
1177	276	21	1	10	\N
1178	277	21	1	6	\N
1179	278	21	1	9	\N
1180	279	21	1	8	\N
1181	280	21	1	2	\N
1182	281	21	1	7	\N
1183	282	21	1	7	\N
1184	283	21	1	2	\N
1185	284	21	1	9	\N
1186	285	21	1	7	\N
1187	286	21	1	10	\N
1188	287	21	1	7	\N
1189	288	21	1	5	\N
1190	289	21	1	3	\N
1191	290	21	1	9	\N
1192	291	21	1	8	\N
1193	292	21	1	9	\N
1194	330	21	1	3	\N
1195	244	6	1	8	\N
1196	273	6	1	2	\N
1197	274	6	1	2	\N
1198	275	6	1	4	\N
1199	276	6	1	4	\N
1200	277	6	1	7	\N
1201	278	6	1	3	\N
1202	279	6	1	5	\N
1203	280	6	1	5	\N
1204	281	6	1	1	\N
1205	282	6	1	7	\N
1206	283	6	1	3	\N
1207	284	6	1	3	\N
1208	285	6	1	5	\N
1209	286	6	1	8	\N
1210	287	6	1	3	\N
1211	288	6	1	10	\N
1212	289	6	1	1	\N
1213	290	6	1	4	\N
1214	291	6	1	6	\N
1215	292	6	1	8	\N
1216	330	6	1	10	\N
1217	244	35	1	5	\N
1218	273	35	1	6	\N
1219	274	35	1	2	\N
1220	275	35	1	4	\N
1221	276	35	1	3	\N
1222	277	35	1	1	\N
1223	278	35	1	8	\N
1224	279	35	1	4	\N
1225	280	35	1	6	\N
1226	281	35	1	9	\N
1227	282	35	1	7	\N
1228	283	35	1	5	\N
1229	284	35	1	6	\N
1230	285	35	1	4	\N
1231	286	35	1	3	\N
1232	287	35	1	2	\N
1233	288	35	1	9	\N
1234	289	35	1	10	\N
1235	290	35	1	4	\N
1236	291	35	1	8	\N
1237	292	35	1	8	\N
1238	330	35	1	5	\N
1239	244	32	1	2	\N
1240	273	32	1	4	\N
1241	274	32	1	5	\N
1242	275	32	1	9	\N
1243	276	32	1	2	\N
1244	277	32	1	10	\N
1245	278	32	1	3	\N
1246	279	32	1	8	\N
1247	280	32	1	8	\N
1248	281	32	1	8	\N
1249	282	32	1	3	\N
1250	283	32	1	1	\N
1251	284	32	1	7	\N
1252	285	32	1	9	\N
1253	286	32	1	5	\N
1254	287	32	1	9	\N
1255	288	32	1	7	\N
1256	289	32	1	9	\N
1257	290	32	1	5	\N
1258	291	32	1	5	\N
1259	292	32	1	9	\N
1260	330	32	1	9	\N
1261	244	33	1	10	\N
1262	273	33	1	3	\N
1263	274	33	1	3	\N
1264	275	33	1	7	\N
1265	276	33	1	4	\N
1266	277	33	1	1	\N
1267	278	33	1	1	\N
1268	279	33	1	10	\N
1269	280	33	1	7	\N
1270	281	33	1	6	\N
1271	282	33	1	8	\N
1272	283	33	1	2	\N
1273	284	33	1	7	\N
1274	285	33	1	7	\N
1275	286	33	1	5	\N
1276	287	33	1	1	\N
1277	288	33	1	2	\N
1278	289	33	1	4	\N
1279	290	33	1	3	\N
1280	291	33	1	4	\N
1281	292	33	1	2	\N
1282	330	33	1	6	\N
1283	244	16	1	4	\N
1284	273	16	1	3	\N
1285	274	16	1	1	\N
1286	275	16	1	6	\N
1287	276	16	1	7	\N
1288	277	16	1	4	\N
1289	278	16	1	1	\N
1290	279	16	1	2	\N
1291	280	16	1	7	\N
1292	281	16	1	9	\N
1293	282	16	1	4	\N
1294	283	16	1	3	\N
1295	284	16	1	3	\N
1296	285	16	1	4	\N
1297	286	16	1	2	\N
1298	287	16	1	5	\N
1299	288	16	1	8	\N
1300	289	16	1	5	\N
1301	290	16	1	1	\N
1302	291	16	1	9	\N
1303	292	16	1	10	\N
1304	330	16	1	5	\N
1305	244	17	1	5	\N
1306	273	17	1	9	\N
1307	274	17	1	8	\N
1308	275	17	1	4	\N
1309	276	17	1	4	\N
1310	277	17	1	8	\N
1311	278	17	1	6	\N
1312	279	17	1	6	\N
1313	280	17	1	3	\N
1314	281	17	1	7	\N
1315	282	17	1	4	\N
1316	283	17	1	8	\N
1317	284	17	1	9	\N
1318	285	17	1	1	\N
1319	286	17	1	6	\N
1320	287	17	1	6	\N
1321	288	17	1	1	\N
1322	289	17	1	7	\N
1323	290	17	1	2	\N
1324	291	17	1	9	\N
1325	292	17	1	9	\N
1326	330	17	1	7	\N
1327	244	27	1	9	\N
1328	273	27	1	4	\N
1329	274	27	1	3	\N
1330	275	27	1	2	\N
1331	276	27	1	1	\N
1332	277	27	1	8	\N
1333	278	27	1	4	\N
1334	279	27	1	9	\N
1335	280	27	1	1	\N
1336	281	27	1	7	\N
1337	282	27	1	1	\N
1338	283	27	1	4	\N
1339	284	27	1	5	\N
1340	285	27	1	2	\N
1341	286	27	1	6	\N
1342	287	27	1	2	\N
1343	288	27	1	4	\N
1344	289	27	1	1	\N
1345	290	27	1	2	\N
1346	291	27	1	9	\N
1347	292	27	1	7	\N
1348	330	27	1	8	\N
1349	244	14	1	10	\N
1350	273	14	1	5	\N
1351	274	14	1	5	\N
1352	275	14	1	9	\N
1353	276	14	1	10	\N
1354	277	14	1	4	\N
1355	278	14	1	9	\N
1356	279	14	1	2	\N
1357	280	14	1	1	\N
1358	281	14	1	1	\N
1359	282	14	1	10	\N
1360	283	14	1	7	\N
1361	284	14	1	3	\N
1362	285	14	1	6	\N
1363	286	14	1	7	\N
1364	287	14	1	9	\N
1365	288	14	1	5	\N
1366	289	14	1	6	\N
1367	290	14	1	4	\N
1368	291	14	1	3	\N
1369	292	14	1	3	\N
1370	330	14	1	4	\N
1371	244	15	1	1	\N
1372	273	15	1	2	\N
1373	274	15	1	6	\N
1374	275	15	1	4	\N
1375	276	15	1	4	\N
1376	277	15	1	4	\N
1377	278	15	1	9	\N
1378	279	15	1	3	\N
1379	280	15	1	8	\N
1380	281	15	1	1	\N
1381	282	15	1	8	\N
1382	283	15	1	7	\N
1383	284	15	1	1	\N
1384	285	15	1	2	\N
1385	286	15	1	7	\N
1386	287	15	1	1	\N
1387	288	15	1	2	\N
1388	289	15	1	6	\N
1389	290	15	1	4	\N
1390	291	15	1	3	\N
1391	292	15	1	3	\N
1392	330	15	1	5	\N
1393	244	22	1	6	\N
1394	273	22	1	5	\N
1395	274	22	1	6	\N
1396	275	22	1	4	\N
1397	276	22	1	2	\N
1398	277	22	1	9	\N
1399	278	22	1	9	\N
1400	279	22	1	4	\N
1401	280	22	1	6	\N
1402	281	22	1	5	\N
1403	282	22	1	8	\N
1404	283	22	1	6	\N
1405	284	22	1	5	\N
1406	285	22	1	4	\N
1407	286	22	1	6	\N
1408	287	22	1	5	\N
1409	288	22	1	10	\N
1410	289	22	1	2	\N
1411	290	22	1	1	\N
1412	291	22	1	8	\N
1413	292	22	1	8	\N
1414	330	22	1	3	\N
1415	244	23	1	1	\N
1416	273	23	1	7	\N
1417	274	23	1	9	\N
1418	275	23	1	4	\N
1419	276	23	1	3	\N
1420	277	23	1	3	\N
1421	278	23	1	4	\N
1422	279	23	1	10	\N
1423	280	23	1	7	\N
1424	281	23	1	4	\N
1425	282	23	1	8	\N
1426	283	23	1	7	\N
1427	284	23	1	2	\N
1428	285	23	1	8	\N
1429	286	23	1	3	\N
1430	287	23	1	1	\N
1431	288	23	1	7	\N
1432	289	23	1	9	\N
1433	290	23	1	7	\N
1434	291	23	1	4	\N
1435	292	23	1	8	\N
1436	330	23	1	10	\N
1437	244	24	1	6	\N
1438	273	24	1	10	\N
1439	274	24	1	1	\N
1440	275	24	1	5	\N
1441	276	24	1	10	\N
1442	277	24	1	3	\N
1443	278	24	1	8	\N
1444	279	24	1	8	\N
1445	280	24	1	6	\N
1446	281	24	1	5	\N
1447	282	24	1	9	\N
1448	283	24	1	7	\N
1449	284	24	1	6	\N
1450	285	24	1	2	\N
1451	286	24	1	8	\N
1452	287	24	1	7	\N
1453	288	24	1	9	\N
1454	289	24	1	6	\N
1455	290	24	1	10	\N
1456	291	24	1	10	\N
1457	292	24	1	1	\N
1458	330	24	1	4	\N
1459	244	25	1	8	\N
1460	273	25	1	7	\N
1461	274	25	1	9	\N
1462	275	25	1	5	\N
1463	276	25	1	5	\N
1464	277	25	1	3	\N
1465	278	25	1	6	\N
1466	279	25	1	3	\N
1467	280	25	1	7	\N
1468	281	25	1	4	\N
1469	282	25	1	10	\N
1470	283	25	1	8	\N
1471	284	25	1	1	\N
1472	285	25	1	7	\N
1473	286	25	1	4	\N
1474	287	25	1	6	\N
1475	288	25	1	7	\N
1476	289	25	1	6	\N
1477	290	25	1	9	\N
1478	291	25	1	8	\N
1479	292	25	1	1	\N
1480	330	25	1	1	\N
1481	244	28	1	2	\N
1482	273	28	1	8	\N
1483	274	28	1	2	\N
1484	275	28	1	5	\N
1485	276	28	1	8	\N
1486	277	28	1	5	\N
1487	278	28	1	9	\N
1488	279	28	1	10	\N
1489	280	28	1	4	\N
1490	281	28	1	8	\N
1491	282	28	1	7	\N
1492	283	28	1	1	\N
1493	284	28	1	5	\N
1494	285	28	1	10	\N
1495	286	28	1	3	\N
1496	287	28	1	8	\N
1497	288	28	1	7	\N
1498	289	28	1	3	\N
1499	290	28	1	3	\N
1500	291	28	1	5	\N
1501	292	28	1	4	\N
1502	330	28	1	4	\N
1503	244	29	1	9	\N
1504	273	29	1	2	\N
1505	274	29	1	3	\N
1506	275	29	1	10	\N
1507	276	29	1	8	\N
1508	277	29	1	8	\N
1509	278	29	1	8	\N
1510	279	29	1	6	\N
1511	280	29	1	7	\N
1512	281	29	1	4	\N
1513	282	29	1	8	\N
1514	283	29	1	6	\N
1515	284	29	1	8	\N
1516	285	29	1	10	\N
1517	286	29	1	7	\N
1518	287	29	1	4	\N
1519	288	29	1	8	\N
1520	289	29	1	6	\N
1521	290	29	1	9	\N
1522	291	29	1	3	\N
1523	292	29	1	8	\N
1524	330	29	1	2	\N
1525	244	30	1	5	\N
1526	273	30	1	10	\N
1527	274	30	1	2	\N
1528	275	30	1	1	\N
1529	276	30	1	6	\N
1530	277	30	1	9	\N
1531	278	30	1	4	\N
1532	279	30	1	5	\N
1533	280	30	1	2	\N
1534	281	30	1	6	\N
1535	282	30	1	10	\N
1536	283	30	1	5	\N
1537	284	30	1	3	\N
1538	285	30	1	1	\N
1539	286	30	1	5	\N
1540	287	30	1	1	\N
1541	288	30	1	9	\N
1542	289	30	1	4	\N
1543	290	30	1	10	\N
1544	291	30	1	10	\N
1545	292	30	1	9	\N
1546	330	30	1	3	\N
1547	244	31	1	8	\N
1548	273	31	1	9	\N
1549	274	31	1	2	\N
1550	275	31	1	2	\N
1551	276	31	1	4	\N
1552	277	31	1	1	\N
1553	278	31	1	6	\N
1554	279	31	1	4	\N
1555	280	31	1	10	\N
1556	281	31	1	1	\N
1557	282	31	1	2	\N
1558	283	31	1	6	\N
1559	284	31	1	5	\N
1560	285	31	1	8	\N
1561	286	31	1	2	\N
1562	287	31	1	4	\N
1563	288	31	1	6	\N
1564	289	31	1	6	\N
1565	290	31	1	4	\N
1566	291	31	1	10	\N
1567	292	31	1	7	\N
1568	330	31	1	7	\N
1569	244	36	1	1	\N
1570	273	36	1	5	\N
1571	274	36	1	10	\N
1572	275	36	1	6	\N
1573	276	36	1	10	\N
1574	277	36	1	2	\N
1575	278	36	1	7	\N
1576	279	36	1	10	\N
1577	280	36	1	7	\N
1578	281	36	1	4	\N
1579	282	36	1	10	\N
1580	283	36	1	6	\N
1581	284	36	1	2	\N
1582	285	36	1	10	\N
1583	286	36	1	4	\N
1584	287	36	1	8	\N
1585	288	36	1	7	\N
1586	289	36	1	2	\N
1587	290	36	1	8	\N
1588	291	36	1	2	\N
1589	292	36	1	8	\N
1590	330	36	1	3	\N
1591	244	37	1	9	\N
1592	273	37	1	6	\N
1593	274	37	1	2	\N
1594	275	37	1	4	\N
1595	276	37	1	3	\N
1596	277	37	1	10	\N
1597	278	37	1	2	\N
1598	279	37	1	10	\N
1599	280	37	1	4	\N
1600	281	37	1	7	\N
1601	282	37	1	4	\N
1602	283	37	1	9	\N
1603	284	37	1	10	\N
1604	285	37	1	5	\N
1605	286	37	1	5	\N
1606	287	37	1	10	\N
1607	288	37	1	5	\N
1608	289	37	1	5	\N
1609	290	37	1	8	\N
1610	291	37	1	3	\N
1611	292	37	1	3	\N
1612	330	37	1	7	\N
1613	244	38	1	3	\N
1614	273	38	1	2	\N
1615	274	38	1	3	\N
1616	275	38	1	9	\N
1617	276	38	1	3	\N
1618	277	38	1	8	\N
1619	278	38	1	3	\N
1620	279	38	1	8	\N
1621	280	38	1	10	\N
1622	281	38	1	5	\N
1623	282	38	1	5	\N
1624	283	38	1	1	\N
1625	284	38	1	7	\N
1626	285	38	1	1	\N
1627	286	38	1	7	\N
1628	287	38	1	8	\N
1629	288	38	1	1	\N
1630	289	38	1	1	\N
1631	290	38	1	1	\N
1632	291	38	1	10	\N
1633	292	38	1	7	\N
1634	330	38	1	7	\N
1636	211	2	\N	1	\N
8	178	1	1	7	\N
\.


--
-- Data for Name: tasques; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tasques (id, idunitatformativa, nomtasca, descripcio, hores, obligatoria) FROM stdin;
1	34	usuaris	gestio usuaris	4	1
3	34	privilegis	gestio de privilegis	4	1
4	34	programacio sql	programacio sql	2	\N
5	34	programacio plpgsql	programacio plpgsql	2	0
6	34	triggers	triggers (1)	2	1
7	34	cursors	cursors	2	\N
8	34	alquimista	construeix triggers	4	1
\.


--
-- Data for Name: tasques_alumnes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tasques_alumnes (id, idtasca, idalumne, nota, observacions) FROM stdin;
1	1	171	7	\N
2	1	172	3	\N
3	1	173	3	\N
4	1	174	5	\N
5	1	175	6	\N
6	1	176	4	\N
7	1	177	3	\N
8	1	178	1	\N
9	1	179	6	\N
10	1	180	8	\N
11	1	181	10	\N
12	1	182	8	\N
13	1	183	8	\N
14	1	184	4	\N
15	1	185	7	\N
16	1	186	3	\N
17	1	187	1	\N
18	1	265	7	\N
19	1	266	2	\N
20	1	267	6	\N
21	1	268	5	\N
22	3	171	6	\N
23	3	172	7	\N
24	3	173	3	\N
25	3	174	6	\N
26	3	175	10	\N
27	3	176	10	\N
28	3	177	1	\N
29	3	178	6	\N
30	3	179	6	\N
31	3	180	10	\N
32	3	181	4	\N
33	3	182	9	\N
34	3	183	3	\N
35	3	184	6	\N
36	3	185	10	\N
37	3	186	1	\N
38	3	187	6	\N
39	3	265	4	\N
40	3	266	5	\N
41	3	267	10	\N
42	3	268	6	\N
43	4	171	10	\N
44	4	172	9	\N
45	4	173	5	\N
46	4	174	7	\N
47	4	175	9	\N
48	4	176	4	\N
49	4	177	4	\N
50	4	178	5	\N
51	4	179	5	\N
52	4	180	9	\N
53	4	181	10	\N
54	4	182	4	\N
55	4	183	6	\N
56	4	184	10	\N
57	4	185	10	\N
58	4	186	9	\N
59	4	187	3	\N
60	4	265	9	\N
61	4	266	7	\N
62	4	267	6	\N
63	4	268	1	\N
64	5	171	3	\N
65	5	172	4	\N
66	5	173	10	\N
67	5	174	8	\N
68	5	175	1	\N
69	5	176	7	\N
70	5	177	3	\N
71	5	178	3	\N
72	5	179	6	\N
73	5	180	2	\N
74	5	181	10	\N
75	5	182	5	\N
76	5	183	3	\N
77	5	184	1	\N
78	5	185	3	\N
79	5	186	3	\N
80	5	187	5	\N
81	5	265	6	\N
82	5	266	6	\N
83	5	267	1	\N
84	5	268	5	\N
85	6	171	4	\N
86	6	172	4	\N
87	6	173	6	\N
88	6	174	8	\N
89	6	175	1	\N
90	6	176	1	\N
91	6	177	10	\N
92	6	178	9	\N
93	6	179	3	\N
94	6	180	9	\N
95	6	181	4	\N
96	6	182	3	\N
97	6	183	1	\N
98	6	184	8	\N
99	6	185	3	\N
100	6	186	4	\N
101	6	187	10	\N
102	6	265	8	\N
103	6	266	9	\N
104	6	267	10	\N
105	6	268	3	\N
106	7	171	3	\N
107	7	172	6	\N
108	7	173	10	\N
109	7	174	2	\N
110	7	175	8	\N
111	7	176	4	\N
112	7	177	5	\N
113	7	178	4	\N
114	7	179	3	\N
115	7	180	2	\N
116	7	181	3	\N
117	7	182	6	\N
118	7	183	2	\N
119	7	184	2	\N
120	7	185	2	\N
121	7	186	6	\N
122	7	187	1	\N
123	7	265	9	\N
124	7	266	9	\N
125	7	267	10	\N
126	7	268	1	\N
127	8	171	3	\N
128	8	172	2	\N
129	8	173	2	\N
130	8	174	5	\N
131	8	175	6	\N
132	8	176	5	\N
133	8	177	8	\N
134	8	178	4	\N
135	8	179	5	\N
136	8	180	3	\N
137	8	181	7	\N
138	8	182	8	\N
139	8	183	8	\N
140	8	184	6	\N
141	8	185	5	\N
142	8	186	6	\N
143	8	187	6	\N
144	8	265	10	\N
145	8	266	10	\N
146	8	267	2	\N
147	8	268	10	\N
148	1	244	4	\N
149	1	273	10	\N
150	1	274	6	\N
151	1	275	2	\N
152	1	276	10	\N
153	1	277	3	\N
154	1	278	4	\N
155	1	279	2	\N
156	1	280	5	\N
157	1	281	10	\N
158	1	282	3	\N
159	1	283	7	\N
160	1	284	3	\N
161	1	285	4	\N
162	1	286	10	\N
163	1	287	8	\N
164	1	288	1	\N
165	1	289	9	\N
166	1	290	4	\N
167	1	291	10	\N
168	1	292	7	\N
169	1	330	4	\N
170	3	244	8	\N
171	3	273	9	\N
172	3	274	8	\N
173	3	275	6	\N
174	3	276	3	\N
175	3	277	6	\N
176	3	278	3	\N
177	3	279	7	\N
178	3	280	6	\N
179	3	281	8	\N
180	3	282	2	\N
181	3	283	6	\N
182	3	284	1	\N
183	3	285	9	\N
184	3	286	9	\N
185	3	287	1	\N
186	3	288	4	\N
187	3	289	7	\N
188	3	290	2	\N
189	3	291	1	\N
190	3	292	7	\N
191	3	330	2	\N
192	4	244	8	\N
193	4	273	5	\N
194	4	274	9	\N
195	4	275	10	\N
196	4	276	2	\N
197	4	277	10	\N
198	4	278	4	\N
199	4	279	10	\N
200	4	280	6	\N
201	4	281	1	\N
202	4	282	7	\N
203	4	283	10	\N
204	4	284	8	\N
205	4	285	9	\N
206	4	286	10	\N
207	4	287	4	\N
208	4	288	1	\N
209	4	289	10	\N
210	4	290	9	\N
211	4	291	2	\N
212	4	292	3	\N
213	4	330	9	\N
214	5	244	7	\N
215	5	273	6	\N
216	5	274	8	\N
217	5	275	3	\N
218	5	276	1	\N
219	5	277	3	\N
220	5	278	4	\N
221	5	279	2	\N
222	5	280	4	\N
223	5	281	6	\N
224	5	282	8	\N
225	5	283	1	\N
226	5	284	8	\N
227	5	285	8	\N
228	5	286	7	\N
229	5	287	1	\N
230	5	288	2	\N
231	5	289	9	\N
232	5	290	5	\N
233	5	291	10	\N
234	5	292	3	\N
235	5	330	4	\N
236	6	244	1	\N
237	6	273	4	\N
238	6	274	6	\N
239	6	275	7	\N
240	6	276	7	\N
241	6	277	5	\N
242	6	278	10	\N
243	6	279	7	\N
244	6	280	4	\N
245	6	281	8	\N
246	6	282	9	\N
247	6	283	10	\N
248	6	284	1	\N
249	6	285	4	\N
250	6	286	9	\N
251	6	287	3	\N
252	6	288	8	\N
253	6	289	10	\N
254	6	290	6	\N
255	6	291	9	\N
256	6	292	5	\N
257	6	330	10	\N
258	7	244	5	\N
259	7	273	5	\N
260	7	274	8	\N
261	7	275	4	\N
262	7	276	6	\N
263	7	277	8	\N
264	7	278	3	\N
265	7	279	10	\N
266	7	280	8	\N
267	7	281	1	\N
268	7	282	2	\N
269	7	283	4	\N
270	7	284	6	\N
271	7	285	5	\N
272	7	286	6	\N
273	7	287	4	\N
274	7	288	3	\N
275	7	289	3	\N
276	7	290	5	\N
277	7	291	5	\N
278	7	292	10	\N
279	7	330	4	\N
280	8	244	9	\N
281	8	273	3	\N
282	8	274	6	\N
283	8	275	1	\N
284	8	276	7	\N
285	8	277	2	\N
286	8	278	9	\N
287	8	279	9	\N
288	8	280	6	\N
289	8	281	4	\N
290	8	282	10	\N
291	8	283	9	\N
292	8	284	6	\N
293	8	285	10	\N
294	8	286	4	\N
295	8	287	7	\N
296	8	288	2	\N
297	8	289	1	\N
298	8	290	7	\N
299	8	291	3	\N
300	8	292	4	\N
301	8	330	8	\N
\.


--
-- Data for Name: unitatsformatives; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.unitatsformatives (id, idmodul, unitat, hores) FROM stdin;
14	10	1	66
15	10	2	33
18	1	4	33
22	12	1	25
23	12	2	25
24	12	3	25
32	8	1	33
33	8	2	33
35	7	3	33
36	14	1	66
1	1	1	49
2	1	2	69
3	1	3	14
4	2	1	25
5	2	2	58
107	24	1	45
8	3	2	39
9	3	3	19
7	3	1	107
10	4	1	45
11	4	2	27
12	4	3	27
13	5	1	22
19	5	2	22
16	9	1	44
17	9	2	44
6	7	1	33
34	2	3	49
26	5	3	22
20	6	1	70
21	6	2	62
27	9	3	44
25	12	4	24
28	13	1	16
29	13	2	16
30	13	3	19
31	13	4	15
37	15	1	297
38	16	1	383
39	47	1	45
40	47	2	36
41	47	3	18
42	48	1	15
43	48	2	38
44	48	3	29
45	48	4	28
50	49	4	30
49	49	3	25
48	49	2	40
47	49	1	54
46	47	4	22
52	50	1	39
53	50	2	23
54	50	2	15
55	51	1	18
56	51	2	18
57	51	3	19
58	52	1	22
51	49	5	38
59	52	2	22
60	52	3	22
61	52	4	22
62	53	1	67
63	53	2	21
64	54	1	36
65	54	2	18
66	54	3	23
67	55	1	32
108	24	2	24
68	55	2	23
69	56	1	27
70	56	2	28
71	57	1	33
72	57	2	33
73	58	1	66
74	59	1	297
75	60	1	383
76	61	1	33
77	62	1	30
78	62	2	58
79	63	1	33
80	63	2	33
81	63	3	44
82	63	4	44
83	17	1	49
84	17	2	69
85	17	3	14
86	18	1	25
87	18	2	58
88	18	3	66
89	18	4	16
90	19	1	74
91	19	2	39
92	19	3	19
93	19	4	24
94	19	5	57
95	19	6	18
96	20	1	45
97	20	2	27
98	20	3	27
99	21	1	20
100	21	2	20
101	22	1	24
102	22	2	27
103	22	3	24
104	22	4	24
105	23	1	79
106	23	2	20
109	24	3	66
110	25	1	14
111	25	2	26
112	25	3	26
113	26	1	33
114	26	2	66
115	27	1	33
116	27	2	33
117	28	1	66
118	29	1	297
119	30	1	383
\.


--
-- Data for Name: usuari_cicle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuari_cicle (idusuari, cicle, notafinal, data) FROM stdin;
\.


--
-- Data for Name: usuaris; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuaris (idusuari, nomusuari, cognom1usuari, emailusuari, password, idgrup, rol, datanaixement, actiu, repetidor, cognom2usuari, bloquejat, intents, sexe, ultimacces) FROM stdin;
173	Santi	Pineiro 	SPY003@ies-sabadell.cat	S03P	6	A	2002-08-31	-1	0	Yagues	0	0	H	\N
1	root	root	root@ies-sabadell.cat	root	\N	R	\N	1	0	\N	0	0	H	\N
174	Ruben	Mendez 	RMH004@ies-sabadell.cat	R04M	6	A	2001-02-03	-1	0	Hernandez	0	0	H	\N
175	Nerea	Peralta 	NPR005@ies-sabadell.cat	N05P	6	A	1997-02-23	-1	1	Rojas	0	0	D	\N
176	Arnau	Porqueres 	APM006@ies-sabadell.cat	A06P	6	A	2000-11-17	-1	1	Marfany	0	0	H	\N
177	Felix	Pellicer 	FPB007@ies-sabadell.cat	F07P	6	A	2001-03-19	-1	0	Bautista	0	0	H	\N
178	Francisco jose	Vargas 	FVM008@ies-sabadell.cat	F08V	6	A	1998-02-06	-1	0	Moreno	0	0	H	\N
179	Cristian	Fernandez 	CFM009@ies-sabadell.cat	C09F	6	A	2002-10-02	-1	1	Marquez	0	0	H	\N
180	angel	Mendez 	aMR010@ies-sabadell.cat	a10M	6	A	2001-01-08	-1	1	Reyes	0	0	H	\N
182	Carles	Folch 	CFG012@ies-sabadell.cat	C12F	6	A	1975-09-01	-1	1	Gonzalez	0	0	H	\N
183	Alejandro	Lopez 	ALL013@ies-sabadell.cat	A13L	6	A	2003-02-04	-1	1	Lopez	0	0	H	\N
185	George Dragos	Ion	GIZ015@ies-sabadell.cat	G15I	6	A	2003-07-15	-1	1	\N	0	0	H	\N
186	Miguel	Carrizosa 	MCC016@ies-sabadell.cat	M16C	6	A	2001-04-12	-1	1	Caballero	0	0	H	\N
187	Christian	De La Rubia	CDC017@ies-sabadell.cat	C17D	6	A	1997-03-20	-1	1	Collado	0	0	H	\N
188	Alex	Lopez 	ALM018@ies-sabadell.cat	A18L	6	A	2002-03-14	-1	1	Murcia	0	0	H	\N
189	Fouad	Hassidou	FHZ019@ies-sabadell.cat	F19H	6	A	2001-07-14	-1	1	\N	0	0	H	\N
190	Abraham	Vanegas 	AVG020@ies-sabadell.cat	A20V	6	A	2003-03-21	-1	1	Gutierrez	0	0	H	\N
191	Alex	Quinonero 	AQM021@ies-sabadell.cat	A21Q	6	A	2002-12-10	-1	1	Marquez	0	0	H	\N
192	Naomy	Del Aguila	NDA022@ies-sabadell.cat	N22D	3	A	2002-05-31	-1	1	Angulo	0	0	D	\N
193	Raul Marti	Edo 	REA023@ies-sabadell.cat	R23E	3	A	2003-07-03	-1	1	Andres	0	0	H	\N
194	David	Casado 	DCR024@ies-sabadell.cat	D24C	6	A	2002-09-07	-1	1	Rojo	0	0	H	\N
195	Alejandro	Villar 	AVP025@ies-sabadell.cat	A25V	1	A	2002-10-13	-1	1	Perez	0	0	H	\N
196	Abraham	Lucena 	ALC026@ies-sabadell.cat	A26L	3	A	2000-10-23	-1	1	Cuenda	0	0	H	\N
197	Raul	Godinez 	RGG027@ies-sabadell.cat	R27G	6	A	1983-04-28	-1	0	Gonzalez	0	0	H	\N
198	Salvador	Cieza 	SCI028@ies-sabadell.cat	S28C	8	A	2003-05-21	-1	1	Ibanez	0	0	H	\N
199	Noah	Ramos 	NRB029@ies-sabadell.cat	N29R	8	A	2003-12-05	-1	1	Bobis	0	0	H	\N
200	Izan	Mata 	IMD030@ies-sabadell.cat	I30M	8	A	2003-03-03	-1	1	Diaz	0	0	H	\N
201	Alex	Rodriguez 	ARL031@ies-sabadell.cat	A31R	9	A	2001-05-22	-1	0	Lores	0	0	H	\N
202	Jose Manuel	Fuentes 	JFP032@ies-sabadell.cat	J32F	9	A	1991-12-25	-1	1	Pena	0	0	H	\N
203	Luis	Medina 	LMF033@ies-sabadell.cat	L33M	9	A	2000-02-29	-1	0	Fernandez	0	0	H	\N
204	Ruben	Urbano 	RUM034@ies-sabadell.cat	R34U	9	A	1998-07-22	-1	0	Martinez	0	0	H	\N
205	Eric	Noguera 	ENG035@ies-sabadell.cat	E35N	9	A	1998-02-03	-1	0	Galan	0	0	H	\N
206	Carmen Pilar	Perez 	CPC036@ies-sabadell.cat	C36P	9	A	2000-01-26	-1	0	Contreras	0	0	D	\N
208	Pol	Sotillos 	PSH038@ies-sabadell.cat	P38S	9	A	2001-12-21	-1	0	Heritsch	0	0	H	\N
209	Oscar	Troya 	OTC039@ies-sabadell.cat	O39T	9	A	2003-02-03	-1	1	Castilla	0	0	H	\N
210	Raul	Martin-Caro 	RMG040@ies-sabadell.cat	R40M	9	A	1991-08-15	-1	1	Gomez	0	0	H	\N
211	Carlos	Alises 	CAM041@ies-sabadell.cat	C41A	9	A	2003-09-21	-1	1	Mora	0	0	H	\N
212	Alex	de Pablo	AdS042@ies-sabadell.cat	A42d	9	A	2003-03-31	-1	1	Sanchez	0	0	H	\N
213	Hang	Chen	HCZ043@ies-sabadell.cat	H43C	9	A	2003-05-22	-1	1	\N	0	0	H	\N
214	Keivn Israel	Sandoya 	KSC044@ies-sabadell.cat	K44S	9	A	1997-10-17	-1	0	Chavez	0	0	H	\N
215	Adria	Moyano 	AMG045@ies-sabadell.cat	A45M	9	A	2003-11-03	-1	1	Garcia 	0	0	H	\N
216	Alex	Castet 	ACP046@ies-sabadell.cat	A46C	9	A	2003-07-12	-1	1	Pedrosa	0	0	H	\N
217	Alejandro	Andujar 	AAG047@ies-sabadell.cat	A47A	9	A	2003-09-19	-1	1	Garcia	0	0	H	\N
219	Tony	Pulido 	TPD049@ies-sabadell.cat	T49P	9	A	2003-05-13	-1	1	Dragon	0	0	H	\N
220	Alejandro	Munoz 	AMM050@ies-sabadell.cat	A50M	8	A	2002-10-05	-1	1	Marin	0	0	H	\N
221	Ana Maria	Gomez 	AGL051@ies-sabadell.cat	A51G	5	A	1999-01-26	-1	1	Leon	0	0	D	\N
172	Iker	Monino 	IMM002@ies-sabadell.cat	I02M	6	A	2002-02-15	-1	1	Martin	0	0	H	\N
223	Selene	Milanes 	SMR053@ies-sabadell.cat	S53M	5	A	1995-02-03	-1	1	Rodriguez	0	0	D	\N
224	Francesc	Zamora 	FZM054@ies-sabadell.cat	F54Z	5	A	2003-01-31	-1	1	Montero	0	0	H	\N
225	Lucas	Lopez 	LLD055@ies-sabadell.cat	L55L	5	A	2003-07-09	-1	1	Daunis	0	0	H	\N
226	Alex	Abalos 	AAA056@ies-sabadell.cat	A56A	5	A	2003-10-15	-1	1	Arcas	0	0	H	\N
227	Adria	Pages 	APE057@ies-sabadell.cat	A57P	5	A	2003-11-12	-1	1	Exposito	0	0	H	\N
228	Ethan	Corchero 	ECM058@ies-sabadell.cat	E58C	5	A	2002-08-25	-1	1	Martin	0	0	H	\N
229	Aitor	Saldana 	ASS059@ies-sabadell.cat	A59S	5	A	1996-06-26	-1	1	Siles	0	0	H	\N
230	Izan	Blanco 	IBS060@ies-sabadell.cat	I60B	5	A	2004-06-24	-1	1	Segovia	0	0	H	\N
231	Ramon	Salvatierra 	RSG061@ies-sabadell.cat	R61S	5	A	2004-06-09	-1	1	Garcia	0	0	H	\N
232	Pablo Andres	Gonzalez 	PGS062@ies-sabadell.cat	P62G	1	A	2003-10-03	-1	1	Sierra	0	0	H	\N
234	Alvaro	Lopez 	aLC064@ies-sabadell.cat	a64L	5	A	2000-12-08	-1	1	Cervera	0	0	H	\N
181	Xavier 	Bastos	XBZ011@ies-sabadell.cat	X11B	6	A	2002-10-17	-1	0	\N	0	0	H	\N
245	Ismael	Trujillo 	ITT075@ies-sabadell.cat	I75T	1	A	1999-05-04	-1	1	Taipe	0	0	H	\N
235	David	Aperador 	DAS065@ies-sabadell.cat	D65A	9	A	2003-02-25	-1	1	Salcedo	0	0	H	\N
270	Sergi	Castarnado 	SCO100@ies-sabadell.cat	S100C	7	A	2002-07-30	-1	1	Ordonez	0	0	H	\N
271	Alma	Morante 	AMM101@ies-sabadell.cat	A101M	7	A	1996-09-06	-1	1	Morales	0	0	D	\N
272	Alex Vancells	Vancells 	AVC102@ies-sabadell.cat	A102V	7	A	1996-01-07	-1	1	Ceron	0	0	H	\N
273	Arnau	Lopez 	ALC103@ies-sabadell.cat	A103L	7	A	1998-12-03	-1	1	Cano	0	0	H	\N
236	Ona	Aneas 	OAT066@ies-sabadell.cat	O66A	4	A	2004-10-17	-1	1	Tarres	0	0	D	\N
237	Raul	Pavon 	RPM067@ies-sabadell.cat	R67P	4	A	2004-01-30	-1	1	Martinez	0	0	H	\N
298	Isaac	Gonzalez 	IGM128@ies-sabadell.cat	I128G	8	A	1987-08-21	-1	1	Morera	0	0	H	\N
238	Aaron	Leon 	ALG068@ies-sabadell.cat	A68L	4	A	2004-05-07	-1	1	Garcia	0	0	H	\N
239	Jonatan	Funes 	JFA069@ies-sabadell.cat	J69F	4	A	2004-02-12	-1	1	Aguera	0	0	H	\N
240	Gisela	Maiz 	GMM070@ies-sabadell.cat	G70M	4	A	2003-07-29	-1	1	Martin	0	0	D	\N
241	Marti	Comas 	MCC071@ies-sabadell.cat	M71C	4	A	2004-12-09	-1	1	Canal	0	0	H	\N
242	Frank	Harter 	FHM072@ies-sabadell.cat	F72H	1	A	2004-04-22	-1	1	Molina	0	0	H	\N
243	Roger	Cots 	RCA073@ies-sabadell.cat	R73C	1	A	1997-10-12	-1	0	Aldama	0	0	H	\N
244	Alex 	Marti 	AMM074@ies-sabadell.cat	A74M	6	A	1997-08-26	-1	0	Maza	0	0	H	\N
246	Jordi	Gomez 	JGL076@ies-sabadell.cat	J76G	1	A	2002-08-22	-1	1	Lindo	0	0	H	\N
247	David	Sanchez 	DSN077@ies-sabadell.cat	D77S	1	A	2000-03-02	-1	0	Navarrete	0	0	H	\N
248	Valeria Lisset	Ortiz 	VOL078@ies-sabadell.cat	V78O	1	A	2001-11-19	-1	1	Loor	0	0	D	\N
249	alex	Fernandez 	aFS079@ies-sabadell.cat	a79F	1	A	2004-04-11	-1	1	Sanchez	0	0	H	\N
250	robinzon	gastelu 	rgc080@ies-sabadell.cat	r80g	1	A	2001-06-20	-1	1	coha	0	0	H	\N
251	Alex	Gil 	AGB081@ies-sabadell.cat	A81G	1	A	2002-11-17	-1	1	Bascunana	0	0	H	\N
253	Marc	Gomez 	MGG083@ies-sabadell.cat	M83G	1	A	2004-05-04	-1	1	Gonzalez	0	0	H	\N
254	Pablo	Rodriguez 	PRM084@ies-sabadell.cat	P84R	1	A	2004-06-19	-1	1	Martinez	0	0	H	\N
255	Ivan	Aperador 	IAM085@ies-sabadell.cat	I85A	1	A	2004-11-20	-1	1	Morante	0	0	H	\N
256	Ivan	Perez 	IPS086@ies-sabadell.cat	I86P	1	A	2004-10-03	-1	1	Suarez	0	0	H	\N
257	Jan 	Carbonell 	JCF087@ies-sabadell.cat	J87C	1	A	2003-09-17	-1	1	Frutos	0	0	H	\N
258	Marc	Moreno 	MMC088@ies-sabadell.cat	M88M	1	A	2004-07-28	-1	1	Cabacas	0	0	H	\N
259	Manuel Jose	Quintero 	MQC089@ies-sabadell.cat	M89Q	1	A	2000-02-14	-1	1	Cruz	0	0	H	\N
260	Oscar	Rejas 	ORM090@ies-sabadell.cat	O90R	1	A	2003-03-24	-1	1	Marin	0	0	H	\N
261	Unai 	Fernandez 	UFB091@ies-sabadell.cat	U91F	7	A	2002-05-08	-1	1	Barrada	0	0	H	\N
264	Eloi	Redondo 	ERP094@ies-sabadell.cat	E94R	7	A	1999-11-11	-1	1	Pereira	0	0	H	\N
265	Ismael	Rodriguez 	IRA095@ies-sabadell.cat	I95R	7	A	1998-05-20	-1	0	Abselam	0	0	H	\N
266	Roberto	Gonzalez 	RGL096@ies-sabadell.cat	R96G	7	A	2002-02-14	-1	1	Linares	0	0	H	\N
267	Brian Gabriel	Coronel 	BCP097@ies-sabadell.cat	B97C	7	A	1998-03-08	-1	1	Poncelas	0	0	H	\N
268	Jonathan	Santiago 	JSA098@ies-sabadell.cat	J98S	7	A	1988-06-02	-1	1	Alguacil	0	0	H	\N
269	Alejandro	Casanova 	ACO099@ies-sabadell.cat	A99C	7	A	1999-01-11	-1	1	Ortega	0	0	H	\N
274	Pol	Gonzalo 	PGM104@ies-sabadell.cat	P104G	7	A	2001-11-20	-1	1	Mendez	0	0	H	\N
275	Pol	Barba 	PBS105@ies-sabadell.cat	P105B	7	A	2003-10-05	-1	1	Sandoval	0	0	H	\N
276	Zineddine	Chaoui	ZCZ106@ies-sabadell.cat	Z106C	1	A	2001-05-14	-1	0	\N	0	0	H	\N
277	Manuel	Ramirez 	MRS107@ies-sabadell.cat	M107R	3	A	1999-05-23	-1	1	Sanchez	0	0	H	\N
278	Keanu Ian	Torres 	KTA108@ies-sabadell.cat	K108T	3	A	2003-11-25	-1	1	Amancha	0	0	H	\N
279	Marcel	Jimenez 	MJG109@ies-sabadell.cat	M109J	3	A	2004-06-28	-1	1	Gonzalez	0	0	H	\N
280	Jorge Mario	Tous 	JTV110@ies-sabadell.cat	J110T	3	A	1992-02-06	-1	1	Velarde	0	0	H	\N
281	Abel	Anguita 	AAF111@ies-sabadell.cat	A111A	3	A	2003-09-21	-1	1	Fernandez	0	0	H	\N
282	David	Diaz 	DDV112@ies-sabadell.cat	D112D	3	A	1993-09-21	-1	1	Valera	0	0	H	\N
283	Victor	Garrido 	VGT113@ies-sabadell.cat	V113G	3	A	2003-11-27	-1	1	Toribio	0	0	H	\N
284	Xavi	Sanchez 	XSD114@ies-sabadell.cat	X114S	3	A	1999-07-24	-1	1	Delgado	0	0	H	\N
285	Daniel	Serrano 	DSR115@ies-sabadell.cat	D115S	3	A	2004-07-10	-1	1	Ramiro	0	0	H	\N
286	Bernat	Balta 	BBG116@ies-sabadell.cat	B116B	3	A	2002-06-27	-1	1	Gonzalez	0	0	H	\N
287	Pablo	Adelantado 	PAG117@ies-sabadell.cat	P117A	3	A	1996-04-24	-1	1	Gracia	0	0	H	\N
288	Miquel	Puig 	MPV118@ies-sabadell.cat	M118P	3	A	1997-05-02	-1	1	Vigueras	0	0	H	\N
290	David	Mas 	DMO120@ies-sabadell.cat	D120M	3	A	1994-12-19	-1	1	Ojea	0	0	H	\N
291	Juan Jose	Rodriguez 	JRD121@ies-sabadell.cat	J121R	3	A	2004-04-25	-1	1	Duran	0	0	H	\N
292	Alberto	Sanchez 	ASN122@ies-sabadell.cat	A122S	3	A	2004-01-24	-1	1	Nunez	0	0	H	\N
293	David	Nuez 	DNP123@ies-sabadell.cat	D123N	3	A	1999-10-03	-1	1	Pladellorens	0	0	H	\N
294	Pablo	Capitan 	PCG124@ies-sabadell.cat	P124C	3	A	2004-07-18	-1	1	Giraldo	0	0	H	\N
295	Ismael	Marcos 	IMM125@ies-sabadell.cat	I125M	3	A	2004-10-19	-1	1	Martinez	0	0	H	\N
296	David	Rodriguez 	DRG126@ies-sabadell.cat	D126R	4	A	2004-01-26	-1	1	Garcia	0	0	H	\N
297	Isaac	Sanchez 	ISR127@ies-sabadell.cat	I127S	8	A	2001-05-17	-1	1	Rabadan	0	0	H	\N
207	Bernat	Sune 	BSC037@ies-sabadell.cat	B37S	9	A	2001-03-05	-1	0	Cea	0	0	H	\N
289	juan sebastian	canal 	jcZ119@ies-sabadell.cat	j119c	3	A	2000-03-25	-1	1	Zuniga	0	0	H	\N
299	Jordi	Caballeria 	JCM129@ies-sabadell.cat	J129C	8	A	2003-06-02	-1	1	Marti	0	0	H	\N
302	David 	Gomez 	DGR132@ies-sabadell.cat	D132G	4	A	2004-10-08	-1	1	Raya	0	0	H	\N
303	Isaac	Hernandez	IHZ133@ies-sabadell.cat	I133H	4	A	1997-01-09	-1	1	\N	0	0	H	\N
312	Daniel	Casanova 	DCR142@ies-sabadell.cat	D142C	3	A	1988-10-20	-1	1	Ruz	0	0	H	\N
218	Sergio Agustin	Ramos 	SRM048@ies-sabadell.cat	S48R	9	A	2003-11-22	-1	1	Munoz	0	0	H	\N
338	Elena	del Pozo	EEEEE@ies-sabadell.cat	EEEE	\N	T	1980-01-01	-1	0	Gutierrez	0	0	D	\N
327	alex	Martinez 	aMR157@ies-sabadell.cat	a157M	3	A	2001-01-31	-1	1	Roldan	0	0	H	\N
233	oscar	Rodriguez 	oRA063@ies-sabadell.cat	o63R	9	A	2003-07-05	-1	1	Ardila	0	0	H	\N
252	Joel 	Victori 	JVB082@ies-sabadell.cat	J82V	1	A	2004-09-14	-1	1	Baluda	0	0	H	\N
300	Ivan	Espinosa 	IEL130@ies-sabadell.cat	I130E	8	A	2000-01-28	-1	0	Leon	0	0	H	\N
171	Javi	Mauri 	JMU001@ies-sabadell.cat	J01M	6	A	1984-12-20	-1	0	Ullate	0	0	H	\N
184	Luis Alfredo	Miranda 	LMP014@ies-sabadell.cat	L14M	6	A	1987-06-03	-1	1	Palacios	0	0	H	\N
262	Juan Jesus	Gomez 	JGL092@ies-sabadell.cat	J92G	7	A	2003-12-19	-1	1	Leon	0	0	H	\N
263	Francisco	Sanchez 	FSL093@ies-sabadell.cat	F93S	7	A	2003-10-01	-1	1	Lledo	0	0	H	\N
301	Raul	Santiago 	RST131@ies-sabadell.cat	R131S	4	A	2003-10-23	-1	1	Tortosa	0	0	H	\N
304	Ethan	Ponce 	EPM134@ies-sabadell.cat	E134P	4	A	2004-08-19	-1	1	Molina	0	0	H	\N
305	Manuel	Lopez 	MLA135@ies-sabadell.cat	M135L	4	A	1984-07-16	-1	1	Aguilar	0	0	H	\N
306	Cisco	Zamora 	CZM136@ies-sabadell.cat	C136Z	5	A	2003-01-31	-1	1	Montero	0	0	H	\N
307	Arnau	Colomer 	ACM137@ies-sabadell.cat	A137C	5	A	2002-06-29	-1	1	Marques	0	0	H	\N
308	Miguel Ever	Romero	MRZ138@ies-sabadell.cat	M138R	5	A	2002-10-17	-1	1	\N	0	0	H	\N
309	Ivan	Palomino 	IPF139@ies-sabadell.cat	I139P	5	A	2004-03-26	-1	1	Fernandez	0	0	H	\N
310	Alejandro	Ormeno	AOZ140@ies-sabadell.cat	A140O	5	A	1996-07-06	-1	1	\N	0	0	H	\N
311	Ethan 	Corchero 	ECM141@ies-sabadell.cat	E141C	5	A	2002-08-25	-1	1	Martin	0	0	H	\N
313	Benjamin	Porras 	BPG143@ies-sabadell.cat	B143P	2	A	2003-09-24	-1	1	Gomez	0	0	H	\N
314	David	Gimenez 	DGM144@ies-sabadell.cat	D144G	2	A	2004-04-11	-1	1	Munoz	0	0	H	\N
315	Jordi	Orpez 	JOJ145@ies-sabadell.cat	J145O	2	A	2004-11-30	-1	1	Jam	0	0	H	\N
316	oscar	edo 	oej146@ies-sabadell.cat	o146e	2	A	2004-04-13	-1	1	jerez	0	0	H	\N
317	Octavio	Zarate 	OZT147@ies-sabadell.cat	O147Z	2	A	1997-09-02	-1	1	Torres	0	0	H	\N
318	Alberto	Galera 	AGL148@ies-sabadell.cat	A148G	2	A	2004-11-23	-1	1	Larrubia	0	0	H	\N
319	Fatima 	Guerroumi	FGZ149@ies-sabadell.cat	F149G	2	A	2001-10-04	-1	1	\N	0	0	D	\N
320	Marti	Felip 	MFB150@ies-sabadell.cat	M150F	2	A	2003-01-17	-1	1	Badia	0	0	H	\N
321	Alex	Cano 	ACB151@ies-sabadell.cat	A151C	2	A	2003-06-12	-1	0	Barea	0	0	H	\N
322	Nereyda	Amores 	NAR152@ies-sabadell.cat	N152A	2	A	2001-12-07	-1	1	Ramos	0	0	D	\N
323	Joel	Molero 	JMS153@ies-sabadell.cat	J153M	2	A	2004-04-10	-1	1	Segura	0	0	H	\N
324	Ivan	Alvarez 	IAF154@ies-sabadell.cat	I154A	3	A	1983-04-08	-1	0	Fernandez	0	0	H	\N
325	Arnau	Cucurella 	ACD155@ies-sabadell.cat	A155C	2	A	2000-09-21	-1	1	Del Rio	0	0	H	\N
326	Mayerli Paula 	Ocana 	MOO156@ies-sabadell.cat	M156O	6	A	2002-04-27	-1	1	Orihuela	0	0	D	\N
328	Cristhian	Contreras 	CCP158@ies-sabadell.cat	C158C	2	A	1993-05-14	-1	0	Pareja	0	0	H	\N
336	David	Romero 	DRM166@ies-sabadell.cat	D166R	4	A	2001-11-06	-1	1	Malia	0	0	H	\N
340	Gregorio	Santamaria	GGGG@ies-sabadell.cat	GGGG	\N	T	1981-01-01	-1	0	Fernandez	0	0	D	\N
329	Arnau	Mas 	AMC159@ies-sabadell.cat	A159M	2	A	2000-02-09	-1	1	Consuegra	0	0	H	\N
330	Daniel	Serrano 	DSR160@ies-sabadell.cat	D160S	5	A	2004-07-10	-1	1	Ramiro	0	0	H	\N
331	Sergi	Gomez 	SGF161@ies-sabadell.cat	S161G	2	A	2003-07-12	-1	1	Ferrer	0	0	H	\N
332	Alberto 	Cobo 	ACV162@ies-sabadell.cat	A162C	2	A	2003-12-11	-1	1	Valenzuela	0	0	H	\N
333	Felix	Vilanova 	FVM163@ies-sabadell.cat	F163V	1	A	2004-03-14	-1	1	Martin	0	0	H	\N
334	Marina	Martin 	MMS164@ies-sabadell.cat	M164M	1	A	2002-09-23	-1	1	Sanchez	0	0	D	\N
335	Ari	Ruiz 	ARM165@ies-sabadell.cat	A165R	4	A	2003-03-06	-1	1	Martinez	0	0	H	\N
337	Eric	Morales 	EMM167@ies-sabadell.cat	E167M	5	A	2004-07-14	-1	1	Martinez	0	0	H	\N
222	Cristian	Carrera 	CCB052@ies-sabadell.cat	C52C	5	A	1993-02-26	-1	1	Belmonte	0	0	H	\N
341	Isabel	del Andres	IIII@ies-sabadell.cat	IIII	\N	T	1982-01-01	-1	0	Moreno	0	0	\N	\N
342	Marc	Albereda	MMMM@ies-sabadell.cat	MMMM	\N	T	1983-01-01	-1	0	Sirvent	0	0	\N	\N
343	Eloi	Vazquez	VVVV@ies-sabadell.cat	VVVV	\N	T	1984-01-01	-1	0	\N	0	0	\N	\N
344	Alex	Guerrero	AAAA@ies-sabadell.cat	AAAA	\N	R	1985-01-01	-1	0	\N	0	0	\N	\N
\.


--
-- Name: alumnes_moduls_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.alumnes_moduls_id_seq', 645, false);


--
-- Name: grupsunitatsformatives_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.grupsunitatsformatives_id_seq', 75, false);


--
-- Name: log_idlog_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_idlog_seq', 6, true);


--
-- Name: qualificacions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.qualificacions_id_seq', 1639, true);


--
-- Name: tasques_alumnes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tasques_alumnes_id_seq', 301, false);


--
-- Name: usuaris_idusuari_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuaris_idusuari_seq', 161, true);


--
-- Name: alumnes_moduls alumnes_moduls_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.alumnes_moduls
    ADD CONSTRAINT alumnes_moduls_pkey PRIMARY KEY (id);


--
-- Name: aules aules_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aules
    ADD CONSTRAINT aules_pkey PRIMARY KEY (id);


--
-- Name: cicles cicles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cicles
    ADD CONSTRAINT cicles_pkey PRIMARY KEY (id);


--
-- Name: grups grups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grups
    ADD CONSTRAINT grups_pkey PRIMARY KEY (id);


--
-- Name: grupsunitatsformatives grupsunitatsformatives_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grupsunitatsformatives
    ADD CONSTRAINT grupsunitatsformatives_pkey PRIMARY KEY (id);


--
-- Name: log log_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.log
    ADD CONSTRAINT log_pk PRIMARY KEY (idlog);


--
-- Name: moduls moduls_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.moduls
    ADD CONSTRAINT moduls_pkey PRIMARY KEY (id);


--
-- Name: qualificacions qualificacions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qualificacions
    ADD CONSTRAINT qualificacions_pkey PRIMARY KEY (id);


--
-- Name: qualificacions qualificacions_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qualificacions
    ADD CONSTRAINT qualificacions_un UNIQUE (idalumne, idunitatformativa);


--
-- Name: tasques_alumnes tasques_alumnes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasques_alumnes
    ADD CONSTRAINT tasques_alumnes_pkey PRIMARY KEY (id);


--
-- Name: tasques tasques_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasques
    ADD CONSTRAINT tasques_pkey PRIMARY KEY (id);


--
-- Name: unitatsformatives unitatsformatives_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unitatsformatives
    ADD CONSTRAINT unitatsformatives_pkey PRIMARY KEY (id);


--
-- Name: usuari_cicle usuari_cicle_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuari_cicle
    ADD CONSTRAINT usuari_cicle_pk PRIMARY KEY (idusuari, cicle);


--
-- Name: usuaris usuaris_emailusuari_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuaris
    ADD CONSTRAINT usuaris_emailusuari_key UNIQUE (emailusuari);


--
-- Name: usuaris usuaris_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuaris
    ADD CONSTRAINT usuaris_pkey PRIMARY KEY (idusuari);


--
-- Name: alumnes_moduls_idalumne_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX alumnes_moduls_idalumne_idx ON public.alumnes_moduls USING btree (idalumne);


--
-- Name: alumnes_moduls_idmodul_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX alumnes_moduls_idmodul_idx ON public.alumnes_moduls USING btree (idmodul);


--
-- Name: grups_cicle_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX grups_cicle_idx ON public.grups USING btree (cicle);


--
-- Name: grupsunitatsformatives_idaula_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX grupsunitatsformatives_idaula_idx ON public.grupsunitatsformatives USING btree (idaula);


--
-- Name: grupsunitatsformatives_idprofessor_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX grupsunitatsformatives_idprofessor_idx ON public.grupsunitatsformatives USING btree (idprofessor);


--
-- Name: grupsunitatsformatives_idunitatformativa_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX grupsunitatsformatives_idunitatformativa_idx ON public.grupsunitatsformatives USING btree (idunitatformativa);


--
-- Name: moduls_cicle_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX moduls_cicle_idx ON public.moduls USING btree (cicle);


--
-- Name: qualificacions_idalumne_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX qualificacions_idalumne_idx ON public.qualificacions USING btree (idalumne);


--
-- Name: qualificacions_idprofessor_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX qualificacions_idprofessor_idx ON public.qualificacions USING btree (idprofessor);


--
-- Name: qualificacions_idunitatformativa_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX qualificacions_idunitatformativa_idx ON public.qualificacions USING btree (idunitatformativa);


--
-- Name: tasques_alumnes_idalumne_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tasques_alumnes_idalumne_idx ON public.tasques_alumnes USING btree (idalumne);


--
-- Name: tasques_alumnes_idtasca_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tasques_alumnes_idtasca_idx ON public.tasques_alumnes USING btree (idtasca);


--
-- Name: tasques_idunitatformativa_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tasques_idunitatformativa_idx ON public.tasques USING btree (idunitatformativa);


--
-- Name: unitatsformatives_idmodul_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX unitatsformatives_idmodul_idx ON public.unitatsformatives USING btree (idmodul);


--
-- Name: usuaris_idgrup_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX usuaris_idgrup_idx ON public.usuaris USING btree (idgrup);


--
-- Name: qualificacions editar_qualificacions; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER editar_qualificacions BEFORE INSERT OR UPDATE ON public.qualificacions FOR EACH ROW EXECUTE FUNCTION public.editar_qualificacions();


--
-- Name: alumnes_moduls alumnes_moduls_idalumne_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.alumnes_moduls
    ADD CONSTRAINT alumnes_moduls_idalumne_fkey FOREIGN KEY (idalumne) REFERENCES public.usuaris(idusuari);


--
-- Name: alumnes_moduls alumnes_moduls_idmodul_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.alumnes_moduls
    ADD CONSTRAINT alumnes_moduls_idmodul_fkey FOREIGN KEY (idmodul) REFERENCES public.moduls(id);


--
-- Name: grups grups_cicle_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grups
    ADD CONSTRAINT grups_cicle_fkey FOREIGN KEY (cicle) REFERENCES public.cicles(id);


--
-- Name: grupsunitatsformatives grupsunitatsformatives_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grupsunitatsformatives
    ADD CONSTRAINT grupsunitatsformatives_fk FOREIGN KEY (idgrup) REFERENCES public.grups(id);


--
-- Name: grupsunitatsformatives grupsunitatsformatives_idaula_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grupsunitatsformatives
    ADD CONSTRAINT grupsunitatsformatives_idaula_fkey FOREIGN KEY (idaula) REFERENCES public.aules(id);


--
-- Name: grupsunitatsformatives grupsunitatsformatives_idprofessor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grupsunitatsformatives
    ADD CONSTRAINT grupsunitatsformatives_idprofessor_fkey FOREIGN KEY (idprofessor) REFERENCES public.usuaris(idusuari);


--
-- Name: grupsunitatsformatives grupsunitatsformatives_idunitatformativa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grupsunitatsformatives
    ADD CONSTRAINT grupsunitatsformatives_idunitatformativa_fkey FOREIGN KEY (idunitatformativa) REFERENCES public.unitatsformatives(id);


--
-- Name: log log_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.log
    ADD CONSTRAINT log_fk FOREIGN KEY (idusuari) REFERENCES public.usuaris(idusuari) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: moduls moduls_cicle_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.moduls
    ADD CONSTRAINT moduls_cicle_fkey FOREIGN KEY (cicle) REFERENCES public.cicles(id);


--
-- Name: qualificacions qualificacions_idalumne_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qualificacions
    ADD CONSTRAINT qualificacions_idalumne_fkey FOREIGN KEY (idalumne) REFERENCES public.usuaris(idusuari);


--
-- Name: qualificacions qualificacions_idprofessor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qualificacions
    ADD CONSTRAINT qualificacions_idprofessor_fkey FOREIGN KEY (idprofessor) REFERENCES public.usuaris(idusuari);


--
-- Name: qualificacions qualificacions_idunitatformativa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qualificacions
    ADD CONSTRAINT qualificacions_idunitatformativa_fkey FOREIGN KEY (idunitatformativa) REFERENCES public.unitatsformatives(id);


--
-- Name: tasques_alumnes tasques_alumnes_idalumne_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasques_alumnes
    ADD CONSTRAINT tasques_alumnes_idalumne_fkey FOREIGN KEY (idalumne) REFERENCES public.usuaris(idusuari);


--
-- Name: tasques_alumnes tasques_alumnes_idtasca_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasques_alumnes
    ADD CONSTRAINT tasques_alumnes_idtasca_fkey FOREIGN KEY (idtasca) REFERENCES public.tasques(id);


--
-- Name: tasques tasques_idunitatformativa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasques
    ADD CONSTRAINT tasques_idunitatformativa_fkey FOREIGN KEY (idunitatformativa) REFERENCES public.unitatsformatives(id);


--
-- Name: unitatsformatives unitatsformatives_idmodul_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unitatsformatives
    ADD CONSTRAINT unitatsformatives_idmodul_fkey FOREIGN KEY (idmodul) REFERENCES public.moduls(id);


--
-- Name: usuari_cicle usuari_cicle_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuari_cicle
    ADD CONSTRAINT usuari_cicle_fk FOREIGN KEY (idusuari) REFERENCES public.usuaris(idusuari);


--
-- Name: usuari_cicle usuari_cicle_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuari_cicle
    ADD CONSTRAINT usuari_cicle_fk_1 FOREIGN KEY (cicle) REFERENCES public.cicles(id);


--
-- Name: usuaris usuaris_idgrup_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuaris
    ADD CONSTRAINT usuaris_idgrup_fkey FOREIGN KEY (idgrup) REFERENCES public.grups(id);


--
-- PostgreSQL database dump complete
--


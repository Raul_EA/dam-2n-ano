use demo;
DELIMITER ;;
CREATE or replace PROCEDURE stock_proveidor_magatzem(IN d_sup_id INT, IN d_warehouse_id INT, OUT cofee_count INT)
READS SQL DATA
BEGIN
     SELECT SUM(QUAN) INTO cofee_count
     FROM COF_INVENTORY 
     WHERE sup_id = d_sup_id
     AND warehouse_id = d_warehouse_id;
END ;;

set @coffe_count = 10;
select @coffe_count;
call stock_proveidor_magatzem(101,1234,@coffe_count);
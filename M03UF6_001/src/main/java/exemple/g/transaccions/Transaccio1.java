package exemple.g.transaccions;

//Exemple adaptat de: http://docs.oracle.com/javase/tutorial/jdbc/basics/transactions.html
import java.sql.*;
import java.util.Scanner;

/* Per fer funcionar aquest exemple primer:
 * executa aquesta instrucci� sql a phpmyadmin dins de la taula demo:
 * 
 * INSERT INTO `demo`.`MERCH_INVENTORY` (`ITEM_ID`, `ITEM_NAME`, `SUP_ID`, `QUAN`, `DATE_VAL`) 
   VALUES ('12342', 'Cup_Large', '49', '28', '2006-04-01 00:00:00')
 * 
 * Ara disposem de dos articles a la taula inventari amb nom Cup Large:
 * - Un amb id 1234, que pertany al proveeidor amb id 456
 * - Un altre amb id 12341 que pertany al proveidor amb id 49
 * 
 * Imaginem que el proveidor amb id 49, traspassa 10 articles 'Cup Large' al proveidor amb id 456, aix�:
 * 
 * - Implica l'execuci� de dos consultes d'actualitzaci�:
 * - La primera decrementa en 10 l'article 'Cup Large' del proveidor 49
 * - La segona incrementa en 10 l'article 'Cup Large' del proveidor 456.
 * - Si, per exemple,  la segona d'aquestes consultes falla l'integritat de les dades es perdra. 
 * - Es a dir, el primer proveidor haura decrementat la quantitat d'articles.
 * - pero el segon proveidor no els haura incrementat.
 * 
 * Les transaccions permeten executar varies consultes de modificaci� de forma segura, de forma que: 
 * - Si es detecta que alguna d'aquestes consultes no s'ha pogut produir es pot retornar al seu estat inicial les dades de la taula implicada. 
 * 
 */

public class Transaccio1 {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);

		Connection con=null;
		
		PreparedStatement update1 = null;
		PreparedStatement update2 = null;

		
		String updateString1 = "update MERCH_INVENTORY set QUAN = QUAN - ? "
							+ "where ITEM_ID = 12342";

		String updateString2 = "update MERCH_INVENTORY set QUAN = QUAN + ? "
							+ "where ITEM_ID = 1234";
		
// Comenta la consulta anterior i descomenta aquesta per a produir una errada a la transacci�
		
//		String updateString2 = "update MERCH_INVENTORY set QUAN = 'A' "
//		+ "where ITEM_ID = 1234";	
		
		try {
	
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo", "root", "super3");			
			con.setAutoCommit(false);	/*
											Autocommitt=true: cada consulta SQL individual es tractada com a una transacci� i es automaticament confirmada despr�s de ser executada.
											Autocommitt=false: la totalitat de consultes executades no es confirmen fins que es crida al metode commit() de forma explicita. 
			 							*/
			
			update1 = con.prepareStatement(updateString1);
			update2 = con.prepareStatement(updateString2);

			System.out.println("Introdueix la quantitat de dades a traspassar d'un article a altre: ");
			int quantitat=sc.nextInt();
			
			//Executem les dues consultes, per� aquestes no ser�n confirmades a la BD fins que explicitament cridem al m�tode commit.
			update1.setInt(1, quantitat);
			update1.executeUpdate();

			update2.setInt(1, quantitat);
			update2.executeUpdate();
			
			con.commit();	//Confirma tots els canvis fets a la taula
			
			System.out.println("Transaccio realitzada!");
		
		} catch (SQLException e) {
			
			//Si es genera alguna errada a les consultas s'enviar� una SQLException
			if (con != null) {
				try {
					System.err.print("S'ha produit una errada a la transacci�, desfem els canvis");
				
					con.rollback();	//Desf� tots els canvis realitzats a la transacci�
				
				} catch (SQLException excep) {
					e.printStackTrace();
				}
			}
		} finally {
			try {
			
				if (update1 != null) 
					update1.close();
				if (update2 != null) 
					update2.close();				
				if (con!=null)
					con.setAutoCommit(true);
			
			} catch (SQLException e) {
				e.printStackTrace();
			}	
			
			sc.close();
		}
	}

}

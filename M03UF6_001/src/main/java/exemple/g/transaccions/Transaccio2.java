package exemple.g.transaccions;

import java.sql.*;
import java.util.Scanner;

//Exemple adaptat de: http://docs.oracle.com/javase/tutorial/jdbc/basics/transactions.html
public class Transaccio2 {

	public static void main(String[] args) {

		Scanner sc= new Scanner(System.in);
		
		Connection con = null;
		Statement obtePreu = null;
		Statement actualitzaPreu = null;
		ResultSet rs = null;
		
		try {

			//1- Introducci� de dades:
			System.out.println("Introdueix nom cafe: ");
			String nomCafe=sc.next();
			
			System.out.println("Introdueix modificador preu, flotant de 0 a 1: ");
			float modificadorPreu=Float.parseFloat(sc.next());
			
			System.out.println("Introdueix maxim preu: ");
			float maximPreu=Float.parseFloat(sc.next());
			
			
			//2- Connexi� a la base de dades
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo", "root", "super3");
			con.setAutoCommit(false);	//Desactivem autocommit

			Savepoint save1 = con.setSavepoint();
		
			obtePreu = con.createStatement();
			actualitzaPreu = con.createStatement();
			
			String consulta="SELECT COF_NAME, PRICE FROM COFFEES "
						   +"WHERE COF_NAME = '" + nomCafe + "'";
	
			obtePreu.execute(consulta);
			rs = obtePreu.getResultSet();
			
			if (!rs.first()) {
				
				System.out.println("No puc trobar el cafè amb el nom: " + nomCafe);
				System.out.println("fi de l'aplicació");
				
			} else {
				
				float preuAntic = rs.getFloat("PRICE");
				float nouPreu = preuAntic * (1 + modificadorPreu);
				
				System.out.println("El preu de " + nomCafe + " és " + preuAntic);

				System.out.println("El nou preu de " + nomCafe + " és " + nouPreu);

				System.out.println("Fent actualització...");

				actualitzaPreu.executeUpdate("UPDATE COFFEES SET PRICE = "+ nouPreu 
										   + " WHERE COF_NAME = '" + nomCafe + "'");

				if (nouPreu > maximPreu) {
					System.out.println("\nEl nou preu, " + nouPreu
							+ ", és més gran que el màxim preu establert, " 
							+ maximPreu + ". tornem enrrera la transacció.");

					con.rollback(save1);
				}
				
				con.commit();
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
			
		} finally {
			
			try {
				
				if (obtePreu != null) {
					obtePreu.close();
				}
				if (actualitzaPreu != null) {
					actualitzaPreu.close();
				}
				if (con!=null)
					con.setAutoCommit(true);
				sc.close();
				
			} catch (SQLException e) {
				
			}
			
		}
	}

}

package exemple.j.injection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/* Executa això sobre la base de dades demo.sql
 * 
CREATE TABLE Usuaris (
id INTEGER NOT NULL AUTO_INCREMENT UNIQUE,
nom TEXT NOT NULL,
pass TEXT NOT NULL,
secret TEXT NOT NULL,
PRIMARY KEY (id));

INSERT INTO Usuaris (nom,pass,secret) VALUES
('pedro','123456','Pedro realmente es Pablo'),
('pablo','987654321','Pablo es el padre de Juan'),
('miguel','miguel','Miguel no tiene amigos');
*/

public class InjeccioSQL_02 { // SentenciaSensePreparar
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String nom = " ";
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo", "root", "super3");
				Statement st = conn.createStatement();) {
			while (!nom.equals("")) {
				System.out.println("Nom a cercar (en blanc per sortir): ");
				/*
				 * prova amb: miguel ' or '1'='1
				 * 
				 * Ara no pot fer injecció
				 */
				nom = sc.nextLine();
				if (!nom.equals("")) {
					System.out.println("SELECT id, nom, secret FROM Usuaris " + "WHERE nom ='" + nom + "'");

					try {
						PreparedStatement ps;
						ps = conn.prepareStatement("select id, nom, secret from Usuaris where (nom=?)");
						ps.setString(1, nom);
						ResultSet rs = ps.executeQuery();
						//rs.next();

						while (rs.next()) {
							int id = rs.getInt(1);
							nom = rs.getString(2);
							String secret = rs.getString(3);
							System.out.println(id + "\t" + nom + " --->  " + secret);
						}
					} catch (SQLException e) {
						System.err.println("Error SQL: " + e.getMessage());
					}
				}
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sc.close();
	}
}

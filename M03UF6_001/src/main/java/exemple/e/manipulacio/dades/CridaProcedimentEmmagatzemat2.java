package exemple.e.manipulacio.dades;

import java.sql.*;

public class CridaProcedimentEmmagatzemat2 {

/* Primer de tot, i mitjancant dbeaver, executa aquest procediment a la base de dades sakila de mariadb;
 
/*
DELIMITER $$
$$
CREATE PROCEDURE sakila.film_in_stock(IN p_film_id INT, IN p_store_id INT, OUT p_film_count INT)
READS SQL DATA
begin
     SELECT inventory_id
     FROM inventory
     WHERE film_id = p_film_id
     AND store_id = p_store_id;

     SELECT FOUND_ROWS() INTO p_film_count;
END$$
DELIMITER ;
 */

	/**
	 * @param args
	 */

	public static void main(String[] args) {
		Connection connection = null;
		CallableStatement st = null;
		
		String url = "jdbc:mysql://localhost:3306/sakila";
		String sql;
		
			try {
				connection = DriverManager.getConnection(url,"root", "super3");
				sql = "{CALL film_in_stock(1,1,?)};";
				
				st = connection.prepareCall(sql);
				st.registerOutParameter(1, Types.INTEGER);
				
				if (st.execute())	//si 'true' retorna un resultset
				{
					ResultSet rs= st.executeQuery();
					System.out.println("Retorna registres: ");
					while (rs.next()) {
						System.out.println("registre: " + rs.getInt(1));
					}
				}
				
				System.out.println(st.getInt(1));
			
			} catch (SQLException e) {
				System.err.println(e.getMessage());
			} finally {
				if (st != null) {
					try {
						st.close();
					} catch (SQLException e1) {
						;
					}
				}
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e1) {
						;
					}
				}
			}
	}

}

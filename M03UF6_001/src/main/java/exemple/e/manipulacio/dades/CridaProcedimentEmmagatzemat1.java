package exemple.e.manipulacio.dades;

import java.sql.*;

public class CridaProcedimentEmmagatzemat1 {
// Primer de tot, i mitjancant dbeaver o consola, executa aquest procediment a la base de dades demo de mariadb;
/*
use demo;
DELIMITER ;;
CREATE or replace PROCEDURE stock_proveidor_magatzem(IN d_sup_id INT, IN d_warehouse_id INT, OUT cofee_count INT)
READS SQL DATA
BEGIN
     SELECT SUM(QUAN) INTO cofee_count
     FROM COF_INVENTORY 
     WHERE sup_id = d_sup_id
     AND warehouse_id = d_warehouse_id;
END ;;

set @coffe_count = 10;
select @coffe_count;
call stock_proveidor_magatzem(101,1234,@coffe_count);
 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Connection connection = null;
		CallableStatement st = null;	//Callable statement ens serveix per cridar a un procediment emmagatzemat
		String url = "jdbc:mysql://localhost:3306/demo";
			try {
				connection = DriverManager.getConnection(url,"root", "super3");
				
				//1- preparecall crida el metode emmagatzemat
				st = connection.prepareCall("{call stock_proveidor_magatzem(?,?,?)}");
				
				//2- Indiquem quins son els parametres d'entrada
				st.setInt(1, 101);	//sup_id: id de proveidor 101
				st.setInt(2, 1234);	//warehouse_id: id de magatzemn 1234
				
				//3- Indiquem quin es el paràmetre de sortida
				st.registerOutParameter(3, Types.INTEGER);	//
				
				st.execute();
					
				System.out.println("Quantitat: " + st.getInt(3));	//obtenim el resultat d'executar el procediment
				
			} catch (SQLException e) {
				System.err.println(e.getMessage());
			} finally {
				if (st != null) {
					try {
						st.close();
					} catch (SQLException e1) {
						;
					}
				}
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e1) {
						;
					}
				}
			}
	}
}

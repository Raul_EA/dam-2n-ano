package exemple.c.consultes.modificacio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class CrearInsert04 {

	public static void main(String[] args) {
		Connection con = null;
		Statement st = null;

		String url = "jdbc:mysql://localhost:3306/demo";

		Properties propietatsConnexio = new Properties();
		propietatsConnexio.put("user", "root");
		propietatsConnexio.put("password", "super3");
		
		try {

			// 1- CONNEXIO
			con = DriverManager.getConnection(url,propietatsConnexio);
			

			// 2- CREEM CONSULTA
			st = con.createStatement();
			
			//Tamb� es poden executar consultes de creacio de taules
			
			//3- DE CREACIO DE TAULES:
			int numFiles= st.executeUpdate("CREATE TABLE REGISTRATION " +
						                   "(id INTEGER not NULL, " +
						                   " first VARCHAR(255), " + 
						                   " last VARCHAR(255), " + 
						                   " age INTEGER, " + 
						                   " PRIMARY KEY ( id ))");			
			System.out.println("Crear taules afecta a " + numFiles + " registres");
			
			//4- INSERCIO
			numFiles=st.executeUpdate("INSERT INTO REGISTRATION VALUES " + 
										"(1,'JOAN', 'GONZALEZ', 23)," + 
										"(2,'PACO', 'GARCIA', 32)");
			System.out.println("inserir dades afecta a " + numFiles + " registres");
			
			
			//5- D' ACTUALITZACIO
			//UPDATE tambe  es pot fer servir en un PreparedStatement introduint les dades com es feia amb una consulta de tipus INSERT 
			numFiles=st.executeUpdate("UPDATE REGISTRATION SET age=18 WHERE ID=1");
			System.out.println("Actualitzar dades afecta a " + numFiles + " registres");
			
			//6- D'ELIMINACIO DE DADES
			numFiles=st.executeUpdate("DELETE FROM REGISTRATION WHERE ID=2");
			System.out.println("eliminar dades afecta a " + numFiles + " registres");
			
			//6- D'ELIMINACIO DE TAULES 
			numFiles=st.executeUpdate("DROP TABLE REGISTRATION");
			System.out.println("eliminar taula afecta a " + numFiles + " registres");
			
		} catch (SQLException e) {

			System.err.println("Error d'apertura de conexio: " + e.getMessage());

		} finally {

			if (st != null)
				try {
					st.close();
				} catch (SQLException e) {
					System.err.println("Error de tancament de Statment: "
							+ e.getMessage());
				}

			if (con != null)
				try {
					con.close();
				} catch (Exception e) {
					System.err.println("Error de tancament de connexio: "
							+ e.getMessage());
				}
		}
	}
}

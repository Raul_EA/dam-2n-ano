package exemple.c.consultes.modificacio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

public class CrearInsert03 {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		
		Connection con = null;
		PreparedStatement st = null; //En lloc de statement ara farem servir PreparedStatement
		ResultSet rs = null;

		String url = "jdbc:mysql://localhost:3306/demo";

		Properties propietatsConnexio = new Properties();
		propietatsConnexio.put("user", "root");
		propietatsConnexio.put("password", "super3");
		
		try {

			// 1- CONNEXIO
			con = DriverManager.getConnection(url,propietatsConnexio);
			
			/* 2- CREEM CONSULTA: A diferencia de Statement, preparedStatement permet inserir la consulta en la mateixa definició de l'objecte
				L'avantatge d'aixo es que:
				- la sent�ncia s'envia immediatament al controlador de la BBDD
				- Allà serà compilada. 
				- Com a resultat PreparedStatement conté una sentencia SQL precompilada. 
				- Es a dir, quan s'executa PreparedStatement, SGBD l'executa directament sense tenir que tornar a compilarla.
			*/
			st = con.prepareStatement("INSERT INTO COFFEES VALUES(?,?,?,?,?)");	
			
			//3- DEMANEM DADES
			
			String cof_name="";	
			do { //3.1 - nom_cafe es clau principal, comprovem que no es repeteixi
				
				System.out.println("Introdueix nom cafe: ");
				cof_name=sc.nextLine();
				
				rs = st.executeQuery("SELECT * FROM COFFEES WHERE TRIM(LOWER(COF_NAME))='" + cof_name.toLowerCase().trim() + "'");
			} while (rs.next());
			
			st.setString(1, cof_name); //Introdueix com a parametre 0 a la consulta d'insercio cof_name	
			
			rs.close();
			
				//3.2 - sup_id es clau forània, donem les claus de proveidor que existeixin.
			rs = st.executeQuery("SELECT SUP_ID FROM SUPPLIERS");
			ArrayList<Integer> suppliers = new ArrayList<>();
	
			while (rs.next()) {
				System.out.print(rs.getString(1) + " ");
				suppliers.add(rs.getInt(1));
			}
			
			int sup_id=0;
			do {
					
				System.out.println("\nSelecciona proveidor: ");
				sup_id=sc.nextInt();
				
			} while(!suppliers.contains(sup_id)); 
			
			st.setInt(2,sup_id); //Introdueix com a parametre 1 a la consulta d'insercio cof_name	
			
			
				//3.3 - Selecciona preu
			System.out.println("Selecciona preu: ");
			st.setInt(3, sc.nextInt());
			
				//vendes i total es posen a cero per defecte
			st.setInt(4, 0);
			st.setInt(5, 0);
			
			
			
			int numFiles= st.executeUpdate();
						
			System.out.println("Inserci� creada! Num de files afectades: " + numFiles);

			
			
		} catch (SQLException e) {

			System.err.println("Error d'apertura de conexió: " + e.getMessage());

		} finally {

			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					System.err.println("Error de tancament de ResultSet: "
							+ e.getMessage());
				}

			if (st != null)
				try {
					st.close();
				} catch (SQLException e) {
					System.err.println("Error de tancament de Statment: "
							+ e.getMessage());
				}

			if (con != null)
				try {
					con.close();
				} catch (Exception e) {
					System.err.println("Error de tancament de connexió: "
							+ e.getMessage());
				}
		}

	}
}

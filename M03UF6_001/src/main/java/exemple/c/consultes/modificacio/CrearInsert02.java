package exemple.c.consultes.modificacio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

public class CrearInsert02 {

	public static void main(String[] args) {
		
		//Variables per emmagatzemar valors del registre a inserir a la taula COFFEES
		String cof_name="";
		int sup_id=0;
		int price=0;
		
		Scanner sc=new Scanner(System.in);
		
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String url = "jdbc:mysql://localhost:3306/demo";

		Properties propietatsConnexio = new Properties();
		propietatsConnexio.put("user", "root");
		propietatsConnexio.put("password", "super3");
		
		try {

			// 1- CONNEXIO
			con = DriverManager.getConnection(url,propietatsConnexio);
			
			// 2- CREEM CONSULTA
			st = con.createStatement();
		
			
			//3- DEMANEM DADES
				
			do { //3.1 - nom_cafe es clau principal, comprovem que no es repeteixi
				
				System.out.println("Introdueix nom cafe: ");
				cof_name=sc.nextLine();
				
				rs = st.executeQuery("SELECT * FROM COFFEES WHERE TRIM(LOWER(COF_NAME))='" + cof_name.toLowerCase().trim() + "'");
			} while (rs.next());
			
			
			rs.close();	//ATENCIO: per tal de assignar una consulta nova a rs s'ha de tancar
			
			
				//3.2 - sup_id es clau forània, donem les claus de proveidor que existeixin.
			rs = st.executeQuery("SELECT SUP_ID FROM SUPPLIERS");
			ArrayList<Integer> suppliers = new ArrayList<>();
	
			while (rs.next()) {
				System.out.print(rs.getString(1) + " ");
				suppliers.add(rs.getInt(1));
			}
			
			do {
					
				System.out.println("\nSelecciona proveidor: ");
				sup_id=sc.nextInt();
				
			} while(!suppliers.contains(sup_id)); 
			
				//3.3 - Selecciona preu
			System.out.println("Selecciona preu: ");
			price=sc.nextInt();
			
				//vendes i total es posen a zero per defecte
			int numFiles= st.executeUpdate("INSERT INTO COFFEES VALUES('" + cof_name + "'," + sup_id + "," + price + ",0,0");
						
			System.out.println("Inserció creada! Num de files afectades: " + numFiles);

			
			
		} catch (SQLException e) {

			System.err.println("Error d'apertura de conexió: " + e.getMessage());

		} finally {

			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					System.err.println("Error de tancament de ResultSet: "
							+ e.getMessage());
				}

			if (st != null)
				try {
					st.close();
				} catch (SQLException e) {
					System.err.println("Error de tancament de Statment: "
							+ e.getMessage());
				}

			if (con != null)
				try {
					con.close();
				} catch (Exception e) {
					System.err.println("Error de tancament de connexió: "
							+ e.getMessage());
				}
		}

	}
}

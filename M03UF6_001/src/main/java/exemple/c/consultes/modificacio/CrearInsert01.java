package exemple.c.consultes.modificacio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class CrearInsert01 {

	public static void main(String[] args) {
		Connection con = null;
		Statement st = null;

		String url = "jdbc:mysql://localhost:3306/demo";

		Properties propietatsConnexio = new Properties();
		propietatsConnexio.put("user", "root");
		propietatsConnexio.put("password", "super3");
		
		try {

			// 1- CONNEXIO
			con = DriverManager.getConnection(url,propietatsConnexio);
			

			// 2- CREEM CONSULTA
			st = con.createStatement();
			
			//3- EXECUTEM INSERT
			int numFiles= st.executeUpdate("INSERT INTO COFFEES VALUES('Roma',456,8.5,0,0)");
			
			//La següent consulta insereix dues files
//			int numFiles= st.executeUpdate("INSERT INTO COFFEES SELECT CONCAT(COF_NAME,' Gourmet'), 456, PRICE, SALES, TOTAL FROM COFFEES WHERE SUP_ID=49");

						
			System.out.println("Insercio creada! Num de files afectades: " + numFiles);

		} catch (SQLException e) {

			System.err.println("Error d'apertura de conexio: " + e.getMessage());

		} finally {

			if (st != null)
				try {
					st.close();
				} catch (SQLException e) {
					System.err.println("Error de tancament de Statment: "
							+ e.getMessage());
				}

			if (con != null)
				try {
					con.close();
				} catch (Exception e) {
					System.err.println("Error de tancament de connexio: "
							+ e.getMessage());
				}
		}
	}
}

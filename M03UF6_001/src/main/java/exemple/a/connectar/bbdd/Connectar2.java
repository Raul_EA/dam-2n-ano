package exemple.a.connectar.bbdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connectar2 {

	public static void main(String[] args) {

		Connection con = null; 	//Connection: permet connectar a una bbdd 

		String url = "jdbc:mysql://localhost:3306/sakila";
		String user = "root";
		String passwd = "1212";

		try {

			con = DriverManager.getConnection(url, user, passwd);
			System.out.println("Base de dades connectada!");

		} catch (SQLException e) {

			System.err.println("Error d'apertura de connexio: " + e.getMessage());

		} finally {

			if (con != null)
				try {
					con.close();

				} catch (SQLException e) {
					System.err.println("Error de tancament de connexio: " + e.getMessage());
				}
		}
	}
}

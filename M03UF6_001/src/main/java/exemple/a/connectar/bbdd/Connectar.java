package exemple.a.connectar.bbdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connectar {

	public static void main(String[] args) {

		Connection con = null;	//Connection: permet connectar a una bbdd 

		try {

			//1- CONNEXIO
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?user=root&password=1212");
			System.out.println("Base de dades connectada!");

		} catch (SQLException e) {
 
			System.err.println("Error d'apertura de connexio: " + e.getMessage());

		} finally {

			if (con != null)
				try {
					
					con.close();

				} catch (SQLException e) {
					System.err.println("Error de tancament de connexio: " + e.getMessage());
				}
		}
	}

}

package exemple.d.descripcio.dades;

import java.sql.*;

public class SentenciesDescripcioDades {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Connection connection = null;
		DatabaseMetaData dbmd = null;
		String url = "jdbc:mysql://localhost:3306";	
		try {			
			//1- El m�tode getMetadata de la connexi� permet accedir al conjunt de metadades d'aquest SGBD.
			//Observa que ens conectem directament al sgbd, sense espcificar a quina bd accedim
			connection = DriverManager.getConnection(url,"root","1212");	
			dbmd = connection.getMetaData();
			
			mostraCatalegsSGBD(dbmd);					//Mostrem els cat�legs de que disposa el sgbd.
			mostraInfoSGBD(dbmd);						//Mostrem informaci� general del sgbd.
			mostraTaules(dbmd,"sakila");				//Mostrem informaci� de les taules d'una base de dades (cat�leg).
			mostraColumnes(dbmd, "demo","SUPPLIERS");	//Mostrem informaci� de les columnes de la taula suppliers.			
			
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					;
				}
			}
				
		}

	}

	public static void mostraCatalegsSGBD(DatabaseMetaData dbmd) {
		ResultSet rs= null;
		
		System.out.println("1- CATALEGS DE LA BASE DE DADES");
		
		try {
			rs=dbmd.getCatalogs();
			while(rs.next())
				System.out.println(rs.getString(1));
			rs.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println();
		
		
	}
	
	public static void mostraInfoSGBD(DatabaseMetaData dbmd) throws SQLException {
		
		String nom = dbmd.getDatabaseProductName();
		String driver = dbmd.getDriverName();
		String url = dbmd.getURL();
		String usuari = dbmd.getUserName();
		
		
		System.out.println("2- INFORMACIO GENERAL DEL SGBD");
		System.out.println("Nom: "+nom);
		System.out.println("Driver: "+driver);
		System.out.println("URL: "+url);
		System.out.println("Usuari: "+usuari);
		System.out.println();
	}
	
	
	public static void mostraTaules(DatabaseMetaData dbmd, String cataleg) throws SQLException {
		ResultSet rs = null;
		
		try {


			System.out.println("3- TAULES DEL CATALEG: " + cataleg);
			
			 String[] types=null;		// Per obtenir dades de les vistes i taules
//			 String[] types = {"VIEW"};  // Per obtenir nom�s dades de les vistes treu el comentari d'aquesta l�nia:
//			 String[] types = {"TABLE"}; // Per obtenir nom�s dades de les taules treu el comentari d'aquesta l�nia:
			 rs = dbmd.getTables(cataleg, null, null, types);
			
			 while (rs.next()) {
				String esquema = rs.getString(2);
				String taula = rs.getString(3);
				String tipus = rs.getString(4);
				System.out.println(tipus + " - Cataleg: " + rs.getString(1) +
						", Esquema: "+esquema+", Nom: "+taula);

			}
			rs.close();
			
			System.out.println();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					;
				}
			}
		}
	}
	
	public static void mostraColumnes(DatabaseMetaData dbmd, String cataleg, String taula) throws SQLException {
		
		System.out.println("4- COLUMNES DE LA TAULA: "+taula + " DEL CATALEG: " + cataleg);
		ResultSet columnes = null;
		
		try {
			
			columnes = dbmd.getColumns("demo", null, taula, null);

			while (columnes.next()) {
				String nom = columnes.getString("COLUMN_NAME"); 
				String tipus = columnes.getString("TYPE_NAME"); 
				String mida = columnes.getString("COLUMN_SIZE"); 
				String nula = columnes.getString("IS_NULLABLE"); 
				
				System.out.println("Columna: "+nom+", Tipus: "+tipus+
						", Mida: "+mida+", Pot ser nul�la? "+nula);
				
			}
		} finally {
			if (columnes != null) {
				try {
					columnes.close();
				} catch (SQLException e) {
					;
				}
			}
		}
	}
}


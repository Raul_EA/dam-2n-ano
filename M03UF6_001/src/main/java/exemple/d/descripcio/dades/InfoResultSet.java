package exemple.d.descripcio.dades;

import java.sql.*;

public class InfoResultSet {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		ResultSetMetaData rsmd = null;	//ResultSetMetaData permet accedir als resultats de les metadades de la base de dades
		String url = "jdbc:mysql://localhost:3306/demo";
		try {
			connection = DriverManager.getConnection(url,"root","super3");
			st = connection.createStatement();
			
			//1- Obtenim els tres primers registres emmagatzemats en la taula 'COFEES'
			rs = st.executeQuery("SELECT * FROM COFFEES LIMIT 3");
			
			//2- A partir del resultset obtenim les metadades amb el m�tode getMetaData() 
			rsmd = rs.getMetaData();				

			//3- Mostra el numero de columnes de cada registre:
			int nCols = rsmd.getColumnCount();
			System.out.println("Columnes recuperades: "+nCols);
	
			//4- I les dades referents a l'estructura de la taula COFFEES
			for (int i=1; i<=nCols; i++) {
				System.out.println("Columna: "+i+":");
				System.out.println("  Nom: "+rsmd.getColumnName(i));
				System.out.println("  Tipus: "+rsmd.getColumnTypeName(i));
				System.out.println("  Pot ser nul.la? "+(rsmd.isNullable(i)==0?"No":"Sí"));
				System.out.println("  Amplada maxima de columna: "+rsmd.getColumnDisplaySize(i));
			}
			
	
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					;
				}
			}
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
					;
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					;
				}
			}
				
		}

	}

}

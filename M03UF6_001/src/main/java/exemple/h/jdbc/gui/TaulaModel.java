package exemple.h.jdbc.gui;

import java.awt.BorderLayout;






import javax.swing.*;
import javax.swing.table.AbstractTableModel;

//exemple obtingut de: http://docs.oracle.com/javase/tutorial/uiswing/components/table.html
@SuppressWarnings("serial")
public class TaulaModel extends JFrame {


		JTable table;
		
		
	public TaulaModel() {
	
		/*
		 * Cada objecte de taula fa servir un model de taula per fer servir les dades de la taula actual.
		 * Un model de taula implementa la interficie TableModel. 
		 * Generalment es defineix un nou model de taula en una nova instancia d'AbstractTableModel
		 */
		
		AbstractTableModel abstractModel=new AbstractTableModel() {
			String[] columnNames = {"First Name",
				    				"Last Name",
				    				"Sport",
				    				"# of Years",
				    				"Vegetarian"};
			Object[][] data = {
				    {"Kathy", "Smith","Snowboarding", new Integer(5), new Boolean(false)},
				    {"John", "Doe","Rowing", new Integer(3), new Boolean(true)},
				    {"Sue", "Black", "Knitting", new Integer(2), new Boolean(false)},
				    {"Jane", "White", "Speed reading", new Integer(20), new Boolean(true)},
				    {"Joe", "Brown","Pool", new Integer(10), new Boolean(false)}
				};

			@Override
			public Object getValueAt(int row, int col) {
				return data[row][col];
			}

			@Override
			public int getRowCount() {
				// TODO Auto-generated method stub
				return data.length;
			}

			@Override
			public int getColumnCount() {
				return columnNames.length;
			}

			@Override
			public String getColumnName(int col) {
				return columnNames[col];
			}

			@Override
			public Class<? extends Object> getColumnClass(int c) {
				return getValueAt(0, c).getClass();
			}

			//no �s necessari sobreescriure aquest m�tode si totes les dades es poden editar
			@Override
			public boolean isCellEditable(int row, int col) {
				
				if (col < 2) {
					return false;
				} else {
					return true;
				}
				
			}

			//no �s necessari sobreescriure aquest m�tode si les dades no poden canviar
//			@Override
//			public void setValueAt(Object value, int row, int col) {
//				data[row][col] = value;
//				fireTableCellUpdated(row, col);
//			}			
		};


		
		
		//1- Declarar una taula amb el nou model
		table = new JTable(abstractModel);
	
		
			//altres opciones per a declarar-la:
			/* JTable(String[]ColumnNames, Object[] columnNames)
			 * JTable(Object[][] rowData, Object[] columnNames)
			 * JTable(Vector rowData, Vector columnNames)
			 */
		
		
		//2- EL gestor de disposici� ScrollPane �s ideal per contenir taules:
		JScrollPane scrollPane = new JScrollPane(table);
		table.setFillsViewportHeight(false);//fa servir l'alcada total del contenidor, tot i que no hi hagin files suficients per omplir-lo.
		add(scrollPane, BorderLayout.CENTER);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				TaulaModel t=new TaulaModel();
				t.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
				t.pack();
				t.setResizable(false);
				t.setVisible(true);
			}
		}); 
	}
	
}

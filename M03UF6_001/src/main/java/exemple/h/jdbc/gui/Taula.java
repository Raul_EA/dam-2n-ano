package exemple.h.jdbc.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

//exemple obtingut de: http://docs.oracle.com/javase/tutorial/uiswing/components/table.html
@SuppressWarnings("serial")
public class Taula extends JFrame {

		JTable table;
		JButton inserir, eliminar;
		
	public Taula() {
		
		String[] columnNames = {"First Name",
                				"Last Name",
                				"Sport",
                				"# of Years",
                				"Vegetarian"};
		
		Object[][] data = {
			    {"Kathy", "Smith","Snowboarding", 5, false},
			    {"John", "Doe","Rowing", 3, true},
			    {"Sue", "Black", "Knitting", 2, false},
			    {"Jane", "White", "Speed reading", 20, true},
			    {"Joe", "Brown","Pool", 10, false}
		};

		
		//0- Preparem dos botons que ens permetran inserir i eliminar registres a la taula.
		inserir=new JButton("inserir registre");
		eliminar=new JButton("eliminar registre");
		
		
		inserir.addActionListener(new ActionListener() {
			
			@SuppressWarnings("removal")
			@Override
			public void actionPerformed(ActionEvent e) {
					DefaultTableModel model= (DefaultTableModel) table.getModel();
					model.addRow(new Object[]{"Nou", "Registre","inserit", 1, true} );				
			}
		});
		
		
		eliminar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
					DefaultTableModel model= (DefaultTableModel) table.getModel();
					model.removeRow(table.getSelectedRow());
			}
		});
		
		
		
		JPanel sud=new JPanel(new GridLayout(1,2));
		sud.add(inserir);
		sud.add(eliminar);
		add(sud, BorderLayout.SOUTH);
		
		
		//1- Declarar una taula amb dades
		table = new JTable();
		table.setModel(new DefaultTableModel(data, columnNames));	//necessari per a inserir i eliminar valors
		
			//altres opciones per a declarar-la:
			/* JTable(String[] columNames, Object[][] Data)
			 * JTable(Object[][] rowData, Object[] columnNames)
			 * JTable(Vector rowData, Vector columnNames)
			 */
		

		//2- EL gestor de disposici� ScrollPane �s ideal per contenir taules:
		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane, BorderLayout.CENTER);
		
		
		
			//Tot i que tamb� es poden afegir a altres gestors de disposici�:
//		setLayout(new BorderLayout());
//		add(table.getTableHeader(), BorderLayout.PAGE_START);
//		add(table, BorderLayout.CENTER);
		
		
		//3- Es poden canviar les amplades de les columnes 
		for (int i = 0; i < 5; i++) {
		    TableColumn column = table.getColumnModel().getColumn(i);
		    if (i == 2) {
		        column.setPreferredWidth(100); //la tercera columna �s m�s gran
		    } else {
		        column.setPreferredWidth(50);
		    }
		}
				
		//4- I determinar com es poden seleccionar files i columnes
		table.setRowSelectionAllowed(true); //habilita-deshabilita la selecci� de multiples files
		table.setColumnSelectionAllowed(false); //habilita-deshabilita la selecci� de multiples columnes
		//tots dos valors a true permeten seleccionar multiples cel�les
		
//		table.setCellSelectionEnabled(false);	//permet seleccionar multiples cel�les
		//tots tres valors a false permeten seleccionar una �nica cel�la.
		
		
		//5- Si es vol configurar la selecci� de files, columnes i cel�les es pot fer servir: 
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);	//Multiples intervals de selecci� (files, columnes, cel�les)
//		table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);	//un �nic interval de selecci� (files, columnes, cel�les)
//		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);			//una �nica selecci� de files, columnes o cel�les
		
		
		//6- Per obtenir la selecci� de files, columnes o cel�les:
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				
				//retorna true si �s una s�rie d'events multiples
				 if (e.getValueIsAdjusting()) {
		                return;
				 }

		        int[] selectedRow = table.getSelectedRows();
		        int[] selectedColumns = table.getSelectedColumns();
		        
		        System.out.println("Index de files seleccionades: " + Arrays.toString(selectedRow));
		        System.out.println("Index de columnes seleccionades: " + Arrays.toString(selectedColumns));
		        
		        System.out.println("valors del rang: ");
		        
		        /* Selecci� de cel�les, per seleccionar rang files es pot fer servir:
		         * files: table.getRowCount();
		         * columnes: table.getColumnCount();  
		         * 
		         * per definir un rang de 0 a rowCount, o b� de 0 a columnCount
		         *  i aix� mostrar tots els elements de les files i columnes seleccionades
		         */
		        for (int i : selectedRow) {
		        	for (int j : selectedColumns)
		        		System.out.format(table.getValueAt(i, j) + "\t");
		        	System.out.println();
		        }   				
			}
		});

		
		
		
	//7- Per detectar un canvi a les files, columnes, cel�les de la taula
	table.getModel().addTableModelListener(new TableModelListener() {
		
		@Override
		public void tableChanged(TableModelEvent e) {
			
			switch (e.getType()) {
			case TableModelEvent.INSERT:
				System.out.println("S'ha INSERIT un valor");
				break;
			case TableModelEvent.UPDATE:
				System.out.println("S'ha ACTUALIZAT un valor");
				break;
			case TableModelEvent.DELETE:
				System.out.println("S'ha ELIMINAT un valor");
				break;
			default:
				break;
			}
			
		}
	});

		
		
		
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				Taula t=new Taula();
				t.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
				t.pack();
//				t.setResizable(false);
				t.setVisible(true);
			}
		}); 
	}
	
}

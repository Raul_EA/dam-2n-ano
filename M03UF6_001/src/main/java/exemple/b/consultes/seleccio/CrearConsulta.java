package exemple.b.consultes.seleccio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class CrearConsulta {

	public static void main(String[] args) {
		Connection con = null; 	//Connection: permet connectar a una bbdd 
		Statement st = null; 
		ResultSet rs = null;

		//0- Elements necessaris per a la connexio
		String url = "jdbc:mysql://localhost:3306/demo?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		Properties propietatsConnexio = new Properties();
		propietatsConnexio.put("user", "root");
		propietatsConnexio.put("password", "1212");
		//propietatsConnexio.put("password", "usbw");

		try {

			// 1- CONNEXIO
			con = DriverManager.getConnection(url,propietatsConnexio);
			System.out.println("Base de dades connectada!");

			// 2- CREEM CONSULTA

			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM COFFEES");	

			// 3- MOSTREM DADES DE LA CONSULTA
			System.out.println("Dades de la taula COFFEES:");
			System.out.println(rs.getFetchSize());
			while (rs.next()) {
				String coffeeName = rs.getString("COF_NAME");
				int supplierID = rs.getInt("SUP_ID");
				float price = rs.getFloat("PRICE");
				int sales = rs.getInt("SALES");
				int total = rs.getInt("TOTAL");
				System.out.println(coffeeName + "\t" + supplierID + "\t"
						+ price + "\t" + sales + "\t" + total);
			}

		} catch (SQLException e) {

			System.err.println("Error d'apertura de connexio: " + e.getMessage());

		} finally {

			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					System.err.println("Error de tancament de ResultSet: "
							+ e.getMessage());
				}

			if (st != null)
				try {
					st.close();
				} catch (SQLException e) {
					System.err.println("Error de tancament de Statment: "
							+ e.getMessage());
				}

			if (con != null)
				try {
					con.close();
				} catch (Exception e) {
					System.err.println("Error de tancament de connexio: "
							+ e.getMessage());
				}
		}

	}
}

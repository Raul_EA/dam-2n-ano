package exemple.b.consultes.seleccio;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CrearConsulta3 {
	private Connection connexio = null; 	//Connection: permet connectar a una bbdd 
	private Statement statement = null;		//Statement: permet generar consultes SQL
	
	Scanner entrada= new Scanner(System.in);
	
	
	private void estableixConnexio() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/demo";
		String user = "root";
		String passwd = "1212";
		connexio = DriverManager.getConnection(url, user, passwd);
		statement = connexio.createStatement();
	}
	
	private void alliberaConnexio() {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				;
			}
		}
		if (connexio != null) {
			try {
				connexio.close();
			} catch (SQLException e) {
				;
			}
		}
	}
	
	public List<String> cercaProveidors() throws SQLException {
		List<String> proveidors = new ArrayList<String>();
		ResultSet rs = null;
		try {
			
			//Afegeix tots els proveidors a la llista
			rs = statement.executeQuery ("SELECT SUP_NAME FROM SUPPLIERS;");
			
			while (rs.next()) {
				proveidors.add(rs.getString(1));
			}
			
		} finally {
			
			if (rs!=null) {
				try {
					rs.close();
				} catch (SQLException e) {
					System.err.println(e.getMessage());
				}
			}
		}
		
		return proveidors;
	}
	
	public String triaProveidor(List<String> proveidors) {
		
		String proveidor = "";
		
		for(String provs : proveidors) {
			System.out.println(provs);
		}
			
		
		System.out.println("\nQuin Proveidor vols? ");
		while (!proveidors.contains(proveidor)) {
			try {
				proveidor = entrada.nextLine();
				if (!proveidors.contains(proveidor))
					System.out.println("Aquest proveidor no existeix.");
			} catch (IllegalStateException e) {
				System.out.println(e.getMessage());
			}
		}
		return proveidor;
	}
	
	public void cercaCafesPerProveidor(String proveidor) throws SQLException {
		
		ResultSet rs = null;
		
		//
		try {
			rs = statement.executeQuery("SELECT COF_NAME, PRICE "
									  + "FROM COFFEES INNER JOIN SUPPLIERS "
									  + "ON COFFEES.SUP_ID=SUPPLIERS.SUP_ID "
									  + "WHERE SUP_NAME='" + proveidor + "'");
			
			while(rs.next()) {
				System.out.print(rs.getString("COF_NAME"));
				System.out.println(" " + rs.getString("PRICE"));
			}
	
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					;
				}
			}
		}
		
	}
	
	public void executa() {
		String proveidor;
		
		try {

			//1- Connecta a la bbdd
			estableixConnexio();	
	
			//2- Mostra els proveidors i demana a l'usuari que trii un d'ells
			proveidor = triaProveidor(cercaProveidors());
			
			//3- Mostra els cafes amb els que treballa el proveidor triat
			System.out.println("Proveidor triat: "+proveidor);
			cercaCafesPerProveidor(proveidor);
			
			
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			alliberaConnexio();
		}
	}
	
	
	
	
	public static void main(String[] args) {
		CrearConsulta3 ex = new CrearConsulta3();
		ex.executa();
	}

}

package exemple.b.consultes.seleccio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class CrearConsulta2 {

	public static void main(String[] args) {
		boolean teResultats=false;
		
		Connection con = null; 	//Connection: permet connectar a una bbdd 
		Statement st = null;	//Statement: permet generar consultes SQL 
		ResultSet rs = null;	//ResultSet: permet guardar el resultat d'una consulta SQL

		String url = "jdbc:mysql://localhost:3306/demo";

		Properties propietatsConnexio = new Properties();
		propietatsConnexio.put("user", "root");
		propietatsConnexio.put("password", "1212");
		//propietatsConnexio.put("password", "usbw");
		
		try {

			// 1- CONNEXIO
			con = DriverManager.getConnection(url,propietatsConnexio);
			System.out.println("Base de dades connectada!");

			// 2- CREEM CONSULTA
			st = con.createStatement();
			rs = st.executeQuery("SELECT COF_NAME,PRICE FROM COFFEES");

			// 3- MOSTREM DADES DE LA CONSULTA
			while (rs.next()) {
				
				if (!teResultats) {
					teResultats=true;
					System.out.println("Dades de la taula COFFEES:");
				}
				
				String coffeeName = rs.getString(1);	//Agafa el primer camp definit a la consulta SQL, com a String
				float price = rs.getFloat(2);			//Agafa el segon camp definit a la consulta SQL, com a float
				System.out.println(coffeeName + "\t" + price);
			}
			
			if (!teResultats) {
				System.out.println("No hi ha dades a la taula");
			}

		} catch (SQLException e) {

			System.err.println("Error d'apertura de connexio: " + e.getMessage());

		} finally {

			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					System.err.println("Error de tancament de ResultSet: "
							+ e.getMessage());
				}

			if (st != null)
				try {
					st.close();
				} catch (SQLException e) {
					System.err.println("Error de tancament de Statment: "
							+ e.getMessage());
				}

			if (con != null)
				try {
					con.close();
				} catch (Exception e) {
					System.err.println("Error de tancament de connexio: "
							+ e.getMessage());
				}
		}
	}
}

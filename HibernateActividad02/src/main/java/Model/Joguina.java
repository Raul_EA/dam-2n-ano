package Model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Joguina")
public class Joguina {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	@Column(name="nom", length=50,nullable = false)
	private String nom;
	@Column(name="descripcion", length=100,nullable = false)
	private String descripcion;
	@Column(name="nivellDiversio")
	private int nivellDiversio=5;
	
	@OneToOne(mappedBy = "joguina", cascade = CascadeType.ALL)
	private Tamagotchi tamagotchi;
	
	public Joguina() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getNivellDiversio() {
		return nivellDiversio;
	}

	public void setNivellDiversio(int nivellDiversio) {
		this.nivellDiversio = nivellDiversio;
	}

	public Tamagotchi getTamagotchi() {
		return tamagotchi;
	}

	public void setTamagotchi(Tamagotchi tamagotchi) {
		this.tamagotchi = tamagotchi;
	}

	@Override
	public String toString() {
		return "Joguina [id=" + id + ", nom=" + nom + ", descripcion=" + descripcion + ", nivellDiversio="
				+ nivellDiversio + ", tamagotchi=" + tamagotchi + "]";
	}

	
	
}

package Main;

import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import Model.Aliment;
import Model.Etapa;
import Model.Joguina;
import Model.Tamagotchi;

public class Main {
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static synchronized SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			// exception handling omitted for brevityaa
			serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
			sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
		}
		return sessionFactory;
	}

	public static void main(String[] args) {
		try {
			session = getSessionFactory().openSession();
			session.beginTransaction();

			Tamagotchi a = new Tamagotchi();
			a.setNom("Jose");
			a.setDescripcion("cosa random 1");
			a.setEtapa(Etapa.Baby);
			session.save(a);
			Tamagotchi b = new Tamagotchi();
			b.setNom("Miguel");
			b.setDescripcion("cosa random 2");
			b.setEtapa(Etapa.Baby);
			session.save(b);
			Tamagotchi c = new Tamagotchi();
			c.setNom("Pedro");
			c.setDescripcion("cosa random 3");
			c.setEtapa(Etapa.Baby);
			session.save(c);
			Tamagotchi d = new Tamagotchi();
			d.setNom("David");
			d.setDescripcion("cosa random 4");
			d.setEtapa(Etapa.Baby);
			session.save(d);

			session.getTransaction().commit();
			session.getTransaction().begin();

			Aliment z = new Aliment();
			z.setNom("Pan");
			z.setDescripcion("comida 1");
			z.setValorNutricional(500);
			session.save(z);
			Aliment x = new Aliment();
			x.setNom("Nieve");
			x.setDescripcion("comida 2");
			x.setValorNutricional(100);
			session.save(x);
			Aliment y = new Aliment();
			y.setNom("Helado");
			y.setDescripcion("comida 3");
			y.setValorNutricional(1000);
			session.save(y);
			Aliment v = new Aliment();
			v.setNom("Granizado");
			v.setDescripcion("comida 4");
			v.setValorNutricional(200);
			session.save(v);

			session.getTransaction().commit();
			session.getTransaction().begin();

			Tamagotchi u = session.find(Tamagotchi.class, 1);
			Tamagotchi i = session.find(Tamagotchi.class, 2);
			Tamagotchi o = session.find(Tamagotchi.class, 3);
			Tamagotchi p = session.find(Tamagotchi.class, 4);

			List<Tamagotchi> h = u.getAmigos();
			List<Tamagotchi> j = i.getAmigos();
			List<Tamagotchi> k = o.getAmigos();
			List<Tamagotchi> l = p.getAmigos();

			h.add(i);
			h.add(o);
			h.add(p);

			j.add(u);
			j.add(o);
			j.add(p);

			k.add(i);
			k.add(u);
			k.add(p);

			l.add(u);
			l.add(o);
			l.add(i);

			u.setAmigos(h);
			i.setAmigos(j);
			o.setAmigos(k);
			p.setAmigos(l);

			session.merge(u);
			session.merge(i);
			session.merge(o);
			session.merge(p);

			session.getTransaction().commit();
			session.getTransaction().begin();

			u = session.find(Tamagotchi.class, 1);
			i = session.find(Tamagotchi.class, 2);
			o = session.find(Tamagotchi.class, 3);
			p = session.find(Tamagotchi.class, 4);

			System.out.println(u.getAmigos());
			System.out.println(i.getAmigos());
			System.out.println(o.getAmigos());
			System.out.println(p.getAmigos());
			
			z=session.find(Aliment.class, 5);
			y=session.find(Aliment.class, 6);
			x=session.find(Aliment.class, 7);
			v=session.find(Aliment.class, 8);
			
			u.setAliment(x);
			i.setAliment(y);
			o.setAliment(y);
			
			session.merge(u);
			session.merge(i);
			session.merge(o);
			session.merge(p);
			
			session.getTransaction().commit();
			session.getTransaction().begin();
			
			u = session.find(Tamagotchi.class, 1);
			i = session.find(Tamagotchi.class, 2);
			o = session.find(Tamagotchi.class, 3);
			p = session.find(Tamagotchi.class, 4);
			
			System.out.println(u.getAliment().getNom());
			System.out.println(i.getAliment().getNom());
			System.out.println(o.getAliment().getNom());
			
			Joguina g = new Joguina();
			g.setNom("Palo");
			g.setDescripcion("joguina 1");
			g.setNivellDiversio(20);
			Joguina t = new Joguina();
			t.setNom("Pelota");
			t.setDescripcion("joguina 2");
			t.setNivellDiversio(200);
			
			session.save(g);
			session.save(t);
			
			u.setJoguina(g);
			i.setJoguina(t);
			
			session.merge(u);
			session.merge(i);
			
			session.getTransaction().commit();
			session.getTransaction().begin();
			
			t=session.find(Joguina.class, 9);
			i = session.find(Tamagotchi.class, 2);
			
			i.setJoguina(t);
			
			session.merge(i);
			
			session.getTransaction().commit();
			session.getTransaction().begin();
			
			
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
			if (null != session.getTransaction()) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			sqlException.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

}
